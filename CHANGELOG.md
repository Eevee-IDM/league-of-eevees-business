# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]

## [1.2.0] - 2018-06-12
## Changed
- Allow line chart only when a time scale is specified
- Allow `by unit [by time_scale]` statement

## Removed
- Support is no more available as a lane

## [1.1.0] - 2018-06-08
## Changed
- Change DSL's grammar to make it more readable
- Allow day, month and year in _by_ clause

## Fixed
- Fix Lane.BOT.getRiotId() to return BOTTOM instead of BOT

## [1.0.4] - 2018-06-04
### Changed
- Update Gloe's grammar to handle spaces and simple quotes in Champions' name.

## [1.0.1] - 2018-05-30
### Added
- Implement the meta-model of a user request with EMF.
- Define the user request DSL (a.k.a. _Gloe_) with xText.
- Create a web servlet able to provide autocompletion for a Gloe web editor.
- Automate build with Maven Tycho.
- Configure deployment to a Nexus repository.
