/*
 * generated by Xtext 2.12.0
 */
package ice.master.loe.gloe.validation

import ice.master.loe.model.loe.DiagramType
import ice.master.loe.model.loe.LoePackage
import ice.master.loe.model.loe.Request
import ice.master.loe.model.loe.TimeScale
import org.eclipse.xtext.validation.Check

/**
 * This class contains custom validation rules. 
 *
 * See https://www.eclipse.org/Xtext/documentation/303_runtime_concepts.html#validation
 */
class GloeValidator extends AbstractGloeValidator {
	
	@Check
	def lineChartRequiresATimeScale(Request request) {
		if (request.diagram.type == DiagramType.LINE_CHART && request.timeScale == TimeScale.ALL_THE_TIME) {
			error('Cannot display a line chart without a time scale.' + 
				  '\nPlease specify a scale with one of the following statement:\n' +
				  'by day | by month | by year',
				  request,
				  LoePackage.Literals.REQUEST__DIAGRAM);
	  	}
	}
	
	@Check 
	def timeScaleInducesALineChart(Request request) {
		if (request.timeScale != TimeScale.ALL_THE_TIME && request.diagram.type != DiagramType.LINE_CHART) { 
			error("Can only use a time scale in a line chart. Please:\n" +
				  " - either remove the 'by [day|month|year]' statement\n" +
				  " - or use a line chart : 'display [...] in a line chart'",
				  request,
				  LoePackage.Literals.REQUEST__TIME_SCALE);
		}
	}
}
