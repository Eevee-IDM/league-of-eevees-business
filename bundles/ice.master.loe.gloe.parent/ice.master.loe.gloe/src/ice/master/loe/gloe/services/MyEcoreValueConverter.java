package ice.master.loe.gloe.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.eclipse.xtext.common.services.Ecore2XtextTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter;
import org.eclipse.xtext.nodemodel.INode;

import com.google.inject.Singleton;

@Singleton
public class MyEcoreValueConverter extends Ecore2XtextTerminalConverters {

	/** Thrown when parsing a date that is not in a timestamp format */
	public final class NonUTCTimestampException extends RuntimeException {
		private static final long serialVersionUID = -5789301277302290219L;
	}

	private final static String[] formats = {"dd MMM yyyy", "dd MM yyyy"};

	@ValueConverter(rule = "EDate")
	public IValueConverter<Date> EDate() {
		return new AbstractNullSafeConverter<Date>() {

			@Override
			protected String internalToString(Date value) {
				SimpleDateFormat fmt = new SimpleDateFormat(formats[0], Locale.ENGLISH);
				fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
				return '"' + fmt.format(value) + '"';
			}

			@Override
			protected Date internalToValue(String string, INode node) throws ValueConverterException {
				string = string.substring(1, string.length() - 1);
				
				for( String format : formats ) {
					try {
						SimpleDateFormat fmt = new SimpleDateFormat(format, Locale.ENGLISH);
						fmt.setTimeZone(TimeZone.getTimeZone("UTC"));
						return fmt.parse(string);
						
					} catch (ParseException e) {}
				}

				throw tailoredValueConverterException(string, node);
			}

			private RuntimeException tailoredValueConverterException(String string, INode node) {
				try {
					DateFormat.getDateTimeInstance().parse(string);
					throw new ValueConverterException("Not in a timestamp format", node, new NonUTCTimestampException());
					
				} catch (ParseException e) {
					DateFormat fmt = DateFormat.getDateTimeInstance();

					String defaultFormat = (fmt instanceof SimpleDateFormat) 
							? "default format for the locale"
							: ((SimpleDateFormat) fmt).toLocalizedPattern();
					
					String availableFormats = String.join("' or '", formats);
					
					throw new ValueConverterException(
							"Not in valid format: Use '" + availableFormats + "' or " + defaultFormat + ".\n"
						  + "For instance: '08 05 1997' or '08 May 1997'.\n"
						  + "Parse error: " + e.getMessage(), node, null);
				}
			}

		};
	}

}
