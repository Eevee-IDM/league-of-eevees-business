package ice.master.loe.gloe.services;

import org.eclipse.xtext.nodemodel.SyntaxErrorMessage;
import org.eclipse.xtext.parser.antlr.SyntaxErrorMessageProvider;

import ice.master.loe.gloe.services.MyEcoreValueConverter.NonUTCTimestampException;

public class GloeSyntaxErrorMessageProvider extends SyntaxErrorMessageProvider {
	
	@Override
	public SyntaxErrorMessage getSyntaxErrorMessage(IValueConverterErrorContext context) {
		if (context.getValueConverterException().getCause() instanceof NonUTCTimestampException)
			return new SyntaxErrorMessage(context.getDefaultMessage(), "Expected date format is dd MM yyyy, for example: \"23 09 1995\"");
		
		return super.getSyntaxErrorMessage(context);
	}
	
}