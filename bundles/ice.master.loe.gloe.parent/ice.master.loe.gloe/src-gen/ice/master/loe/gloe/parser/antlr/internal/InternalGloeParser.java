package ice.master.loe.gloe.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import ice.master.loe.gloe.services.GloeGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGloeParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'summoners'", "','", "'display'", "'by'", "'when'", "'and'", "'in'", "'a'", "'total'", "'of'", "'average'", "'ratio'", "'games'", "'with'", "'team'", "'from'", "'to'", "'is'", "'playing'", "'lane'", "'ends'", "'called'", "'game result'", "'champion'", "'victory'", "'defeat'", "'victories'", "'defeats'", "'blue'", "'red'", "'bot'", "'top'", "'mid'", "'jungle'", "'kills'", "'deaths'", "'assists'", "'double kills'", "'triple kills'", "'quadra kills'", "'penta kills'", "'damage dealt'", "'magic damage dealt'", "'physical damage dealt'", "'true damage dealt'", "'damage dealt to champions'", "'magic damage dealt to champions'", "'physical damage dealt to champions'", "'true damage dealt to champions'", "'heal'", "'damage self mitigated'", "'damage dealt to objectives'", "'damage dealt to turrets'", "'time cc'", "'damage taken'", "'magical damage taken'", "'physical damage taken'", "'true damage taken'", "'gold earned'", "'gold spent'", "'turret kills'", "'inhibitor kills'", "'minions killed'", "'neutral minions killed'", "'neutral minions killed team jungle'", "'neutral minions killed enemy jungle'", "'time crowd control dealt'", "'vision wards bought in game'", "'wards placed'", "'wards killed'", "'first blood kill'", "'first blood assist'", "'first tower kill'", "'first tower assist'", "'first inhibitor kill'", "'first inhibitor assist'", "'Annie'", "'Olaf'", "'Galio'", "'Twisted Fate'", "'Xin Zhao'", "'Urgot'", "'Leblanc'", "'Vladimir'", "'Fiddlesticks'", "'Kayle'", "'Master Yi'", "'Alistar'", "'Ryze'", "'Sion'", "'Sivir'", "'Soraka'", "'Teemo'", "'Tristana'", "'Warwick'", "'Nunu'", "'Miss Fortune'", "'Ashe'", "'Tryndamere'", "'Jax'", "'Morgana'", "'Zilean'", "'Singed'", "'Evelynn'", "'Twitch'", "'Karthus'", "'Cho\\'Gath'", "'Amumu'", "'Rammus'", "'Anivia'", "'Shaco'", "'Dr. Mundo'", "'Sona'", "'Kassadin'", "'Irelia'", "'Janna'", "'Gangplank'", "'Corki'", "'Karma'", "'Taric'", "'Veigar'", "'Trundle'", "'Swain'", "'Caitlyn'", "'Blitzcrank'", "'Malphite'", "'Katarina'", "'Nocturne'", "'Maokai'", "'Renekton'", "'Jarvan IV'", "'Elise'", "'Orianna'", "'Wukong'", "'Brand'", "'Lee Sin'", "'Vayne'", "'Rumble'", "'Cassiopeia'", "'Skarner'", "'Heimerdinger'", "'Nasus'", "'Nidalee'", "'Udyr'", "'Poppy'", "'Gragas'", "'Pantheon'", "'Ezreal'", "'Mordekaiser'", "'Yorick'", "'Akali'", "'Kennen'", "'Garen'", "'Leona'", "'Malzahar'", "'Talon'", "'Riven'", "'Kog\\'Maw'", "'Shen'", "'Lux'", "'Xerath'", "'Shyvana'", "'Ahri'", "'Graves'", "'Fizz'", "'Volibear'", "'Rengar'", "'Varus'", "'Nautilus'", "'Viktor'", "'Sejuani'", "'Fiora'", "'Ziggs'", "'Lulu'", "'Draven'", "'Hecarim'", "'Kha\\'Zix'", "'Darius'", "'Jayce'", "'Lissandra'", "'Diana'", "'Quinn'", "'Syndra'", "'Aurelion Sol'", "'Kayn'", "'Zoe'", "'Zyra'", "'Kai\\'Sa'", "'Gnar'", "'Zac'", "'Yasuo'", "'Vel\\'Koz'", "'Taliyah'", "'Camille'", "'Braum'", "'Jhin'", "'Kindred'", "'Jinx'", "'Tahm Kench'", "'Lucian'", "'Zed'", "'Kled'", "'Ekko'", "'Vi'", "'Aatrox'", "'Nami'", "'Azir'", "'Thresh'", "'Illaoi'", "'Rek\\'Sai'", "'Ivern'", "'Kalista'", "'Bard'", "'Rakan'", "'Xayah'", "'Ornn'", "'day'", "'month'", "'year'", "'winrate'", "'pie chart'", "'3D pie chart'", "'line chart'", "'bar chart'"
    };
    public static final int T__144=144;
    public static final int T__143=143;
    public static final int T__146=146;
    public static final int T__50=50;
    public static final int T__145=145;
    public static final int T__140=140;
    public static final int T__142=142;
    public static final int T__141=141;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__137=137;
    public static final int T__52=52;
    public static final int T__136=136;
    public static final int T__53=53;
    public static final int T__139=139;
    public static final int T__54=54;
    public static final int T__138=138;
    public static final int T__133=133;
    public static final int T__132=132;
    public static final int T__60=60;
    public static final int T__135=135;
    public static final int T__61=61;
    public static final int T__134=134;
    public static final int RULE_ID=5;
    public static final int T__131=131;
    public static final int T__130=130;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__129=129;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__126=126;
    public static final int T__63=63;
    public static final int T__125=125;
    public static final int T__64=64;
    public static final int T__128=128;
    public static final int T__65=65;
    public static final int T__127=127;
    public static final int T__166=166;
    public static final int T__165=165;
    public static final int T__168=168;
    public static final int T__167=167;
    public static final int T__162=162;
    public static final int T__161=161;
    public static final int T__164=164;
    public static final int T__163=163;
    public static final int T__160=160;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__159=159;
    public static final int T__30=30;
    public static final int T__158=158;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__155=155;
    public static final int T__154=154;
    public static final int T__157=157;
    public static final int T__156=156;
    public static final int T__151=151;
    public static final int T__150=150;
    public static final int T__153=153;
    public static final int T__152=152;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__148=148;
    public static final int T__41=41;
    public static final int T__147=147;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__149=149;
    public static final int T__100=100;
    public static final int T__221=221;
    public static final int T__220=220;
    public static final int T__102=102;
    public static final int T__223=223;
    public static final int T__101=101;
    public static final int T__222=222;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__218=218;
    public static final int T__12=12;
    public static final int T__217=217;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__219=219;
    public static final int T__214=214;
    public static final int T__213=213;
    public static final int T__216=216;
    public static final int T__215=215;
    public static final int T__210=210;
    public static final int T__212=212;
    public static final int T__211=211;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__207=207;
    public static final int T__23=23;
    public static final int T__206=206;
    public static final int T__24=24;
    public static final int T__209=209;
    public static final int T__25=25;
    public static final int T__208=208;
    public static final int T__203=203;
    public static final int T__202=202;
    public static final int T__20=20;
    public static final int T__205=205;
    public static final int T__21=21;
    public static final int T__204=204;
    public static final int T__122=122;
    public static final int T__121=121;
    public static final int T__124=124;
    public static final int T__123=123;
    public static final int T__120=120;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__119=119;
    public static final int T__118=118;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__114=114;
    public static final int T__117=117;
    public static final int T__116=116;
    public static final int T__111=111;
    public static final int T__232=232;
    public static final int T__110=110;
    public static final int T__231=231;
    public static final int T__113=113;
    public static final int T__234=234;
    public static final int T__112=112;
    public static final int T__233=233;
    public static final int T__230=230;
    public static final int T__108=108;
    public static final int T__229=229;
    public static final int T__107=107;
    public static final int T__228=228;
    public static final int T__109=109;
    public static final int T__104=104;
    public static final int T__225=225;
    public static final int T__103=103;
    public static final int T__224=224;
    public static final int T__106=106;
    public static final int T__227=227;
    public static final int T__105=105;
    public static final int T__226=226;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__201=201;
    public static final int T__200=200;
    public static final int T__91=91;
    public static final int T__188=188;
    public static final int T__92=92;
    public static final int T__187=187;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__189=189;
    public static final int T__184=184;
    public static final int T__183=183;
    public static final int T__186=186;
    public static final int T__90=90;
    public static final int T__185=185;
    public static final int T__180=180;
    public static final int T__182=182;
    public static final int T__181=181;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__177=177;
    public static final int T__176=176;
    public static final int T__179=179;
    public static final int T__178=178;
    public static final int T__173=173;
    public static final int T__172=172;
    public static final int T__175=175;
    public static final int T__174=174;
    public static final int T__171=171;
    public static final int T__170=170;
    public static final int T__169=169;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__199=199;
    public static final int T__81=81;
    public static final int T__198=198;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int T__195=195;
    public static final int T__194=194;
    public static final int RULE_WS=9;
    public static final int T__197=197;
    public static final int T__196=196;
    public static final int T__191=191;
    public static final int T__190=190;
    public static final int T__193=193;
    public static final int T__192=192;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalGloeParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGloeParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGloeParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGloe.g"; }



     	private GloeGrammarAccess grammarAccess;

        public InternalGloeParser(TokenStream input, GloeGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Request";
       	}

       	@Override
       	protected GloeGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleRequest"
    // InternalGloe.g:65:1: entryRuleRequest returns [EObject current=null] : iv_ruleRequest= ruleRequest EOF ;
    public final EObject entryRuleRequest() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRequest = null;


        try {
            // InternalGloe.g:65:48: (iv_ruleRequest= ruleRequest EOF )
            // InternalGloe.g:66:2: iv_ruleRequest= ruleRequest EOF
            {
             newCompositeNode(grammarAccess.getRequestRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRequest=ruleRequest();

            state._fsp--;

             current =iv_ruleRequest; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRequest"


    // $ANTLR start "ruleRequest"
    // InternalGloe.g:72:1: ruleRequest returns [EObject current=null] : (otherlv_0= 'summoners' ( (lv_summoners_1_0= ruleSummoner ) ) (otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) ) )* otherlv_4= 'display' ( (lv_yvalues_5_0= ruleYValue ) ) (otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) ) )* (otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) ) )? (otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) ) )? (otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )* )? ( (lv_timelapse_16_0= ruleTimelapse ) )? otherlv_17= 'in' otherlv_18= 'a' ( (lv_diagram_19_0= ruleDiagram ) ) )? ;
    public final EObject ruleRequest() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        Token otherlv_14=null;
        Token otherlv_17=null;
        Token otherlv_18=null;
        EObject lv_summoners_1_0 = null;

        EObject lv_summoners_3_0 = null;

        EObject lv_yvalues_5_0 = null;

        EObject lv_yvalues_7_0 = null;

        Enumerator lv_xUnit_9_0 = null;

        Enumerator lv_timeScale_11_0 = null;

        EObject lv_filters_13_0 = null;

        EObject lv_filters_15_0 = null;

        EObject lv_timelapse_16_0 = null;

        EObject lv_diagram_19_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:78:2: ( (otherlv_0= 'summoners' ( (lv_summoners_1_0= ruleSummoner ) ) (otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) ) )* otherlv_4= 'display' ( (lv_yvalues_5_0= ruleYValue ) ) (otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) ) )* (otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) ) )? (otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) ) )? (otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )* )? ( (lv_timelapse_16_0= ruleTimelapse ) )? otherlv_17= 'in' otherlv_18= 'a' ( (lv_diagram_19_0= ruleDiagram ) ) )? )
            // InternalGloe.g:79:2: (otherlv_0= 'summoners' ( (lv_summoners_1_0= ruleSummoner ) ) (otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) ) )* otherlv_4= 'display' ( (lv_yvalues_5_0= ruleYValue ) ) (otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) ) )* (otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) ) )? (otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) ) )? (otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )* )? ( (lv_timelapse_16_0= ruleTimelapse ) )? otherlv_17= 'in' otherlv_18= 'a' ( (lv_diagram_19_0= ruleDiagram ) ) )?
            {
            // InternalGloe.g:79:2: (otherlv_0= 'summoners' ( (lv_summoners_1_0= ruleSummoner ) ) (otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) ) )* otherlv_4= 'display' ( (lv_yvalues_5_0= ruleYValue ) ) (otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) ) )* (otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) ) )? (otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) ) )? (otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )* )? ( (lv_timelapse_16_0= ruleTimelapse ) )? otherlv_17= 'in' otherlv_18= 'a' ( (lv_diagram_19_0= ruleDiagram ) ) )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==11) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalGloe.g:80:3: otherlv_0= 'summoners' ( (lv_summoners_1_0= ruleSummoner ) ) (otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) ) )* otherlv_4= 'display' ( (lv_yvalues_5_0= ruleYValue ) ) (otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) ) )* (otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) ) )? (otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) ) )? (otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )* )? ( (lv_timelapse_16_0= ruleTimelapse ) )? otherlv_17= 'in' otherlv_18= 'a' ( (lv_diagram_19_0= ruleDiagram ) )
                    {
                    otherlv_0=(Token)match(input,11,FOLLOW_3); 

                    			newLeafNode(otherlv_0, grammarAccess.getRequestAccess().getSummonersKeyword_0());
                    		
                    // InternalGloe.g:84:3: ( (lv_summoners_1_0= ruleSummoner ) )
                    // InternalGloe.g:85:4: (lv_summoners_1_0= ruleSummoner )
                    {
                    // InternalGloe.g:85:4: (lv_summoners_1_0= ruleSummoner )
                    // InternalGloe.g:86:5: lv_summoners_1_0= ruleSummoner
                    {

                    					newCompositeNode(grammarAccess.getRequestAccess().getSummonersSummonerParserRuleCall_1_0());
                    				
                    pushFollow(FOLLOW_4);
                    lv_summoners_1_0=ruleSummoner();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getRequestRule());
                    					}
                    					add(
                    						current,
                    						"summoners",
                    						lv_summoners_1_0,
                    						"ice.master.loe.gloe.Gloe.Summoner");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }

                    // InternalGloe.g:103:3: (otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) ) )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( (LA1_0==12) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalGloe.g:104:4: otherlv_2= ',' ( (lv_summoners_3_0= ruleSummoner ) )
                    	    {
                    	    otherlv_2=(Token)match(input,12,FOLLOW_3); 

                    	    				newLeafNode(otherlv_2, grammarAccess.getRequestAccess().getCommaKeyword_2_0());
                    	    			
                    	    // InternalGloe.g:108:4: ( (lv_summoners_3_0= ruleSummoner ) )
                    	    // InternalGloe.g:109:5: (lv_summoners_3_0= ruleSummoner )
                    	    {
                    	    // InternalGloe.g:109:5: (lv_summoners_3_0= ruleSummoner )
                    	    // InternalGloe.g:110:6: lv_summoners_3_0= ruleSummoner
                    	    {

                    	    						newCompositeNode(grammarAccess.getRequestAccess().getSummonersSummonerParserRuleCall_2_1_0());
                    	    					
                    	    pushFollow(FOLLOW_4);
                    	    lv_summoners_3_0=ruleSummoner();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRequestRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"summoners",
                    	    							lv_summoners_3_0,
                    	    							"ice.master.loe.gloe.Gloe.Summoner");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);

                    otherlv_4=(Token)match(input,13,FOLLOW_5); 

                    			newLeafNode(otherlv_4, grammarAccess.getRequestAccess().getDisplayKeyword_3());
                    		
                    // InternalGloe.g:132:3: ( (lv_yvalues_5_0= ruleYValue ) )
                    // InternalGloe.g:133:4: (lv_yvalues_5_0= ruleYValue )
                    {
                    // InternalGloe.g:133:4: (lv_yvalues_5_0= ruleYValue )
                    // InternalGloe.g:134:5: lv_yvalues_5_0= ruleYValue
                    {

                    					newCompositeNode(grammarAccess.getRequestAccess().getYvaluesYValueParserRuleCall_4_0());
                    				
                    pushFollow(FOLLOW_6);
                    lv_yvalues_5_0=ruleYValue();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getRequestRule());
                    					}
                    					add(
                    						current,
                    						"yvalues",
                    						lv_yvalues_5_0,
                    						"ice.master.loe.gloe.Gloe.YValue");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }

                    // InternalGloe.g:151:3: (otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) ) )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==12) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalGloe.g:152:4: otherlv_6= ',' ( (lv_yvalues_7_0= ruleYValue ) )
                    	    {
                    	    otherlv_6=(Token)match(input,12,FOLLOW_5); 

                    	    				newLeafNode(otherlv_6, grammarAccess.getRequestAccess().getCommaKeyword_5_0());
                    	    			
                    	    // InternalGloe.g:156:4: ( (lv_yvalues_7_0= ruleYValue ) )
                    	    // InternalGloe.g:157:5: (lv_yvalues_7_0= ruleYValue )
                    	    {
                    	    // InternalGloe.g:157:5: (lv_yvalues_7_0= ruleYValue )
                    	    // InternalGloe.g:158:6: lv_yvalues_7_0= ruleYValue
                    	    {

                    	    						newCompositeNode(grammarAccess.getRequestAccess().getYvaluesYValueParserRuleCall_5_1_0());
                    	    					
                    	    pushFollow(FOLLOW_6);
                    	    lv_yvalues_7_0=ruleYValue();

                    	    state._fsp--;


                    	    						if (current==null) {
                    	    							current = createModelElementForParent(grammarAccess.getRequestRule());
                    	    						}
                    	    						add(
                    	    							current,
                    	    							"yvalues",
                    	    							lv_yvalues_7_0,
                    	    							"ice.master.loe.gloe.Gloe.YValue");
                    	    						afterParserOrEnumRuleCall();
                    	    					

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    // InternalGloe.g:176:3: (otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) ) )?
                    int alt3=2;
                    int LA3_0 = input.LA(1);

                    if ( (LA3_0==14) ) {
                        int LA3_1 = input.LA(2);

                        if ( (LA3_1==25||LA3_1==30||(LA3_1>=33 && LA3_1<=34)) ) {
                            alt3=1;
                        }
                    }
                    switch (alt3) {
                        case 1 :
                            // InternalGloe.g:177:4: otherlv_8= 'by' ( (lv_xUnit_9_0= ruleValueType ) )
                            {
                            otherlv_8=(Token)match(input,14,FOLLOW_7); 

                            				newLeafNode(otherlv_8, grammarAccess.getRequestAccess().getByKeyword_6_0());
                            			
                            // InternalGloe.g:181:4: ( (lv_xUnit_9_0= ruleValueType ) )
                            // InternalGloe.g:182:5: (lv_xUnit_9_0= ruleValueType )
                            {
                            // InternalGloe.g:182:5: (lv_xUnit_9_0= ruleValueType )
                            // InternalGloe.g:183:6: lv_xUnit_9_0= ruleValueType
                            {

                            						newCompositeNode(grammarAccess.getRequestAccess().getXUnitValueTypeEnumRuleCall_6_1_0());
                            					
                            pushFollow(FOLLOW_8);
                            lv_xUnit_9_0=ruleValueType();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequestRule());
                            						}
                            						set(
                            							current,
                            							"xUnit",
                            							lv_xUnit_9_0,
                            							"ice.master.loe.gloe.Gloe.ValueType");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }


                            }
                            break;

                    }

                    // InternalGloe.g:201:3: (otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) ) )?
                    int alt4=2;
                    int LA4_0 = input.LA(1);

                    if ( (LA4_0==14) ) {
                        alt4=1;
                    }
                    switch (alt4) {
                        case 1 :
                            // InternalGloe.g:202:4: otherlv_10= 'by' ( (lv_timeScale_11_0= ruleTimeScale ) )
                            {
                            otherlv_10=(Token)match(input,14,FOLLOW_9); 

                            				newLeafNode(otherlv_10, grammarAccess.getRequestAccess().getByKeyword_7_0());
                            			
                            // InternalGloe.g:206:4: ( (lv_timeScale_11_0= ruleTimeScale ) )
                            // InternalGloe.g:207:5: (lv_timeScale_11_0= ruleTimeScale )
                            {
                            // InternalGloe.g:207:5: (lv_timeScale_11_0= ruleTimeScale )
                            // InternalGloe.g:208:6: lv_timeScale_11_0= ruleTimeScale
                            {

                            						newCompositeNode(grammarAccess.getRequestAccess().getTimeScaleTimeScaleEnumRuleCall_7_1_0());
                            					
                            pushFollow(FOLLOW_10);
                            lv_timeScale_11_0=ruleTimeScale();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequestRule());
                            						}
                            						set(
                            							current,
                            							"timeScale",
                            							lv_timeScale_11_0,
                            							"ice.master.loe.gloe.Gloe.TimeScale");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }


                            }
                            break;

                    }

                    // InternalGloe.g:226:3: (otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )* )?
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==15) ) {
                        alt6=1;
                    }
                    switch (alt6) {
                        case 1 :
                            // InternalGloe.g:227:4: otherlv_12= 'when' ( (lv_filters_13_0= ruleFilter ) ) (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )*
                            {
                            otherlv_12=(Token)match(input,15,FOLLOW_11); 

                            				newLeafNode(otherlv_12, grammarAccess.getRequestAccess().getWhenKeyword_8_0());
                            			
                            // InternalGloe.g:231:4: ( (lv_filters_13_0= ruleFilter ) )
                            // InternalGloe.g:232:5: (lv_filters_13_0= ruleFilter )
                            {
                            // InternalGloe.g:232:5: (lv_filters_13_0= ruleFilter )
                            // InternalGloe.g:233:6: lv_filters_13_0= ruleFilter
                            {

                            						newCompositeNode(grammarAccess.getRequestAccess().getFiltersFilterParserRuleCall_8_1_0());
                            					
                            pushFollow(FOLLOW_12);
                            lv_filters_13_0=ruleFilter();

                            state._fsp--;


                            						if (current==null) {
                            							current = createModelElementForParent(grammarAccess.getRequestRule());
                            						}
                            						add(
                            							current,
                            							"filters",
                            							lv_filters_13_0,
                            							"ice.master.loe.gloe.Gloe.Filter");
                            						afterParserOrEnumRuleCall();
                            					

                            }


                            }

                            // InternalGloe.g:250:4: (otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) ) )*
                            loop5:
                            do {
                                int alt5=2;
                                int LA5_0 = input.LA(1);

                                if ( (LA5_0==16) ) {
                                    alt5=1;
                                }


                                switch (alt5) {
                            	case 1 :
                            	    // InternalGloe.g:251:5: otherlv_14= 'and' ( (lv_filters_15_0= ruleFilter ) )
                            	    {
                            	    otherlv_14=(Token)match(input,16,FOLLOW_11); 

                            	    					newLeafNode(otherlv_14, grammarAccess.getRequestAccess().getAndKeyword_8_2_0());
                            	    				
                            	    // InternalGloe.g:255:5: ( (lv_filters_15_0= ruleFilter ) )
                            	    // InternalGloe.g:256:6: (lv_filters_15_0= ruleFilter )
                            	    {
                            	    // InternalGloe.g:256:6: (lv_filters_15_0= ruleFilter )
                            	    // InternalGloe.g:257:7: lv_filters_15_0= ruleFilter
                            	    {

                            	    							newCompositeNode(grammarAccess.getRequestAccess().getFiltersFilterParserRuleCall_8_2_1_0());
                            	    						
                            	    pushFollow(FOLLOW_12);
                            	    lv_filters_15_0=ruleFilter();

                            	    state._fsp--;


                            	    							if (current==null) {
                            	    								current = createModelElementForParent(grammarAccess.getRequestRule());
                            	    							}
                            	    							add(
                            	    								current,
                            	    								"filters",
                            	    								lv_filters_15_0,
                            	    								"ice.master.loe.gloe.Gloe.Filter");
                            	    							afterParserOrEnumRuleCall();
                            	    						

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop5;
                                }
                            } while (true);


                            }
                            break;

                    }

                    // InternalGloe.g:276:3: ( (lv_timelapse_16_0= ruleTimelapse ) )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0==26) ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalGloe.g:277:4: (lv_timelapse_16_0= ruleTimelapse )
                            {
                            // InternalGloe.g:277:4: (lv_timelapse_16_0= ruleTimelapse )
                            // InternalGloe.g:278:5: lv_timelapse_16_0= ruleTimelapse
                            {

                            					newCompositeNode(grammarAccess.getRequestAccess().getTimelapseTimelapseParserRuleCall_9_0());
                            				
                            pushFollow(FOLLOW_13);
                            lv_timelapse_16_0=ruleTimelapse();

                            state._fsp--;


                            					if (current==null) {
                            						current = createModelElementForParent(grammarAccess.getRequestRule());
                            					}
                            					set(
                            						current,
                            						"timelapse",
                            						lv_timelapse_16_0,
                            						"ice.master.loe.gloe.Gloe.Timelapse");
                            					afterParserOrEnumRuleCall();
                            				

                            }


                            }
                            break;

                    }

                    otherlv_17=(Token)match(input,17,FOLLOW_14); 

                    			newLeafNode(otherlv_17, grammarAccess.getRequestAccess().getInKeyword_10());
                    		
                    otherlv_18=(Token)match(input,18,FOLLOW_15); 

                    			newLeafNode(otherlv_18, grammarAccess.getRequestAccess().getAKeyword_11());
                    		
                    // InternalGloe.g:303:3: ( (lv_diagram_19_0= ruleDiagram ) )
                    // InternalGloe.g:304:4: (lv_diagram_19_0= ruleDiagram )
                    {
                    // InternalGloe.g:304:4: (lv_diagram_19_0= ruleDiagram )
                    // InternalGloe.g:305:5: lv_diagram_19_0= ruleDiagram
                    {

                    					newCompositeNode(grammarAccess.getRequestAccess().getDiagramDiagramParserRuleCall_12_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_diagram_19_0=ruleDiagram();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getRequestRule());
                    					}
                    					set(
                    						current,
                    						"diagram",
                    						lv_diagram_19_0,
                    						"ice.master.loe.gloe.Gloe.Diagram");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRequest"


    // $ANTLR start "entryRuleSummoner"
    // InternalGloe.g:326:1: entryRuleSummoner returns [EObject current=null] : iv_ruleSummoner= ruleSummoner EOF ;
    public final EObject entryRuleSummoner() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSummoner = null;


        try {
            // InternalGloe.g:326:49: (iv_ruleSummoner= ruleSummoner EOF )
            // InternalGloe.g:327:2: iv_ruleSummoner= ruleSummoner EOF
            {
             newCompositeNode(grammarAccess.getSummonerRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSummoner=ruleSummoner();

            state._fsp--;

             current =iv_ruleSummoner; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSummoner"


    // $ANTLR start "ruleSummoner"
    // InternalGloe.g:333:1: ruleSummoner returns [EObject current=null] : ( () ( (lv_name_1_0= ruleEString ) ) ) ;
    public final EObject ruleSummoner() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:339:2: ( ( () ( (lv_name_1_0= ruleEString ) ) ) )
            // InternalGloe.g:340:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            {
            // InternalGloe.g:340:2: ( () ( (lv_name_1_0= ruleEString ) ) )
            // InternalGloe.g:341:3: () ( (lv_name_1_0= ruleEString ) )
            {
            // InternalGloe.g:341:3: ()
            // InternalGloe.g:342:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSummonerAccess().getSummonerAction_0(),
            					current);
            			

            }

            // InternalGloe.g:348:3: ( (lv_name_1_0= ruleEString ) )
            // InternalGloe.g:349:4: (lv_name_1_0= ruleEString )
            {
            // InternalGloe.g:349:4: (lv_name_1_0= ruleEString )
            // InternalGloe.g:350:5: lv_name_1_0= ruleEString
            {

            					newCompositeNode(grammarAccess.getSummonerAccess().getNameEStringParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleEString();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSummonerRule());
            					}
            					set(
            						current,
            						"name",
            						lv_name_1_0,
            						"ice.master.loe.gloe.Gloe.EString");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSummoner"


    // $ANTLR start "entryRuleYValue"
    // InternalGloe.g:371:1: entryRuleYValue returns [EObject current=null] : iv_ruleYValue= ruleYValue EOF ;
    public final EObject entryRuleYValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleYValue = null;


        try {
            // InternalGloe.g:371:47: (iv_ruleYValue= ruleYValue EOF )
            // InternalGloe.g:372:2: iv_ruleYValue= ruleYValue EOF
            {
             newCompositeNode(grammarAccess.getYValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleYValue=ruleYValue();

            state._fsp--;

             current =iv_ruleYValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleYValue"


    // $ANTLR start "ruleYValue"
    // InternalGloe.g:378:1: ruleYValue returns [EObject current=null] : (this_ComputedYValue_0= ruleComputedYValue | this_YValueShortcuts_1= ruleYValueShortcuts ) ;
    public final EObject ruleYValue() throws RecognitionException {
        EObject current = null;

        EObject this_ComputedYValue_0 = null;

        EObject this_YValueShortcuts_1 = null;



        	enterRule();

        try {
            // InternalGloe.g:384:2: ( (this_ComputedYValue_0= ruleComputedYValue | this_YValueShortcuts_1= ruleYValueShortcuts ) )
            // InternalGloe.g:385:2: (this_ComputedYValue_0= ruleComputedYValue | this_YValueShortcuts_1= ruleYValueShortcuts )
            {
            // InternalGloe.g:385:2: (this_ComputedYValue_0= ruleComputedYValue | this_YValueShortcuts_1= ruleYValueShortcuts )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==19||(LA9_0>=21 && LA9_0<=22)) ) {
                alt9=1;
            }
            else if ( (LA9_0==230) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalGloe.g:386:3: this_ComputedYValue_0= ruleComputedYValue
                    {

                    			newCompositeNode(grammarAccess.getYValueAccess().getComputedYValueParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_ComputedYValue_0=ruleComputedYValue();

                    state._fsp--;


                    			current = this_ComputedYValue_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGloe.g:395:3: this_YValueShortcuts_1= ruleYValueShortcuts
                    {

                    			newCompositeNode(grammarAccess.getYValueAccess().getYValueShortcutsParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_YValueShortcuts_1=ruleYValueShortcuts();

                    state._fsp--;


                    			current = this_YValueShortcuts_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleYValue"


    // $ANTLR start "entryRuleComputedYValue"
    // InternalGloe.g:407:1: entryRuleComputedYValue returns [EObject current=null] : iv_ruleComputedYValue= ruleComputedYValue EOF ;
    public final EObject entryRuleComputedYValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleComputedYValue = null;


        try {
            // InternalGloe.g:407:55: (iv_ruleComputedYValue= ruleComputedYValue EOF )
            // InternalGloe.g:408:2: iv_ruleComputedYValue= ruleComputedYValue EOF
            {
             newCompositeNode(grammarAccess.getComputedYValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleComputedYValue=ruleComputedYValue();

            state._fsp--;

             current =iv_ruleComputedYValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleComputedYValue"


    // $ANTLR start "ruleComputedYValue"
    // InternalGloe.g:414:1: ruleComputedYValue returns [EObject current=null] : (this_Sum_0= ruleSum | this_Average_1= ruleAverage | this_Ratio_2= ruleRatio ) ;
    public final EObject ruleComputedYValue() throws RecognitionException {
        EObject current = null;

        EObject this_Sum_0 = null;

        EObject this_Average_1 = null;

        EObject this_Ratio_2 = null;



        	enterRule();

        try {
            // InternalGloe.g:420:2: ( (this_Sum_0= ruleSum | this_Average_1= ruleAverage | this_Ratio_2= ruleRatio ) )
            // InternalGloe.g:421:2: (this_Sum_0= ruleSum | this_Average_1= ruleAverage | this_Ratio_2= ruleRatio )
            {
            // InternalGloe.g:421:2: (this_Sum_0= ruleSum | this_Average_1= ruleAverage | this_Ratio_2= ruleRatio )
            int alt10=3;
            switch ( input.LA(1) ) {
            case 19:
                {
                alt10=1;
                }
                break;
            case 21:
                {
                alt10=2;
                }
                break;
            case 22:
                {
                alt10=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalGloe.g:422:3: this_Sum_0= ruleSum
                    {

                    			newCompositeNode(grammarAccess.getComputedYValueAccess().getSumParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Sum_0=ruleSum();

                    state._fsp--;


                    			current = this_Sum_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGloe.g:431:3: this_Average_1= ruleAverage
                    {

                    			newCompositeNode(grammarAccess.getComputedYValueAccess().getAverageParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_Average_1=ruleAverage();

                    state._fsp--;


                    			current = this_Average_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGloe.g:440:3: this_Ratio_2= ruleRatio
                    {

                    			newCompositeNode(grammarAccess.getComputedYValueAccess().getRatioParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_Ratio_2=ruleRatio();

                    state._fsp--;


                    			current = this_Ratio_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComputedYValue"


    // $ANTLR start "entryRuleSum"
    // InternalGloe.g:452:1: entryRuleSum returns [EObject current=null] : iv_ruleSum= ruleSum EOF ;
    public final EObject entryRuleSum() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleSum = null;


        try {
            // InternalGloe.g:452:44: (iv_ruleSum= ruleSum EOF )
            // InternalGloe.g:453:2: iv_ruleSum= ruleSum EOF
            {
             newCompositeNode(grammarAccess.getSumRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleSum=ruleSum();

            state._fsp--;

             current =iv_ruleSum; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleSum"


    // $ANTLR start "ruleSum"
    // InternalGloe.g:459:1: ruleSum returns [EObject current=null] : ( () otherlv_1= 'total' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) ) ;
    public final EObject ruleSum() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:465:2: ( ( () otherlv_1= 'total' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) ) )
            // InternalGloe.g:466:2: ( () otherlv_1= 'total' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) )
            {
            // InternalGloe.g:466:2: ( () otherlv_1= 'total' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) )
            // InternalGloe.g:467:3: () otherlv_1= 'total' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) )
            {
            // InternalGloe.g:467:3: ()
            // InternalGloe.g:468:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getSumAccess().getSumAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,19,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getSumAccess().getTotalKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getSumAccess().getOfKeyword_2());
            		
            // InternalGloe.g:482:3: ( (lv_value_3_0= rulePluralValue ) )
            // InternalGloe.g:483:4: (lv_value_3_0= rulePluralValue )
            {
            // InternalGloe.g:483:4: (lv_value_3_0= rulePluralValue )
            // InternalGloe.g:484:5: lv_value_3_0= rulePluralValue
            {

            					newCompositeNode(grammarAccess.getSumAccess().getValuePluralValueParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_3_0=rulePluralValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getSumRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"ice.master.loe.gloe.Gloe.PluralValue");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleSum"


    // $ANTLR start "entryRuleAverage"
    // InternalGloe.g:505:1: entryRuleAverage returns [EObject current=null] : iv_ruleAverage= ruleAverage EOF ;
    public final EObject entryRuleAverage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAverage = null;


        try {
            // InternalGloe.g:505:48: (iv_ruleAverage= ruleAverage EOF )
            // InternalGloe.g:506:2: iv_ruleAverage= ruleAverage EOF
            {
             newCompositeNode(grammarAccess.getAverageRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAverage=ruleAverage();

            state._fsp--;

             current =iv_ruleAverage; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAverage"


    // $ANTLR start "ruleAverage"
    // InternalGloe.g:512:1: ruleAverage returns [EObject current=null] : ( () otherlv_1= 'average' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) ) ;
    public final EObject ruleAverage() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:518:2: ( ( () otherlv_1= 'average' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) ) )
            // InternalGloe.g:519:2: ( () otherlv_1= 'average' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) )
            {
            // InternalGloe.g:519:2: ( () otherlv_1= 'average' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) ) )
            // InternalGloe.g:520:3: () otherlv_1= 'average' otherlv_2= 'of' ( (lv_value_3_0= rulePluralValue ) )
            {
            // InternalGloe.g:520:3: ()
            // InternalGloe.g:521:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getAverageAccess().getAverageAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,21,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getAverageAccess().getAverageKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_17); 

            			newLeafNode(otherlv_2, grammarAccess.getAverageAccess().getOfKeyword_2());
            		
            // InternalGloe.g:535:3: ( (lv_value_3_0= rulePluralValue ) )
            // InternalGloe.g:536:4: (lv_value_3_0= rulePluralValue )
            {
            // InternalGloe.g:536:4: (lv_value_3_0= rulePluralValue )
            // InternalGloe.g:537:5: lv_value_3_0= rulePluralValue
            {

            					newCompositeNode(grammarAccess.getAverageAccess().getValuePluralValueParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_3_0=rulePluralValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAverageRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"ice.master.loe.gloe.Gloe.PluralValue");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAverage"


    // $ANTLR start "entryRuleRatio"
    // InternalGloe.g:558:1: entryRuleRatio returns [EObject current=null] : iv_ruleRatio= ruleRatio EOF ;
    public final EObject entryRuleRatio() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRatio = null;


        try {
            // InternalGloe.g:558:46: (iv_ruleRatio= ruleRatio EOF )
            // InternalGloe.g:559:2: iv_ruleRatio= ruleRatio EOF
            {
             newCompositeNode(grammarAccess.getRatioRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRatio=ruleRatio();

            state._fsp--;

             current =iv_ruleRatio; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRatio"


    // $ANTLR start "ruleRatio"
    // InternalGloe.g:565:1: ruleRatio returns [EObject current=null] : ( () otherlv_1= 'ratio' otherlv_2= 'of' ( (lv_value_3_0= ruleValue ) ) ) ;
    public final EObject ruleRatio() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        EObject lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:571:2: ( ( () otherlv_1= 'ratio' otherlv_2= 'of' ( (lv_value_3_0= ruleValue ) ) ) )
            // InternalGloe.g:572:2: ( () otherlv_1= 'ratio' otherlv_2= 'of' ( (lv_value_3_0= ruleValue ) ) )
            {
            // InternalGloe.g:572:2: ( () otherlv_1= 'ratio' otherlv_2= 'of' ( (lv_value_3_0= ruleValue ) ) )
            // InternalGloe.g:573:3: () otherlv_1= 'ratio' otherlv_2= 'of' ( (lv_value_3_0= ruleValue ) )
            {
            // InternalGloe.g:573:3: ()
            // InternalGloe.g:574:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getRatioAccess().getRatioAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,22,FOLLOW_16); 

            			newLeafNode(otherlv_1, grammarAccess.getRatioAccess().getRatioKeyword_1());
            		
            otherlv_2=(Token)match(input,20,FOLLOW_18); 

            			newLeafNode(otherlv_2, grammarAccess.getRatioAccess().getOfKeyword_2());
            		
            // InternalGloe.g:588:3: ( (lv_value_3_0= ruleValue ) )
            // InternalGloe.g:589:4: (lv_value_3_0= ruleValue )
            {
            // InternalGloe.g:589:4: (lv_value_3_0= ruleValue )
            // InternalGloe.g:590:5: lv_value_3_0= ruleValue
            {

            					newCompositeNode(grammarAccess.getRatioAccess().getValueValueParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_3_0=ruleValue();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getRatioRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"ice.master.loe.gloe.Gloe.Value");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRatio"


    // $ANTLR start "entryRuleType"
    // InternalGloe.g:611:1: entryRuleType returns [EObject current=null] : iv_ruleType= ruleType EOF ;
    public final EObject entryRuleType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleType = null;


        try {
            // InternalGloe.g:611:45: (iv_ruleType= ruleType EOF )
            // InternalGloe.g:612:2: iv_ruleType= ruleType EOF
            {
             newCompositeNode(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleType=ruleType();

            state._fsp--;

             current =iv_ruleType; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalGloe.g:618:1: ruleType returns [EObject current=null] : ( () ( (lv_type_1_0= ruleValueType ) ) ) ;
    public final EObject ruleType() throws RecognitionException {
        EObject current = null;

        Enumerator lv_type_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:624:2: ( ( () ( (lv_type_1_0= ruleValueType ) ) ) )
            // InternalGloe.g:625:2: ( () ( (lv_type_1_0= ruleValueType ) ) )
            {
            // InternalGloe.g:625:2: ( () ( (lv_type_1_0= ruleValueType ) ) )
            // InternalGloe.g:626:3: () ( (lv_type_1_0= ruleValueType ) )
            {
            // InternalGloe.g:626:3: ()
            // InternalGloe.g:627:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTypeAccess().getTypeAction_0(),
            					current);
            			

            }

            // InternalGloe.g:633:3: ( (lv_type_1_0= ruleValueType ) )
            // InternalGloe.g:634:4: (lv_type_1_0= ruleValueType )
            {
            // InternalGloe.g:634:4: (lv_type_1_0= ruleValueType )
            // InternalGloe.g:635:5: lv_type_1_0= ruleValueType
            {

            					newCompositeNode(grammarAccess.getTypeAccess().getTypeValueTypeEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_type_1_0=ruleValueType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTypeRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_1_0,
            						"ice.master.loe.gloe.Gloe.ValueType");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleValue"
    // InternalGloe.g:656:1: entryRuleValue returns [EObject current=null] : iv_ruleValue= ruleValue EOF ;
    public final EObject entryRuleValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleValue = null;


        try {
            // InternalGloe.g:656:46: (iv_ruleValue= ruleValue EOF )
            // InternalGloe.g:657:2: iv_ruleValue= ruleValue EOF
            {
             newCompositeNode(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleValue=ruleValue();

            state._fsp--;

             current =iv_ruleValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalGloe.g:663:1: ruleValue returns [EObject current=null] : (this_LaneValue_0= ruleLaneValue | this_ResultValue_1= ruleResultValue | this_TeamValue_2= ruleTeamValue | (otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue ) ) ;
    public final EObject ruleValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject this_LaneValue_0 = null;

        EObject this_ResultValue_1 = null;

        EObject this_TeamValue_2 = null;

        EObject this_ChampionValue_5 = null;



        	enterRule();

        try {
            // InternalGloe.g:669:2: ( (this_LaneValue_0= ruleLaneValue | this_ResultValue_1= ruleResultValue | this_TeamValue_2= ruleTeamValue | (otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue ) ) )
            // InternalGloe.g:670:2: (this_LaneValue_0= ruleLaneValue | this_ResultValue_1= ruleResultValue | this_TeamValue_2= ruleTeamValue | (otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue ) )
            {
            // InternalGloe.g:670:2: (this_LaneValue_0= ruleLaneValue | this_ResultValue_1= ruleResultValue | this_TeamValue_2= ruleTeamValue | (otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue ) )
            int alt11=4;
            switch ( input.LA(1) ) {
            case 41:
            case 42:
            case 43:
            case 44:
                {
                alt11=1;
                }
                break;
            case 35:
            case 36:
                {
                alt11=2;
                }
                break;
            case 39:
            case 40:
                {
                alt11=3;
                }
                break;
            case 23:
                {
                alt11=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }

            switch (alt11) {
                case 1 :
                    // InternalGloe.g:671:3: this_LaneValue_0= ruleLaneValue
                    {

                    			newCompositeNode(grammarAccess.getValueAccess().getLaneValueParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_LaneValue_0=ruleLaneValue();

                    state._fsp--;


                    			current = this_LaneValue_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGloe.g:680:3: this_ResultValue_1= ruleResultValue
                    {

                    			newCompositeNode(grammarAccess.getValueAccess().getResultValueParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_ResultValue_1=ruleResultValue();

                    state._fsp--;


                    			current = this_ResultValue_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGloe.g:689:3: this_TeamValue_2= ruleTeamValue
                    {

                    			newCompositeNode(grammarAccess.getValueAccess().getTeamValueParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_TeamValue_2=ruleTeamValue();

                    state._fsp--;


                    			current = this_TeamValue_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalGloe.g:698:3: (otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue )
                    {
                    // InternalGloe.g:698:3: (otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue )
                    // InternalGloe.g:699:4: otherlv_3= 'games' otherlv_4= 'with' this_ChampionValue_5= ruleChampionValue
                    {
                    otherlv_3=(Token)match(input,23,FOLLOW_19); 

                    				newLeafNode(otherlv_3, grammarAccess.getValueAccess().getGamesKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,24,FOLLOW_20); 

                    				newLeafNode(otherlv_4, grammarAccess.getValueAccess().getWithKeyword_3_1());
                    			

                    				newCompositeNode(grammarAccess.getValueAccess().getChampionValueParserRuleCall_3_2());
                    			
                    pushFollow(FOLLOW_2);
                    this_ChampionValue_5=ruleChampionValue();

                    state._fsp--;


                    				current = this_ChampionValue_5;
                    				afterParserOrEnumRuleCall();
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRulePluralValue"
    // InternalGloe.g:720:1: entryRulePluralValue returns [EObject current=null] : iv_rulePluralValue= rulePluralValue EOF ;
    public final EObject entryRulePluralValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePluralValue = null;


        try {
            // InternalGloe.g:720:52: (iv_rulePluralValue= rulePluralValue EOF )
            // InternalGloe.g:721:2: iv_rulePluralValue= rulePluralValue EOF
            {
             newCompositeNode(grammarAccess.getPluralValueRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePluralValue=rulePluralValue();

            state._fsp--;

             current =iv_rulePluralValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePluralValue"


    // $ANTLR start "rulePluralValue"
    // InternalGloe.g:727:1: rulePluralValue returns [EObject current=null] : (this_StatValue_0= ruleStatValue | (otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team' ) | this_PluralGameResultValue_5= rulePluralGameResultValue | (otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue ) | (otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue ) ) ;
    public final EObject rulePluralValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject this_StatValue_0 = null;

        EObject this_TeamValue_3 = null;

        EObject this_PluralGameResultValue_5 = null;

        EObject this_ChampionValue_8 = null;

        EObject this_LaneValue_11 = null;



        	enterRule();

        try {
            // InternalGloe.g:733:2: ( (this_StatValue_0= ruleStatValue | (otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team' ) | this_PluralGameResultValue_5= rulePluralGameResultValue | (otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue ) | (otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue ) ) )
            // InternalGloe.g:734:2: (this_StatValue_0= ruleStatValue | (otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team' ) | this_PluralGameResultValue_5= rulePluralGameResultValue | (otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue ) | (otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue ) )
            {
            // InternalGloe.g:734:2: (this_StatValue_0= ruleStatValue | (otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team' ) | this_PluralGameResultValue_5= rulePluralGameResultValue | (otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue ) | (otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue ) )
            int alt12=5;
            switch ( input.LA(1) ) {
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 68:
            case 69:
            case 70:
            case 71:
            case 72:
            case 73:
            case 74:
            case 75:
            case 76:
            case 77:
            case 78:
            case 79:
            case 80:
            case 81:
            case 82:
            case 83:
            case 84:
            case 85:
            case 86:
                {
                alt12=1;
                }
                break;
            case 23:
                {
                int LA12_2 = input.LA(2);

                if ( (LA12_2==17) ) {
                    int LA12_4 = input.LA(3);

                    if ( ((LA12_4>=41 && LA12_4<=44)) ) {
                        alt12=5;
                    }
                    else if ( ((LA12_4>=39 && LA12_4<=40)) ) {
                        alt12=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 12, 4, input);

                        throw nvae;
                    }
                }
                else if ( (LA12_2==24) ) {
                    alt12=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 12, 2, input);

                    throw nvae;
                }
                }
                break;
            case 37:
            case 38:
                {
                alt12=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }

            switch (alt12) {
                case 1 :
                    // InternalGloe.g:735:3: this_StatValue_0= ruleStatValue
                    {

                    			newCompositeNode(grammarAccess.getPluralValueAccess().getStatValueParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_StatValue_0=ruleStatValue();

                    state._fsp--;


                    			current = this_StatValue_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGloe.g:744:3: (otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team' )
                    {
                    // InternalGloe.g:744:3: (otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team' )
                    // InternalGloe.g:745:4: otherlv_1= 'games' otherlv_2= 'in' this_TeamValue_3= ruleTeamValue otherlv_4= 'team'
                    {
                    otherlv_1=(Token)match(input,23,FOLLOW_13); 

                    				newLeafNode(otherlv_1, grammarAccess.getPluralValueAccess().getGamesKeyword_1_0());
                    			
                    otherlv_2=(Token)match(input,17,FOLLOW_21); 

                    				newLeafNode(otherlv_2, grammarAccess.getPluralValueAccess().getInKeyword_1_1());
                    			

                    				newCompositeNode(grammarAccess.getPluralValueAccess().getTeamValueParserRuleCall_1_2());
                    			
                    pushFollow(FOLLOW_22);
                    this_TeamValue_3=ruleTeamValue();

                    state._fsp--;


                    				current = this_TeamValue_3;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_4=(Token)match(input,25,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getPluralValueAccess().getTeamKeyword_1_3());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:767:3: this_PluralGameResultValue_5= rulePluralGameResultValue
                    {

                    			newCompositeNode(grammarAccess.getPluralValueAccess().getPluralGameResultValueParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_PluralGameResultValue_5=rulePluralGameResultValue();

                    state._fsp--;


                    			current = this_PluralGameResultValue_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalGloe.g:776:3: (otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue )
                    {
                    // InternalGloe.g:776:3: (otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue )
                    // InternalGloe.g:777:4: otherlv_6= 'games' otherlv_7= 'with' this_ChampionValue_8= ruleChampionValue
                    {
                    otherlv_6=(Token)match(input,23,FOLLOW_19); 

                    				newLeafNode(otherlv_6, grammarAccess.getPluralValueAccess().getGamesKeyword_3_0());
                    			
                    otherlv_7=(Token)match(input,24,FOLLOW_20); 

                    				newLeafNode(otherlv_7, grammarAccess.getPluralValueAccess().getWithKeyword_3_1());
                    			

                    				newCompositeNode(grammarAccess.getPluralValueAccess().getChampionValueParserRuleCall_3_2());
                    			
                    pushFollow(FOLLOW_2);
                    this_ChampionValue_8=ruleChampionValue();

                    state._fsp--;


                    				current = this_ChampionValue_8;
                    				afterParserOrEnumRuleCall();
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalGloe.g:795:3: (otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue )
                    {
                    // InternalGloe.g:795:3: (otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue )
                    // InternalGloe.g:796:4: otherlv_9= 'games' otherlv_10= 'in' this_LaneValue_11= ruleLaneValue
                    {
                    otherlv_9=(Token)match(input,23,FOLLOW_13); 

                    				newLeafNode(otherlv_9, grammarAccess.getPluralValueAccess().getGamesKeyword_4_0());
                    			
                    otherlv_10=(Token)match(input,17,FOLLOW_23); 

                    				newLeafNode(otherlv_10, grammarAccess.getPluralValueAccess().getInKeyword_4_1());
                    			

                    				newCompositeNode(grammarAccess.getPluralValueAccess().getLaneValueParserRuleCall_4_2());
                    			
                    pushFollow(FOLLOW_2);
                    this_LaneValue_11=ruleLaneValue();

                    state._fsp--;


                    				current = this_LaneValue_11;
                    				afterParserOrEnumRuleCall();
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePluralValue"


    // $ANTLR start "entryRuleResultValue"
    // InternalGloe.g:817:1: entryRuleResultValue returns [EObject current=null] : iv_ruleResultValue= ruleResultValue EOF ;
    public final EObject entryRuleResultValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleResultValue = null;


        try {
            // InternalGloe.g:817:52: (iv_ruleResultValue= ruleResultValue EOF )
            // InternalGloe.g:818:2: iv_ruleResultValue= ruleResultValue EOF
            {
             newCompositeNode(grammarAccess.getResultValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleResultValue=ruleResultValue();

            state._fsp--;

             current =iv_ruleResultValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleResultValue"


    // $ANTLR start "ruleResultValue"
    // InternalGloe.g:824:1: ruleResultValue returns [EObject current=null] : ( () ( (lv_result_1_0= ruleResult ) ) ) ;
    public final EObject ruleResultValue() throws RecognitionException {
        EObject current = null;

        Enumerator lv_result_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:830:2: ( ( () ( (lv_result_1_0= ruleResult ) ) ) )
            // InternalGloe.g:831:2: ( () ( (lv_result_1_0= ruleResult ) ) )
            {
            // InternalGloe.g:831:2: ( () ( (lv_result_1_0= ruleResult ) ) )
            // InternalGloe.g:832:3: () ( (lv_result_1_0= ruleResult ) )
            {
            // InternalGloe.g:832:3: ()
            // InternalGloe.g:833:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getResultValueAccess().getResultValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:839:3: ( (lv_result_1_0= ruleResult ) )
            // InternalGloe.g:840:4: (lv_result_1_0= ruleResult )
            {
            // InternalGloe.g:840:4: (lv_result_1_0= ruleResult )
            // InternalGloe.g:841:5: lv_result_1_0= ruleResult
            {

            					newCompositeNode(grammarAccess.getResultValueAccess().getResultResultEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_result_1_0=ruleResult();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getResultValueRule());
            					}
            					set(
            						current,
            						"result",
            						lv_result_1_0,
            						"ice.master.loe.gloe.Gloe.Result");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResultValue"


    // $ANTLR start "entryRulePluralGameResultValue"
    // InternalGloe.g:862:1: entryRulePluralGameResultValue returns [EObject current=null] : iv_rulePluralGameResultValue= rulePluralGameResultValue EOF ;
    public final EObject entryRulePluralGameResultValue() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePluralGameResultValue = null;


        try {
            // InternalGloe.g:862:62: (iv_rulePluralGameResultValue= rulePluralGameResultValue EOF )
            // InternalGloe.g:863:2: iv_rulePluralGameResultValue= rulePluralGameResultValue EOF
            {
             newCompositeNode(grammarAccess.getPluralGameResultValueRule()); 
            pushFollow(FOLLOW_1);
            iv_rulePluralGameResultValue=rulePluralGameResultValue();

            state._fsp--;

             current =iv_rulePluralGameResultValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePluralGameResultValue"


    // $ANTLR start "rulePluralGameResultValue"
    // InternalGloe.g:869:1: rulePluralGameResultValue returns [EObject current=null] : ( () ( (lv_result_1_0= rulePluralResult ) ) ) ;
    public final EObject rulePluralGameResultValue() throws RecognitionException {
        EObject current = null;

        Enumerator lv_result_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:875:2: ( ( () ( (lv_result_1_0= rulePluralResult ) ) ) )
            // InternalGloe.g:876:2: ( () ( (lv_result_1_0= rulePluralResult ) ) )
            {
            // InternalGloe.g:876:2: ( () ( (lv_result_1_0= rulePluralResult ) ) )
            // InternalGloe.g:877:3: () ( (lv_result_1_0= rulePluralResult ) )
            {
            // InternalGloe.g:877:3: ()
            // InternalGloe.g:878:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getPluralGameResultValueAccess().getResultValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:884:3: ( (lv_result_1_0= rulePluralResult ) )
            // InternalGloe.g:885:4: (lv_result_1_0= rulePluralResult )
            {
            // InternalGloe.g:885:4: (lv_result_1_0= rulePluralResult )
            // InternalGloe.g:886:5: lv_result_1_0= rulePluralResult
            {

            					newCompositeNode(grammarAccess.getPluralGameResultValueAccess().getResultPluralResultEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_result_1_0=rulePluralResult();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getPluralGameResultValueRule());
            					}
            					set(
            						current,
            						"result",
            						lv_result_1_0,
            						"ice.master.loe.gloe.Gloe.PluralResult");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePluralGameResultValue"


    // $ANTLR start "entryRuleTeamValue"
    // InternalGloe.g:907:1: entryRuleTeamValue returns [EObject current=null] : iv_ruleTeamValue= ruleTeamValue EOF ;
    public final EObject entryRuleTeamValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTeamValue = null;


        try {
            // InternalGloe.g:907:50: (iv_ruleTeamValue= ruleTeamValue EOF )
            // InternalGloe.g:908:2: iv_ruleTeamValue= ruleTeamValue EOF
            {
             newCompositeNode(grammarAccess.getTeamValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTeamValue=ruleTeamValue();

            state._fsp--;

             current =iv_ruleTeamValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTeamValue"


    // $ANTLR start "ruleTeamValue"
    // InternalGloe.g:914:1: ruleTeamValue returns [EObject current=null] : ( () ( (lv_team_1_0= ruleTeam ) ) ) ;
    public final EObject ruleTeamValue() throws RecognitionException {
        EObject current = null;

        Enumerator lv_team_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:920:2: ( ( () ( (lv_team_1_0= ruleTeam ) ) ) )
            // InternalGloe.g:921:2: ( () ( (lv_team_1_0= ruleTeam ) ) )
            {
            // InternalGloe.g:921:2: ( () ( (lv_team_1_0= ruleTeam ) ) )
            // InternalGloe.g:922:3: () ( (lv_team_1_0= ruleTeam ) )
            {
            // InternalGloe.g:922:3: ()
            // InternalGloe.g:923:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTeamValueAccess().getTeamValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:929:3: ( (lv_team_1_0= ruleTeam ) )
            // InternalGloe.g:930:4: (lv_team_1_0= ruleTeam )
            {
            // InternalGloe.g:930:4: (lv_team_1_0= ruleTeam )
            // InternalGloe.g:931:5: lv_team_1_0= ruleTeam
            {

            					newCompositeNode(grammarAccess.getTeamValueAccess().getTeamTeamEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_team_1_0=ruleTeam();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTeamValueRule());
            					}
            					set(
            						current,
            						"team",
            						lv_team_1_0,
            						"ice.master.loe.gloe.Gloe.Team");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTeamValue"


    // $ANTLR start "entryRuleLaneValue"
    // InternalGloe.g:952:1: entryRuleLaneValue returns [EObject current=null] : iv_ruleLaneValue= ruleLaneValue EOF ;
    public final EObject entryRuleLaneValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLaneValue = null;


        try {
            // InternalGloe.g:952:50: (iv_ruleLaneValue= ruleLaneValue EOF )
            // InternalGloe.g:953:2: iv_ruleLaneValue= ruleLaneValue EOF
            {
             newCompositeNode(grammarAccess.getLaneValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLaneValue=ruleLaneValue();

            state._fsp--;

             current =iv_ruleLaneValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLaneValue"


    // $ANTLR start "ruleLaneValue"
    // InternalGloe.g:959:1: ruleLaneValue returns [EObject current=null] : ( () ( (lv_lane_1_0= ruleLane ) ) ) ;
    public final EObject ruleLaneValue() throws RecognitionException {
        EObject current = null;

        Enumerator lv_lane_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:965:2: ( ( () ( (lv_lane_1_0= ruleLane ) ) ) )
            // InternalGloe.g:966:2: ( () ( (lv_lane_1_0= ruleLane ) ) )
            {
            // InternalGloe.g:966:2: ( () ( (lv_lane_1_0= ruleLane ) ) )
            // InternalGloe.g:967:3: () ( (lv_lane_1_0= ruleLane ) )
            {
            // InternalGloe.g:967:3: ()
            // InternalGloe.g:968:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getLaneValueAccess().getLaneValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:974:3: ( (lv_lane_1_0= ruleLane ) )
            // InternalGloe.g:975:4: (lv_lane_1_0= ruleLane )
            {
            // InternalGloe.g:975:4: (lv_lane_1_0= ruleLane )
            // InternalGloe.g:976:5: lv_lane_1_0= ruleLane
            {

            					newCompositeNode(grammarAccess.getLaneValueAccess().getLaneLaneEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_lane_1_0=ruleLane();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getLaneValueRule());
            					}
            					set(
            						current,
            						"lane",
            						lv_lane_1_0,
            						"ice.master.loe.gloe.Gloe.Lane");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLaneValue"


    // $ANTLR start "entryRuleStatValue"
    // InternalGloe.g:997:1: entryRuleStatValue returns [EObject current=null] : iv_ruleStatValue= ruleStatValue EOF ;
    public final EObject entryRuleStatValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatValue = null;


        try {
            // InternalGloe.g:997:50: (iv_ruleStatValue= ruleStatValue EOF )
            // InternalGloe.g:998:2: iv_ruleStatValue= ruleStatValue EOF
            {
             newCompositeNode(grammarAccess.getStatValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStatValue=ruleStatValue();

            state._fsp--;

             current =iv_ruleStatValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatValue"


    // $ANTLR start "ruleStatValue"
    // InternalGloe.g:1004:1: ruleStatValue returns [EObject current=null] : ( () ( (lv_stat_1_0= ruleStat ) ) ) ;
    public final EObject ruleStatValue() throws RecognitionException {
        EObject current = null;

        Enumerator lv_stat_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1010:2: ( ( () ( (lv_stat_1_0= ruleStat ) ) ) )
            // InternalGloe.g:1011:2: ( () ( (lv_stat_1_0= ruleStat ) ) )
            {
            // InternalGloe.g:1011:2: ( () ( (lv_stat_1_0= ruleStat ) ) )
            // InternalGloe.g:1012:3: () ( (lv_stat_1_0= ruleStat ) )
            {
            // InternalGloe.g:1012:3: ()
            // InternalGloe.g:1013:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getStatValueAccess().getStatValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:1019:3: ( (lv_stat_1_0= ruleStat ) )
            // InternalGloe.g:1020:4: (lv_stat_1_0= ruleStat )
            {
            // InternalGloe.g:1020:4: (lv_stat_1_0= ruleStat )
            // InternalGloe.g:1021:5: lv_stat_1_0= ruleStat
            {

            					newCompositeNode(grammarAccess.getStatValueAccess().getStatStatEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_stat_1_0=ruleStat();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getStatValueRule());
            					}
            					set(
            						current,
            						"stat",
            						lv_stat_1_0,
            						"ice.master.loe.gloe.Gloe.Stat");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatValue"


    // $ANTLR start "entryRuleChampionValue"
    // InternalGloe.g:1042:1: entryRuleChampionValue returns [EObject current=null] : iv_ruleChampionValue= ruleChampionValue EOF ;
    public final EObject entryRuleChampionValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChampionValue = null;


        try {
            // InternalGloe.g:1042:54: (iv_ruleChampionValue= ruleChampionValue EOF )
            // InternalGloe.g:1043:2: iv_ruleChampionValue= ruleChampionValue EOF
            {
             newCompositeNode(grammarAccess.getChampionValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleChampionValue=ruleChampionValue();

            state._fsp--;

             current =iv_ruleChampionValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChampionValue"


    // $ANTLR start "ruleChampionValue"
    // InternalGloe.g:1049:1: ruleChampionValue returns [EObject current=null] : ( () ( (lv_champion_1_0= ruleChampion ) ) ) ;
    public final EObject ruleChampionValue() throws RecognitionException {
        EObject current = null;

        Enumerator lv_champion_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1055:2: ( ( () ( (lv_champion_1_0= ruleChampion ) ) ) )
            // InternalGloe.g:1056:2: ( () ( (lv_champion_1_0= ruleChampion ) ) )
            {
            // InternalGloe.g:1056:2: ( () ( (lv_champion_1_0= ruleChampion ) ) )
            // InternalGloe.g:1057:3: () ( (lv_champion_1_0= ruleChampion ) )
            {
            // InternalGloe.g:1057:3: ()
            // InternalGloe.g:1058:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getChampionValueAccess().getChampionValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:1064:3: ( (lv_champion_1_0= ruleChampion ) )
            // InternalGloe.g:1065:4: (lv_champion_1_0= ruleChampion )
            {
            // InternalGloe.g:1065:4: (lv_champion_1_0= ruleChampion )
            // InternalGloe.g:1066:5: lv_champion_1_0= ruleChampion
            {

            					newCompositeNode(grammarAccess.getChampionValueAccess().getChampionChampionEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_champion_1_0=ruleChampion();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getChampionValueRule());
            					}
            					set(
            						current,
            						"champion",
            						lv_champion_1_0,
            						"ice.master.loe.gloe.Gloe.Champion");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChampionValue"


    // $ANTLR start "entryRuleTimelapse"
    // InternalGloe.g:1087:1: entryRuleTimelapse returns [EObject current=null] : iv_ruleTimelapse= ruleTimelapse EOF ;
    public final EObject entryRuleTimelapse() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTimelapse = null;


        try {
            // InternalGloe.g:1087:50: (iv_ruleTimelapse= ruleTimelapse EOF )
            // InternalGloe.g:1088:2: iv_ruleTimelapse= ruleTimelapse EOF
            {
             newCompositeNode(grammarAccess.getTimelapseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTimelapse=ruleTimelapse();

            state._fsp--;

             current =iv_ruleTimelapse; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTimelapse"


    // $ANTLR start "ruleTimelapse"
    // InternalGloe.g:1094:1: ruleTimelapse returns [EObject current=null] : ( () otherlv_1= 'from' ( (lv_from_2_0= ruleEDate ) ) (otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) ) )? ) ;
    public final EObject ruleTimelapse() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        AntlrDatatypeRuleToken lv_from_2_0 = null;

        AntlrDatatypeRuleToken lv_to_4_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1100:2: ( ( () otherlv_1= 'from' ( (lv_from_2_0= ruleEDate ) ) (otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) ) )? ) )
            // InternalGloe.g:1101:2: ( () otherlv_1= 'from' ( (lv_from_2_0= ruleEDate ) ) (otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) ) )? )
            {
            // InternalGloe.g:1101:2: ( () otherlv_1= 'from' ( (lv_from_2_0= ruleEDate ) ) (otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) ) )? )
            // InternalGloe.g:1102:3: () otherlv_1= 'from' ( (lv_from_2_0= ruleEDate ) ) (otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) ) )?
            {
            // InternalGloe.g:1102:3: ()
            // InternalGloe.g:1103:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getTimelapseAccess().getTimelapseAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getTimelapseAccess().getFromKeyword_1());
            		
            // InternalGloe.g:1113:3: ( (lv_from_2_0= ruleEDate ) )
            // InternalGloe.g:1114:4: (lv_from_2_0= ruleEDate )
            {
            // InternalGloe.g:1114:4: (lv_from_2_0= ruleEDate )
            // InternalGloe.g:1115:5: lv_from_2_0= ruleEDate
            {

            					newCompositeNode(grammarAccess.getTimelapseAccess().getFromEDateParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_24);
            lv_from_2_0=ruleEDate();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTimelapseRule());
            					}
            					set(
            						current,
            						"from",
            						lv_from_2_0,
            						"ice.master.loe.gloe.Gloe.EDate");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGloe.g:1132:3: (otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==27) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalGloe.g:1133:4: otherlv_3= 'to' ( (lv_to_4_0= ruleEDate ) )
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_3); 

                    				newLeafNode(otherlv_3, grammarAccess.getTimelapseAccess().getToKeyword_3_0());
                    			
                    // InternalGloe.g:1137:4: ( (lv_to_4_0= ruleEDate ) )
                    // InternalGloe.g:1138:5: (lv_to_4_0= ruleEDate )
                    {
                    // InternalGloe.g:1138:5: (lv_to_4_0= ruleEDate )
                    // InternalGloe.g:1139:6: lv_to_4_0= ruleEDate
                    {

                    						newCompositeNode(grammarAccess.getTimelapseAccess().getToEDateParserRuleCall_3_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_to_4_0=ruleEDate();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getTimelapseRule());
                    						}
                    						set(
                    							current,
                    							"to",
                    							lv_to_4_0,
                    							"ice.master.loe.gloe.Gloe.EDate");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimelapse"


    // $ANTLR start "entryRuleYValueShortcuts"
    // InternalGloe.g:1161:1: entryRuleYValueShortcuts returns [EObject current=null] : iv_ruleYValueShortcuts= ruleYValueShortcuts EOF ;
    public final EObject entryRuleYValueShortcuts() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleYValueShortcuts = null;


        try {
            // InternalGloe.g:1161:56: (iv_ruleYValueShortcuts= ruleYValueShortcuts EOF )
            // InternalGloe.g:1162:2: iv_ruleYValueShortcuts= ruleYValueShortcuts EOF
            {
             newCompositeNode(grammarAccess.getYValueShortcutsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleYValueShortcuts=ruleYValueShortcuts();

            state._fsp--;

             current =iv_ruleYValueShortcuts; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleYValueShortcuts"


    // $ANTLR start "ruleYValueShortcuts"
    // InternalGloe.g:1168:1: ruleYValueShortcuts returns [EObject current=null] : this_Winrate_0= ruleWinrate ;
    public final EObject ruleYValueShortcuts() throws RecognitionException {
        EObject current = null;

        EObject this_Winrate_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1174:2: (this_Winrate_0= ruleWinrate )
            // InternalGloe.g:1175:2: this_Winrate_0= ruleWinrate
            {

            		newCompositeNode(grammarAccess.getYValueShortcutsAccess().getWinrateParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_Winrate_0=ruleWinrate();

            state._fsp--;


            		current = this_Winrate_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleYValueShortcuts"


    // $ANTLR start "entryRuleWinrate"
    // InternalGloe.g:1186:1: entryRuleWinrate returns [EObject current=null] : iv_ruleWinrate= ruleWinrate EOF ;
    public final EObject entryRuleWinrate() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWinrate = null;


        try {
            // InternalGloe.g:1186:48: (iv_ruleWinrate= ruleWinrate EOF )
            // InternalGloe.g:1187:2: iv_ruleWinrate= ruleWinrate EOF
            {
             newCompositeNode(grammarAccess.getWinrateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWinrate=ruleWinrate();

            state._fsp--;

             current =iv_ruleWinrate; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWinrate"


    // $ANTLR start "ruleWinrate"
    // InternalGloe.g:1193:1: ruleWinrate returns [EObject current=null] : ( () ( (lv_value_1_0= ruleGameResultValueWinrateShortcut ) ) ) ;
    public final EObject ruleWinrate() throws RecognitionException {
        EObject current = null;

        EObject lv_value_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1199:2: ( ( () ( (lv_value_1_0= ruleGameResultValueWinrateShortcut ) ) ) )
            // InternalGloe.g:1200:2: ( () ( (lv_value_1_0= ruleGameResultValueWinrateShortcut ) ) )
            {
            // InternalGloe.g:1200:2: ( () ( (lv_value_1_0= ruleGameResultValueWinrateShortcut ) ) )
            // InternalGloe.g:1201:3: () ( (lv_value_1_0= ruleGameResultValueWinrateShortcut ) )
            {
            // InternalGloe.g:1201:3: ()
            // InternalGloe.g:1202:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getWinrateAccess().getRatioAction_0(),
            					current);
            			

            }

            // InternalGloe.g:1208:3: ( (lv_value_1_0= ruleGameResultValueWinrateShortcut ) )
            // InternalGloe.g:1209:4: (lv_value_1_0= ruleGameResultValueWinrateShortcut )
            {
            // InternalGloe.g:1209:4: (lv_value_1_0= ruleGameResultValueWinrateShortcut )
            // InternalGloe.g:1210:5: lv_value_1_0= ruleGameResultValueWinrateShortcut
            {

            					newCompositeNode(grammarAccess.getWinrateAccess().getValueGameResultValueWinrateShortcutParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleGameResultValueWinrateShortcut();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWinrateRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_1_0,
            						"ice.master.loe.gloe.Gloe.GameResultValueWinrateShortcut");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWinrate"


    // $ANTLR start "entryRuleGameResultValueWinrateShortcut"
    // InternalGloe.g:1231:1: entryRuleGameResultValueWinrateShortcut returns [EObject current=null] : iv_ruleGameResultValueWinrateShortcut= ruleGameResultValueWinrateShortcut EOF ;
    public final EObject entryRuleGameResultValueWinrateShortcut() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGameResultValueWinrateShortcut = null;


        try {
            // InternalGloe.g:1231:71: (iv_ruleGameResultValueWinrateShortcut= ruleGameResultValueWinrateShortcut EOF )
            // InternalGloe.g:1232:2: iv_ruleGameResultValueWinrateShortcut= ruleGameResultValueWinrateShortcut EOF
            {
             newCompositeNode(grammarAccess.getGameResultValueWinrateShortcutRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGameResultValueWinrateShortcut=ruleGameResultValueWinrateShortcut();

            state._fsp--;

             current =iv_ruleGameResultValueWinrateShortcut; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGameResultValueWinrateShortcut"


    // $ANTLR start "ruleGameResultValueWinrateShortcut"
    // InternalGloe.g:1238:1: ruleGameResultValueWinrateShortcut returns [EObject current=null] : ( () ( (lv_result_1_0= ruleGameResultWinrateShortcut ) ) ) ;
    public final EObject ruleGameResultValueWinrateShortcut() throws RecognitionException {
        EObject current = null;

        Enumerator lv_result_1_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1244:2: ( ( () ( (lv_result_1_0= ruleGameResultWinrateShortcut ) ) ) )
            // InternalGloe.g:1245:2: ( () ( (lv_result_1_0= ruleGameResultWinrateShortcut ) ) )
            {
            // InternalGloe.g:1245:2: ( () ( (lv_result_1_0= ruleGameResultWinrateShortcut ) ) )
            // InternalGloe.g:1246:3: () ( (lv_result_1_0= ruleGameResultWinrateShortcut ) )
            {
            // InternalGloe.g:1246:3: ()
            // InternalGloe.g:1247:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getGameResultValueWinrateShortcutAccess().getResultValueAction_0(),
            					current);
            			

            }

            // InternalGloe.g:1253:3: ( (lv_result_1_0= ruleGameResultWinrateShortcut ) )
            // InternalGloe.g:1254:4: (lv_result_1_0= ruleGameResultWinrateShortcut )
            {
            // InternalGloe.g:1254:4: (lv_result_1_0= ruleGameResultWinrateShortcut )
            // InternalGloe.g:1255:5: lv_result_1_0= ruleGameResultWinrateShortcut
            {

            					newCompositeNode(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultGameResultWinrateShortcutEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_result_1_0=ruleGameResultWinrateShortcut();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getGameResultValueWinrateShortcutRule());
            					}
            					set(
            						current,
            						"result",
            						lv_result_1_0,
            						"ice.master.loe.gloe.Gloe.GameResultWinrateShortcut");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGameResultValueWinrateShortcut"


    // $ANTLR start "entryRuleFilter"
    // InternalGloe.g:1276:1: entryRuleFilter returns [EObject current=null] : iv_ruleFilter= ruleFilter EOF ;
    public final EObject entryRuleFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFilter = null;


        try {
            // InternalGloe.g:1276:47: (iv_ruleFilter= ruleFilter EOF )
            // InternalGloe.g:1277:2: iv_ruleFilter= ruleFilter EOF
            {
             newCompositeNode(grammarAccess.getFilterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFilter=ruleFilter();

            state._fsp--;

             current =iv_ruleFilter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFilter"


    // $ANTLR start "ruleFilter"
    // InternalGloe.g:1283:1: ruleFilter returns [EObject current=null] : this_FilterByValue_0= ruleFilterByValue ;
    public final EObject ruleFilter() throws RecognitionException {
        EObject current = null;

        EObject this_FilterByValue_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1289:2: (this_FilterByValue_0= ruleFilterByValue )
            // InternalGloe.g:1290:2: this_FilterByValue_0= ruleFilterByValue
            {

            		newCompositeNode(grammarAccess.getFilterAccess().getFilterByValueParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_FilterByValue_0=ruleFilterByValue();

            state._fsp--;


            		current = this_FilterByValue_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFilter"


    // $ANTLR start "entryRuleFilterByValue"
    // InternalGloe.g:1301:1: entryRuleFilterByValue returns [EObject current=null] : iv_ruleFilterByValue= ruleFilterByValue EOF ;
    public final EObject entryRuleFilterByValue() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFilterByValue = null;


        try {
            // InternalGloe.g:1301:54: (iv_ruleFilterByValue= ruleFilterByValue EOF )
            // InternalGloe.g:1302:2: iv_ruleFilterByValue= ruleFilterByValue EOF
            {
             newCompositeNode(grammarAccess.getFilterByValueRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFilterByValue=ruleFilterByValue();

            state._fsp--;

             current =iv_ruleFilterByValue; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFilterByValue"


    // $ANTLR start "ruleFilterByValue"
    // InternalGloe.g:1308:1: ruleFilterByValue returns [EObject current=null] : ( ( () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) ) ) | (otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) ) ) | (otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) ) ) | (otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) ) ) ) ;
    public final EObject ruleFilterByValue() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        EObject lv_value_3_0 = null;

        EObject lv_value_5_0 = null;

        EObject lv_value_8_0 = null;

        EObject lv_value_11_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1314:2: ( ( ( () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) ) ) | (otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) ) ) | (otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) ) ) | (otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) ) ) ) )
            // InternalGloe.g:1315:2: ( ( () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) ) ) | (otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) ) ) | (otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) ) ) | (otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) ) ) )
            {
            // InternalGloe.g:1315:2: ( ( () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) ) ) | (otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) ) ) | (otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) ) ) | (otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) ) ) )
            int alt14=4;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt14=1;
                }
                break;
            case 29:
                {
                alt14=2;
                }
                break;
            case 30:
                {
                alt14=3;
                }
                break;
            case 31:
                {
                alt14=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalGloe.g:1316:3: ( () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) ) )
                    {
                    // InternalGloe.g:1316:3: ( () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) ) )
                    // InternalGloe.g:1317:4: () (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) )
                    {
                    // InternalGloe.g:1317:4: ()
                    // InternalGloe.g:1318:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getFilterByValueAccess().getFilterByValueAction_0_0(),
                    						current);
                    				

                    }

                    // InternalGloe.g:1324:4: (otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) ) )
                    // InternalGloe.g:1325:5: otherlv_1= 'team' otherlv_2= 'is' ( (lv_value_3_0= ruleTeamValue ) )
                    {
                    otherlv_1=(Token)match(input,25,FOLLOW_25); 

                    					newLeafNode(otherlv_1, grammarAccess.getFilterByValueAccess().getTeamKeyword_0_1_0());
                    				
                    otherlv_2=(Token)match(input,28,FOLLOW_21); 

                    					newLeafNode(otherlv_2, grammarAccess.getFilterByValueAccess().getIsKeyword_0_1_1());
                    				
                    // InternalGloe.g:1333:5: ( (lv_value_3_0= ruleTeamValue ) )
                    // InternalGloe.g:1334:6: (lv_value_3_0= ruleTeamValue )
                    {
                    // InternalGloe.g:1334:6: (lv_value_3_0= ruleTeamValue )
                    // InternalGloe.g:1335:7: lv_value_3_0= ruleTeamValue
                    {

                    							newCompositeNode(grammarAccess.getFilterByValueAccess().getValueTeamValueParserRuleCall_0_1_2_0());
                    						
                    pushFollow(FOLLOW_2);
                    lv_value_3_0=ruleTeamValue();

                    state._fsp--;


                    							if (current==null) {
                    								current = createModelElementForParent(grammarAccess.getFilterByValueRule());
                    							}
                    							set(
                    								current,
                    								"value",
                    								lv_value_3_0,
                    								"ice.master.loe.gloe.Gloe.TeamValue");
                    							afterParserOrEnumRuleCall();
                    						

                    }


                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1355:3: (otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) ) )
                    {
                    // InternalGloe.g:1355:3: (otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) ) )
                    // InternalGloe.g:1356:4: otherlv_4= 'playing' ( (lv_value_5_0= ruleChampionValue ) )
                    {
                    otherlv_4=(Token)match(input,29,FOLLOW_20); 

                    				newLeafNode(otherlv_4, grammarAccess.getFilterByValueAccess().getPlayingKeyword_1_0());
                    			
                    // InternalGloe.g:1360:4: ( (lv_value_5_0= ruleChampionValue ) )
                    // InternalGloe.g:1361:5: (lv_value_5_0= ruleChampionValue )
                    {
                    // InternalGloe.g:1361:5: (lv_value_5_0= ruleChampionValue )
                    // InternalGloe.g:1362:6: lv_value_5_0= ruleChampionValue
                    {

                    						newCompositeNode(grammarAccess.getFilterByValueAccess().getValueChampionValueParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_5_0=ruleChampionValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilterByValueRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_5_0,
                    							"ice.master.loe.gloe.Gloe.ChampionValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1381:3: (otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) ) )
                    {
                    // InternalGloe.g:1381:3: (otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) ) )
                    // InternalGloe.g:1382:4: otherlv_6= 'lane' otherlv_7= 'is' ( (lv_value_8_0= ruleLaneValue ) )
                    {
                    otherlv_6=(Token)match(input,30,FOLLOW_25); 

                    				newLeafNode(otherlv_6, grammarAccess.getFilterByValueAccess().getLaneKeyword_2_0());
                    			
                    otherlv_7=(Token)match(input,28,FOLLOW_23); 

                    				newLeafNode(otherlv_7, grammarAccess.getFilterByValueAccess().getIsKeyword_2_1());
                    			
                    // InternalGloe.g:1390:4: ( (lv_value_8_0= ruleLaneValue ) )
                    // InternalGloe.g:1391:5: (lv_value_8_0= ruleLaneValue )
                    {
                    // InternalGloe.g:1391:5: (lv_value_8_0= ruleLaneValue )
                    // InternalGloe.g:1392:6: lv_value_8_0= ruleLaneValue
                    {

                    						newCompositeNode(grammarAccess.getFilterByValueAccess().getValueLaneValueParserRuleCall_2_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_8_0=ruleLaneValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilterByValueRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_8_0,
                    							"ice.master.loe.gloe.Gloe.LaneValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1411:3: (otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) ) )
                    {
                    // InternalGloe.g:1411:3: (otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) ) )
                    // InternalGloe.g:1412:4: otherlv_9= 'ends' otherlv_10= 'with' ( (lv_value_11_0= ruleResultValue ) )
                    {
                    otherlv_9=(Token)match(input,31,FOLLOW_19); 

                    				newLeafNode(otherlv_9, grammarAccess.getFilterByValueAccess().getEndsKeyword_3_0());
                    			
                    otherlv_10=(Token)match(input,24,FOLLOW_26); 

                    				newLeafNode(otherlv_10, grammarAccess.getFilterByValueAccess().getWithKeyword_3_1());
                    			
                    // InternalGloe.g:1420:4: ( (lv_value_11_0= ruleResultValue ) )
                    // InternalGloe.g:1421:5: (lv_value_11_0= ruleResultValue )
                    {
                    // InternalGloe.g:1421:5: (lv_value_11_0= ruleResultValue )
                    // InternalGloe.g:1422:6: lv_value_11_0= ruleResultValue
                    {

                    						newCompositeNode(grammarAccess.getFilterByValueAccess().getValueResultValueParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_11_0=ruleResultValue();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFilterByValueRule());
                    						}
                    						set(
                    							current,
                    							"value",
                    							lv_value_11_0,
                    							"ice.master.loe.gloe.Gloe.ResultValue");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFilterByValue"


    // $ANTLR start "entryRuleDiagram"
    // InternalGloe.g:1444:1: entryRuleDiagram returns [EObject current=null] : iv_ruleDiagram= ruleDiagram EOF ;
    public final EObject entryRuleDiagram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDiagram = null;


        try {
            // InternalGloe.g:1444:48: (iv_ruleDiagram= ruleDiagram EOF )
            // InternalGloe.g:1445:2: iv_ruleDiagram= ruleDiagram EOF
            {
             newCompositeNode(grammarAccess.getDiagramRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDiagram=ruleDiagram();

            state._fsp--;

             current =iv_ruleDiagram; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDiagram"


    // $ANTLR start "ruleDiagram"
    // InternalGloe.g:1451:1: ruleDiagram returns [EObject current=null] : ( () ( (lv_type_1_0= ruleDiagramType ) ) (otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) ) )? ) ;
    public final EObject ruleDiagram() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Enumerator lv_type_1_0 = null;

        AntlrDatatypeRuleToken lv_name_3_0 = null;



        	enterRule();

        try {
            // InternalGloe.g:1457:2: ( ( () ( (lv_type_1_0= ruleDiagramType ) ) (otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) ) )? ) )
            // InternalGloe.g:1458:2: ( () ( (lv_type_1_0= ruleDiagramType ) ) (otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) ) )? )
            {
            // InternalGloe.g:1458:2: ( () ( (lv_type_1_0= ruleDiagramType ) ) (otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) ) )? )
            // InternalGloe.g:1459:3: () ( (lv_type_1_0= ruleDiagramType ) ) (otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) ) )?
            {
            // InternalGloe.g:1459:3: ()
            // InternalGloe.g:1460:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getDiagramAccess().getDiagramAction_0(),
            					current);
            			

            }

            // InternalGloe.g:1466:3: ( (lv_type_1_0= ruleDiagramType ) )
            // InternalGloe.g:1467:4: (lv_type_1_0= ruleDiagramType )
            {
            // InternalGloe.g:1467:4: (lv_type_1_0= ruleDiagramType )
            // InternalGloe.g:1468:5: lv_type_1_0= ruleDiagramType
            {

            					newCompositeNode(grammarAccess.getDiagramAccess().getTypeDiagramTypeEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_27);
            lv_type_1_0=ruleDiagramType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDiagramRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_1_0,
            						"ice.master.loe.gloe.Gloe.DiagramType");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGloe.g:1485:3: (otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) ) )?
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==32) ) {
                alt15=1;
            }
            switch (alt15) {
                case 1 :
                    // InternalGloe.g:1486:4: otherlv_2= 'called' ( (lv_name_3_0= ruleEString ) )
                    {
                    otherlv_2=(Token)match(input,32,FOLLOW_3); 

                    				newLeafNode(otherlv_2, grammarAccess.getDiagramAccess().getCalledKeyword_2_0());
                    			
                    // InternalGloe.g:1490:4: ( (lv_name_3_0= ruleEString ) )
                    // InternalGloe.g:1491:5: (lv_name_3_0= ruleEString )
                    {
                    // InternalGloe.g:1491:5: (lv_name_3_0= ruleEString )
                    // InternalGloe.g:1492:6: lv_name_3_0= ruleEString
                    {

                    						newCompositeNode(grammarAccess.getDiagramAccess().getNameEStringParserRuleCall_2_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_name_3_0=ruleEString();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getDiagramRule());
                    						}
                    						set(
                    							current,
                    							"name",
                    							lv_name_3_0,
                    							"ice.master.loe.gloe.Gloe.EString");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDiagram"


    // $ANTLR start "entryRuleEString"
    // InternalGloe.g:1514:1: entryRuleEString returns [String current=null] : iv_ruleEString= ruleEString EOF ;
    public final String entryRuleEString() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEString = null;


        try {
            // InternalGloe.g:1514:47: (iv_ruleEString= ruleEString EOF )
            // InternalGloe.g:1515:2: iv_ruleEString= ruleEString EOF
            {
             newCompositeNode(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEString=ruleEString();

            state._fsp--;

             current =iv_ruleEString.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalGloe.g:1521:1: ruleEString returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEString() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalGloe.g:1527:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalGloe.g:1528:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalGloe.g:1528:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==RULE_STRING) ) {
                alt16=1;
            }
            else if ( (LA16_0==RULE_ID) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalGloe.g:1529:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalGloe.g:1537:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEStringAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEDate"
    // InternalGloe.g:1548:1: entryRuleEDate returns [String current=null] : iv_ruleEDate= ruleEDate EOF ;
    public final String entryRuleEDate() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleEDate = null;


        try {
            // InternalGloe.g:1548:45: (iv_ruleEDate= ruleEDate EOF )
            // InternalGloe.g:1549:2: iv_ruleEDate= ruleEDate EOF
            {
             newCompositeNode(grammarAccess.getEDateRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleEDate=ruleEDate();

            state._fsp--;

             current =iv_ruleEDate.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleEDate"


    // $ANTLR start "ruleEDate"
    // InternalGloe.g:1555:1: ruleEDate returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) ;
    public final AntlrDatatypeRuleToken ruleEDate() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token this_ID_1=null;


        	enterRule();

        try {
            // InternalGloe.g:1561:2: ( (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID ) )
            // InternalGloe.g:1562:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            {
            // InternalGloe.g:1562:2: (this_STRING_0= RULE_STRING | this_ID_1= RULE_ID )
            int alt17=2;
            int LA17_0 = input.LA(1);

            if ( (LA17_0==RULE_STRING) ) {
                alt17=1;
            }
            else if ( (LA17_0==RULE_ID) ) {
                alt17=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }
            switch (alt17) {
                case 1 :
                    // InternalGloe.g:1563:3: this_STRING_0= RULE_STRING
                    {
                    this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

                    			current.merge(this_STRING_0);
                    		

                    			newLeafNode(this_STRING_0, grammarAccess.getEDateAccess().getSTRINGTerminalRuleCall_0());
                    		

                    }
                    break;
                case 2 :
                    // InternalGloe.g:1571:3: this_ID_1= RULE_ID
                    {
                    this_ID_1=(Token)match(input,RULE_ID,FOLLOW_2); 

                    			current.merge(this_ID_1);
                    		

                    			newLeafNode(this_ID_1, grammarAccess.getEDateAccess().getIDTerminalRuleCall_1());
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleEDate"


    // $ANTLR start "ruleValueType"
    // InternalGloe.g:1582:1: ruleValueType returns [Enumerator current=null] : ( (enumLiteral_0= 'lane' ) | (enumLiteral_1= 'game result' ) | (enumLiteral_2= 'champion' ) | (enumLiteral_3= 'team' ) ) ;
    public final Enumerator ruleValueType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalGloe.g:1588:2: ( ( (enumLiteral_0= 'lane' ) | (enumLiteral_1= 'game result' ) | (enumLiteral_2= 'champion' ) | (enumLiteral_3= 'team' ) ) )
            // InternalGloe.g:1589:2: ( (enumLiteral_0= 'lane' ) | (enumLiteral_1= 'game result' ) | (enumLiteral_2= 'champion' ) | (enumLiteral_3= 'team' ) )
            {
            // InternalGloe.g:1589:2: ( (enumLiteral_0= 'lane' ) | (enumLiteral_1= 'game result' ) | (enumLiteral_2= 'champion' ) | (enumLiteral_3= 'team' ) )
            int alt18=4;
            switch ( input.LA(1) ) {
            case 30:
                {
                alt18=1;
                }
                break;
            case 33:
                {
                alt18=2;
                }
                break;
            case 34:
                {
                alt18=3;
                }
                break;
            case 25:
                {
                alt18=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }

            switch (alt18) {
                case 1 :
                    // InternalGloe.g:1590:3: (enumLiteral_0= 'lane' )
                    {
                    // InternalGloe.g:1590:3: (enumLiteral_0= 'lane' )
                    // InternalGloe.g:1591:4: enumLiteral_0= 'lane'
                    {
                    enumLiteral_0=(Token)match(input,30,FOLLOW_2); 

                    				current = grammarAccess.getValueTypeAccess().getLANEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getValueTypeAccess().getLANEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1598:3: (enumLiteral_1= 'game result' )
                    {
                    // InternalGloe.g:1598:3: (enumLiteral_1= 'game result' )
                    // InternalGloe.g:1599:4: enumLiteral_1= 'game result'
                    {
                    enumLiteral_1=(Token)match(input,33,FOLLOW_2); 

                    				current = grammarAccess.getValueTypeAccess().getGAME_RESULTEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getValueTypeAccess().getGAME_RESULTEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1606:3: (enumLiteral_2= 'champion' )
                    {
                    // InternalGloe.g:1606:3: (enumLiteral_2= 'champion' )
                    // InternalGloe.g:1607:4: enumLiteral_2= 'champion'
                    {
                    enumLiteral_2=(Token)match(input,34,FOLLOW_2); 

                    				current = grammarAccess.getValueTypeAccess().getCHAMPIONEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getValueTypeAccess().getCHAMPIONEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1614:3: (enumLiteral_3= 'team' )
                    {
                    // InternalGloe.g:1614:3: (enumLiteral_3= 'team' )
                    // InternalGloe.g:1615:4: enumLiteral_3= 'team'
                    {
                    enumLiteral_3=(Token)match(input,25,FOLLOW_2); 

                    				current = grammarAccess.getValueTypeAccess().getTEAMEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getValueTypeAccess().getTEAMEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValueType"


    // $ANTLR start "ruleResult"
    // InternalGloe.g:1625:1: ruleResult returns [Enumerator current=null] : ( (enumLiteral_0= 'victory' ) | (enumLiteral_1= 'defeat' ) ) ;
    public final Enumerator ruleResult() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGloe.g:1631:2: ( ( (enumLiteral_0= 'victory' ) | (enumLiteral_1= 'defeat' ) ) )
            // InternalGloe.g:1632:2: ( (enumLiteral_0= 'victory' ) | (enumLiteral_1= 'defeat' ) )
            {
            // InternalGloe.g:1632:2: ( (enumLiteral_0= 'victory' ) | (enumLiteral_1= 'defeat' ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==35) ) {
                alt19=1;
            }
            else if ( (LA19_0==36) ) {
                alt19=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalGloe.g:1633:3: (enumLiteral_0= 'victory' )
                    {
                    // InternalGloe.g:1633:3: (enumLiteral_0= 'victory' )
                    // InternalGloe.g:1634:4: enumLiteral_0= 'victory'
                    {
                    enumLiteral_0=(Token)match(input,35,FOLLOW_2); 

                    				current = grammarAccess.getResultAccess().getWINEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getResultAccess().getWINEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1641:3: (enumLiteral_1= 'defeat' )
                    {
                    // InternalGloe.g:1641:3: (enumLiteral_1= 'defeat' )
                    // InternalGloe.g:1642:4: enumLiteral_1= 'defeat'
                    {
                    enumLiteral_1=(Token)match(input,36,FOLLOW_2); 

                    				current = grammarAccess.getResultAccess().getLOSEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getResultAccess().getLOSEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleResult"


    // $ANTLR start "rulePluralResult"
    // InternalGloe.g:1652:1: rulePluralResult returns [Enumerator current=null] : ( (enumLiteral_0= 'victories' ) | (enumLiteral_1= 'defeats' ) ) ;
    public final Enumerator rulePluralResult() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGloe.g:1658:2: ( ( (enumLiteral_0= 'victories' ) | (enumLiteral_1= 'defeats' ) ) )
            // InternalGloe.g:1659:2: ( (enumLiteral_0= 'victories' ) | (enumLiteral_1= 'defeats' ) )
            {
            // InternalGloe.g:1659:2: ( (enumLiteral_0= 'victories' ) | (enumLiteral_1= 'defeats' ) )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==37) ) {
                alt20=1;
            }
            else if ( (LA20_0==38) ) {
                alt20=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalGloe.g:1660:3: (enumLiteral_0= 'victories' )
                    {
                    // InternalGloe.g:1660:3: (enumLiteral_0= 'victories' )
                    // InternalGloe.g:1661:4: enumLiteral_0= 'victories'
                    {
                    enumLiteral_0=(Token)match(input,37,FOLLOW_2); 

                    				current = grammarAccess.getPluralResultAccess().getWINEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getPluralResultAccess().getWINEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1668:3: (enumLiteral_1= 'defeats' )
                    {
                    // InternalGloe.g:1668:3: (enumLiteral_1= 'defeats' )
                    // InternalGloe.g:1669:4: enumLiteral_1= 'defeats'
                    {
                    enumLiteral_1=(Token)match(input,38,FOLLOW_2); 

                    				current = grammarAccess.getPluralResultAccess().getLOSEEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getPluralResultAccess().getLOSEEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePluralResult"


    // $ANTLR start "ruleTeam"
    // InternalGloe.g:1679:1: ruleTeam returns [Enumerator current=null] : ( (enumLiteral_0= 'blue' ) | (enumLiteral_1= 'red' ) ) ;
    public final Enumerator ruleTeam() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGloe.g:1685:2: ( ( (enumLiteral_0= 'blue' ) | (enumLiteral_1= 'red' ) ) )
            // InternalGloe.g:1686:2: ( (enumLiteral_0= 'blue' ) | (enumLiteral_1= 'red' ) )
            {
            // InternalGloe.g:1686:2: ( (enumLiteral_0= 'blue' ) | (enumLiteral_1= 'red' ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==39) ) {
                alt21=1;
            }
            else if ( (LA21_0==40) ) {
                alt21=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalGloe.g:1687:3: (enumLiteral_0= 'blue' )
                    {
                    // InternalGloe.g:1687:3: (enumLiteral_0= 'blue' )
                    // InternalGloe.g:1688:4: enumLiteral_0= 'blue'
                    {
                    enumLiteral_0=(Token)match(input,39,FOLLOW_2); 

                    				current = grammarAccess.getTeamAccess().getBLUEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTeamAccess().getBLUEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1695:3: (enumLiteral_1= 'red' )
                    {
                    // InternalGloe.g:1695:3: (enumLiteral_1= 'red' )
                    // InternalGloe.g:1696:4: enumLiteral_1= 'red'
                    {
                    enumLiteral_1=(Token)match(input,40,FOLLOW_2); 

                    				current = grammarAccess.getTeamAccess().getREDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTeamAccess().getREDEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTeam"


    // $ANTLR start "ruleLane"
    // InternalGloe.g:1706:1: ruleLane returns [Enumerator current=null] : ( (enumLiteral_0= 'bot' ) | (enumLiteral_1= 'top' ) | (enumLiteral_2= 'mid' ) | (enumLiteral_3= 'jungle' ) ) ;
    public final Enumerator ruleLane() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalGloe.g:1712:2: ( ( (enumLiteral_0= 'bot' ) | (enumLiteral_1= 'top' ) | (enumLiteral_2= 'mid' ) | (enumLiteral_3= 'jungle' ) ) )
            // InternalGloe.g:1713:2: ( (enumLiteral_0= 'bot' ) | (enumLiteral_1= 'top' ) | (enumLiteral_2= 'mid' ) | (enumLiteral_3= 'jungle' ) )
            {
            // InternalGloe.g:1713:2: ( (enumLiteral_0= 'bot' ) | (enumLiteral_1= 'top' ) | (enumLiteral_2= 'mid' ) | (enumLiteral_3= 'jungle' ) )
            int alt22=4;
            switch ( input.LA(1) ) {
            case 41:
                {
                alt22=1;
                }
                break;
            case 42:
                {
                alt22=2;
                }
                break;
            case 43:
                {
                alt22=3;
                }
                break;
            case 44:
                {
                alt22=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // InternalGloe.g:1714:3: (enumLiteral_0= 'bot' )
                    {
                    // InternalGloe.g:1714:3: (enumLiteral_0= 'bot' )
                    // InternalGloe.g:1715:4: enumLiteral_0= 'bot'
                    {
                    enumLiteral_0=(Token)match(input,41,FOLLOW_2); 

                    				current = grammarAccess.getLaneAccess().getBOTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getLaneAccess().getBOTEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1722:3: (enumLiteral_1= 'top' )
                    {
                    // InternalGloe.g:1722:3: (enumLiteral_1= 'top' )
                    // InternalGloe.g:1723:4: enumLiteral_1= 'top'
                    {
                    enumLiteral_1=(Token)match(input,42,FOLLOW_2); 

                    				current = grammarAccess.getLaneAccess().getTOPEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getLaneAccess().getTOPEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1730:3: (enumLiteral_2= 'mid' )
                    {
                    // InternalGloe.g:1730:3: (enumLiteral_2= 'mid' )
                    // InternalGloe.g:1731:4: enumLiteral_2= 'mid'
                    {
                    enumLiteral_2=(Token)match(input,43,FOLLOW_2); 

                    				current = grammarAccess.getLaneAccess().getMIDEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getLaneAccess().getMIDEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1738:3: (enumLiteral_3= 'jungle' )
                    {
                    // InternalGloe.g:1738:3: (enumLiteral_3= 'jungle' )
                    // InternalGloe.g:1739:4: enumLiteral_3= 'jungle'
                    {
                    enumLiteral_3=(Token)match(input,44,FOLLOW_2); 

                    				current = grammarAccess.getLaneAccess().getJUNGLEEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getLaneAccess().getJUNGLEEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLane"


    // $ANTLR start "ruleStat"
    // InternalGloe.g:1749:1: ruleStat returns [Enumerator current=null] : ( (enumLiteral_0= 'kills' ) | (enumLiteral_1= 'deaths' ) | (enumLiteral_2= 'assists' ) | (enumLiteral_3= 'double kills' ) | (enumLiteral_4= 'triple kills' ) | (enumLiteral_5= 'quadra kills' ) | (enumLiteral_6= 'penta kills' ) | (enumLiteral_7= 'damage dealt' ) | (enumLiteral_8= 'magic damage dealt' ) | (enumLiteral_9= 'physical damage dealt' ) | (enumLiteral_10= 'true damage dealt' ) | (enumLiteral_11= 'damage dealt to champions' ) | (enumLiteral_12= 'magic damage dealt to champions' ) | (enumLiteral_13= 'physical damage dealt to champions' ) | (enumLiteral_14= 'true damage dealt to champions' ) | (enumLiteral_15= 'heal' ) | (enumLiteral_16= 'damage self mitigated' ) | (enumLiteral_17= 'damage dealt to objectives' ) | (enumLiteral_18= 'damage dealt to turrets' ) | (enumLiteral_19= 'time cc' ) | (enumLiteral_20= 'damage taken' ) | (enumLiteral_21= 'magical damage taken' ) | (enumLiteral_22= 'physical damage taken' ) | (enumLiteral_23= 'true damage taken' ) | (enumLiteral_24= 'gold earned' ) | (enumLiteral_25= 'gold spent' ) | (enumLiteral_26= 'turret kills' ) | (enumLiteral_27= 'inhibitor kills' ) | (enumLiteral_28= 'minions killed' ) | (enumLiteral_29= 'neutral minions killed' ) | (enumLiteral_30= 'neutral minions killed team jungle' ) | (enumLiteral_31= 'neutral minions killed enemy jungle' ) | (enumLiteral_32= 'time crowd control dealt' ) | (enumLiteral_33= 'vision wards bought in game' ) | (enumLiteral_34= 'wards placed' ) | (enumLiteral_35= 'wards killed' ) | (enumLiteral_36= 'first blood kill' ) | (enumLiteral_37= 'first blood assist' ) | (enumLiteral_38= 'first tower kill' ) | (enumLiteral_39= 'first tower assist' ) | (enumLiteral_40= 'first inhibitor kill' ) | (enumLiteral_41= 'first inhibitor assist' ) ) ;
    public final Enumerator ruleStat() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;
        Token enumLiteral_12=null;
        Token enumLiteral_13=null;
        Token enumLiteral_14=null;
        Token enumLiteral_15=null;
        Token enumLiteral_16=null;
        Token enumLiteral_17=null;
        Token enumLiteral_18=null;
        Token enumLiteral_19=null;
        Token enumLiteral_20=null;
        Token enumLiteral_21=null;
        Token enumLiteral_22=null;
        Token enumLiteral_23=null;
        Token enumLiteral_24=null;
        Token enumLiteral_25=null;
        Token enumLiteral_26=null;
        Token enumLiteral_27=null;
        Token enumLiteral_28=null;
        Token enumLiteral_29=null;
        Token enumLiteral_30=null;
        Token enumLiteral_31=null;
        Token enumLiteral_32=null;
        Token enumLiteral_33=null;
        Token enumLiteral_34=null;
        Token enumLiteral_35=null;
        Token enumLiteral_36=null;
        Token enumLiteral_37=null;
        Token enumLiteral_38=null;
        Token enumLiteral_39=null;
        Token enumLiteral_40=null;
        Token enumLiteral_41=null;


        	enterRule();

        try {
            // InternalGloe.g:1755:2: ( ( (enumLiteral_0= 'kills' ) | (enumLiteral_1= 'deaths' ) | (enumLiteral_2= 'assists' ) | (enumLiteral_3= 'double kills' ) | (enumLiteral_4= 'triple kills' ) | (enumLiteral_5= 'quadra kills' ) | (enumLiteral_6= 'penta kills' ) | (enumLiteral_7= 'damage dealt' ) | (enumLiteral_8= 'magic damage dealt' ) | (enumLiteral_9= 'physical damage dealt' ) | (enumLiteral_10= 'true damage dealt' ) | (enumLiteral_11= 'damage dealt to champions' ) | (enumLiteral_12= 'magic damage dealt to champions' ) | (enumLiteral_13= 'physical damage dealt to champions' ) | (enumLiteral_14= 'true damage dealt to champions' ) | (enumLiteral_15= 'heal' ) | (enumLiteral_16= 'damage self mitigated' ) | (enumLiteral_17= 'damage dealt to objectives' ) | (enumLiteral_18= 'damage dealt to turrets' ) | (enumLiteral_19= 'time cc' ) | (enumLiteral_20= 'damage taken' ) | (enumLiteral_21= 'magical damage taken' ) | (enumLiteral_22= 'physical damage taken' ) | (enumLiteral_23= 'true damage taken' ) | (enumLiteral_24= 'gold earned' ) | (enumLiteral_25= 'gold spent' ) | (enumLiteral_26= 'turret kills' ) | (enumLiteral_27= 'inhibitor kills' ) | (enumLiteral_28= 'minions killed' ) | (enumLiteral_29= 'neutral minions killed' ) | (enumLiteral_30= 'neutral minions killed team jungle' ) | (enumLiteral_31= 'neutral minions killed enemy jungle' ) | (enumLiteral_32= 'time crowd control dealt' ) | (enumLiteral_33= 'vision wards bought in game' ) | (enumLiteral_34= 'wards placed' ) | (enumLiteral_35= 'wards killed' ) | (enumLiteral_36= 'first blood kill' ) | (enumLiteral_37= 'first blood assist' ) | (enumLiteral_38= 'first tower kill' ) | (enumLiteral_39= 'first tower assist' ) | (enumLiteral_40= 'first inhibitor kill' ) | (enumLiteral_41= 'first inhibitor assist' ) ) )
            // InternalGloe.g:1756:2: ( (enumLiteral_0= 'kills' ) | (enumLiteral_1= 'deaths' ) | (enumLiteral_2= 'assists' ) | (enumLiteral_3= 'double kills' ) | (enumLiteral_4= 'triple kills' ) | (enumLiteral_5= 'quadra kills' ) | (enumLiteral_6= 'penta kills' ) | (enumLiteral_7= 'damage dealt' ) | (enumLiteral_8= 'magic damage dealt' ) | (enumLiteral_9= 'physical damage dealt' ) | (enumLiteral_10= 'true damage dealt' ) | (enumLiteral_11= 'damage dealt to champions' ) | (enumLiteral_12= 'magic damage dealt to champions' ) | (enumLiteral_13= 'physical damage dealt to champions' ) | (enumLiteral_14= 'true damage dealt to champions' ) | (enumLiteral_15= 'heal' ) | (enumLiteral_16= 'damage self mitigated' ) | (enumLiteral_17= 'damage dealt to objectives' ) | (enumLiteral_18= 'damage dealt to turrets' ) | (enumLiteral_19= 'time cc' ) | (enumLiteral_20= 'damage taken' ) | (enumLiteral_21= 'magical damage taken' ) | (enumLiteral_22= 'physical damage taken' ) | (enumLiteral_23= 'true damage taken' ) | (enumLiteral_24= 'gold earned' ) | (enumLiteral_25= 'gold spent' ) | (enumLiteral_26= 'turret kills' ) | (enumLiteral_27= 'inhibitor kills' ) | (enumLiteral_28= 'minions killed' ) | (enumLiteral_29= 'neutral minions killed' ) | (enumLiteral_30= 'neutral minions killed team jungle' ) | (enumLiteral_31= 'neutral minions killed enemy jungle' ) | (enumLiteral_32= 'time crowd control dealt' ) | (enumLiteral_33= 'vision wards bought in game' ) | (enumLiteral_34= 'wards placed' ) | (enumLiteral_35= 'wards killed' ) | (enumLiteral_36= 'first blood kill' ) | (enumLiteral_37= 'first blood assist' ) | (enumLiteral_38= 'first tower kill' ) | (enumLiteral_39= 'first tower assist' ) | (enumLiteral_40= 'first inhibitor kill' ) | (enumLiteral_41= 'first inhibitor assist' ) )
            {
            // InternalGloe.g:1756:2: ( (enumLiteral_0= 'kills' ) | (enumLiteral_1= 'deaths' ) | (enumLiteral_2= 'assists' ) | (enumLiteral_3= 'double kills' ) | (enumLiteral_4= 'triple kills' ) | (enumLiteral_5= 'quadra kills' ) | (enumLiteral_6= 'penta kills' ) | (enumLiteral_7= 'damage dealt' ) | (enumLiteral_8= 'magic damage dealt' ) | (enumLiteral_9= 'physical damage dealt' ) | (enumLiteral_10= 'true damage dealt' ) | (enumLiteral_11= 'damage dealt to champions' ) | (enumLiteral_12= 'magic damage dealt to champions' ) | (enumLiteral_13= 'physical damage dealt to champions' ) | (enumLiteral_14= 'true damage dealt to champions' ) | (enumLiteral_15= 'heal' ) | (enumLiteral_16= 'damage self mitigated' ) | (enumLiteral_17= 'damage dealt to objectives' ) | (enumLiteral_18= 'damage dealt to turrets' ) | (enumLiteral_19= 'time cc' ) | (enumLiteral_20= 'damage taken' ) | (enumLiteral_21= 'magical damage taken' ) | (enumLiteral_22= 'physical damage taken' ) | (enumLiteral_23= 'true damage taken' ) | (enumLiteral_24= 'gold earned' ) | (enumLiteral_25= 'gold spent' ) | (enumLiteral_26= 'turret kills' ) | (enumLiteral_27= 'inhibitor kills' ) | (enumLiteral_28= 'minions killed' ) | (enumLiteral_29= 'neutral minions killed' ) | (enumLiteral_30= 'neutral minions killed team jungle' ) | (enumLiteral_31= 'neutral minions killed enemy jungle' ) | (enumLiteral_32= 'time crowd control dealt' ) | (enumLiteral_33= 'vision wards bought in game' ) | (enumLiteral_34= 'wards placed' ) | (enumLiteral_35= 'wards killed' ) | (enumLiteral_36= 'first blood kill' ) | (enumLiteral_37= 'first blood assist' ) | (enumLiteral_38= 'first tower kill' ) | (enumLiteral_39= 'first tower assist' ) | (enumLiteral_40= 'first inhibitor kill' ) | (enumLiteral_41= 'first inhibitor assist' ) )
            int alt23=42;
            switch ( input.LA(1) ) {
            case 45:
                {
                alt23=1;
                }
                break;
            case 46:
                {
                alt23=2;
                }
                break;
            case 47:
                {
                alt23=3;
                }
                break;
            case 48:
                {
                alt23=4;
                }
                break;
            case 49:
                {
                alt23=5;
                }
                break;
            case 50:
                {
                alt23=6;
                }
                break;
            case 51:
                {
                alt23=7;
                }
                break;
            case 52:
                {
                alt23=8;
                }
                break;
            case 53:
                {
                alt23=9;
                }
                break;
            case 54:
                {
                alt23=10;
                }
                break;
            case 55:
                {
                alt23=11;
                }
                break;
            case 56:
                {
                alt23=12;
                }
                break;
            case 57:
                {
                alt23=13;
                }
                break;
            case 58:
                {
                alt23=14;
                }
                break;
            case 59:
                {
                alt23=15;
                }
                break;
            case 60:
                {
                alt23=16;
                }
                break;
            case 61:
                {
                alt23=17;
                }
                break;
            case 62:
                {
                alt23=18;
                }
                break;
            case 63:
                {
                alt23=19;
                }
                break;
            case 64:
                {
                alt23=20;
                }
                break;
            case 65:
                {
                alt23=21;
                }
                break;
            case 66:
                {
                alt23=22;
                }
                break;
            case 67:
                {
                alt23=23;
                }
                break;
            case 68:
                {
                alt23=24;
                }
                break;
            case 69:
                {
                alt23=25;
                }
                break;
            case 70:
                {
                alt23=26;
                }
                break;
            case 71:
                {
                alt23=27;
                }
                break;
            case 72:
                {
                alt23=28;
                }
                break;
            case 73:
                {
                alt23=29;
                }
                break;
            case 74:
                {
                alt23=30;
                }
                break;
            case 75:
                {
                alt23=31;
                }
                break;
            case 76:
                {
                alt23=32;
                }
                break;
            case 77:
                {
                alt23=33;
                }
                break;
            case 78:
                {
                alt23=34;
                }
                break;
            case 79:
                {
                alt23=35;
                }
                break;
            case 80:
                {
                alt23=36;
                }
                break;
            case 81:
                {
                alt23=37;
                }
                break;
            case 82:
                {
                alt23=38;
                }
                break;
            case 83:
                {
                alt23=39;
                }
                break;
            case 84:
                {
                alt23=40;
                }
                break;
            case 85:
                {
                alt23=41;
                }
                break;
            case 86:
                {
                alt23=42;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }

            switch (alt23) {
                case 1 :
                    // InternalGloe.g:1757:3: (enumLiteral_0= 'kills' )
                    {
                    // InternalGloe.g:1757:3: (enumLiteral_0= 'kills' )
                    // InternalGloe.g:1758:4: enumLiteral_0= 'kills'
                    {
                    enumLiteral_0=(Token)match(input,45,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getKILLSEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getStatAccess().getKILLSEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1765:3: (enumLiteral_1= 'deaths' )
                    {
                    // InternalGloe.g:1765:3: (enumLiteral_1= 'deaths' )
                    // InternalGloe.g:1766:4: enumLiteral_1= 'deaths'
                    {
                    enumLiteral_1=(Token)match(input,46,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getDEATHSEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getStatAccess().getDEATHSEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1773:3: (enumLiteral_2= 'assists' )
                    {
                    // InternalGloe.g:1773:3: (enumLiteral_2= 'assists' )
                    // InternalGloe.g:1774:4: enumLiteral_2= 'assists'
                    {
                    enumLiteral_2=(Token)match(input,47,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getASSISTSEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getStatAccess().getASSISTSEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1781:3: (enumLiteral_3= 'double kills' )
                    {
                    // InternalGloe.g:1781:3: (enumLiteral_3= 'double kills' )
                    // InternalGloe.g:1782:4: enumLiteral_3= 'double kills'
                    {
                    enumLiteral_3=(Token)match(input,48,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getDOUBLE_KILLSEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getStatAccess().getDOUBLE_KILLSEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalGloe.g:1789:3: (enumLiteral_4= 'triple kills' )
                    {
                    // InternalGloe.g:1789:3: (enumLiteral_4= 'triple kills' )
                    // InternalGloe.g:1790:4: enumLiteral_4= 'triple kills'
                    {
                    enumLiteral_4=(Token)match(input,49,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTRIPLE_KILLSEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getStatAccess().getTRIPLE_KILLSEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalGloe.g:1797:3: (enumLiteral_5= 'quadra kills' )
                    {
                    // InternalGloe.g:1797:3: (enumLiteral_5= 'quadra kills' )
                    // InternalGloe.g:1798:4: enumLiteral_5= 'quadra kills'
                    {
                    enumLiteral_5=(Token)match(input,50,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getQUADRA_KILLSEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getStatAccess().getQUADRA_KILLSEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalGloe.g:1805:3: (enumLiteral_6= 'penta kills' )
                    {
                    // InternalGloe.g:1805:3: (enumLiteral_6= 'penta kills' )
                    // InternalGloe.g:1806:4: enumLiteral_6= 'penta kills'
                    {
                    enumLiteral_6=(Token)match(input,51,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getPENTA_KILLSEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getStatAccess().getPENTA_KILLSEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalGloe.g:1813:3: (enumLiteral_7= 'damage dealt' )
                    {
                    // InternalGloe.g:1813:3: (enumLiteral_7= 'damage dealt' )
                    // InternalGloe.g:1814:4: enumLiteral_7= 'damage dealt'
                    {
                    enumLiteral_7=(Token)match(input,52,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALTEnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_7, grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALTEnumLiteralDeclaration_7());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalGloe.g:1821:3: (enumLiteral_8= 'magic damage dealt' )
                    {
                    // InternalGloe.g:1821:3: (enumLiteral_8= 'magic damage dealt' )
                    // InternalGloe.g:1822:4: enumLiteral_8= 'magic damage dealt'
                    {
                    enumLiteral_8=(Token)match(input,53,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALTEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_8, grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALTEnumLiteralDeclaration_8());
                    			

                    }


                    }
                    break;
                case 10 :
                    // InternalGloe.g:1829:3: (enumLiteral_9= 'physical damage dealt' )
                    {
                    // InternalGloe.g:1829:3: (enumLiteral_9= 'physical damage dealt' )
                    // InternalGloe.g:1830:4: enumLiteral_9= 'physical damage dealt'
                    {
                    enumLiteral_9=(Token)match(input,54,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALTEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_9, grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALTEnumLiteralDeclaration_9());
                    			

                    }


                    }
                    break;
                case 11 :
                    // InternalGloe.g:1837:3: (enumLiteral_10= 'true damage dealt' )
                    {
                    // InternalGloe.g:1837:3: (enumLiteral_10= 'true damage dealt' )
                    // InternalGloe.g:1838:4: enumLiteral_10= 'true damage dealt'
                    {
                    enumLiteral_10=(Token)match(input,55,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALTEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_10, grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALTEnumLiteralDeclaration_10());
                    			

                    }


                    }
                    break;
                case 12 :
                    // InternalGloe.g:1845:3: (enumLiteral_11= 'damage dealt to champions' )
                    {
                    // InternalGloe.g:1845:3: (enumLiteral_11= 'damage dealt to champions' )
                    // InternalGloe.g:1846:4: enumLiteral_11= 'damage dealt to champions'
                    {
                    enumLiteral_11=(Token)match(input,56,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_11, grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_11());
                    			

                    }


                    }
                    break;
                case 13 :
                    // InternalGloe.g:1853:3: (enumLiteral_12= 'magic damage dealt to champions' )
                    {
                    // InternalGloe.g:1853:3: (enumLiteral_12= 'magic damage dealt to champions' )
                    // InternalGloe.g:1854:4: enumLiteral_12= 'magic damage dealt to champions'
                    {
                    enumLiteral_12=(Token)match(input,57,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_12().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_12, grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_12());
                    			

                    }


                    }
                    break;
                case 14 :
                    // InternalGloe.g:1861:3: (enumLiteral_13= 'physical damage dealt to champions' )
                    {
                    // InternalGloe.g:1861:3: (enumLiteral_13= 'physical damage dealt to champions' )
                    // InternalGloe.g:1862:4: enumLiteral_13= 'physical damage dealt to champions'
                    {
                    enumLiteral_13=(Token)match(input,58,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_13().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_13, grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_13());
                    			

                    }


                    }
                    break;
                case 15 :
                    // InternalGloe.g:1869:3: (enumLiteral_14= 'true damage dealt to champions' )
                    {
                    // InternalGloe.g:1869:3: (enumLiteral_14= 'true damage dealt to champions' )
                    // InternalGloe.g:1870:4: enumLiteral_14= 'true damage dealt to champions'
                    {
                    enumLiteral_14=(Token)match(input,59,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_14().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_14, grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_14());
                    			

                    }


                    }
                    break;
                case 16 :
                    // InternalGloe.g:1877:3: (enumLiteral_15= 'heal' )
                    {
                    // InternalGloe.g:1877:3: (enumLiteral_15= 'heal' )
                    // InternalGloe.g:1878:4: enumLiteral_15= 'heal'
                    {
                    enumLiteral_15=(Token)match(input,60,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTOTAL_HEALEnumLiteralDeclaration_15().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_15, grammarAccess.getStatAccess().getTOTAL_HEALEnumLiteralDeclaration_15());
                    			

                    }


                    }
                    break;
                case 17 :
                    // InternalGloe.g:1885:3: (enumLiteral_16= 'damage self mitigated' )
                    {
                    // InternalGloe.g:1885:3: (enumLiteral_16= 'damage self mitigated' )
                    // InternalGloe.g:1886:4: enumLiteral_16= 'damage self mitigated'
                    {
                    enumLiteral_16=(Token)match(input,61,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getDAMAGE_SELF_MITIGATEDEnumLiteralDeclaration_16().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_16, grammarAccess.getStatAccess().getDAMAGE_SELF_MITIGATEDEnumLiteralDeclaration_16());
                    			

                    }


                    }
                    break;
                case 18 :
                    // InternalGloe.g:1893:3: (enumLiteral_17= 'damage dealt to objectives' )
                    {
                    // InternalGloe.g:1893:3: (enumLiteral_17= 'damage dealt to objectives' )
                    // InternalGloe.g:1894:4: enumLiteral_17= 'damage dealt to objectives'
                    {
                    enumLiteral_17=(Token)match(input,62,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_OBJECTIVESEnumLiteralDeclaration_17().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_17, grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_OBJECTIVESEnumLiteralDeclaration_17());
                    			

                    }


                    }
                    break;
                case 19 :
                    // InternalGloe.g:1901:3: (enumLiteral_18= 'damage dealt to turrets' )
                    {
                    // InternalGloe.g:1901:3: (enumLiteral_18= 'damage dealt to turrets' )
                    // InternalGloe.g:1902:4: enumLiteral_18= 'damage dealt to turrets'
                    {
                    enumLiteral_18=(Token)match(input,63,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_TURRETSEnumLiteralDeclaration_18().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_18, grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_TURRETSEnumLiteralDeclaration_18());
                    			

                    }


                    }
                    break;
                case 20 :
                    // InternalGloe.g:1909:3: (enumLiteral_19= 'time cc' )
                    {
                    // InternalGloe.g:1909:3: (enumLiteral_19= 'time cc' )
                    // InternalGloe.g:1910:4: enumLiteral_19= 'time cc'
                    {
                    enumLiteral_19=(Token)match(input,64,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTIME_C_CING_OTHERSEnumLiteralDeclaration_19().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_19, grammarAccess.getStatAccess().getTIME_C_CING_OTHERSEnumLiteralDeclaration_19());
                    			

                    }


                    }
                    break;
                case 21 :
                    // InternalGloe.g:1917:3: (enumLiteral_20= 'damage taken' )
                    {
                    // InternalGloe.g:1917:3: (enumLiteral_20= 'damage taken' )
                    // InternalGloe.g:1918:4: enumLiteral_20= 'damage taken'
                    {
                    enumLiteral_20=(Token)match(input,65,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTOTAL_DAMAGE_TAKENEnumLiteralDeclaration_20().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_20, grammarAccess.getStatAccess().getTOTAL_DAMAGE_TAKENEnumLiteralDeclaration_20());
                    			

                    }


                    }
                    break;
                case 22 :
                    // InternalGloe.g:1925:3: (enumLiteral_21= 'magical damage taken' )
                    {
                    // InternalGloe.g:1925:3: (enumLiteral_21= 'magical damage taken' )
                    // InternalGloe.g:1926:4: enumLiteral_21= 'magical damage taken'
                    {
                    enumLiteral_21=(Token)match(input,66,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getMAGICAL_DAMAGE_TAKENEnumLiteralDeclaration_21().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_21, grammarAccess.getStatAccess().getMAGICAL_DAMAGE_TAKENEnumLiteralDeclaration_21());
                    			

                    }


                    }
                    break;
                case 23 :
                    // InternalGloe.g:1933:3: (enumLiteral_22= 'physical damage taken' )
                    {
                    // InternalGloe.g:1933:3: (enumLiteral_22= 'physical damage taken' )
                    // InternalGloe.g:1934:4: enumLiteral_22= 'physical damage taken'
                    {
                    enumLiteral_22=(Token)match(input,67,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_TAKENEnumLiteralDeclaration_22().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_22, grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_TAKENEnumLiteralDeclaration_22());
                    			

                    }


                    }
                    break;
                case 24 :
                    // InternalGloe.g:1941:3: (enumLiteral_23= 'true damage taken' )
                    {
                    // InternalGloe.g:1941:3: (enumLiteral_23= 'true damage taken' )
                    // InternalGloe.g:1942:4: enumLiteral_23= 'true damage taken'
                    {
                    enumLiteral_23=(Token)match(input,68,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTRUE_DAMAGE_TAKENEnumLiteralDeclaration_23().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_23, grammarAccess.getStatAccess().getTRUE_DAMAGE_TAKENEnumLiteralDeclaration_23());
                    			

                    }


                    }
                    break;
                case 25 :
                    // InternalGloe.g:1949:3: (enumLiteral_24= 'gold earned' )
                    {
                    // InternalGloe.g:1949:3: (enumLiteral_24= 'gold earned' )
                    // InternalGloe.g:1950:4: enumLiteral_24= 'gold earned'
                    {
                    enumLiteral_24=(Token)match(input,69,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getGOLD_EARNEDEnumLiteralDeclaration_24().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_24, grammarAccess.getStatAccess().getGOLD_EARNEDEnumLiteralDeclaration_24());
                    			

                    }


                    }
                    break;
                case 26 :
                    // InternalGloe.g:1957:3: (enumLiteral_25= 'gold spent' )
                    {
                    // InternalGloe.g:1957:3: (enumLiteral_25= 'gold spent' )
                    // InternalGloe.g:1958:4: enumLiteral_25= 'gold spent'
                    {
                    enumLiteral_25=(Token)match(input,70,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getGOLD_SPENTEnumLiteralDeclaration_25().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_25, grammarAccess.getStatAccess().getGOLD_SPENTEnumLiteralDeclaration_25());
                    			

                    }


                    }
                    break;
                case 27 :
                    // InternalGloe.g:1965:3: (enumLiteral_26= 'turret kills' )
                    {
                    // InternalGloe.g:1965:3: (enumLiteral_26= 'turret kills' )
                    // InternalGloe.g:1966:4: enumLiteral_26= 'turret kills'
                    {
                    enumLiteral_26=(Token)match(input,71,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTURRET_KILLSEnumLiteralDeclaration_26().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_26, grammarAccess.getStatAccess().getTURRET_KILLSEnumLiteralDeclaration_26());
                    			

                    }


                    }
                    break;
                case 28 :
                    // InternalGloe.g:1973:3: (enumLiteral_27= 'inhibitor kills' )
                    {
                    // InternalGloe.g:1973:3: (enumLiteral_27= 'inhibitor kills' )
                    // InternalGloe.g:1974:4: enumLiteral_27= 'inhibitor kills'
                    {
                    enumLiteral_27=(Token)match(input,72,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getINHIBITOR_KILLSEnumLiteralDeclaration_27().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_27, grammarAccess.getStatAccess().getINHIBITOR_KILLSEnumLiteralDeclaration_27());
                    			

                    }


                    }
                    break;
                case 29 :
                    // InternalGloe.g:1981:3: (enumLiteral_28= 'minions killed' )
                    {
                    // InternalGloe.g:1981:3: (enumLiteral_28= 'minions killed' )
                    // InternalGloe.g:1982:4: enumLiteral_28= 'minions killed'
                    {
                    enumLiteral_28=(Token)match(input,73,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTOTAL_MINIONS_KILLEDEnumLiteralDeclaration_28().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_28, grammarAccess.getStatAccess().getTOTAL_MINIONS_KILLEDEnumLiteralDeclaration_28());
                    			

                    }


                    }
                    break;
                case 30 :
                    // InternalGloe.g:1989:3: (enumLiteral_29= 'neutral minions killed' )
                    {
                    // InternalGloe.g:1989:3: (enumLiteral_29= 'neutral minions killed' )
                    // InternalGloe.g:1990:4: enumLiteral_29= 'neutral minions killed'
                    {
                    enumLiteral_29=(Token)match(input,74,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLEDEnumLiteralDeclaration_29().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_29, grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLEDEnumLiteralDeclaration_29());
                    			

                    }


                    }
                    break;
                case 31 :
                    // InternalGloe.g:1997:3: (enumLiteral_30= 'neutral minions killed team jungle' )
                    {
                    // InternalGloe.g:1997:3: (enumLiteral_30= 'neutral minions killed team jungle' )
                    // InternalGloe.g:1998:4: enumLiteral_30= 'neutral minions killed team jungle'
                    {
                    enumLiteral_30=(Token)match(input,75,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_TEAM_JUNGLEEnumLiteralDeclaration_30().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_30, grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_TEAM_JUNGLEEnumLiteralDeclaration_30());
                    			

                    }


                    }
                    break;
                case 32 :
                    // InternalGloe.g:2005:3: (enumLiteral_31= 'neutral minions killed enemy jungle' )
                    {
                    // InternalGloe.g:2005:3: (enumLiteral_31= 'neutral minions killed enemy jungle' )
                    // InternalGloe.g:2006:4: enumLiteral_31= 'neutral minions killed enemy jungle'
                    {
                    enumLiteral_31=(Token)match(input,76,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_ENEMY_JUNGLEEnumLiteralDeclaration_31().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_31, grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_ENEMY_JUNGLEEnumLiteralDeclaration_31());
                    			

                    }


                    }
                    break;
                case 33 :
                    // InternalGloe.g:2013:3: (enumLiteral_32= 'time crowd control dealt' )
                    {
                    // InternalGloe.g:2013:3: (enumLiteral_32= 'time crowd control dealt' )
                    // InternalGloe.g:2014:4: enumLiteral_32= 'time crowd control dealt'
                    {
                    enumLiteral_32=(Token)match(input,77,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getTOTAL_TIME_CROWD_CONTROL_DEALTEnumLiteralDeclaration_32().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_32, grammarAccess.getStatAccess().getTOTAL_TIME_CROWD_CONTROL_DEALTEnumLiteralDeclaration_32());
                    			

                    }


                    }
                    break;
                case 34 :
                    // InternalGloe.g:2021:3: (enumLiteral_33= 'vision wards bought in game' )
                    {
                    // InternalGloe.g:2021:3: (enumLiteral_33= 'vision wards bought in game' )
                    // InternalGloe.g:2022:4: enumLiteral_33= 'vision wards bought in game'
                    {
                    enumLiteral_33=(Token)match(input,78,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getVISION_WARDS_BOUGHT_IN_GAMEEnumLiteralDeclaration_33().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_33, grammarAccess.getStatAccess().getVISION_WARDS_BOUGHT_IN_GAMEEnumLiteralDeclaration_33());
                    			

                    }


                    }
                    break;
                case 35 :
                    // InternalGloe.g:2029:3: (enumLiteral_34= 'wards placed' )
                    {
                    // InternalGloe.g:2029:3: (enumLiteral_34= 'wards placed' )
                    // InternalGloe.g:2030:4: enumLiteral_34= 'wards placed'
                    {
                    enumLiteral_34=(Token)match(input,79,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getWARDS_PLACEDEnumLiteralDeclaration_34().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_34, grammarAccess.getStatAccess().getWARDS_PLACEDEnumLiteralDeclaration_34());
                    			

                    }


                    }
                    break;
                case 36 :
                    // InternalGloe.g:2037:3: (enumLiteral_35= 'wards killed' )
                    {
                    // InternalGloe.g:2037:3: (enumLiteral_35= 'wards killed' )
                    // InternalGloe.g:2038:4: enumLiteral_35= 'wards killed'
                    {
                    enumLiteral_35=(Token)match(input,80,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getWARDS_KILLEDEnumLiteralDeclaration_35().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_35, grammarAccess.getStatAccess().getWARDS_KILLEDEnumLiteralDeclaration_35());
                    			

                    }


                    }
                    break;
                case 37 :
                    // InternalGloe.g:2045:3: (enumLiteral_36= 'first blood kill' )
                    {
                    // InternalGloe.g:2045:3: (enumLiteral_36= 'first blood kill' )
                    // InternalGloe.g:2046:4: enumLiteral_36= 'first blood kill'
                    {
                    enumLiteral_36=(Token)match(input,81,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getFIRST_BLOOD_KILLEnumLiteralDeclaration_36().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_36, grammarAccess.getStatAccess().getFIRST_BLOOD_KILLEnumLiteralDeclaration_36());
                    			

                    }


                    }
                    break;
                case 38 :
                    // InternalGloe.g:2053:3: (enumLiteral_37= 'first blood assist' )
                    {
                    // InternalGloe.g:2053:3: (enumLiteral_37= 'first blood assist' )
                    // InternalGloe.g:2054:4: enumLiteral_37= 'first blood assist'
                    {
                    enumLiteral_37=(Token)match(input,82,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getFIRST_BLOOD_ASSISTEnumLiteralDeclaration_37().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_37, grammarAccess.getStatAccess().getFIRST_BLOOD_ASSISTEnumLiteralDeclaration_37());
                    			

                    }


                    }
                    break;
                case 39 :
                    // InternalGloe.g:2061:3: (enumLiteral_38= 'first tower kill' )
                    {
                    // InternalGloe.g:2061:3: (enumLiteral_38= 'first tower kill' )
                    // InternalGloe.g:2062:4: enumLiteral_38= 'first tower kill'
                    {
                    enumLiteral_38=(Token)match(input,83,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getFIRST_TOWER_KILLEnumLiteralDeclaration_38().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_38, grammarAccess.getStatAccess().getFIRST_TOWER_KILLEnumLiteralDeclaration_38());
                    			

                    }


                    }
                    break;
                case 40 :
                    // InternalGloe.g:2069:3: (enumLiteral_39= 'first tower assist' )
                    {
                    // InternalGloe.g:2069:3: (enumLiteral_39= 'first tower assist' )
                    // InternalGloe.g:2070:4: enumLiteral_39= 'first tower assist'
                    {
                    enumLiteral_39=(Token)match(input,84,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getFIRST_TOWER_ASSISTEnumLiteralDeclaration_39().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_39, grammarAccess.getStatAccess().getFIRST_TOWER_ASSISTEnumLiteralDeclaration_39());
                    			

                    }


                    }
                    break;
                case 41 :
                    // InternalGloe.g:2077:3: (enumLiteral_40= 'first inhibitor kill' )
                    {
                    // InternalGloe.g:2077:3: (enumLiteral_40= 'first inhibitor kill' )
                    // InternalGloe.g:2078:4: enumLiteral_40= 'first inhibitor kill'
                    {
                    enumLiteral_40=(Token)match(input,85,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getFIRST_INHIBITOR_KILLEnumLiteralDeclaration_40().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_40, grammarAccess.getStatAccess().getFIRST_INHIBITOR_KILLEnumLiteralDeclaration_40());
                    			

                    }


                    }
                    break;
                case 42 :
                    // InternalGloe.g:2085:3: (enumLiteral_41= 'first inhibitor assist' )
                    {
                    // InternalGloe.g:2085:3: (enumLiteral_41= 'first inhibitor assist' )
                    // InternalGloe.g:2086:4: enumLiteral_41= 'first inhibitor assist'
                    {
                    enumLiteral_41=(Token)match(input,86,FOLLOW_2); 

                    				current = grammarAccess.getStatAccess().getFIRST_INHIBITOR_ASSISTEnumLiteralDeclaration_41().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_41, grammarAccess.getStatAccess().getFIRST_INHIBITOR_ASSISTEnumLiteralDeclaration_41());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStat"


    // $ANTLR start "ruleChampion"
    // InternalGloe.g:2096:1: ruleChampion returns [Enumerator current=null] : ( (enumLiteral_0= 'Annie' ) | (enumLiteral_1= 'Olaf' ) | (enumLiteral_2= 'Galio' ) | (enumLiteral_3= 'Twisted Fate' ) | (enumLiteral_4= 'Xin Zhao' ) | (enumLiteral_5= 'Urgot' ) | (enumLiteral_6= 'Leblanc' ) | (enumLiteral_7= 'Vladimir' ) | (enumLiteral_8= 'Fiddlesticks' ) | (enumLiteral_9= 'Kayle' ) | (enumLiteral_10= 'Master Yi' ) | (enumLiteral_11= 'Alistar' ) | (enumLiteral_12= 'Ryze' ) | (enumLiteral_13= 'Sion' ) | (enumLiteral_14= 'Sivir' ) | (enumLiteral_15= 'Soraka' ) | (enumLiteral_16= 'Teemo' ) | (enumLiteral_17= 'Tristana' ) | (enumLiteral_18= 'Warwick' ) | (enumLiteral_19= 'Nunu' ) | (enumLiteral_20= 'Miss Fortune' ) | (enumLiteral_21= 'Ashe' ) | (enumLiteral_22= 'Tryndamere' ) | (enumLiteral_23= 'Jax' ) | (enumLiteral_24= 'Morgana' ) | (enumLiteral_25= 'Zilean' ) | (enumLiteral_26= 'Singed' ) | (enumLiteral_27= 'Evelynn' ) | (enumLiteral_28= 'Twitch' ) | (enumLiteral_29= 'Karthus' ) | (enumLiteral_30= 'Cho\\'Gath' ) | (enumLiteral_31= 'Amumu' ) | (enumLiteral_32= 'Rammus' ) | (enumLiteral_33= 'Anivia' ) | (enumLiteral_34= 'Shaco' ) | (enumLiteral_35= 'Dr. Mundo' ) | (enumLiteral_36= 'Sona' ) | (enumLiteral_37= 'Kassadin' ) | (enumLiteral_38= 'Irelia' ) | (enumLiteral_39= 'Janna' ) | (enumLiteral_40= 'Gangplank' ) | (enumLiteral_41= 'Corki' ) | (enumLiteral_42= 'Karma' ) | (enumLiteral_43= 'Taric' ) | (enumLiteral_44= 'Veigar' ) | (enumLiteral_45= 'Trundle' ) | (enumLiteral_46= 'Swain' ) | (enumLiteral_47= 'Caitlyn' ) | (enumLiteral_48= 'Blitzcrank' ) | (enumLiteral_49= 'Malphite' ) | (enumLiteral_50= 'Katarina' ) | (enumLiteral_51= 'Nocturne' ) | (enumLiteral_52= 'Maokai' ) | (enumLiteral_53= 'Renekton' ) | (enumLiteral_54= 'Jarvan IV' ) | (enumLiteral_55= 'Elise' ) | (enumLiteral_56= 'Orianna' ) | (enumLiteral_57= 'Wukong' ) | (enumLiteral_58= 'Brand' ) | (enumLiteral_59= 'Lee Sin' ) | (enumLiteral_60= 'Vayne' ) | (enumLiteral_61= 'Rumble' ) | (enumLiteral_62= 'Cassiopeia' ) | (enumLiteral_63= 'Skarner' ) | (enumLiteral_64= 'Heimerdinger' ) | (enumLiteral_65= 'Nasus' ) | (enumLiteral_66= 'Nidalee' ) | (enumLiteral_67= 'Udyr' ) | (enumLiteral_68= 'Poppy' ) | (enumLiteral_69= 'Gragas' ) | (enumLiteral_70= 'Pantheon' ) | (enumLiteral_71= 'Ezreal' ) | (enumLiteral_72= 'Mordekaiser' ) | (enumLiteral_73= 'Yorick' ) | (enumLiteral_74= 'Akali' ) | (enumLiteral_75= 'Kennen' ) | (enumLiteral_76= 'Garen' ) | (enumLiteral_77= 'Leona' ) | (enumLiteral_78= 'Malzahar' ) | (enumLiteral_79= 'Talon' ) | (enumLiteral_80= 'Riven' ) | (enumLiteral_81= 'Kog\\'Maw' ) | (enumLiteral_82= 'Shen' ) | (enumLiteral_83= 'Lux' ) | (enumLiteral_84= 'Xerath' ) | (enumLiteral_85= 'Shyvana' ) | (enumLiteral_86= 'Ahri' ) | (enumLiteral_87= 'Graves' ) | (enumLiteral_88= 'Fizz' ) | (enumLiteral_89= 'Volibear' ) | (enumLiteral_90= 'Rengar' ) | (enumLiteral_91= 'Varus' ) | (enumLiteral_92= 'Nautilus' ) | (enumLiteral_93= 'Viktor' ) | (enumLiteral_94= 'Sejuani' ) | (enumLiteral_95= 'Fiora' ) | (enumLiteral_96= 'Ziggs' ) | (enumLiteral_97= 'Lulu' ) | (enumLiteral_98= 'Draven' ) | (enumLiteral_99= 'Hecarim' ) | (enumLiteral_100= 'Kha\\'Zix' ) | (enumLiteral_101= 'Darius' ) | (enumLiteral_102= 'Jayce' ) | (enumLiteral_103= 'Lissandra' ) | (enumLiteral_104= 'Diana' ) | (enumLiteral_105= 'Quinn' ) | (enumLiteral_106= 'Syndra' ) | (enumLiteral_107= 'Aurelion Sol' ) | (enumLiteral_108= 'Kayn' ) | (enumLiteral_109= 'Zoe' ) | (enumLiteral_110= 'Zyra' ) | (enumLiteral_111= 'Kai\\'Sa' ) | (enumLiteral_112= 'Gnar' ) | (enumLiteral_113= 'Zac' ) | (enumLiteral_114= 'Yasuo' ) | (enumLiteral_115= 'Vel\\'Koz' ) | (enumLiteral_116= 'Taliyah' ) | (enumLiteral_117= 'Camille' ) | (enumLiteral_118= 'Braum' ) | (enumLiteral_119= 'Jhin' ) | (enumLiteral_120= 'Kindred' ) | (enumLiteral_121= 'Jinx' ) | (enumLiteral_122= 'Tahm Kench' ) | (enumLiteral_123= 'Lucian' ) | (enumLiteral_124= 'Zed' ) | (enumLiteral_125= 'Kled' ) | (enumLiteral_126= 'Ekko' ) | (enumLiteral_127= 'Vi' ) | (enumLiteral_128= 'Aatrox' ) | (enumLiteral_129= 'Nami' ) | (enumLiteral_130= 'Azir' ) | (enumLiteral_131= 'Thresh' ) | (enumLiteral_132= 'Illaoi' ) | (enumLiteral_133= 'Rek\\'Sai' ) | (enumLiteral_134= 'Ivern' ) | (enumLiteral_135= 'Kalista' ) | (enumLiteral_136= 'Bard' ) | (enumLiteral_137= 'Rakan' ) | (enumLiteral_138= 'Xayah' ) | (enumLiteral_139= 'Ornn' ) ) ;
    public final Enumerator ruleChampion() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;
        Token enumLiteral_6=null;
        Token enumLiteral_7=null;
        Token enumLiteral_8=null;
        Token enumLiteral_9=null;
        Token enumLiteral_10=null;
        Token enumLiteral_11=null;
        Token enumLiteral_12=null;
        Token enumLiteral_13=null;
        Token enumLiteral_14=null;
        Token enumLiteral_15=null;
        Token enumLiteral_16=null;
        Token enumLiteral_17=null;
        Token enumLiteral_18=null;
        Token enumLiteral_19=null;
        Token enumLiteral_20=null;
        Token enumLiteral_21=null;
        Token enumLiteral_22=null;
        Token enumLiteral_23=null;
        Token enumLiteral_24=null;
        Token enumLiteral_25=null;
        Token enumLiteral_26=null;
        Token enumLiteral_27=null;
        Token enumLiteral_28=null;
        Token enumLiteral_29=null;
        Token enumLiteral_30=null;
        Token enumLiteral_31=null;
        Token enumLiteral_32=null;
        Token enumLiteral_33=null;
        Token enumLiteral_34=null;
        Token enumLiteral_35=null;
        Token enumLiteral_36=null;
        Token enumLiteral_37=null;
        Token enumLiteral_38=null;
        Token enumLiteral_39=null;
        Token enumLiteral_40=null;
        Token enumLiteral_41=null;
        Token enumLiteral_42=null;
        Token enumLiteral_43=null;
        Token enumLiteral_44=null;
        Token enumLiteral_45=null;
        Token enumLiteral_46=null;
        Token enumLiteral_47=null;
        Token enumLiteral_48=null;
        Token enumLiteral_49=null;
        Token enumLiteral_50=null;
        Token enumLiteral_51=null;
        Token enumLiteral_52=null;
        Token enumLiteral_53=null;
        Token enumLiteral_54=null;
        Token enumLiteral_55=null;
        Token enumLiteral_56=null;
        Token enumLiteral_57=null;
        Token enumLiteral_58=null;
        Token enumLiteral_59=null;
        Token enumLiteral_60=null;
        Token enumLiteral_61=null;
        Token enumLiteral_62=null;
        Token enumLiteral_63=null;
        Token enumLiteral_64=null;
        Token enumLiteral_65=null;
        Token enumLiteral_66=null;
        Token enumLiteral_67=null;
        Token enumLiteral_68=null;
        Token enumLiteral_69=null;
        Token enumLiteral_70=null;
        Token enumLiteral_71=null;
        Token enumLiteral_72=null;
        Token enumLiteral_73=null;
        Token enumLiteral_74=null;
        Token enumLiteral_75=null;
        Token enumLiteral_76=null;
        Token enumLiteral_77=null;
        Token enumLiteral_78=null;
        Token enumLiteral_79=null;
        Token enumLiteral_80=null;
        Token enumLiteral_81=null;
        Token enumLiteral_82=null;
        Token enumLiteral_83=null;
        Token enumLiteral_84=null;
        Token enumLiteral_85=null;
        Token enumLiteral_86=null;
        Token enumLiteral_87=null;
        Token enumLiteral_88=null;
        Token enumLiteral_89=null;
        Token enumLiteral_90=null;
        Token enumLiteral_91=null;
        Token enumLiteral_92=null;
        Token enumLiteral_93=null;
        Token enumLiteral_94=null;
        Token enumLiteral_95=null;
        Token enumLiteral_96=null;
        Token enumLiteral_97=null;
        Token enumLiteral_98=null;
        Token enumLiteral_99=null;
        Token enumLiteral_100=null;
        Token enumLiteral_101=null;
        Token enumLiteral_102=null;
        Token enumLiteral_103=null;
        Token enumLiteral_104=null;
        Token enumLiteral_105=null;
        Token enumLiteral_106=null;
        Token enumLiteral_107=null;
        Token enumLiteral_108=null;
        Token enumLiteral_109=null;
        Token enumLiteral_110=null;
        Token enumLiteral_111=null;
        Token enumLiteral_112=null;
        Token enumLiteral_113=null;
        Token enumLiteral_114=null;
        Token enumLiteral_115=null;
        Token enumLiteral_116=null;
        Token enumLiteral_117=null;
        Token enumLiteral_118=null;
        Token enumLiteral_119=null;
        Token enumLiteral_120=null;
        Token enumLiteral_121=null;
        Token enumLiteral_122=null;
        Token enumLiteral_123=null;
        Token enumLiteral_124=null;
        Token enumLiteral_125=null;
        Token enumLiteral_126=null;
        Token enumLiteral_127=null;
        Token enumLiteral_128=null;
        Token enumLiteral_129=null;
        Token enumLiteral_130=null;
        Token enumLiteral_131=null;
        Token enumLiteral_132=null;
        Token enumLiteral_133=null;
        Token enumLiteral_134=null;
        Token enumLiteral_135=null;
        Token enumLiteral_136=null;
        Token enumLiteral_137=null;
        Token enumLiteral_138=null;
        Token enumLiteral_139=null;


        	enterRule();

        try {
            // InternalGloe.g:2102:2: ( ( (enumLiteral_0= 'Annie' ) | (enumLiteral_1= 'Olaf' ) | (enumLiteral_2= 'Galio' ) | (enumLiteral_3= 'Twisted Fate' ) | (enumLiteral_4= 'Xin Zhao' ) | (enumLiteral_5= 'Urgot' ) | (enumLiteral_6= 'Leblanc' ) | (enumLiteral_7= 'Vladimir' ) | (enumLiteral_8= 'Fiddlesticks' ) | (enumLiteral_9= 'Kayle' ) | (enumLiteral_10= 'Master Yi' ) | (enumLiteral_11= 'Alistar' ) | (enumLiteral_12= 'Ryze' ) | (enumLiteral_13= 'Sion' ) | (enumLiteral_14= 'Sivir' ) | (enumLiteral_15= 'Soraka' ) | (enumLiteral_16= 'Teemo' ) | (enumLiteral_17= 'Tristana' ) | (enumLiteral_18= 'Warwick' ) | (enumLiteral_19= 'Nunu' ) | (enumLiteral_20= 'Miss Fortune' ) | (enumLiteral_21= 'Ashe' ) | (enumLiteral_22= 'Tryndamere' ) | (enumLiteral_23= 'Jax' ) | (enumLiteral_24= 'Morgana' ) | (enumLiteral_25= 'Zilean' ) | (enumLiteral_26= 'Singed' ) | (enumLiteral_27= 'Evelynn' ) | (enumLiteral_28= 'Twitch' ) | (enumLiteral_29= 'Karthus' ) | (enumLiteral_30= 'Cho\\'Gath' ) | (enumLiteral_31= 'Amumu' ) | (enumLiteral_32= 'Rammus' ) | (enumLiteral_33= 'Anivia' ) | (enumLiteral_34= 'Shaco' ) | (enumLiteral_35= 'Dr. Mundo' ) | (enumLiteral_36= 'Sona' ) | (enumLiteral_37= 'Kassadin' ) | (enumLiteral_38= 'Irelia' ) | (enumLiteral_39= 'Janna' ) | (enumLiteral_40= 'Gangplank' ) | (enumLiteral_41= 'Corki' ) | (enumLiteral_42= 'Karma' ) | (enumLiteral_43= 'Taric' ) | (enumLiteral_44= 'Veigar' ) | (enumLiteral_45= 'Trundle' ) | (enumLiteral_46= 'Swain' ) | (enumLiteral_47= 'Caitlyn' ) | (enumLiteral_48= 'Blitzcrank' ) | (enumLiteral_49= 'Malphite' ) | (enumLiteral_50= 'Katarina' ) | (enumLiteral_51= 'Nocturne' ) | (enumLiteral_52= 'Maokai' ) | (enumLiteral_53= 'Renekton' ) | (enumLiteral_54= 'Jarvan IV' ) | (enumLiteral_55= 'Elise' ) | (enumLiteral_56= 'Orianna' ) | (enumLiteral_57= 'Wukong' ) | (enumLiteral_58= 'Brand' ) | (enumLiteral_59= 'Lee Sin' ) | (enumLiteral_60= 'Vayne' ) | (enumLiteral_61= 'Rumble' ) | (enumLiteral_62= 'Cassiopeia' ) | (enumLiteral_63= 'Skarner' ) | (enumLiteral_64= 'Heimerdinger' ) | (enumLiteral_65= 'Nasus' ) | (enumLiteral_66= 'Nidalee' ) | (enumLiteral_67= 'Udyr' ) | (enumLiteral_68= 'Poppy' ) | (enumLiteral_69= 'Gragas' ) | (enumLiteral_70= 'Pantheon' ) | (enumLiteral_71= 'Ezreal' ) | (enumLiteral_72= 'Mordekaiser' ) | (enumLiteral_73= 'Yorick' ) | (enumLiteral_74= 'Akali' ) | (enumLiteral_75= 'Kennen' ) | (enumLiteral_76= 'Garen' ) | (enumLiteral_77= 'Leona' ) | (enumLiteral_78= 'Malzahar' ) | (enumLiteral_79= 'Talon' ) | (enumLiteral_80= 'Riven' ) | (enumLiteral_81= 'Kog\\'Maw' ) | (enumLiteral_82= 'Shen' ) | (enumLiteral_83= 'Lux' ) | (enumLiteral_84= 'Xerath' ) | (enumLiteral_85= 'Shyvana' ) | (enumLiteral_86= 'Ahri' ) | (enumLiteral_87= 'Graves' ) | (enumLiteral_88= 'Fizz' ) | (enumLiteral_89= 'Volibear' ) | (enumLiteral_90= 'Rengar' ) | (enumLiteral_91= 'Varus' ) | (enumLiteral_92= 'Nautilus' ) | (enumLiteral_93= 'Viktor' ) | (enumLiteral_94= 'Sejuani' ) | (enumLiteral_95= 'Fiora' ) | (enumLiteral_96= 'Ziggs' ) | (enumLiteral_97= 'Lulu' ) | (enumLiteral_98= 'Draven' ) | (enumLiteral_99= 'Hecarim' ) | (enumLiteral_100= 'Kha\\'Zix' ) | (enumLiteral_101= 'Darius' ) | (enumLiteral_102= 'Jayce' ) | (enumLiteral_103= 'Lissandra' ) | (enumLiteral_104= 'Diana' ) | (enumLiteral_105= 'Quinn' ) | (enumLiteral_106= 'Syndra' ) | (enumLiteral_107= 'Aurelion Sol' ) | (enumLiteral_108= 'Kayn' ) | (enumLiteral_109= 'Zoe' ) | (enumLiteral_110= 'Zyra' ) | (enumLiteral_111= 'Kai\\'Sa' ) | (enumLiteral_112= 'Gnar' ) | (enumLiteral_113= 'Zac' ) | (enumLiteral_114= 'Yasuo' ) | (enumLiteral_115= 'Vel\\'Koz' ) | (enumLiteral_116= 'Taliyah' ) | (enumLiteral_117= 'Camille' ) | (enumLiteral_118= 'Braum' ) | (enumLiteral_119= 'Jhin' ) | (enumLiteral_120= 'Kindred' ) | (enumLiteral_121= 'Jinx' ) | (enumLiteral_122= 'Tahm Kench' ) | (enumLiteral_123= 'Lucian' ) | (enumLiteral_124= 'Zed' ) | (enumLiteral_125= 'Kled' ) | (enumLiteral_126= 'Ekko' ) | (enumLiteral_127= 'Vi' ) | (enumLiteral_128= 'Aatrox' ) | (enumLiteral_129= 'Nami' ) | (enumLiteral_130= 'Azir' ) | (enumLiteral_131= 'Thresh' ) | (enumLiteral_132= 'Illaoi' ) | (enumLiteral_133= 'Rek\\'Sai' ) | (enumLiteral_134= 'Ivern' ) | (enumLiteral_135= 'Kalista' ) | (enumLiteral_136= 'Bard' ) | (enumLiteral_137= 'Rakan' ) | (enumLiteral_138= 'Xayah' ) | (enumLiteral_139= 'Ornn' ) ) )
            // InternalGloe.g:2103:2: ( (enumLiteral_0= 'Annie' ) | (enumLiteral_1= 'Olaf' ) | (enumLiteral_2= 'Galio' ) | (enumLiteral_3= 'Twisted Fate' ) | (enumLiteral_4= 'Xin Zhao' ) | (enumLiteral_5= 'Urgot' ) | (enumLiteral_6= 'Leblanc' ) | (enumLiteral_7= 'Vladimir' ) | (enumLiteral_8= 'Fiddlesticks' ) | (enumLiteral_9= 'Kayle' ) | (enumLiteral_10= 'Master Yi' ) | (enumLiteral_11= 'Alistar' ) | (enumLiteral_12= 'Ryze' ) | (enumLiteral_13= 'Sion' ) | (enumLiteral_14= 'Sivir' ) | (enumLiteral_15= 'Soraka' ) | (enumLiteral_16= 'Teemo' ) | (enumLiteral_17= 'Tristana' ) | (enumLiteral_18= 'Warwick' ) | (enumLiteral_19= 'Nunu' ) | (enumLiteral_20= 'Miss Fortune' ) | (enumLiteral_21= 'Ashe' ) | (enumLiteral_22= 'Tryndamere' ) | (enumLiteral_23= 'Jax' ) | (enumLiteral_24= 'Morgana' ) | (enumLiteral_25= 'Zilean' ) | (enumLiteral_26= 'Singed' ) | (enumLiteral_27= 'Evelynn' ) | (enumLiteral_28= 'Twitch' ) | (enumLiteral_29= 'Karthus' ) | (enumLiteral_30= 'Cho\\'Gath' ) | (enumLiteral_31= 'Amumu' ) | (enumLiteral_32= 'Rammus' ) | (enumLiteral_33= 'Anivia' ) | (enumLiteral_34= 'Shaco' ) | (enumLiteral_35= 'Dr. Mundo' ) | (enumLiteral_36= 'Sona' ) | (enumLiteral_37= 'Kassadin' ) | (enumLiteral_38= 'Irelia' ) | (enumLiteral_39= 'Janna' ) | (enumLiteral_40= 'Gangplank' ) | (enumLiteral_41= 'Corki' ) | (enumLiteral_42= 'Karma' ) | (enumLiteral_43= 'Taric' ) | (enumLiteral_44= 'Veigar' ) | (enumLiteral_45= 'Trundle' ) | (enumLiteral_46= 'Swain' ) | (enumLiteral_47= 'Caitlyn' ) | (enumLiteral_48= 'Blitzcrank' ) | (enumLiteral_49= 'Malphite' ) | (enumLiteral_50= 'Katarina' ) | (enumLiteral_51= 'Nocturne' ) | (enumLiteral_52= 'Maokai' ) | (enumLiteral_53= 'Renekton' ) | (enumLiteral_54= 'Jarvan IV' ) | (enumLiteral_55= 'Elise' ) | (enumLiteral_56= 'Orianna' ) | (enumLiteral_57= 'Wukong' ) | (enumLiteral_58= 'Brand' ) | (enumLiteral_59= 'Lee Sin' ) | (enumLiteral_60= 'Vayne' ) | (enumLiteral_61= 'Rumble' ) | (enumLiteral_62= 'Cassiopeia' ) | (enumLiteral_63= 'Skarner' ) | (enumLiteral_64= 'Heimerdinger' ) | (enumLiteral_65= 'Nasus' ) | (enumLiteral_66= 'Nidalee' ) | (enumLiteral_67= 'Udyr' ) | (enumLiteral_68= 'Poppy' ) | (enumLiteral_69= 'Gragas' ) | (enumLiteral_70= 'Pantheon' ) | (enumLiteral_71= 'Ezreal' ) | (enumLiteral_72= 'Mordekaiser' ) | (enumLiteral_73= 'Yorick' ) | (enumLiteral_74= 'Akali' ) | (enumLiteral_75= 'Kennen' ) | (enumLiteral_76= 'Garen' ) | (enumLiteral_77= 'Leona' ) | (enumLiteral_78= 'Malzahar' ) | (enumLiteral_79= 'Talon' ) | (enumLiteral_80= 'Riven' ) | (enumLiteral_81= 'Kog\\'Maw' ) | (enumLiteral_82= 'Shen' ) | (enumLiteral_83= 'Lux' ) | (enumLiteral_84= 'Xerath' ) | (enumLiteral_85= 'Shyvana' ) | (enumLiteral_86= 'Ahri' ) | (enumLiteral_87= 'Graves' ) | (enumLiteral_88= 'Fizz' ) | (enumLiteral_89= 'Volibear' ) | (enumLiteral_90= 'Rengar' ) | (enumLiteral_91= 'Varus' ) | (enumLiteral_92= 'Nautilus' ) | (enumLiteral_93= 'Viktor' ) | (enumLiteral_94= 'Sejuani' ) | (enumLiteral_95= 'Fiora' ) | (enumLiteral_96= 'Ziggs' ) | (enumLiteral_97= 'Lulu' ) | (enumLiteral_98= 'Draven' ) | (enumLiteral_99= 'Hecarim' ) | (enumLiteral_100= 'Kha\\'Zix' ) | (enumLiteral_101= 'Darius' ) | (enumLiteral_102= 'Jayce' ) | (enumLiteral_103= 'Lissandra' ) | (enumLiteral_104= 'Diana' ) | (enumLiteral_105= 'Quinn' ) | (enumLiteral_106= 'Syndra' ) | (enumLiteral_107= 'Aurelion Sol' ) | (enumLiteral_108= 'Kayn' ) | (enumLiteral_109= 'Zoe' ) | (enumLiteral_110= 'Zyra' ) | (enumLiteral_111= 'Kai\\'Sa' ) | (enumLiteral_112= 'Gnar' ) | (enumLiteral_113= 'Zac' ) | (enumLiteral_114= 'Yasuo' ) | (enumLiteral_115= 'Vel\\'Koz' ) | (enumLiteral_116= 'Taliyah' ) | (enumLiteral_117= 'Camille' ) | (enumLiteral_118= 'Braum' ) | (enumLiteral_119= 'Jhin' ) | (enumLiteral_120= 'Kindred' ) | (enumLiteral_121= 'Jinx' ) | (enumLiteral_122= 'Tahm Kench' ) | (enumLiteral_123= 'Lucian' ) | (enumLiteral_124= 'Zed' ) | (enumLiteral_125= 'Kled' ) | (enumLiteral_126= 'Ekko' ) | (enumLiteral_127= 'Vi' ) | (enumLiteral_128= 'Aatrox' ) | (enumLiteral_129= 'Nami' ) | (enumLiteral_130= 'Azir' ) | (enumLiteral_131= 'Thresh' ) | (enumLiteral_132= 'Illaoi' ) | (enumLiteral_133= 'Rek\\'Sai' ) | (enumLiteral_134= 'Ivern' ) | (enumLiteral_135= 'Kalista' ) | (enumLiteral_136= 'Bard' ) | (enumLiteral_137= 'Rakan' ) | (enumLiteral_138= 'Xayah' ) | (enumLiteral_139= 'Ornn' ) )
            {
            // InternalGloe.g:2103:2: ( (enumLiteral_0= 'Annie' ) | (enumLiteral_1= 'Olaf' ) | (enumLiteral_2= 'Galio' ) | (enumLiteral_3= 'Twisted Fate' ) | (enumLiteral_4= 'Xin Zhao' ) | (enumLiteral_5= 'Urgot' ) | (enumLiteral_6= 'Leblanc' ) | (enumLiteral_7= 'Vladimir' ) | (enumLiteral_8= 'Fiddlesticks' ) | (enumLiteral_9= 'Kayle' ) | (enumLiteral_10= 'Master Yi' ) | (enumLiteral_11= 'Alistar' ) | (enumLiteral_12= 'Ryze' ) | (enumLiteral_13= 'Sion' ) | (enumLiteral_14= 'Sivir' ) | (enumLiteral_15= 'Soraka' ) | (enumLiteral_16= 'Teemo' ) | (enumLiteral_17= 'Tristana' ) | (enumLiteral_18= 'Warwick' ) | (enumLiteral_19= 'Nunu' ) | (enumLiteral_20= 'Miss Fortune' ) | (enumLiteral_21= 'Ashe' ) | (enumLiteral_22= 'Tryndamere' ) | (enumLiteral_23= 'Jax' ) | (enumLiteral_24= 'Morgana' ) | (enumLiteral_25= 'Zilean' ) | (enumLiteral_26= 'Singed' ) | (enumLiteral_27= 'Evelynn' ) | (enumLiteral_28= 'Twitch' ) | (enumLiteral_29= 'Karthus' ) | (enumLiteral_30= 'Cho\\'Gath' ) | (enumLiteral_31= 'Amumu' ) | (enumLiteral_32= 'Rammus' ) | (enumLiteral_33= 'Anivia' ) | (enumLiteral_34= 'Shaco' ) | (enumLiteral_35= 'Dr. Mundo' ) | (enumLiteral_36= 'Sona' ) | (enumLiteral_37= 'Kassadin' ) | (enumLiteral_38= 'Irelia' ) | (enumLiteral_39= 'Janna' ) | (enumLiteral_40= 'Gangplank' ) | (enumLiteral_41= 'Corki' ) | (enumLiteral_42= 'Karma' ) | (enumLiteral_43= 'Taric' ) | (enumLiteral_44= 'Veigar' ) | (enumLiteral_45= 'Trundle' ) | (enumLiteral_46= 'Swain' ) | (enumLiteral_47= 'Caitlyn' ) | (enumLiteral_48= 'Blitzcrank' ) | (enumLiteral_49= 'Malphite' ) | (enumLiteral_50= 'Katarina' ) | (enumLiteral_51= 'Nocturne' ) | (enumLiteral_52= 'Maokai' ) | (enumLiteral_53= 'Renekton' ) | (enumLiteral_54= 'Jarvan IV' ) | (enumLiteral_55= 'Elise' ) | (enumLiteral_56= 'Orianna' ) | (enumLiteral_57= 'Wukong' ) | (enumLiteral_58= 'Brand' ) | (enumLiteral_59= 'Lee Sin' ) | (enumLiteral_60= 'Vayne' ) | (enumLiteral_61= 'Rumble' ) | (enumLiteral_62= 'Cassiopeia' ) | (enumLiteral_63= 'Skarner' ) | (enumLiteral_64= 'Heimerdinger' ) | (enumLiteral_65= 'Nasus' ) | (enumLiteral_66= 'Nidalee' ) | (enumLiteral_67= 'Udyr' ) | (enumLiteral_68= 'Poppy' ) | (enumLiteral_69= 'Gragas' ) | (enumLiteral_70= 'Pantheon' ) | (enumLiteral_71= 'Ezreal' ) | (enumLiteral_72= 'Mordekaiser' ) | (enumLiteral_73= 'Yorick' ) | (enumLiteral_74= 'Akali' ) | (enumLiteral_75= 'Kennen' ) | (enumLiteral_76= 'Garen' ) | (enumLiteral_77= 'Leona' ) | (enumLiteral_78= 'Malzahar' ) | (enumLiteral_79= 'Talon' ) | (enumLiteral_80= 'Riven' ) | (enumLiteral_81= 'Kog\\'Maw' ) | (enumLiteral_82= 'Shen' ) | (enumLiteral_83= 'Lux' ) | (enumLiteral_84= 'Xerath' ) | (enumLiteral_85= 'Shyvana' ) | (enumLiteral_86= 'Ahri' ) | (enumLiteral_87= 'Graves' ) | (enumLiteral_88= 'Fizz' ) | (enumLiteral_89= 'Volibear' ) | (enumLiteral_90= 'Rengar' ) | (enumLiteral_91= 'Varus' ) | (enumLiteral_92= 'Nautilus' ) | (enumLiteral_93= 'Viktor' ) | (enumLiteral_94= 'Sejuani' ) | (enumLiteral_95= 'Fiora' ) | (enumLiteral_96= 'Ziggs' ) | (enumLiteral_97= 'Lulu' ) | (enumLiteral_98= 'Draven' ) | (enumLiteral_99= 'Hecarim' ) | (enumLiteral_100= 'Kha\\'Zix' ) | (enumLiteral_101= 'Darius' ) | (enumLiteral_102= 'Jayce' ) | (enumLiteral_103= 'Lissandra' ) | (enumLiteral_104= 'Diana' ) | (enumLiteral_105= 'Quinn' ) | (enumLiteral_106= 'Syndra' ) | (enumLiteral_107= 'Aurelion Sol' ) | (enumLiteral_108= 'Kayn' ) | (enumLiteral_109= 'Zoe' ) | (enumLiteral_110= 'Zyra' ) | (enumLiteral_111= 'Kai\\'Sa' ) | (enumLiteral_112= 'Gnar' ) | (enumLiteral_113= 'Zac' ) | (enumLiteral_114= 'Yasuo' ) | (enumLiteral_115= 'Vel\\'Koz' ) | (enumLiteral_116= 'Taliyah' ) | (enumLiteral_117= 'Camille' ) | (enumLiteral_118= 'Braum' ) | (enumLiteral_119= 'Jhin' ) | (enumLiteral_120= 'Kindred' ) | (enumLiteral_121= 'Jinx' ) | (enumLiteral_122= 'Tahm Kench' ) | (enumLiteral_123= 'Lucian' ) | (enumLiteral_124= 'Zed' ) | (enumLiteral_125= 'Kled' ) | (enumLiteral_126= 'Ekko' ) | (enumLiteral_127= 'Vi' ) | (enumLiteral_128= 'Aatrox' ) | (enumLiteral_129= 'Nami' ) | (enumLiteral_130= 'Azir' ) | (enumLiteral_131= 'Thresh' ) | (enumLiteral_132= 'Illaoi' ) | (enumLiteral_133= 'Rek\\'Sai' ) | (enumLiteral_134= 'Ivern' ) | (enumLiteral_135= 'Kalista' ) | (enumLiteral_136= 'Bard' ) | (enumLiteral_137= 'Rakan' ) | (enumLiteral_138= 'Xayah' ) | (enumLiteral_139= 'Ornn' ) )
            int alt24=140;
            switch ( input.LA(1) ) {
            case 87:
                {
                alt24=1;
                }
                break;
            case 88:
                {
                alt24=2;
                }
                break;
            case 89:
                {
                alt24=3;
                }
                break;
            case 90:
                {
                alt24=4;
                }
                break;
            case 91:
                {
                alt24=5;
                }
                break;
            case 92:
                {
                alt24=6;
                }
                break;
            case 93:
                {
                alt24=7;
                }
                break;
            case 94:
                {
                alt24=8;
                }
                break;
            case 95:
                {
                alt24=9;
                }
                break;
            case 96:
                {
                alt24=10;
                }
                break;
            case 97:
                {
                alt24=11;
                }
                break;
            case 98:
                {
                alt24=12;
                }
                break;
            case 99:
                {
                alt24=13;
                }
                break;
            case 100:
                {
                alt24=14;
                }
                break;
            case 101:
                {
                alt24=15;
                }
                break;
            case 102:
                {
                alt24=16;
                }
                break;
            case 103:
                {
                alt24=17;
                }
                break;
            case 104:
                {
                alt24=18;
                }
                break;
            case 105:
                {
                alt24=19;
                }
                break;
            case 106:
                {
                alt24=20;
                }
                break;
            case 107:
                {
                alt24=21;
                }
                break;
            case 108:
                {
                alt24=22;
                }
                break;
            case 109:
                {
                alt24=23;
                }
                break;
            case 110:
                {
                alt24=24;
                }
                break;
            case 111:
                {
                alt24=25;
                }
                break;
            case 112:
                {
                alt24=26;
                }
                break;
            case 113:
                {
                alt24=27;
                }
                break;
            case 114:
                {
                alt24=28;
                }
                break;
            case 115:
                {
                alt24=29;
                }
                break;
            case 116:
                {
                alt24=30;
                }
                break;
            case 117:
                {
                alt24=31;
                }
                break;
            case 118:
                {
                alt24=32;
                }
                break;
            case 119:
                {
                alt24=33;
                }
                break;
            case 120:
                {
                alt24=34;
                }
                break;
            case 121:
                {
                alt24=35;
                }
                break;
            case 122:
                {
                alt24=36;
                }
                break;
            case 123:
                {
                alt24=37;
                }
                break;
            case 124:
                {
                alt24=38;
                }
                break;
            case 125:
                {
                alt24=39;
                }
                break;
            case 126:
                {
                alt24=40;
                }
                break;
            case 127:
                {
                alt24=41;
                }
                break;
            case 128:
                {
                alt24=42;
                }
                break;
            case 129:
                {
                alt24=43;
                }
                break;
            case 130:
                {
                alt24=44;
                }
                break;
            case 131:
                {
                alt24=45;
                }
                break;
            case 132:
                {
                alt24=46;
                }
                break;
            case 133:
                {
                alt24=47;
                }
                break;
            case 134:
                {
                alt24=48;
                }
                break;
            case 135:
                {
                alt24=49;
                }
                break;
            case 136:
                {
                alt24=50;
                }
                break;
            case 137:
                {
                alt24=51;
                }
                break;
            case 138:
                {
                alt24=52;
                }
                break;
            case 139:
                {
                alt24=53;
                }
                break;
            case 140:
                {
                alt24=54;
                }
                break;
            case 141:
                {
                alt24=55;
                }
                break;
            case 142:
                {
                alt24=56;
                }
                break;
            case 143:
                {
                alt24=57;
                }
                break;
            case 144:
                {
                alt24=58;
                }
                break;
            case 145:
                {
                alt24=59;
                }
                break;
            case 146:
                {
                alt24=60;
                }
                break;
            case 147:
                {
                alt24=61;
                }
                break;
            case 148:
                {
                alt24=62;
                }
                break;
            case 149:
                {
                alt24=63;
                }
                break;
            case 150:
                {
                alt24=64;
                }
                break;
            case 151:
                {
                alt24=65;
                }
                break;
            case 152:
                {
                alt24=66;
                }
                break;
            case 153:
                {
                alt24=67;
                }
                break;
            case 154:
                {
                alt24=68;
                }
                break;
            case 155:
                {
                alt24=69;
                }
                break;
            case 156:
                {
                alt24=70;
                }
                break;
            case 157:
                {
                alt24=71;
                }
                break;
            case 158:
                {
                alt24=72;
                }
                break;
            case 159:
                {
                alt24=73;
                }
                break;
            case 160:
                {
                alt24=74;
                }
                break;
            case 161:
                {
                alt24=75;
                }
                break;
            case 162:
                {
                alt24=76;
                }
                break;
            case 163:
                {
                alt24=77;
                }
                break;
            case 164:
                {
                alt24=78;
                }
                break;
            case 165:
                {
                alt24=79;
                }
                break;
            case 166:
                {
                alt24=80;
                }
                break;
            case 167:
                {
                alt24=81;
                }
                break;
            case 168:
                {
                alt24=82;
                }
                break;
            case 169:
                {
                alt24=83;
                }
                break;
            case 170:
                {
                alt24=84;
                }
                break;
            case 171:
                {
                alt24=85;
                }
                break;
            case 172:
                {
                alt24=86;
                }
                break;
            case 173:
                {
                alt24=87;
                }
                break;
            case 174:
                {
                alt24=88;
                }
                break;
            case 175:
                {
                alt24=89;
                }
                break;
            case 176:
                {
                alt24=90;
                }
                break;
            case 177:
                {
                alt24=91;
                }
                break;
            case 178:
                {
                alt24=92;
                }
                break;
            case 179:
                {
                alt24=93;
                }
                break;
            case 180:
                {
                alt24=94;
                }
                break;
            case 181:
                {
                alt24=95;
                }
                break;
            case 182:
                {
                alt24=96;
                }
                break;
            case 183:
                {
                alt24=97;
                }
                break;
            case 184:
                {
                alt24=98;
                }
                break;
            case 185:
                {
                alt24=99;
                }
                break;
            case 186:
                {
                alt24=100;
                }
                break;
            case 187:
                {
                alt24=101;
                }
                break;
            case 188:
                {
                alt24=102;
                }
                break;
            case 189:
                {
                alt24=103;
                }
                break;
            case 190:
                {
                alt24=104;
                }
                break;
            case 191:
                {
                alt24=105;
                }
                break;
            case 192:
                {
                alt24=106;
                }
                break;
            case 193:
                {
                alt24=107;
                }
                break;
            case 194:
                {
                alt24=108;
                }
                break;
            case 195:
                {
                alt24=109;
                }
                break;
            case 196:
                {
                alt24=110;
                }
                break;
            case 197:
                {
                alt24=111;
                }
                break;
            case 198:
                {
                alt24=112;
                }
                break;
            case 199:
                {
                alt24=113;
                }
                break;
            case 200:
                {
                alt24=114;
                }
                break;
            case 201:
                {
                alt24=115;
                }
                break;
            case 202:
                {
                alt24=116;
                }
                break;
            case 203:
                {
                alt24=117;
                }
                break;
            case 204:
                {
                alt24=118;
                }
                break;
            case 205:
                {
                alt24=119;
                }
                break;
            case 206:
                {
                alt24=120;
                }
                break;
            case 207:
                {
                alt24=121;
                }
                break;
            case 208:
                {
                alt24=122;
                }
                break;
            case 209:
                {
                alt24=123;
                }
                break;
            case 210:
                {
                alt24=124;
                }
                break;
            case 211:
                {
                alt24=125;
                }
                break;
            case 212:
                {
                alt24=126;
                }
                break;
            case 213:
                {
                alt24=127;
                }
                break;
            case 214:
                {
                alt24=128;
                }
                break;
            case 215:
                {
                alt24=129;
                }
                break;
            case 216:
                {
                alt24=130;
                }
                break;
            case 217:
                {
                alt24=131;
                }
                break;
            case 218:
                {
                alt24=132;
                }
                break;
            case 219:
                {
                alt24=133;
                }
                break;
            case 220:
                {
                alt24=134;
                }
                break;
            case 221:
                {
                alt24=135;
                }
                break;
            case 222:
                {
                alt24=136;
                }
                break;
            case 223:
                {
                alt24=137;
                }
                break;
            case 224:
                {
                alt24=138;
                }
                break;
            case 225:
                {
                alt24=139;
                }
                break;
            case 226:
                {
                alt24=140;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }

            switch (alt24) {
                case 1 :
                    // InternalGloe.g:2104:3: (enumLiteral_0= 'Annie' )
                    {
                    // InternalGloe.g:2104:3: (enumLiteral_0= 'Annie' )
                    // InternalGloe.g:2105:4: enumLiteral_0= 'Annie'
                    {
                    enumLiteral_0=(Token)match(input,87,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getANNIEEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getChampionAccess().getANNIEEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:2112:3: (enumLiteral_1= 'Olaf' )
                    {
                    // InternalGloe.g:2112:3: (enumLiteral_1= 'Olaf' )
                    // InternalGloe.g:2113:4: enumLiteral_1= 'Olaf'
                    {
                    enumLiteral_1=(Token)match(input,88,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getOLAFEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getChampionAccess().getOLAFEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:2120:3: (enumLiteral_2= 'Galio' )
                    {
                    // InternalGloe.g:2120:3: (enumLiteral_2= 'Galio' )
                    // InternalGloe.g:2121:4: enumLiteral_2= 'Galio'
                    {
                    enumLiteral_2=(Token)match(input,89,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getGALIOEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getChampionAccess().getGALIOEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:2128:3: (enumLiteral_3= 'Twisted Fate' )
                    {
                    // InternalGloe.g:2128:3: (enumLiteral_3= 'Twisted Fate' )
                    // InternalGloe.g:2129:4: enumLiteral_3= 'Twisted Fate'
                    {
                    enumLiteral_3=(Token)match(input,90,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTWISTEDFATEEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getChampionAccess().getTWISTEDFATEEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalGloe.g:2136:3: (enumLiteral_4= 'Xin Zhao' )
                    {
                    // InternalGloe.g:2136:3: (enumLiteral_4= 'Xin Zhao' )
                    // InternalGloe.g:2137:4: enumLiteral_4= 'Xin Zhao'
                    {
                    enumLiteral_4=(Token)match(input,91,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getXINZHAOEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getChampionAccess().getXINZHAOEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalGloe.g:2144:3: (enumLiteral_5= 'Urgot' )
                    {
                    // InternalGloe.g:2144:3: (enumLiteral_5= 'Urgot' )
                    // InternalGloe.g:2145:4: enumLiteral_5= 'Urgot'
                    {
                    enumLiteral_5=(Token)match(input,92,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getURGOTEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getChampionAccess().getURGOTEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;
                case 7 :
                    // InternalGloe.g:2152:3: (enumLiteral_6= 'Leblanc' )
                    {
                    // InternalGloe.g:2152:3: (enumLiteral_6= 'Leblanc' )
                    // InternalGloe.g:2153:4: enumLiteral_6= 'Leblanc'
                    {
                    enumLiteral_6=(Token)match(input,93,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLEBLANCEnumLiteralDeclaration_6().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_6, grammarAccess.getChampionAccess().getLEBLANCEnumLiteralDeclaration_6());
                    			

                    }


                    }
                    break;
                case 8 :
                    // InternalGloe.g:2160:3: (enumLiteral_7= 'Vladimir' )
                    {
                    // InternalGloe.g:2160:3: (enumLiteral_7= 'Vladimir' )
                    // InternalGloe.g:2161:4: enumLiteral_7= 'Vladimir'
                    {
                    enumLiteral_7=(Token)match(input,94,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVLADIMIREnumLiteralDeclaration_7().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_7, grammarAccess.getChampionAccess().getVLADIMIREnumLiteralDeclaration_7());
                    			

                    }


                    }
                    break;
                case 9 :
                    // InternalGloe.g:2168:3: (enumLiteral_8= 'Fiddlesticks' )
                    {
                    // InternalGloe.g:2168:3: (enumLiteral_8= 'Fiddlesticks' )
                    // InternalGloe.g:2169:4: enumLiteral_8= 'Fiddlesticks'
                    {
                    enumLiteral_8=(Token)match(input,95,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getFIDDLESTICKSEnumLiteralDeclaration_8().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_8, grammarAccess.getChampionAccess().getFIDDLESTICKSEnumLiteralDeclaration_8());
                    			

                    }


                    }
                    break;
                case 10 :
                    // InternalGloe.g:2176:3: (enumLiteral_9= 'Kayle' )
                    {
                    // InternalGloe.g:2176:3: (enumLiteral_9= 'Kayle' )
                    // InternalGloe.g:2177:4: enumLiteral_9= 'Kayle'
                    {
                    enumLiteral_9=(Token)match(input,96,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKAYLEEnumLiteralDeclaration_9().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_9, grammarAccess.getChampionAccess().getKAYLEEnumLiteralDeclaration_9());
                    			

                    }


                    }
                    break;
                case 11 :
                    // InternalGloe.g:2184:3: (enumLiteral_10= 'Master Yi' )
                    {
                    // InternalGloe.g:2184:3: (enumLiteral_10= 'Master Yi' )
                    // InternalGloe.g:2185:4: enumLiteral_10= 'Master Yi'
                    {
                    enumLiteral_10=(Token)match(input,97,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMASTERYIEnumLiteralDeclaration_10().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_10, grammarAccess.getChampionAccess().getMASTERYIEnumLiteralDeclaration_10());
                    			

                    }


                    }
                    break;
                case 12 :
                    // InternalGloe.g:2192:3: (enumLiteral_11= 'Alistar' )
                    {
                    // InternalGloe.g:2192:3: (enumLiteral_11= 'Alistar' )
                    // InternalGloe.g:2193:4: enumLiteral_11= 'Alistar'
                    {
                    enumLiteral_11=(Token)match(input,98,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getALISTAREnumLiteralDeclaration_11().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_11, grammarAccess.getChampionAccess().getALISTAREnumLiteralDeclaration_11());
                    			

                    }


                    }
                    break;
                case 13 :
                    // InternalGloe.g:2200:3: (enumLiteral_12= 'Ryze' )
                    {
                    // InternalGloe.g:2200:3: (enumLiteral_12= 'Ryze' )
                    // InternalGloe.g:2201:4: enumLiteral_12= 'Ryze'
                    {
                    enumLiteral_12=(Token)match(input,99,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRYZEEnumLiteralDeclaration_12().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_12, grammarAccess.getChampionAccess().getRYZEEnumLiteralDeclaration_12());
                    			

                    }


                    }
                    break;
                case 14 :
                    // InternalGloe.g:2208:3: (enumLiteral_13= 'Sion' )
                    {
                    // InternalGloe.g:2208:3: (enumLiteral_13= 'Sion' )
                    // InternalGloe.g:2209:4: enumLiteral_13= 'Sion'
                    {
                    enumLiteral_13=(Token)match(input,100,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSIONEnumLiteralDeclaration_13().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_13, grammarAccess.getChampionAccess().getSIONEnumLiteralDeclaration_13());
                    			

                    }


                    }
                    break;
                case 15 :
                    // InternalGloe.g:2216:3: (enumLiteral_14= 'Sivir' )
                    {
                    // InternalGloe.g:2216:3: (enumLiteral_14= 'Sivir' )
                    // InternalGloe.g:2217:4: enumLiteral_14= 'Sivir'
                    {
                    enumLiteral_14=(Token)match(input,101,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSIVIREnumLiteralDeclaration_14().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_14, grammarAccess.getChampionAccess().getSIVIREnumLiteralDeclaration_14());
                    			

                    }


                    }
                    break;
                case 16 :
                    // InternalGloe.g:2224:3: (enumLiteral_15= 'Soraka' )
                    {
                    // InternalGloe.g:2224:3: (enumLiteral_15= 'Soraka' )
                    // InternalGloe.g:2225:4: enumLiteral_15= 'Soraka'
                    {
                    enumLiteral_15=(Token)match(input,102,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSORAKAEnumLiteralDeclaration_15().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_15, grammarAccess.getChampionAccess().getSORAKAEnumLiteralDeclaration_15());
                    			

                    }


                    }
                    break;
                case 17 :
                    // InternalGloe.g:2232:3: (enumLiteral_16= 'Teemo' )
                    {
                    // InternalGloe.g:2232:3: (enumLiteral_16= 'Teemo' )
                    // InternalGloe.g:2233:4: enumLiteral_16= 'Teemo'
                    {
                    enumLiteral_16=(Token)match(input,103,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTEEMOEnumLiteralDeclaration_16().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_16, grammarAccess.getChampionAccess().getTEEMOEnumLiteralDeclaration_16());
                    			

                    }


                    }
                    break;
                case 18 :
                    // InternalGloe.g:2240:3: (enumLiteral_17= 'Tristana' )
                    {
                    // InternalGloe.g:2240:3: (enumLiteral_17= 'Tristana' )
                    // InternalGloe.g:2241:4: enumLiteral_17= 'Tristana'
                    {
                    enumLiteral_17=(Token)match(input,104,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTRISTANAEnumLiteralDeclaration_17().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_17, grammarAccess.getChampionAccess().getTRISTANAEnumLiteralDeclaration_17());
                    			

                    }


                    }
                    break;
                case 19 :
                    // InternalGloe.g:2248:3: (enumLiteral_18= 'Warwick' )
                    {
                    // InternalGloe.g:2248:3: (enumLiteral_18= 'Warwick' )
                    // InternalGloe.g:2249:4: enumLiteral_18= 'Warwick'
                    {
                    enumLiteral_18=(Token)match(input,105,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getWARWICKEnumLiteralDeclaration_18().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_18, grammarAccess.getChampionAccess().getWARWICKEnumLiteralDeclaration_18());
                    			

                    }


                    }
                    break;
                case 20 :
                    // InternalGloe.g:2256:3: (enumLiteral_19= 'Nunu' )
                    {
                    // InternalGloe.g:2256:3: (enumLiteral_19= 'Nunu' )
                    // InternalGloe.g:2257:4: enumLiteral_19= 'Nunu'
                    {
                    enumLiteral_19=(Token)match(input,106,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getNUNUEnumLiteralDeclaration_19().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_19, grammarAccess.getChampionAccess().getNUNUEnumLiteralDeclaration_19());
                    			

                    }


                    }
                    break;
                case 21 :
                    // InternalGloe.g:2264:3: (enumLiteral_20= 'Miss Fortune' )
                    {
                    // InternalGloe.g:2264:3: (enumLiteral_20= 'Miss Fortune' )
                    // InternalGloe.g:2265:4: enumLiteral_20= 'Miss Fortune'
                    {
                    enumLiteral_20=(Token)match(input,107,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMISSFORTUNEEnumLiteralDeclaration_20().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_20, grammarAccess.getChampionAccess().getMISSFORTUNEEnumLiteralDeclaration_20());
                    			

                    }


                    }
                    break;
                case 22 :
                    // InternalGloe.g:2272:3: (enumLiteral_21= 'Ashe' )
                    {
                    // InternalGloe.g:2272:3: (enumLiteral_21= 'Ashe' )
                    // InternalGloe.g:2273:4: enumLiteral_21= 'Ashe'
                    {
                    enumLiteral_21=(Token)match(input,108,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getASHEEnumLiteralDeclaration_21().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_21, grammarAccess.getChampionAccess().getASHEEnumLiteralDeclaration_21());
                    			

                    }


                    }
                    break;
                case 23 :
                    // InternalGloe.g:2280:3: (enumLiteral_22= 'Tryndamere' )
                    {
                    // InternalGloe.g:2280:3: (enumLiteral_22= 'Tryndamere' )
                    // InternalGloe.g:2281:4: enumLiteral_22= 'Tryndamere'
                    {
                    enumLiteral_22=(Token)match(input,109,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTRYNDAMEREEnumLiteralDeclaration_22().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_22, grammarAccess.getChampionAccess().getTRYNDAMEREEnumLiteralDeclaration_22());
                    			

                    }


                    }
                    break;
                case 24 :
                    // InternalGloe.g:2288:3: (enumLiteral_23= 'Jax' )
                    {
                    // InternalGloe.g:2288:3: (enumLiteral_23= 'Jax' )
                    // InternalGloe.g:2289:4: enumLiteral_23= 'Jax'
                    {
                    enumLiteral_23=(Token)match(input,110,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getJAXEnumLiteralDeclaration_23().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_23, grammarAccess.getChampionAccess().getJAXEnumLiteralDeclaration_23());
                    			

                    }


                    }
                    break;
                case 25 :
                    // InternalGloe.g:2296:3: (enumLiteral_24= 'Morgana' )
                    {
                    // InternalGloe.g:2296:3: (enumLiteral_24= 'Morgana' )
                    // InternalGloe.g:2297:4: enumLiteral_24= 'Morgana'
                    {
                    enumLiteral_24=(Token)match(input,111,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMORGANAEnumLiteralDeclaration_24().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_24, grammarAccess.getChampionAccess().getMORGANAEnumLiteralDeclaration_24());
                    			

                    }


                    }
                    break;
                case 26 :
                    // InternalGloe.g:2304:3: (enumLiteral_25= 'Zilean' )
                    {
                    // InternalGloe.g:2304:3: (enumLiteral_25= 'Zilean' )
                    // InternalGloe.g:2305:4: enumLiteral_25= 'Zilean'
                    {
                    enumLiteral_25=(Token)match(input,112,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getZILEANEnumLiteralDeclaration_25().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_25, grammarAccess.getChampionAccess().getZILEANEnumLiteralDeclaration_25());
                    			

                    }


                    }
                    break;
                case 27 :
                    // InternalGloe.g:2312:3: (enumLiteral_26= 'Singed' )
                    {
                    // InternalGloe.g:2312:3: (enumLiteral_26= 'Singed' )
                    // InternalGloe.g:2313:4: enumLiteral_26= 'Singed'
                    {
                    enumLiteral_26=(Token)match(input,113,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSINGEDEnumLiteralDeclaration_26().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_26, grammarAccess.getChampionAccess().getSINGEDEnumLiteralDeclaration_26());
                    			

                    }


                    }
                    break;
                case 28 :
                    // InternalGloe.g:2320:3: (enumLiteral_27= 'Evelynn' )
                    {
                    // InternalGloe.g:2320:3: (enumLiteral_27= 'Evelynn' )
                    // InternalGloe.g:2321:4: enumLiteral_27= 'Evelynn'
                    {
                    enumLiteral_27=(Token)match(input,114,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getEVELYNNEnumLiteralDeclaration_27().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_27, grammarAccess.getChampionAccess().getEVELYNNEnumLiteralDeclaration_27());
                    			

                    }


                    }
                    break;
                case 29 :
                    // InternalGloe.g:2328:3: (enumLiteral_28= 'Twitch' )
                    {
                    // InternalGloe.g:2328:3: (enumLiteral_28= 'Twitch' )
                    // InternalGloe.g:2329:4: enumLiteral_28= 'Twitch'
                    {
                    enumLiteral_28=(Token)match(input,115,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTWITCHEnumLiteralDeclaration_28().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_28, grammarAccess.getChampionAccess().getTWITCHEnumLiteralDeclaration_28());
                    			

                    }


                    }
                    break;
                case 30 :
                    // InternalGloe.g:2336:3: (enumLiteral_29= 'Karthus' )
                    {
                    // InternalGloe.g:2336:3: (enumLiteral_29= 'Karthus' )
                    // InternalGloe.g:2337:4: enumLiteral_29= 'Karthus'
                    {
                    enumLiteral_29=(Token)match(input,116,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKARTHUSEnumLiteralDeclaration_29().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_29, grammarAccess.getChampionAccess().getKARTHUSEnumLiteralDeclaration_29());
                    			

                    }


                    }
                    break;
                case 31 :
                    // InternalGloe.g:2344:3: (enumLiteral_30= 'Cho\\'Gath' )
                    {
                    // InternalGloe.g:2344:3: (enumLiteral_30= 'Cho\\'Gath' )
                    // InternalGloe.g:2345:4: enumLiteral_30= 'Cho\\'Gath'
                    {
                    enumLiteral_30=(Token)match(input,117,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getCHOGATHEnumLiteralDeclaration_30().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_30, grammarAccess.getChampionAccess().getCHOGATHEnumLiteralDeclaration_30());
                    			

                    }


                    }
                    break;
                case 32 :
                    // InternalGloe.g:2352:3: (enumLiteral_31= 'Amumu' )
                    {
                    // InternalGloe.g:2352:3: (enumLiteral_31= 'Amumu' )
                    // InternalGloe.g:2353:4: enumLiteral_31= 'Amumu'
                    {
                    enumLiteral_31=(Token)match(input,118,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getAMUMUEnumLiteralDeclaration_31().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_31, grammarAccess.getChampionAccess().getAMUMUEnumLiteralDeclaration_31());
                    			

                    }


                    }
                    break;
                case 33 :
                    // InternalGloe.g:2360:3: (enumLiteral_32= 'Rammus' )
                    {
                    // InternalGloe.g:2360:3: (enumLiteral_32= 'Rammus' )
                    // InternalGloe.g:2361:4: enumLiteral_32= 'Rammus'
                    {
                    enumLiteral_32=(Token)match(input,119,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRAMMUSEnumLiteralDeclaration_32().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_32, grammarAccess.getChampionAccess().getRAMMUSEnumLiteralDeclaration_32());
                    			

                    }


                    }
                    break;
                case 34 :
                    // InternalGloe.g:2368:3: (enumLiteral_33= 'Anivia' )
                    {
                    // InternalGloe.g:2368:3: (enumLiteral_33= 'Anivia' )
                    // InternalGloe.g:2369:4: enumLiteral_33= 'Anivia'
                    {
                    enumLiteral_33=(Token)match(input,120,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getANIVIAEnumLiteralDeclaration_33().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_33, grammarAccess.getChampionAccess().getANIVIAEnumLiteralDeclaration_33());
                    			

                    }


                    }
                    break;
                case 35 :
                    // InternalGloe.g:2376:3: (enumLiteral_34= 'Shaco' )
                    {
                    // InternalGloe.g:2376:3: (enumLiteral_34= 'Shaco' )
                    // InternalGloe.g:2377:4: enumLiteral_34= 'Shaco'
                    {
                    enumLiteral_34=(Token)match(input,121,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSHACOEnumLiteralDeclaration_34().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_34, grammarAccess.getChampionAccess().getSHACOEnumLiteralDeclaration_34());
                    			

                    }


                    }
                    break;
                case 36 :
                    // InternalGloe.g:2384:3: (enumLiteral_35= 'Dr. Mundo' )
                    {
                    // InternalGloe.g:2384:3: (enumLiteral_35= 'Dr. Mundo' )
                    // InternalGloe.g:2385:4: enumLiteral_35= 'Dr. Mundo'
                    {
                    enumLiteral_35=(Token)match(input,122,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getDRMUNDOEnumLiteralDeclaration_35().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_35, grammarAccess.getChampionAccess().getDRMUNDOEnumLiteralDeclaration_35());
                    			

                    }


                    }
                    break;
                case 37 :
                    // InternalGloe.g:2392:3: (enumLiteral_36= 'Sona' )
                    {
                    // InternalGloe.g:2392:3: (enumLiteral_36= 'Sona' )
                    // InternalGloe.g:2393:4: enumLiteral_36= 'Sona'
                    {
                    enumLiteral_36=(Token)match(input,123,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSONAEnumLiteralDeclaration_36().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_36, grammarAccess.getChampionAccess().getSONAEnumLiteralDeclaration_36());
                    			

                    }


                    }
                    break;
                case 38 :
                    // InternalGloe.g:2400:3: (enumLiteral_37= 'Kassadin' )
                    {
                    // InternalGloe.g:2400:3: (enumLiteral_37= 'Kassadin' )
                    // InternalGloe.g:2401:4: enumLiteral_37= 'Kassadin'
                    {
                    enumLiteral_37=(Token)match(input,124,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKASSADINEnumLiteralDeclaration_37().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_37, grammarAccess.getChampionAccess().getKASSADINEnumLiteralDeclaration_37());
                    			

                    }


                    }
                    break;
                case 39 :
                    // InternalGloe.g:2408:3: (enumLiteral_38= 'Irelia' )
                    {
                    // InternalGloe.g:2408:3: (enumLiteral_38= 'Irelia' )
                    // InternalGloe.g:2409:4: enumLiteral_38= 'Irelia'
                    {
                    enumLiteral_38=(Token)match(input,125,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getIRELIAEnumLiteralDeclaration_38().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_38, grammarAccess.getChampionAccess().getIRELIAEnumLiteralDeclaration_38());
                    			

                    }


                    }
                    break;
                case 40 :
                    // InternalGloe.g:2416:3: (enumLiteral_39= 'Janna' )
                    {
                    // InternalGloe.g:2416:3: (enumLiteral_39= 'Janna' )
                    // InternalGloe.g:2417:4: enumLiteral_39= 'Janna'
                    {
                    enumLiteral_39=(Token)match(input,126,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getJANNAEnumLiteralDeclaration_39().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_39, grammarAccess.getChampionAccess().getJANNAEnumLiteralDeclaration_39());
                    			

                    }


                    }
                    break;
                case 41 :
                    // InternalGloe.g:2424:3: (enumLiteral_40= 'Gangplank' )
                    {
                    // InternalGloe.g:2424:3: (enumLiteral_40= 'Gangplank' )
                    // InternalGloe.g:2425:4: enumLiteral_40= 'Gangplank'
                    {
                    enumLiteral_40=(Token)match(input,127,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getGANGPLANKEnumLiteralDeclaration_40().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_40, grammarAccess.getChampionAccess().getGANGPLANKEnumLiteralDeclaration_40());
                    			

                    }


                    }
                    break;
                case 42 :
                    // InternalGloe.g:2432:3: (enumLiteral_41= 'Corki' )
                    {
                    // InternalGloe.g:2432:3: (enumLiteral_41= 'Corki' )
                    // InternalGloe.g:2433:4: enumLiteral_41= 'Corki'
                    {
                    enumLiteral_41=(Token)match(input,128,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getCORKIEnumLiteralDeclaration_41().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_41, grammarAccess.getChampionAccess().getCORKIEnumLiteralDeclaration_41());
                    			

                    }


                    }
                    break;
                case 43 :
                    // InternalGloe.g:2440:3: (enumLiteral_42= 'Karma' )
                    {
                    // InternalGloe.g:2440:3: (enumLiteral_42= 'Karma' )
                    // InternalGloe.g:2441:4: enumLiteral_42= 'Karma'
                    {
                    enumLiteral_42=(Token)match(input,129,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKARMAEnumLiteralDeclaration_42().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_42, grammarAccess.getChampionAccess().getKARMAEnumLiteralDeclaration_42());
                    			

                    }


                    }
                    break;
                case 44 :
                    // InternalGloe.g:2448:3: (enumLiteral_43= 'Taric' )
                    {
                    // InternalGloe.g:2448:3: (enumLiteral_43= 'Taric' )
                    // InternalGloe.g:2449:4: enumLiteral_43= 'Taric'
                    {
                    enumLiteral_43=(Token)match(input,130,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTARICEnumLiteralDeclaration_43().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_43, grammarAccess.getChampionAccess().getTARICEnumLiteralDeclaration_43());
                    			

                    }


                    }
                    break;
                case 45 :
                    // InternalGloe.g:2456:3: (enumLiteral_44= 'Veigar' )
                    {
                    // InternalGloe.g:2456:3: (enumLiteral_44= 'Veigar' )
                    // InternalGloe.g:2457:4: enumLiteral_44= 'Veigar'
                    {
                    enumLiteral_44=(Token)match(input,131,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVEIGAREnumLiteralDeclaration_44().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_44, grammarAccess.getChampionAccess().getVEIGAREnumLiteralDeclaration_44());
                    			

                    }


                    }
                    break;
                case 46 :
                    // InternalGloe.g:2464:3: (enumLiteral_45= 'Trundle' )
                    {
                    // InternalGloe.g:2464:3: (enumLiteral_45= 'Trundle' )
                    // InternalGloe.g:2465:4: enumLiteral_45= 'Trundle'
                    {
                    enumLiteral_45=(Token)match(input,132,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTRUNDLEEnumLiteralDeclaration_45().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_45, grammarAccess.getChampionAccess().getTRUNDLEEnumLiteralDeclaration_45());
                    			

                    }


                    }
                    break;
                case 47 :
                    // InternalGloe.g:2472:3: (enumLiteral_46= 'Swain' )
                    {
                    // InternalGloe.g:2472:3: (enumLiteral_46= 'Swain' )
                    // InternalGloe.g:2473:4: enumLiteral_46= 'Swain'
                    {
                    enumLiteral_46=(Token)match(input,133,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSWAINEnumLiteralDeclaration_46().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_46, grammarAccess.getChampionAccess().getSWAINEnumLiteralDeclaration_46());
                    			

                    }


                    }
                    break;
                case 48 :
                    // InternalGloe.g:2480:3: (enumLiteral_47= 'Caitlyn' )
                    {
                    // InternalGloe.g:2480:3: (enumLiteral_47= 'Caitlyn' )
                    // InternalGloe.g:2481:4: enumLiteral_47= 'Caitlyn'
                    {
                    enumLiteral_47=(Token)match(input,134,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getCAITLYNEnumLiteralDeclaration_47().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_47, grammarAccess.getChampionAccess().getCAITLYNEnumLiteralDeclaration_47());
                    			

                    }


                    }
                    break;
                case 49 :
                    // InternalGloe.g:2488:3: (enumLiteral_48= 'Blitzcrank' )
                    {
                    // InternalGloe.g:2488:3: (enumLiteral_48= 'Blitzcrank' )
                    // InternalGloe.g:2489:4: enumLiteral_48= 'Blitzcrank'
                    {
                    enumLiteral_48=(Token)match(input,135,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getBLITZCRANKEnumLiteralDeclaration_48().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_48, grammarAccess.getChampionAccess().getBLITZCRANKEnumLiteralDeclaration_48());
                    			

                    }


                    }
                    break;
                case 50 :
                    // InternalGloe.g:2496:3: (enumLiteral_49= 'Malphite' )
                    {
                    // InternalGloe.g:2496:3: (enumLiteral_49= 'Malphite' )
                    // InternalGloe.g:2497:4: enumLiteral_49= 'Malphite'
                    {
                    enumLiteral_49=(Token)match(input,136,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMALPHITEEnumLiteralDeclaration_49().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_49, grammarAccess.getChampionAccess().getMALPHITEEnumLiteralDeclaration_49());
                    			

                    }


                    }
                    break;
                case 51 :
                    // InternalGloe.g:2504:3: (enumLiteral_50= 'Katarina' )
                    {
                    // InternalGloe.g:2504:3: (enumLiteral_50= 'Katarina' )
                    // InternalGloe.g:2505:4: enumLiteral_50= 'Katarina'
                    {
                    enumLiteral_50=(Token)match(input,137,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKATARINAEnumLiteralDeclaration_50().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_50, grammarAccess.getChampionAccess().getKATARINAEnumLiteralDeclaration_50());
                    			

                    }


                    }
                    break;
                case 52 :
                    // InternalGloe.g:2512:3: (enumLiteral_51= 'Nocturne' )
                    {
                    // InternalGloe.g:2512:3: (enumLiteral_51= 'Nocturne' )
                    // InternalGloe.g:2513:4: enumLiteral_51= 'Nocturne'
                    {
                    enumLiteral_51=(Token)match(input,138,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getNOCTURNEEnumLiteralDeclaration_51().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_51, grammarAccess.getChampionAccess().getNOCTURNEEnumLiteralDeclaration_51());
                    			

                    }


                    }
                    break;
                case 53 :
                    // InternalGloe.g:2520:3: (enumLiteral_52= 'Maokai' )
                    {
                    // InternalGloe.g:2520:3: (enumLiteral_52= 'Maokai' )
                    // InternalGloe.g:2521:4: enumLiteral_52= 'Maokai'
                    {
                    enumLiteral_52=(Token)match(input,139,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMAOKAIEnumLiteralDeclaration_52().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_52, grammarAccess.getChampionAccess().getMAOKAIEnumLiteralDeclaration_52());
                    			

                    }


                    }
                    break;
                case 54 :
                    // InternalGloe.g:2528:3: (enumLiteral_53= 'Renekton' )
                    {
                    // InternalGloe.g:2528:3: (enumLiteral_53= 'Renekton' )
                    // InternalGloe.g:2529:4: enumLiteral_53= 'Renekton'
                    {
                    enumLiteral_53=(Token)match(input,140,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRENEKTONEnumLiteralDeclaration_53().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_53, grammarAccess.getChampionAccess().getRENEKTONEnumLiteralDeclaration_53());
                    			

                    }


                    }
                    break;
                case 55 :
                    // InternalGloe.g:2536:3: (enumLiteral_54= 'Jarvan IV' )
                    {
                    // InternalGloe.g:2536:3: (enumLiteral_54= 'Jarvan IV' )
                    // InternalGloe.g:2537:4: enumLiteral_54= 'Jarvan IV'
                    {
                    enumLiteral_54=(Token)match(input,141,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getJARVANIVEnumLiteralDeclaration_54().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_54, grammarAccess.getChampionAccess().getJARVANIVEnumLiteralDeclaration_54());
                    			

                    }


                    }
                    break;
                case 56 :
                    // InternalGloe.g:2544:3: (enumLiteral_55= 'Elise' )
                    {
                    // InternalGloe.g:2544:3: (enumLiteral_55= 'Elise' )
                    // InternalGloe.g:2545:4: enumLiteral_55= 'Elise'
                    {
                    enumLiteral_55=(Token)match(input,142,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getELISEEnumLiteralDeclaration_55().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_55, grammarAccess.getChampionAccess().getELISEEnumLiteralDeclaration_55());
                    			

                    }


                    }
                    break;
                case 57 :
                    // InternalGloe.g:2552:3: (enumLiteral_56= 'Orianna' )
                    {
                    // InternalGloe.g:2552:3: (enumLiteral_56= 'Orianna' )
                    // InternalGloe.g:2553:4: enumLiteral_56= 'Orianna'
                    {
                    enumLiteral_56=(Token)match(input,143,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getORIANNAEnumLiteralDeclaration_56().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_56, grammarAccess.getChampionAccess().getORIANNAEnumLiteralDeclaration_56());
                    			

                    }


                    }
                    break;
                case 58 :
                    // InternalGloe.g:2560:3: (enumLiteral_57= 'Wukong' )
                    {
                    // InternalGloe.g:2560:3: (enumLiteral_57= 'Wukong' )
                    // InternalGloe.g:2561:4: enumLiteral_57= 'Wukong'
                    {
                    enumLiteral_57=(Token)match(input,144,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMONKEYKINGEnumLiteralDeclaration_57().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_57, grammarAccess.getChampionAccess().getMONKEYKINGEnumLiteralDeclaration_57());
                    			

                    }


                    }
                    break;
                case 59 :
                    // InternalGloe.g:2568:3: (enumLiteral_58= 'Brand' )
                    {
                    // InternalGloe.g:2568:3: (enumLiteral_58= 'Brand' )
                    // InternalGloe.g:2569:4: enumLiteral_58= 'Brand'
                    {
                    enumLiteral_58=(Token)match(input,145,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getBRANDEnumLiteralDeclaration_58().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_58, grammarAccess.getChampionAccess().getBRANDEnumLiteralDeclaration_58());
                    			

                    }


                    }
                    break;
                case 60 :
                    // InternalGloe.g:2576:3: (enumLiteral_59= 'Lee Sin' )
                    {
                    // InternalGloe.g:2576:3: (enumLiteral_59= 'Lee Sin' )
                    // InternalGloe.g:2577:4: enumLiteral_59= 'Lee Sin'
                    {
                    enumLiteral_59=(Token)match(input,146,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLEESINEnumLiteralDeclaration_59().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_59, grammarAccess.getChampionAccess().getLEESINEnumLiteralDeclaration_59());
                    			

                    }


                    }
                    break;
                case 61 :
                    // InternalGloe.g:2584:3: (enumLiteral_60= 'Vayne' )
                    {
                    // InternalGloe.g:2584:3: (enumLiteral_60= 'Vayne' )
                    // InternalGloe.g:2585:4: enumLiteral_60= 'Vayne'
                    {
                    enumLiteral_60=(Token)match(input,147,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVAYNEEnumLiteralDeclaration_60().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_60, grammarAccess.getChampionAccess().getVAYNEEnumLiteralDeclaration_60());
                    			

                    }


                    }
                    break;
                case 62 :
                    // InternalGloe.g:2592:3: (enumLiteral_61= 'Rumble' )
                    {
                    // InternalGloe.g:2592:3: (enumLiteral_61= 'Rumble' )
                    // InternalGloe.g:2593:4: enumLiteral_61= 'Rumble'
                    {
                    enumLiteral_61=(Token)match(input,148,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRUMBLEEnumLiteralDeclaration_61().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_61, grammarAccess.getChampionAccess().getRUMBLEEnumLiteralDeclaration_61());
                    			

                    }


                    }
                    break;
                case 63 :
                    // InternalGloe.g:2600:3: (enumLiteral_62= 'Cassiopeia' )
                    {
                    // InternalGloe.g:2600:3: (enumLiteral_62= 'Cassiopeia' )
                    // InternalGloe.g:2601:4: enumLiteral_62= 'Cassiopeia'
                    {
                    enumLiteral_62=(Token)match(input,149,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getCASSIOPEIAEnumLiteralDeclaration_62().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_62, grammarAccess.getChampionAccess().getCASSIOPEIAEnumLiteralDeclaration_62());
                    			

                    }


                    }
                    break;
                case 64 :
                    // InternalGloe.g:2608:3: (enumLiteral_63= 'Skarner' )
                    {
                    // InternalGloe.g:2608:3: (enumLiteral_63= 'Skarner' )
                    // InternalGloe.g:2609:4: enumLiteral_63= 'Skarner'
                    {
                    enumLiteral_63=(Token)match(input,150,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSKARNEREnumLiteralDeclaration_63().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_63, grammarAccess.getChampionAccess().getSKARNEREnumLiteralDeclaration_63());
                    			

                    }


                    }
                    break;
                case 65 :
                    // InternalGloe.g:2616:3: (enumLiteral_64= 'Heimerdinger' )
                    {
                    // InternalGloe.g:2616:3: (enumLiteral_64= 'Heimerdinger' )
                    // InternalGloe.g:2617:4: enumLiteral_64= 'Heimerdinger'
                    {
                    enumLiteral_64=(Token)match(input,151,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getHEIMERDINGEREnumLiteralDeclaration_64().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_64, grammarAccess.getChampionAccess().getHEIMERDINGEREnumLiteralDeclaration_64());
                    			

                    }


                    }
                    break;
                case 66 :
                    // InternalGloe.g:2624:3: (enumLiteral_65= 'Nasus' )
                    {
                    // InternalGloe.g:2624:3: (enumLiteral_65= 'Nasus' )
                    // InternalGloe.g:2625:4: enumLiteral_65= 'Nasus'
                    {
                    enumLiteral_65=(Token)match(input,152,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getNASUSEnumLiteralDeclaration_65().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_65, grammarAccess.getChampionAccess().getNASUSEnumLiteralDeclaration_65());
                    			

                    }


                    }
                    break;
                case 67 :
                    // InternalGloe.g:2632:3: (enumLiteral_66= 'Nidalee' )
                    {
                    // InternalGloe.g:2632:3: (enumLiteral_66= 'Nidalee' )
                    // InternalGloe.g:2633:4: enumLiteral_66= 'Nidalee'
                    {
                    enumLiteral_66=(Token)match(input,153,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getNIDALEEEnumLiteralDeclaration_66().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_66, grammarAccess.getChampionAccess().getNIDALEEEnumLiteralDeclaration_66());
                    			

                    }


                    }
                    break;
                case 68 :
                    // InternalGloe.g:2640:3: (enumLiteral_67= 'Udyr' )
                    {
                    // InternalGloe.g:2640:3: (enumLiteral_67= 'Udyr' )
                    // InternalGloe.g:2641:4: enumLiteral_67= 'Udyr'
                    {
                    enumLiteral_67=(Token)match(input,154,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getUDYREnumLiteralDeclaration_67().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_67, grammarAccess.getChampionAccess().getUDYREnumLiteralDeclaration_67());
                    			

                    }


                    }
                    break;
                case 69 :
                    // InternalGloe.g:2648:3: (enumLiteral_68= 'Poppy' )
                    {
                    // InternalGloe.g:2648:3: (enumLiteral_68= 'Poppy' )
                    // InternalGloe.g:2649:4: enumLiteral_68= 'Poppy'
                    {
                    enumLiteral_68=(Token)match(input,155,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getPOPPYEnumLiteralDeclaration_68().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_68, grammarAccess.getChampionAccess().getPOPPYEnumLiteralDeclaration_68());
                    			

                    }


                    }
                    break;
                case 70 :
                    // InternalGloe.g:2656:3: (enumLiteral_69= 'Gragas' )
                    {
                    // InternalGloe.g:2656:3: (enumLiteral_69= 'Gragas' )
                    // InternalGloe.g:2657:4: enumLiteral_69= 'Gragas'
                    {
                    enumLiteral_69=(Token)match(input,156,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getGRAGASEnumLiteralDeclaration_69().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_69, grammarAccess.getChampionAccess().getGRAGASEnumLiteralDeclaration_69());
                    			

                    }


                    }
                    break;
                case 71 :
                    // InternalGloe.g:2664:3: (enumLiteral_70= 'Pantheon' )
                    {
                    // InternalGloe.g:2664:3: (enumLiteral_70= 'Pantheon' )
                    // InternalGloe.g:2665:4: enumLiteral_70= 'Pantheon'
                    {
                    enumLiteral_70=(Token)match(input,157,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getPANTHEONEnumLiteralDeclaration_70().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_70, grammarAccess.getChampionAccess().getPANTHEONEnumLiteralDeclaration_70());
                    			

                    }


                    }
                    break;
                case 72 :
                    // InternalGloe.g:2672:3: (enumLiteral_71= 'Ezreal' )
                    {
                    // InternalGloe.g:2672:3: (enumLiteral_71= 'Ezreal' )
                    // InternalGloe.g:2673:4: enumLiteral_71= 'Ezreal'
                    {
                    enumLiteral_71=(Token)match(input,158,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getEZREALEnumLiteralDeclaration_71().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_71, grammarAccess.getChampionAccess().getEZREALEnumLiteralDeclaration_71());
                    			

                    }


                    }
                    break;
                case 73 :
                    // InternalGloe.g:2680:3: (enumLiteral_72= 'Mordekaiser' )
                    {
                    // InternalGloe.g:2680:3: (enumLiteral_72= 'Mordekaiser' )
                    // InternalGloe.g:2681:4: enumLiteral_72= 'Mordekaiser'
                    {
                    enumLiteral_72=(Token)match(input,159,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMORDEKAISEREnumLiteralDeclaration_72().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_72, grammarAccess.getChampionAccess().getMORDEKAISEREnumLiteralDeclaration_72());
                    			

                    }


                    }
                    break;
                case 74 :
                    // InternalGloe.g:2688:3: (enumLiteral_73= 'Yorick' )
                    {
                    // InternalGloe.g:2688:3: (enumLiteral_73= 'Yorick' )
                    // InternalGloe.g:2689:4: enumLiteral_73= 'Yorick'
                    {
                    enumLiteral_73=(Token)match(input,160,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getYORICKEnumLiteralDeclaration_73().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_73, grammarAccess.getChampionAccess().getYORICKEnumLiteralDeclaration_73());
                    			

                    }


                    }
                    break;
                case 75 :
                    // InternalGloe.g:2696:3: (enumLiteral_74= 'Akali' )
                    {
                    // InternalGloe.g:2696:3: (enumLiteral_74= 'Akali' )
                    // InternalGloe.g:2697:4: enumLiteral_74= 'Akali'
                    {
                    enumLiteral_74=(Token)match(input,161,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getAKALIEnumLiteralDeclaration_74().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_74, grammarAccess.getChampionAccess().getAKALIEnumLiteralDeclaration_74());
                    			

                    }


                    }
                    break;
                case 76 :
                    // InternalGloe.g:2704:3: (enumLiteral_75= 'Kennen' )
                    {
                    // InternalGloe.g:2704:3: (enumLiteral_75= 'Kennen' )
                    // InternalGloe.g:2705:4: enumLiteral_75= 'Kennen'
                    {
                    enumLiteral_75=(Token)match(input,162,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKENNENEnumLiteralDeclaration_75().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_75, grammarAccess.getChampionAccess().getKENNENEnumLiteralDeclaration_75());
                    			

                    }


                    }
                    break;
                case 77 :
                    // InternalGloe.g:2712:3: (enumLiteral_76= 'Garen' )
                    {
                    // InternalGloe.g:2712:3: (enumLiteral_76= 'Garen' )
                    // InternalGloe.g:2713:4: enumLiteral_76= 'Garen'
                    {
                    enumLiteral_76=(Token)match(input,163,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getGARENEnumLiteralDeclaration_76().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_76, grammarAccess.getChampionAccess().getGARENEnumLiteralDeclaration_76());
                    			

                    }


                    }
                    break;
                case 78 :
                    // InternalGloe.g:2720:3: (enumLiteral_77= 'Leona' )
                    {
                    // InternalGloe.g:2720:3: (enumLiteral_77= 'Leona' )
                    // InternalGloe.g:2721:4: enumLiteral_77= 'Leona'
                    {
                    enumLiteral_77=(Token)match(input,164,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLEONAEnumLiteralDeclaration_77().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_77, grammarAccess.getChampionAccess().getLEONAEnumLiteralDeclaration_77());
                    			

                    }


                    }
                    break;
                case 79 :
                    // InternalGloe.g:2728:3: (enumLiteral_78= 'Malzahar' )
                    {
                    // InternalGloe.g:2728:3: (enumLiteral_78= 'Malzahar' )
                    // InternalGloe.g:2729:4: enumLiteral_78= 'Malzahar'
                    {
                    enumLiteral_78=(Token)match(input,165,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getMALZAHAREnumLiteralDeclaration_78().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_78, grammarAccess.getChampionAccess().getMALZAHAREnumLiteralDeclaration_78());
                    			

                    }


                    }
                    break;
                case 80 :
                    // InternalGloe.g:2736:3: (enumLiteral_79= 'Talon' )
                    {
                    // InternalGloe.g:2736:3: (enumLiteral_79= 'Talon' )
                    // InternalGloe.g:2737:4: enumLiteral_79= 'Talon'
                    {
                    enumLiteral_79=(Token)match(input,166,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTALONEnumLiteralDeclaration_79().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_79, grammarAccess.getChampionAccess().getTALONEnumLiteralDeclaration_79());
                    			

                    }


                    }
                    break;
                case 81 :
                    // InternalGloe.g:2744:3: (enumLiteral_80= 'Riven' )
                    {
                    // InternalGloe.g:2744:3: (enumLiteral_80= 'Riven' )
                    // InternalGloe.g:2745:4: enumLiteral_80= 'Riven'
                    {
                    enumLiteral_80=(Token)match(input,167,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRIVENEnumLiteralDeclaration_80().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_80, grammarAccess.getChampionAccess().getRIVENEnumLiteralDeclaration_80());
                    			

                    }


                    }
                    break;
                case 82 :
                    // InternalGloe.g:2752:3: (enumLiteral_81= 'Kog\\'Maw' )
                    {
                    // InternalGloe.g:2752:3: (enumLiteral_81= 'Kog\\'Maw' )
                    // InternalGloe.g:2753:4: enumLiteral_81= 'Kog\\'Maw'
                    {
                    enumLiteral_81=(Token)match(input,168,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKOGMAWEnumLiteralDeclaration_81().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_81, grammarAccess.getChampionAccess().getKOGMAWEnumLiteralDeclaration_81());
                    			

                    }


                    }
                    break;
                case 83 :
                    // InternalGloe.g:2760:3: (enumLiteral_82= 'Shen' )
                    {
                    // InternalGloe.g:2760:3: (enumLiteral_82= 'Shen' )
                    // InternalGloe.g:2761:4: enumLiteral_82= 'Shen'
                    {
                    enumLiteral_82=(Token)match(input,169,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSHENEnumLiteralDeclaration_82().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_82, grammarAccess.getChampionAccess().getSHENEnumLiteralDeclaration_82());
                    			

                    }


                    }
                    break;
                case 84 :
                    // InternalGloe.g:2768:3: (enumLiteral_83= 'Lux' )
                    {
                    // InternalGloe.g:2768:3: (enumLiteral_83= 'Lux' )
                    // InternalGloe.g:2769:4: enumLiteral_83= 'Lux'
                    {
                    enumLiteral_83=(Token)match(input,170,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLUXEnumLiteralDeclaration_83().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_83, grammarAccess.getChampionAccess().getLUXEnumLiteralDeclaration_83());
                    			

                    }


                    }
                    break;
                case 85 :
                    // InternalGloe.g:2776:3: (enumLiteral_84= 'Xerath' )
                    {
                    // InternalGloe.g:2776:3: (enumLiteral_84= 'Xerath' )
                    // InternalGloe.g:2777:4: enumLiteral_84= 'Xerath'
                    {
                    enumLiteral_84=(Token)match(input,171,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getXERATHEnumLiteralDeclaration_84().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_84, grammarAccess.getChampionAccess().getXERATHEnumLiteralDeclaration_84());
                    			

                    }


                    }
                    break;
                case 86 :
                    // InternalGloe.g:2784:3: (enumLiteral_85= 'Shyvana' )
                    {
                    // InternalGloe.g:2784:3: (enumLiteral_85= 'Shyvana' )
                    // InternalGloe.g:2785:4: enumLiteral_85= 'Shyvana'
                    {
                    enumLiteral_85=(Token)match(input,172,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSHYVANAEnumLiteralDeclaration_85().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_85, grammarAccess.getChampionAccess().getSHYVANAEnumLiteralDeclaration_85());
                    			

                    }


                    }
                    break;
                case 87 :
                    // InternalGloe.g:2792:3: (enumLiteral_86= 'Ahri' )
                    {
                    // InternalGloe.g:2792:3: (enumLiteral_86= 'Ahri' )
                    // InternalGloe.g:2793:4: enumLiteral_86= 'Ahri'
                    {
                    enumLiteral_86=(Token)match(input,173,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getAHRIEnumLiteralDeclaration_86().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_86, grammarAccess.getChampionAccess().getAHRIEnumLiteralDeclaration_86());
                    			

                    }


                    }
                    break;
                case 88 :
                    // InternalGloe.g:2800:3: (enumLiteral_87= 'Graves' )
                    {
                    // InternalGloe.g:2800:3: (enumLiteral_87= 'Graves' )
                    // InternalGloe.g:2801:4: enumLiteral_87= 'Graves'
                    {
                    enumLiteral_87=(Token)match(input,174,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getGRAVESEnumLiteralDeclaration_87().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_87, grammarAccess.getChampionAccess().getGRAVESEnumLiteralDeclaration_87());
                    			

                    }


                    }
                    break;
                case 89 :
                    // InternalGloe.g:2808:3: (enumLiteral_88= 'Fizz' )
                    {
                    // InternalGloe.g:2808:3: (enumLiteral_88= 'Fizz' )
                    // InternalGloe.g:2809:4: enumLiteral_88= 'Fizz'
                    {
                    enumLiteral_88=(Token)match(input,175,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getFIZZEnumLiteralDeclaration_88().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_88, grammarAccess.getChampionAccess().getFIZZEnumLiteralDeclaration_88());
                    			

                    }


                    }
                    break;
                case 90 :
                    // InternalGloe.g:2816:3: (enumLiteral_89= 'Volibear' )
                    {
                    // InternalGloe.g:2816:3: (enumLiteral_89= 'Volibear' )
                    // InternalGloe.g:2817:4: enumLiteral_89= 'Volibear'
                    {
                    enumLiteral_89=(Token)match(input,176,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVOLIBEAREnumLiteralDeclaration_89().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_89, grammarAccess.getChampionAccess().getVOLIBEAREnumLiteralDeclaration_89());
                    			

                    }


                    }
                    break;
                case 91 :
                    // InternalGloe.g:2824:3: (enumLiteral_90= 'Rengar' )
                    {
                    // InternalGloe.g:2824:3: (enumLiteral_90= 'Rengar' )
                    // InternalGloe.g:2825:4: enumLiteral_90= 'Rengar'
                    {
                    enumLiteral_90=(Token)match(input,177,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRENGAREnumLiteralDeclaration_90().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_90, grammarAccess.getChampionAccess().getRENGAREnumLiteralDeclaration_90());
                    			

                    }


                    }
                    break;
                case 92 :
                    // InternalGloe.g:2832:3: (enumLiteral_91= 'Varus' )
                    {
                    // InternalGloe.g:2832:3: (enumLiteral_91= 'Varus' )
                    // InternalGloe.g:2833:4: enumLiteral_91= 'Varus'
                    {
                    enumLiteral_91=(Token)match(input,178,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVARUSEnumLiteralDeclaration_91().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_91, grammarAccess.getChampionAccess().getVARUSEnumLiteralDeclaration_91());
                    			

                    }


                    }
                    break;
                case 93 :
                    // InternalGloe.g:2840:3: (enumLiteral_92= 'Nautilus' )
                    {
                    // InternalGloe.g:2840:3: (enumLiteral_92= 'Nautilus' )
                    // InternalGloe.g:2841:4: enumLiteral_92= 'Nautilus'
                    {
                    enumLiteral_92=(Token)match(input,179,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getNAUTILUSEnumLiteralDeclaration_92().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_92, grammarAccess.getChampionAccess().getNAUTILUSEnumLiteralDeclaration_92());
                    			

                    }


                    }
                    break;
                case 94 :
                    // InternalGloe.g:2848:3: (enumLiteral_93= 'Viktor' )
                    {
                    // InternalGloe.g:2848:3: (enumLiteral_93= 'Viktor' )
                    // InternalGloe.g:2849:4: enumLiteral_93= 'Viktor'
                    {
                    enumLiteral_93=(Token)match(input,180,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVIKTOREnumLiteralDeclaration_93().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_93, grammarAccess.getChampionAccess().getVIKTOREnumLiteralDeclaration_93());
                    			

                    }


                    }
                    break;
                case 95 :
                    // InternalGloe.g:2856:3: (enumLiteral_94= 'Sejuani' )
                    {
                    // InternalGloe.g:2856:3: (enumLiteral_94= 'Sejuani' )
                    // InternalGloe.g:2857:4: enumLiteral_94= 'Sejuani'
                    {
                    enumLiteral_94=(Token)match(input,181,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSEJUANIEnumLiteralDeclaration_94().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_94, grammarAccess.getChampionAccess().getSEJUANIEnumLiteralDeclaration_94());
                    			

                    }


                    }
                    break;
                case 96 :
                    // InternalGloe.g:2864:3: (enumLiteral_95= 'Fiora' )
                    {
                    // InternalGloe.g:2864:3: (enumLiteral_95= 'Fiora' )
                    // InternalGloe.g:2865:4: enumLiteral_95= 'Fiora'
                    {
                    enumLiteral_95=(Token)match(input,182,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getFIORAEnumLiteralDeclaration_95().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_95, grammarAccess.getChampionAccess().getFIORAEnumLiteralDeclaration_95());
                    			

                    }


                    }
                    break;
                case 97 :
                    // InternalGloe.g:2872:3: (enumLiteral_96= 'Ziggs' )
                    {
                    // InternalGloe.g:2872:3: (enumLiteral_96= 'Ziggs' )
                    // InternalGloe.g:2873:4: enumLiteral_96= 'Ziggs'
                    {
                    enumLiteral_96=(Token)match(input,183,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getZIGGSEnumLiteralDeclaration_96().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_96, grammarAccess.getChampionAccess().getZIGGSEnumLiteralDeclaration_96());
                    			

                    }


                    }
                    break;
                case 98 :
                    // InternalGloe.g:2880:3: (enumLiteral_97= 'Lulu' )
                    {
                    // InternalGloe.g:2880:3: (enumLiteral_97= 'Lulu' )
                    // InternalGloe.g:2881:4: enumLiteral_97= 'Lulu'
                    {
                    enumLiteral_97=(Token)match(input,184,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLULUEnumLiteralDeclaration_97().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_97, grammarAccess.getChampionAccess().getLULUEnumLiteralDeclaration_97());
                    			

                    }


                    }
                    break;
                case 99 :
                    // InternalGloe.g:2888:3: (enumLiteral_98= 'Draven' )
                    {
                    // InternalGloe.g:2888:3: (enumLiteral_98= 'Draven' )
                    // InternalGloe.g:2889:4: enumLiteral_98= 'Draven'
                    {
                    enumLiteral_98=(Token)match(input,185,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getDRAVENEnumLiteralDeclaration_98().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_98, grammarAccess.getChampionAccess().getDRAVENEnumLiteralDeclaration_98());
                    			

                    }


                    }
                    break;
                case 100 :
                    // InternalGloe.g:2896:3: (enumLiteral_99= 'Hecarim' )
                    {
                    // InternalGloe.g:2896:3: (enumLiteral_99= 'Hecarim' )
                    // InternalGloe.g:2897:4: enumLiteral_99= 'Hecarim'
                    {
                    enumLiteral_99=(Token)match(input,186,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getHECARIMEnumLiteralDeclaration_99().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_99, grammarAccess.getChampionAccess().getHECARIMEnumLiteralDeclaration_99());
                    			

                    }


                    }
                    break;
                case 101 :
                    // InternalGloe.g:2904:3: (enumLiteral_100= 'Kha\\'Zix' )
                    {
                    // InternalGloe.g:2904:3: (enumLiteral_100= 'Kha\\'Zix' )
                    // InternalGloe.g:2905:4: enumLiteral_100= 'Kha\\'Zix'
                    {
                    enumLiteral_100=(Token)match(input,187,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKHAZIXEnumLiteralDeclaration_100().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_100, grammarAccess.getChampionAccess().getKHAZIXEnumLiteralDeclaration_100());
                    			

                    }


                    }
                    break;
                case 102 :
                    // InternalGloe.g:2912:3: (enumLiteral_101= 'Darius' )
                    {
                    // InternalGloe.g:2912:3: (enumLiteral_101= 'Darius' )
                    // InternalGloe.g:2913:4: enumLiteral_101= 'Darius'
                    {
                    enumLiteral_101=(Token)match(input,188,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getDARIUSEnumLiteralDeclaration_101().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_101, grammarAccess.getChampionAccess().getDARIUSEnumLiteralDeclaration_101());
                    			

                    }


                    }
                    break;
                case 103 :
                    // InternalGloe.g:2920:3: (enumLiteral_102= 'Jayce' )
                    {
                    // InternalGloe.g:2920:3: (enumLiteral_102= 'Jayce' )
                    // InternalGloe.g:2921:4: enumLiteral_102= 'Jayce'
                    {
                    enumLiteral_102=(Token)match(input,189,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getJAYCEEnumLiteralDeclaration_102().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_102, grammarAccess.getChampionAccess().getJAYCEEnumLiteralDeclaration_102());
                    			

                    }


                    }
                    break;
                case 104 :
                    // InternalGloe.g:2928:3: (enumLiteral_103= 'Lissandra' )
                    {
                    // InternalGloe.g:2928:3: (enumLiteral_103= 'Lissandra' )
                    // InternalGloe.g:2929:4: enumLiteral_103= 'Lissandra'
                    {
                    enumLiteral_103=(Token)match(input,190,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLISSANDRAEnumLiteralDeclaration_103().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_103, grammarAccess.getChampionAccess().getLISSANDRAEnumLiteralDeclaration_103());
                    			

                    }


                    }
                    break;
                case 105 :
                    // InternalGloe.g:2936:3: (enumLiteral_104= 'Diana' )
                    {
                    // InternalGloe.g:2936:3: (enumLiteral_104= 'Diana' )
                    // InternalGloe.g:2937:4: enumLiteral_104= 'Diana'
                    {
                    enumLiteral_104=(Token)match(input,191,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getDIANAEnumLiteralDeclaration_104().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_104, grammarAccess.getChampionAccess().getDIANAEnumLiteralDeclaration_104());
                    			

                    }


                    }
                    break;
                case 106 :
                    // InternalGloe.g:2944:3: (enumLiteral_105= 'Quinn' )
                    {
                    // InternalGloe.g:2944:3: (enumLiteral_105= 'Quinn' )
                    // InternalGloe.g:2945:4: enumLiteral_105= 'Quinn'
                    {
                    enumLiteral_105=(Token)match(input,192,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getQUINNEnumLiteralDeclaration_105().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_105, grammarAccess.getChampionAccess().getQUINNEnumLiteralDeclaration_105());
                    			

                    }


                    }
                    break;
                case 107 :
                    // InternalGloe.g:2952:3: (enumLiteral_106= 'Syndra' )
                    {
                    // InternalGloe.g:2952:3: (enumLiteral_106= 'Syndra' )
                    // InternalGloe.g:2953:4: enumLiteral_106= 'Syndra'
                    {
                    enumLiteral_106=(Token)match(input,193,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getSYNDRAEnumLiteralDeclaration_106().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_106, grammarAccess.getChampionAccess().getSYNDRAEnumLiteralDeclaration_106());
                    			

                    }


                    }
                    break;
                case 108 :
                    // InternalGloe.g:2960:3: (enumLiteral_107= 'Aurelion Sol' )
                    {
                    // InternalGloe.g:2960:3: (enumLiteral_107= 'Aurelion Sol' )
                    // InternalGloe.g:2961:4: enumLiteral_107= 'Aurelion Sol'
                    {
                    enumLiteral_107=(Token)match(input,194,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getAURELIONSOLEnumLiteralDeclaration_107().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_107, grammarAccess.getChampionAccess().getAURELIONSOLEnumLiteralDeclaration_107());
                    			

                    }


                    }
                    break;
                case 109 :
                    // InternalGloe.g:2968:3: (enumLiteral_108= 'Kayn' )
                    {
                    // InternalGloe.g:2968:3: (enumLiteral_108= 'Kayn' )
                    // InternalGloe.g:2969:4: enumLiteral_108= 'Kayn'
                    {
                    enumLiteral_108=(Token)match(input,195,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKAYNEnumLiteralDeclaration_108().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_108, grammarAccess.getChampionAccess().getKAYNEnumLiteralDeclaration_108());
                    			

                    }


                    }
                    break;
                case 110 :
                    // InternalGloe.g:2976:3: (enumLiteral_109= 'Zoe' )
                    {
                    // InternalGloe.g:2976:3: (enumLiteral_109= 'Zoe' )
                    // InternalGloe.g:2977:4: enumLiteral_109= 'Zoe'
                    {
                    enumLiteral_109=(Token)match(input,196,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getZOEEnumLiteralDeclaration_109().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_109, grammarAccess.getChampionAccess().getZOEEnumLiteralDeclaration_109());
                    			

                    }


                    }
                    break;
                case 111 :
                    // InternalGloe.g:2984:3: (enumLiteral_110= 'Zyra' )
                    {
                    // InternalGloe.g:2984:3: (enumLiteral_110= 'Zyra' )
                    // InternalGloe.g:2985:4: enumLiteral_110= 'Zyra'
                    {
                    enumLiteral_110=(Token)match(input,197,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getZYRAEnumLiteralDeclaration_110().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_110, grammarAccess.getChampionAccess().getZYRAEnumLiteralDeclaration_110());
                    			

                    }


                    }
                    break;
                case 112 :
                    // InternalGloe.g:2992:3: (enumLiteral_111= 'Kai\\'Sa' )
                    {
                    // InternalGloe.g:2992:3: (enumLiteral_111= 'Kai\\'Sa' )
                    // InternalGloe.g:2993:4: enumLiteral_111= 'Kai\\'Sa'
                    {
                    enumLiteral_111=(Token)match(input,198,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKAISAEnumLiteralDeclaration_111().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_111, grammarAccess.getChampionAccess().getKAISAEnumLiteralDeclaration_111());
                    			

                    }


                    }
                    break;
                case 113 :
                    // InternalGloe.g:3000:3: (enumLiteral_112= 'Gnar' )
                    {
                    // InternalGloe.g:3000:3: (enumLiteral_112= 'Gnar' )
                    // InternalGloe.g:3001:4: enumLiteral_112= 'Gnar'
                    {
                    enumLiteral_112=(Token)match(input,199,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getGNAREnumLiteralDeclaration_112().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_112, grammarAccess.getChampionAccess().getGNAREnumLiteralDeclaration_112());
                    			

                    }


                    }
                    break;
                case 114 :
                    // InternalGloe.g:3008:3: (enumLiteral_113= 'Zac' )
                    {
                    // InternalGloe.g:3008:3: (enumLiteral_113= 'Zac' )
                    // InternalGloe.g:3009:4: enumLiteral_113= 'Zac'
                    {
                    enumLiteral_113=(Token)match(input,200,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getZACEnumLiteralDeclaration_113().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_113, grammarAccess.getChampionAccess().getZACEnumLiteralDeclaration_113());
                    			

                    }


                    }
                    break;
                case 115 :
                    // InternalGloe.g:3016:3: (enumLiteral_114= 'Yasuo' )
                    {
                    // InternalGloe.g:3016:3: (enumLiteral_114= 'Yasuo' )
                    // InternalGloe.g:3017:4: enumLiteral_114= 'Yasuo'
                    {
                    enumLiteral_114=(Token)match(input,201,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getYASUOEnumLiteralDeclaration_114().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_114, grammarAccess.getChampionAccess().getYASUOEnumLiteralDeclaration_114());
                    			

                    }


                    }
                    break;
                case 116 :
                    // InternalGloe.g:3024:3: (enumLiteral_115= 'Vel\\'Koz' )
                    {
                    // InternalGloe.g:3024:3: (enumLiteral_115= 'Vel\\'Koz' )
                    // InternalGloe.g:3025:4: enumLiteral_115= 'Vel\\'Koz'
                    {
                    enumLiteral_115=(Token)match(input,202,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVELKOZEnumLiteralDeclaration_115().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_115, grammarAccess.getChampionAccess().getVELKOZEnumLiteralDeclaration_115());
                    			

                    }


                    }
                    break;
                case 117 :
                    // InternalGloe.g:3032:3: (enumLiteral_116= 'Taliyah' )
                    {
                    // InternalGloe.g:3032:3: (enumLiteral_116= 'Taliyah' )
                    // InternalGloe.g:3033:4: enumLiteral_116= 'Taliyah'
                    {
                    enumLiteral_116=(Token)match(input,203,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTALIYAHEnumLiteralDeclaration_116().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_116, grammarAccess.getChampionAccess().getTALIYAHEnumLiteralDeclaration_116());
                    			

                    }


                    }
                    break;
                case 118 :
                    // InternalGloe.g:3040:3: (enumLiteral_117= 'Camille' )
                    {
                    // InternalGloe.g:3040:3: (enumLiteral_117= 'Camille' )
                    // InternalGloe.g:3041:4: enumLiteral_117= 'Camille'
                    {
                    enumLiteral_117=(Token)match(input,204,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getCAMILLEEnumLiteralDeclaration_117().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_117, grammarAccess.getChampionAccess().getCAMILLEEnumLiteralDeclaration_117());
                    			

                    }


                    }
                    break;
                case 119 :
                    // InternalGloe.g:3048:3: (enumLiteral_118= 'Braum' )
                    {
                    // InternalGloe.g:3048:3: (enumLiteral_118= 'Braum' )
                    // InternalGloe.g:3049:4: enumLiteral_118= 'Braum'
                    {
                    enumLiteral_118=(Token)match(input,205,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getBRAUMEnumLiteralDeclaration_118().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_118, grammarAccess.getChampionAccess().getBRAUMEnumLiteralDeclaration_118());
                    			

                    }


                    }
                    break;
                case 120 :
                    // InternalGloe.g:3056:3: (enumLiteral_119= 'Jhin' )
                    {
                    // InternalGloe.g:3056:3: (enumLiteral_119= 'Jhin' )
                    // InternalGloe.g:3057:4: enumLiteral_119= 'Jhin'
                    {
                    enumLiteral_119=(Token)match(input,206,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getJHINEnumLiteralDeclaration_119().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_119, grammarAccess.getChampionAccess().getJHINEnumLiteralDeclaration_119());
                    			

                    }


                    }
                    break;
                case 121 :
                    // InternalGloe.g:3064:3: (enumLiteral_120= 'Kindred' )
                    {
                    // InternalGloe.g:3064:3: (enumLiteral_120= 'Kindred' )
                    // InternalGloe.g:3065:4: enumLiteral_120= 'Kindred'
                    {
                    enumLiteral_120=(Token)match(input,207,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKINDREDEnumLiteralDeclaration_120().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_120, grammarAccess.getChampionAccess().getKINDREDEnumLiteralDeclaration_120());
                    			

                    }


                    }
                    break;
                case 122 :
                    // InternalGloe.g:3072:3: (enumLiteral_121= 'Jinx' )
                    {
                    // InternalGloe.g:3072:3: (enumLiteral_121= 'Jinx' )
                    // InternalGloe.g:3073:4: enumLiteral_121= 'Jinx'
                    {
                    enumLiteral_121=(Token)match(input,208,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getJINXEnumLiteralDeclaration_121().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_121, grammarAccess.getChampionAccess().getJINXEnumLiteralDeclaration_121());
                    			

                    }


                    }
                    break;
                case 123 :
                    // InternalGloe.g:3080:3: (enumLiteral_122= 'Tahm Kench' )
                    {
                    // InternalGloe.g:3080:3: (enumLiteral_122= 'Tahm Kench' )
                    // InternalGloe.g:3081:4: enumLiteral_122= 'Tahm Kench'
                    {
                    enumLiteral_122=(Token)match(input,209,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTAHMKENCHEnumLiteralDeclaration_122().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_122, grammarAccess.getChampionAccess().getTAHMKENCHEnumLiteralDeclaration_122());
                    			

                    }


                    }
                    break;
                case 124 :
                    // InternalGloe.g:3088:3: (enumLiteral_123= 'Lucian' )
                    {
                    // InternalGloe.g:3088:3: (enumLiteral_123= 'Lucian' )
                    // InternalGloe.g:3089:4: enumLiteral_123= 'Lucian'
                    {
                    enumLiteral_123=(Token)match(input,210,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getLUCIANEnumLiteralDeclaration_123().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_123, grammarAccess.getChampionAccess().getLUCIANEnumLiteralDeclaration_123());
                    			

                    }


                    }
                    break;
                case 125 :
                    // InternalGloe.g:3096:3: (enumLiteral_124= 'Zed' )
                    {
                    // InternalGloe.g:3096:3: (enumLiteral_124= 'Zed' )
                    // InternalGloe.g:3097:4: enumLiteral_124= 'Zed'
                    {
                    enumLiteral_124=(Token)match(input,211,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getZEDEnumLiteralDeclaration_124().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_124, grammarAccess.getChampionAccess().getZEDEnumLiteralDeclaration_124());
                    			

                    }


                    }
                    break;
                case 126 :
                    // InternalGloe.g:3104:3: (enumLiteral_125= 'Kled' )
                    {
                    // InternalGloe.g:3104:3: (enumLiteral_125= 'Kled' )
                    // InternalGloe.g:3105:4: enumLiteral_125= 'Kled'
                    {
                    enumLiteral_125=(Token)match(input,212,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKLEDEnumLiteralDeclaration_125().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_125, grammarAccess.getChampionAccess().getKLEDEnumLiteralDeclaration_125());
                    			

                    }


                    }
                    break;
                case 127 :
                    // InternalGloe.g:3112:3: (enumLiteral_126= 'Ekko' )
                    {
                    // InternalGloe.g:3112:3: (enumLiteral_126= 'Ekko' )
                    // InternalGloe.g:3113:4: enumLiteral_126= 'Ekko'
                    {
                    enumLiteral_126=(Token)match(input,213,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getEKKOEnumLiteralDeclaration_126().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_126, grammarAccess.getChampionAccess().getEKKOEnumLiteralDeclaration_126());
                    			

                    }


                    }
                    break;
                case 128 :
                    // InternalGloe.g:3120:3: (enumLiteral_127= 'Vi' )
                    {
                    // InternalGloe.g:3120:3: (enumLiteral_127= 'Vi' )
                    // InternalGloe.g:3121:4: enumLiteral_127= 'Vi'
                    {
                    enumLiteral_127=(Token)match(input,214,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getVIEnumLiteralDeclaration_127().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_127, grammarAccess.getChampionAccess().getVIEnumLiteralDeclaration_127());
                    			

                    }


                    }
                    break;
                case 129 :
                    // InternalGloe.g:3128:3: (enumLiteral_128= 'Aatrox' )
                    {
                    // InternalGloe.g:3128:3: (enumLiteral_128= 'Aatrox' )
                    // InternalGloe.g:3129:4: enumLiteral_128= 'Aatrox'
                    {
                    enumLiteral_128=(Token)match(input,215,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getAATROXEnumLiteralDeclaration_128().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_128, grammarAccess.getChampionAccess().getAATROXEnumLiteralDeclaration_128());
                    			

                    }


                    }
                    break;
                case 130 :
                    // InternalGloe.g:3136:3: (enumLiteral_129= 'Nami' )
                    {
                    // InternalGloe.g:3136:3: (enumLiteral_129= 'Nami' )
                    // InternalGloe.g:3137:4: enumLiteral_129= 'Nami'
                    {
                    enumLiteral_129=(Token)match(input,216,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getNAMIEnumLiteralDeclaration_129().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_129, grammarAccess.getChampionAccess().getNAMIEnumLiteralDeclaration_129());
                    			

                    }


                    }
                    break;
                case 131 :
                    // InternalGloe.g:3144:3: (enumLiteral_130= 'Azir' )
                    {
                    // InternalGloe.g:3144:3: (enumLiteral_130= 'Azir' )
                    // InternalGloe.g:3145:4: enumLiteral_130= 'Azir'
                    {
                    enumLiteral_130=(Token)match(input,217,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getAZIREnumLiteralDeclaration_130().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_130, grammarAccess.getChampionAccess().getAZIREnumLiteralDeclaration_130());
                    			

                    }


                    }
                    break;
                case 132 :
                    // InternalGloe.g:3152:3: (enumLiteral_131= 'Thresh' )
                    {
                    // InternalGloe.g:3152:3: (enumLiteral_131= 'Thresh' )
                    // InternalGloe.g:3153:4: enumLiteral_131= 'Thresh'
                    {
                    enumLiteral_131=(Token)match(input,218,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getTHRESHEnumLiteralDeclaration_131().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_131, grammarAccess.getChampionAccess().getTHRESHEnumLiteralDeclaration_131());
                    			

                    }


                    }
                    break;
                case 133 :
                    // InternalGloe.g:3160:3: (enumLiteral_132= 'Illaoi' )
                    {
                    // InternalGloe.g:3160:3: (enumLiteral_132= 'Illaoi' )
                    // InternalGloe.g:3161:4: enumLiteral_132= 'Illaoi'
                    {
                    enumLiteral_132=(Token)match(input,219,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getILLAOIEnumLiteralDeclaration_132().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_132, grammarAccess.getChampionAccess().getILLAOIEnumLiteralDeclaration_132());
                    			

                    }


                    }
                    break;
                case 134 :
                    // InternalGloe.g:3168:3: (enumLiteral_133= 'Rek\\'Sai' )
                    {
                    // InternalGloe.g:3168:3: (enumLiteral_133= 'Rek\\'Sai' )
                    // InternalGloe.g:3169:4: enumLiteral_133= 'Rek\\'Sai'
                    {
                    enumLiteral_133=(Token)match(input,220,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getREKSAIEnumLiteralDeclaration_133().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_133, grammarAccess.getChampionAccess().getREKSAIEnumLiteralDeclaration_133());
                    			

                    }


                    }
                    break;
                case 135 :
                    // InternalGloe.g:3176:3: (enumLiteral_134= 'Ivern' )
                    {
                    // InternalGloe.g:3176:3: (enumLiteral_134= 'Ivern' )
                    // InternalGloe.g:3177:4: enumLiteral_134= 'Ivern'
                    {
                    enumLiteral_134=(Token)match(input,221,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getIVERNEnumLiteralDeclaration_134().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_134, grammarAccess.getChampionAccess().getIVERNEnumLiteralDeclaration_134());
                    			

                    }


                    }
                    break;
                case 136 :
                    // InternalGloe.g:3184:3: (enumLiteral_135= 'Kalista' )
                    {
                    // InternalGloe.g:3184:3: (enumLiteral_135= 'Kalista' )
                    // InternalGloe.g:3185:4: enumLiteral_135= 'Kalista'
                    {
                    enumLiteral_135=(Token)match(input,222,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getKALISTAEnumLiteralDeclaration_135().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_135, grammarAccess.getChampionAccess().getKALISTAEnumLiteralDeclaration_135());
                    			

                    }


                    }
                    break;
                case 137 :
                    // InternalGloe.g:3192:3: (enumLiteral_136= 'Bard' )
                    {
                    // InternalGloe.g:3192:3: (enumLiteral_136= 'Bard' )
                    // InternalGloe.g:3193:4: enumLiteral_136= 'Bard'
                    {
                    enumLiteral_136=(Token)match(input,223,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getBARDEnumLiteralDeclaration_136().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_136, grammarAccess.getChampionAccess().getBARDEnumLiteralDeclaration_136());
                    			

                    }


                    }
                    break;
                case 138 :
                    // InternalGloe.g:3200:3: (enumLiteral_137= 'Rakan' )
                    {
                    // InternalGloe.g:3200:3: (enumLiteral_137= 'Rakan' )
                    // InternalGloe.g:3201:4: enumLiteral_137= 'Rakan'
                    {
                    enumLiteral_137=(Token)match(input,224,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getRAKANEnumLiteralDeclaration_137().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_137, grammarAccess.getChampionAccess().getRAKANEnumLiteralDeclaration_137());
                    			

                    }


                    }
                    break;
                case 139 :
                    // InternalGloe.g:3208:3: (enumLiteral_138= 'Xayah' )
                    {
                    // InternalGloe.g:3208:3: (enumLiteral_138= 'Xayah' )
                    // InternalGloe.g:3209:4: enumLiteral_138= 'Xayah'
                    {
                    enumLiteral_138=(Token)match(input,225,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getXAYAHEnumLiteralDeclaration_138().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_138, grammarAccess.getChampionAccess().getXAYAHEnumLiteralDeclaration_138());
                    			

                    }


                    }
                    break;
                case 140 :
                    // InternalGloe.g:3216:3: (enumLiteral_139= 'Ornn' )
                    {
                    // InternalGloe.g:3216:3: (enumLiteral_139= 'Ornn' )
                    // InternalGloe.g:3217:4: enumLiteral_139= 'Ornn'
                    {
                    enumLiteral_139=(Token)match(input,226,FOLLOW_2); 

                    				current = grammarAccess.getChampionAccess().getORNNEnumLiteralDeclaration_139().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_139, grammarAccess.getChampionAccess().getORNNEnumLiteralDeclaration_139());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChampion"


    // $ANTLR start "ruleTimeScale"
    // InternalGloe.g:3227:1: ruleTimeScale returns [Enumerator current=null] : ( (enumLiteral_0= 'day' ) | (enumLiteral_1= 'month' ) | (enumLiteral_2= 'year' ) ) ;
    public final Enumerator ruleTimeScale() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;


        	enterRule();

        try {
            // InternalGloe.g:3233:2: ( ( (enumLiteral_0= 'day' ) | (enumLiteral_1= 'month' ) | (enumLiteral_2= 'year' ) ) )
            // InternalGloe.g:3234:2: ( (enumLiteral_0= 'day' ) | (enumLiteral_1= 'month' ) | (enumLiteral_2= 'year' ) )
            {
            // InternalGloe.g:3234:2: ( (enumLiteral_0= 'day' ) | (enumLiteral_1= 'month' ) | (enumLiteral_2= 'year' ) )
            int alt25=3;
            switch ( input.LA(1) ) {
            case 227:
                {
                alt25=1;
                }
                break;
            case 228:
                {
                alt25=2;
                }
                break;
            case 229:
                {
                alt25=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 25, 0, input);

                throw nvae;
            }

            switch (alt25) {
                case 1 :
                    // InternalGloe.g:3235:3: (enumLiteral_0= 'day' )
                    {
                    // InternalGloe.g:3235:3: (enumLiteral_0= 'day' )
                    // InternalGloe.g:3236:4: enumLiteral_0= 'day'
                    {
                    enumLiteral_0=(Token)match(input,227,FOLLOW_2); 

                    				current = grammarAccess.getTimeScaleAccess().getDAYEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTimeScaleAccess().getDAYEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:3243:3: (enumLiteral_1= 'month' )
                    {
                    // InternalGloe.g:3243:3: (enumLiteral_1= 'month' )
                    // InternalGloe.g:3244:4: enumLiteral_1= 'month'
                    {
                    enumLiteral_1=(Token)match(input,228,FOLLOW_2); 

                    				current = grammarAccess.getTimeScaleAccess().getMONTHEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTimeScaleAccess().getMONTHEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:3251:3: (enumLiteral_2= 'year' )
                    {
                    // InternalGloe.g:3251:3: (enumLiteral_2= 'year' )
                    // InternalGloe.g:3252:4: enumLiteral_2= 'year'
                    {
                    enumLiteral_2=(Token)match(input,229,FOLLOW_2); 

                    				current = grammarAccess.getTimeScaleAccess().getYEAREnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getTimeScaleAccess().getYEAREnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTimeScale"


    // $ANTLR start "ruleGameResultWinrateShortcut"
    // InternalGloe.g:3262:1: ruleGameResultWinrateShortcut returns [Enumerator current=null] : (enumLiteral_0= 'winrate' ) ;
    public final Enumerator ruleGameResultWinrateShortcut() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;


        	enterRule();

        try {
            // InternalGloe.g:3268:2: ( (enumLiteral_0= 'winrate' ) )
            // InternalGloe.g:3269:2: (enumLiteral_0= 'winrate' )
            {
            // InternalGloe.g:3269:2: (enumLiteral_0= 'winrate' )
            // InternalGloe.g:3270:3: enumLiteral_0= 'winrate'
            {
            enumLiteral_0=(Token)match(input,230,FOLLOW_2); 

            			current = grammarAccess.getGameResultWinrateShortcutAccess().getWINEnumLiteralDeclaration().getEnumLiteral().getInstance();
            			newLeafNode(enumLiteral_0, grammarAccess.getGameResultWinrateShortcutAccess().getWINEnumLiteralDeclaration());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGameResultWinrateShortcut"


    // $ANTLR start "ruleDiagramType"
    // InternalGloe.g:3279:1: ruleDiagramType returns [Enumerator current=null] : ( (enumLiteral_0= 'pie chart' ) | (enumLiteral_1= '3D pie chart' ) | (enumLiteral_2= 'line chart' ) | (enumLiteral_3= 'bar chart' ) ) ;
    public final Enumerator ruleDiagramType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;


        	enterRule();

        try {
            // InternalGloe.g:3285:2: ( ( (enumLiteral_0= 'pie chart' ) | (enumLiteral_1= '3D pie chart' ) | (enumLiteral_2= 'line chart' ) | (enumLiteral_3= 'bar chart' ) ) )
            // InternalGloe.g:3286:2: ( (enumLiteral_0= 'pie chart' ) | (enumLiteral_1= '3D pie chart' ) | (enumLiteral_2= 'line chart' ) | (enumLiteral_3= 'bar chart' ) )
            {
            // InternalGloe.g:3286:2: ( (enumLiteral_0= 'pie chart' ) | (enumLiteral_1= '3D pie chart' ) | (enumLiteral_2= 'line chart' ) | (enumLiteral_3= 'bar chart' ) )
            int alt26=4;
            switch ( input.LA(1) ) {
            case 231:
                {
                alt26=1;
                }
                break;
            case 232:
                {
                alt26=2;
                }
                break;
            case 233:
                {
                alt26=3;
                }
                break;
            case 234:
                {
                alt26=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 26, 0, input);

                throw nvae;
            }

            switch (alt26) {
                case 1 :
                    // InternalGloe.g:3287:3: (enumLiteral_0= 'pie chart' )
                    {
                    // InternalGloe.g:3287:3: (enumLiteral_0= 'pie chart' )
                    // InternalGloe.g:3288:4: enumLiteral_0= 'pie chart'
                    {
                    enumLiteral_0=(Token)match(input,231,FOLLOW_2); 

                    				current = grammarAccess.getDiagramTypeAccess().getPIE_CHARTEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getDiagramTypeAccess().getPIE_CHARTEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:3295:3: (enumLiteral_1= '3D pie chart' )
                    {
                    // InternalGloe.g:3295:3: (enumLiteral_1= '3D pie chart' )
                    // InternalGloe.g:3296:4: enumLiteral_1= '3D pie chart'
                    {
                    enumLiteral_1=(Token)match(input,232,FOLLOW_2); 

                    				current = grammarAccess.getDiagramTypeAccess().getPIE_CHART_3DEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getDiagramTypeAccess().getPIE_CHART_3DEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:3303:3: (enumLiteral_2= 'line chart' )
                    {
                    // InternalGloe.g:3303:3: (enumLiteral_2= 'line chart' )
                    // InternalGloe.g:3304:4: enumLiteral_2= 'line chart'
                    {
                    enumLiteral_2=(Token)match(input,233,FOLLOW_2); 

                    				current = grammarAccess.getDiagramTypeAccess().getLINE_CHARTEnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getDiagramTypeAccess().getLINE_CHARTEnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:3311:3: (enumLiteral_3= 'bar chart' )
                    {
                    // InternalGloe.g:3311:3: (enumLiteral_3= 'bar chart' )
                    // InternalGloe.g:3312:4: enumLiteral_3= 'bar chart'
                    {
                    enumLiteral_3=(Token)match(input,234,FOLLOW_2); 

                    				current = grammarAccess.getDiagramTypeAccess().getBAR_CHARTEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getDiagramTypeAccess().getBAR_CHARTEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDiagramType"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000003000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000680000L,0x0000000000000000L,0x0000000000000000L,0x0000004000000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000000402D000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000642000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x000000000402C000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000003800000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000004028000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000E2000000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000004030000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000078000000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0xFFFFE06000800000L,0x00000000007FFFFFL});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x00001F9800800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000000L,0xFFFFFFFFFF800000L,0xFFFFFFFFFFFFFFFFL,0x00000007FFFFFFFFL});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000018000000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x00001E0000000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000008000002L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000001800000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000100000002L});

}