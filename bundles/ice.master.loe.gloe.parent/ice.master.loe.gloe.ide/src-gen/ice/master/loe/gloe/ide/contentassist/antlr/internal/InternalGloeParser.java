package ice.master.loe.gloe.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import ice.master.loe.gloe.services.GloeGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGloeParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'winrate'", "'lane'", "'game result'", "'champion'", "'team'", "'victory'", "'defeat'", "'victories'", "'defeats'", "'blue'", "'red'", "'bot'", "'top'", "'mid'", "'jungle'", "'kills'", "'deaths'", "'assists'", "'double kills'", "'triple kills'", "'quadra kills'", "'penta kills'", "'damage dealt'", "'magic damage dealt'", "'physical damage dealt'", "'true damage dealt'", "'damage dealt to champions'", "'magic damage dealt to champions'", "'physical damage dealt to champions'", "'true damage dealt to champions'", "'heal'", "'damage self mitigated'", "'damage dealt to objectives'", "'damage dealt to turrets'", "'time cc'", "'damage taken'", "'magical damage taken'", "'physical damage taken'", "'true damage taken'", "'gold earned'", "'gold spent'", "'turret kills'", "'inhibitor kills'", "'minions killed'", "'neutral minions killed'", "'neutral minions killed team jungle'", "'neutral minions killed enemy jungle'", "'time crowd control dealt'", "'vision wards bought in game'", "'wards placed'", "'wards killed'", "'first blood kill'", "'first blood assist'", "'first tower kill'", "'first tower assist'", "'first inhibitor kill'", "'first inhibitor assist'", "'Annie'", "'Olaf'", "'Galio'", "'Twisted Fate'", "'Xin Zhao'", "'Urgot'", "'Leblanc'", "'Vladimir'", "'Fiddlesticks'", "'Kayle'", "'Master Yi'", "'Alistar'", "'Ryze'", "'Sion'", "'Sivir'", "'Soraka'", "'Teemo'", "'Tristana'", "'Warwick'", "'Nunu'", "'Miss Fortune'", "'Ashe'", "'Tryndamere'", "'Jax'", "'Morgana'", "'Zilean'", "'Singed'", "'Evelynn'", "'Twitch'", "'Karthus'", "'Cho\\'Gath'", "'Amumu'", "'Rammus'", "'Anivia'", "'Shaco'", "'Dr. Mundo'", "'Sona'", "'Kassadin'", "'Irelia'", "'Janna'", "'Gangplank'", "'Corki'", "'Karma'", "'Taric'", "'Veigar'", "'Trundle'", "'Swain'", "'Caitlyn'", "'Blitzcrank'", "'Malphite'", "'Katarina'", "'Nocturne'", "'Maokai'", "'Renekton'", "'Jarvan IV'", "'Elise'", "'Orianna'", "'Wukong'", "'Brand'", "'Lee Sin'", "'Vayne'", "'Rumble'", "'Cassiopeia'", "'Skarner'", "'Heimerdinger'", "'Nasus'", "'Nidalee'", "'Udyr'", "'Poppy'", "'Gragas'", "'Pantheon'", "'Ezreal'", "'Mordekaiser'", "'Yorick'", "'Akali'", "'Kennen'", "'Garen'", "'Leona'", "'Malzahar'", "'Talon'", "'Riven'", "'Kog\\'Maw'", "'Shen'", "'Lux'", "'Xerath'", "'Shyvana'", "'Ahri'", "'Graves'", "'Fizz'", "'Volibear'", "'Rengar'", "'Varus'", "'Nautilus'", "'Viktor'", "'Sejuani'", "'Fiora'", "'Ziggs'", "'Lulu'", "'Draven'", "'Hecarim'", "'Kha\\'Zix'", "'Darius'", "'Jayce'", "'Lissandra'", "'Diana'", "'Quinn'", "'Syndra'", "'Aurelion Sol'", "'Kayn'", "'Zoe'", "'Zyra'", "'Kai\\'Sa'", "'Gnar'", "'Zac'", "'Yasuo'", "'Vel\\'Koz'", "'Taliyah'", "'Camille'", "'Braum'", "'Jhin'", "'Kindred'", "'Jinx'", "'Tahm Kench'", "'Lucian'", "'Zed'", "'Kled'", "'Ekko'", "'Vi'", "'Aatrox'", "'Nami'", "'Azir'", "'Thresh'", "'Illaoi'", "'Rek\\'Sai'", "'Ivern'", "'Kalista'", "'Bard'", "'Rakan'", "'Xayah'", "'Ornn'", "'day'", "'month'", "'year'", "'pie chart'", "'3D pie chart'", "'line chart'", "'bar chart'", "'summoners'", "'display'", "'in'", "'a'", "','", "'by'", "'when'", "'and'", "'total'", "'of'", "'average'", "'ratio'", "'games'", "'with'", "'from'", "'to'", "'is'", "'playing'", "'ends'", "'called'"
    };
    public static final int T__144=144;
    public static final int T__143=143;
    public static final int T__146=146;
    public static final int T__50=50;
    public static final int T__145=145;
    public static final int T__140=140;
    public static final int T__142=142;
    public static final int T__141=141;
    public static final int T__59=59;
    public static final int T__55=55;
    public static final int T__56=56;
    public static final int T__57=57;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__137=137;
    public static final int T__52=52;
    public static final int T__136=136;
    public static final int T__53=53;
    public static final int T__139=139;
    public static final int T__54=54;
    public static final int T__138=138;
    public static final int T__133=133;
    public static final int T__132=132;
    public static final int T__60=60;
    public static final int T__135=135;
    public static final int T__61=61;
    public static final int T__134=134;
    public static final int RULE_ID=5;
    public static final int T__131=131;
    public static final int T__130=130;
    public static final int RULE_INT=6;
    public static final int T__66=66;
    public static final int T__67=67;
    public static final int T__129=129;
    public static final int T__68=68;
    public static final int T__69=69;
    public static final int T__62=62;
    public static final int T__126=126;
    public static final int T__63=63;
    public static final int T__125=125;
    public static final int T__64=64;
    public static final int T__128=128;
    public static final int T__65=65;
    public static final int T__127=127;
    public static final int T__166=166;
    public static final int T__165=165;
    public static final int T__168=168;
    public static final int T__167=167;
    public static final int T__162=162;
    public static final int T__161=161;
    public static final int T__164=164;
    public static final int T__163=163;
    public static final int T__160=160;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int T__159=159;
    public static final int T__30=30;
    public static final int T__158=158;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int T__155=155;
    public static final int T__154=154;
    public static final int T__157=157;
    public static final int T__156=156;
    public static final int T__151=151;
    public static final int T__150=150;
    public static final int T__153=153;
    public static final int T__152=152;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__148=148;
    public static final int T__41=41;
    public static final int T__147=147;
    public static final int T__42=42;
    public static final int T__43=43;
    public static final int T__149=149;
    public static final int T__100=100;
    public static final int T__221=221;
    public static final int T__220=220;
    public static final int T__102=102;
    public static final int T__223=223;
    public static final int T__101=101;
    public static final int T__222=222;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__218=218;
    public static final int T__12=12;
    public static final int T__217=217;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int T__219=219;
    public static final int T__214=214;
    public static final int T__213=213;
    public static final int T__216=216;
    public static final int T__215=215;
    public static final int T__210=210;
    public static final int T__212=212;
    public static final int T__211=211;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int T__207=207;
    public static final int T__23=23;
    public static final int T__206=206;
    public static final int T__24=24;
    public static final int T__209=209;
    public static final int T__25=25;
    public static final int T__208=208;
    public static final int T__203=203;
    public static final int T__202=202;
    public static final int T__20=20;
    public static final int T__205=205;
    public static final int T__21=21;
    public static final int T__204=204;
    public static final int T__122=122;
    public static final int T__121=121;
    public static final int T__124=124;
    public static final int T__123=123;
    public static final int T__120=120;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__119=119;
    public static final int T__118=118;
    public static final int T__115=115;
    public static final int EOF=-1;
    public static final int T__114=114;
    public static final int T__117=117;
    public static final int T__116=116;
    public static final int T__111=111;
    public static final int T__232=232;
    public static final int T__110=110;
    public static final int T__231=231;
    public static final int T__113=113;
    public static final int T__234=234;
    public static final int T__112=112;
    public static final int T__233=233;
    public static final int T__230=230;
    public static final int T__108=108;
    public static final int T__229=229;
    public static final int T__107=107;
    public static final int T__228=228;
    public static final int T__109=109;
    public static final int T__104=104;
    public static final int T__225=225;
    public static final int T__103=103;
    public static final int T__224=224;
    public static final int T__106=106;
    public static final int T__227=227;
    public static final int T__105=105;
    public static final int T__226=226;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__201=201;
    public static final int T__200=200;
    public static final int T__91=91;
    public static final int T__188=188;
    public static final int T__92=92;
    public static final int T__187=187;
    public static final int T__93=93;
    public static final int T__94=94;
    public static final int T__189=189;
    public static final int T__184=184;
    public static final int T__183=183;
    public static final int T__186=186;
    public static final int T__90=90;
    public static final int T__185=185;
    public static final int T__180=180;
    public static final int T__182=182;
    public static final int T__181=181;
    public static final int T__99=99;
    public static final int T__95=95;
    public static final int T__96=96;
    public static final int T__97=97;
    public static final int T__98=98;
    public static final int T__177=177;
    public static final int T__176=176;
    public static final int T__179=179;
    public static final int T__178=178;
    public static final int T__173=173;
    public static final int T__172=172;
    public static final int T__175=175;
    public static final int T__174=174;
    public static final int T__171=171;
    public static final int T__170=170;
    public static final int T__169=169;
    public static final int T__70=70;
    public static final int T__71=71;
    public static final int T__72=72;
    public static final int RULE_STRING=4;
    public static final int T__77=77;
    public static final int T__78=78;
    public static final int T__79=79;
    public static final int T__73=73;
    public static final int T__74=74;
    public static final int T__75=75;
    public static final int T__76=76;
    public static final int T__80=80;
    public static final int T__199=199;
    public static final int T__81=81;
    public static final int T__198=198;
    public static final int T__82=82;
    public static final int T__83=83;
    public static final int T__195=195;
    public static final int T__194=194;
    public static final int RULE_WS=9;
    public static final int T__197=197;
    public static final int T__196=196;
    public static final int T__191=191;
    public static final int T__190=190;
    public static final int T__193=193;
    public static final int T__192=192;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__88=88;
    public static final int T__89=89;
    public static final int T__84=84;
    public static final int T__85=85;
    public static final int T__86=86;
    public static final int T__87=87;

    // delegates
    // delegators


        public InternalGloeParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGloeParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGloeParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGloe.g"; }


    	private GloeGrammarAccess grammarAccess;

    	public void setGrammarAccess(GloeGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleRequest"
    // InternalGloe.g:53:1: entryRuleRequest : ruleRequest EOF ;
    public final void entryRuleRequest() throws RecognitionException {
        try {
            // InternalGloe.g:54:1: ( ruleRequest EOF )
            // InternalGloe.g:55:1: ruleRequest EOF
            {
             before(grammarAccess.getRequestRule()); 
            pushFollow(FOLLOW_1);
            ruleRequest();

            state._fsp--;

             after(grammarAccess.getRequestRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRequest"


    // $ANTLR start "ruleRequest"
    // InternalGloe.g:62:1: ruleRequest : ( ( rule__Request__Group__0 )? ) ;
    public final void ruleRequest() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:66:2: ( ( ( rule__Request__Group__0 )? ) )
            // InternalGloe.g:67:2: ( ( rule__Request__Group__0 )? )
            {
            // InternalGloe.g:67:2: ( ( rule__Request__Group__0 )? )
            // InternalGloe.g:68:3: ( rule__Request__Group__0 )?
            {
             before(grammarAccess.getRequestAccess().getGroup()); 
            // InternalGloe.g:69:3: ( rule__Request__Group__0 )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==215) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalGloe.g:69:4: rule__Request__Group__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Request__Group__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRequestAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRequest"


    // $ANTLR start "entryRuleSummoner"
    // InternalGloe.g:78:1: entryRuleSummoner : ruleSummoner EOF ;
    public final void entryRuleSummoner() throws RecognitionException {
        try {
            // InternalGloe.g:79:1: ( ruleSummoner EOF )
            // InternalGloe.g:80:1: ruleSummoner EOF
            {
             before(grammarAccess.getSummonerRule()); 
            pushFollow(FOLLOW_1);
            ruleSummoner();

            state._fsp--;

             after(grammarAccess.getSummonerRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSummoner"


    // $ANTLR start "ruleSummoner"
    // InternalGloe.g:87:1: ruleSummoner : ( ( rule__Summoner__Group__0 ) ) ;
    public final void ruleSummoner() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:91:2: ( ( ( rule__Summoner__Group__0 ) ) )
            // InternalGloe.g:92:2: ( ( rule__Summoner__Group__0 ) )
            {
            // InternalGloe.g:92:2: ( ( rule__Summoner__Group__0 ) )
            // InternalGloe.g:93:3: ( rule__Summoner__Group__0 )
            {
             before(grammarAccess.getSummonerAccess().getGroup()); 
            // InternalGloe.g:94:3: ( rule__Summoner__Group__0 )
            // InternalGloe.g:94:4: rule__Summoner__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Summoner__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSummonerAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSummoner"


    // $ANTLR start "entryRuleYValue"
    // InternalGloe.g:103:1: entryRuleYValue : ruleYValue EOF ;
    public final void entryRuleYValue() throws RecognitionException {
        try {
            // InternalGloe.g:104:1: ( ruleYValue EOF )
            // InternalGloe.g:105:1: ruleYValue EOF
            {
             before(grammarAccess.getYValueRule()); 
            pushFollow(FOLLOW_1);
            ruleYValue();

            state._fsp--;

             after(grammarAccess.getYValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleYValue"


    // $ANTLR start "ruleYValue"
    // InternalGloe.g:112:1: ruleYValue : ( ( rule__YValue__Alternatives ) ) ;
    public final void ruleYValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:116:2: ( ( ( rule__YValue__Alternatives ) ) )
            // InternalGloe.g:117:2: ( ( rule__YValue__Alternatives ) )
            {
            // InternalGloe.g:117:2: ( ( rule__YValue__Alternatives ) )
            // InternalGloe.g:118:3: ( rule__YValue__Alternatives )
            {
             before(grammarAccess.getYValueAccess().getAlternatives()); 
            // InternalGloe.g:119:3: ( rule__YValue__Alternatives )
            // InternalGloe.g:119:4: rule__YValue__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__YValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getYValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleYValue"


    // $ANTLR start "entryRuleComputedYValue"
    // InternalGloe.g:128:1: entryRuleComputedYValue : ruleComputedYValue EOF ;
    public final void entryRuleComputedYValue() throws RecognitionException {
        try {
            // InternalGloe.g:129:1: ( ruleComputedYValue EOF )
            // InternalGloe.g:130:1: ruleComputedYValue EOF
            {
             before(grammarAccess.getComputedYValueRule()); 
            pushFollow(FOLLOW_1);
            ruleComputedYValue();

            state._fsp--;

             after(grammarAccess.getComputedYValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleComputedYValue"


    // $ANTLR start "ruleComputedYValue"
    // InternalGloe.g:137:1: ruleComputedYValue : ( ( rule__ComputedYValue__Alternatives ) ) ;
    public final void ruleComputedYValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:141:2: ( ( ( rule__ComputedYValue__Alternatives ) ) )
            // InternalGloe.g:142:2: ( ( rule__ComputedYValue__Alternatives ) )
            {
            // InternalGloe.g:142:2: ( ( rule__ComputedYValue__Alternatives ) )
            // InternalGloe.g:143:3: ( rule__ComputedYValue__Alternatives )
            {
             before(grammarAccess.getComputedYValueAccess().getAlternatives()); 
            // InternalGloe.g:144:3: ( rule__ComputedYValue__Alternatives )
            // InternalGloe.g:144:4: rule__ComputedYValue__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ComputedYValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComputedYValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComputedYValue"


    // $ANTLR start "entryRuleSum"
    // InternalGloe.g:153:1: entryRuleSum : ruleSum EOF ;
    public final void entryRuleSum() throws RecognitionException {
        try {
            // InternalGloe.g:154:1: ( ruleSum EOF )
            // InternalGloe.g:155:1: ruleSum EOF
            {
             before(grammarAccess.getSumRule()); 
            pushFollow(FOLLOW_1);
            ruleSum();

            state._fsp--;

             after(grammarAccess.getSumRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleSum"


    // $ANTLR start "ruleSum"
    // InternalGloe.g:162:1: ruleSum : ( ( rule__Sum__Group__0 ) ) ;
    public final void ruleSum() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:166:2: ( ( ( rule__Sum__Group__0 ) ) )
            // InternalGloe.g:167:2: ( ( rule__Sum__Group__0 ) )
            {
            // InternalGloe.g:167:2: ( ( rule__Sum__Group__0 ) )
            // InternalGloe.g:168:3: ( rule__Sum__Group__0 )
            {
             before(grammarAccess.getSumAccess().getGroup()); 
            // InternalGloe.g:169:3: ( rule__Sum__Group__0 )
            // InternalGloe.g:169:4: rule__Sum__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Sum__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getSumAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleSum"


    // $ANTLR start "entryRuleAverage"
    // InternalGloe.g:178:1: entryRuleAverage : ruleAverage EOF ;
    public final void entryRuleAverage() throws RecognitionException {
        try {
            // InternalGloe.g:179:1: ( ruleAverage EOF )
            // InternalGloe.g:180:1: ruleAverage EOF
            {
             before(grammarAccess.getAverageRule()); 
            pushFollow(FOLLOW_1);
            ruleAverage();

            state._fsp--;

             after(grammarAccess.getAverageRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAverage"


    // $ANTLR start "ruleAverage"
    // InternalGloe.g:187:1: ruleAverage : ( ( rule__Average__Group__0 ) ) ;
    public final void ruleAverage() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:191:2: ( ( ( rule__Average__Group__0 ) ) )
            // InternalGloe.g:192:2: ( ( rule__Average__Group__0 ) )
            {
            // InternalGloe.g:192:2: ( ( rule__Average__Group__0 ) )
            // InternalGloe.g:193:3: ( rule__Average__Group__0 )
            {
             before(grammarAccess.getAverageAccess().getGroup()); 
            // InternalGloe.g:194:3: ( rule__Average__Group__0 )
            // InternalGloe.g:194:4: rule__Average__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Average__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAverageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAverage"


    // $ANTLR start "entryRuleRatio"
    // InternalGloe.g:203:1: entryRuleRatio : ruleRatio EOF ;
    public final void entryRuleRatio() throws RecognitionException {
        try {
            // InternalGloe.g:204:1: ( ruleRatio EOF )
            // InternalGloe.g:205:1: ruleRatio EOF
            {
             before(grammarAccess.getRatioRule()); 
            pushFollow(FOLLOW_1);
            ruleRatio();

            state._fsp--;

             after(grammarAccess.getRatioRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRatio"


    // $ANTLR start "ruleRatio"
    // InternalGloe.g:212:1: ruleRatio : ( ( rule__Ratio__Group__0 ) ) ;
    public final void ruleRatio() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:216:2: ( ( ( rule__Ratio__Group__0 ) ) )
            // InternalGloe.g:217:2: ( ( rule__Ratio__Group__0 ) )
            {
            // InternalGloe.g:217:2: ( ( rule__Ratio__Group__0 ) )
            // InternalGloe.g:218:3: ( rule__Ratio__Group__0 )
            {
             before(grammarAccess.getRatioAccess().getGroup()); 
            // InternalGloe.g:219:3: ( rule__Ratio__Group__0 )
            // InternalGloe.g:219:4: rule__Ratio__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Ratio__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRatioAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRatio"


    // $ANTLR start "entryRuleType"
    // InternalGloe.g:228:1: entryRuleType : ruleType EOF ;
    public final void entryRuleType() throws RecognitionException {
        try {
            // InternalGloe.g:229:1: ( ruleType EOF )
            // InternalGloe.g:230:1: ruleType EOF
            {
             before(grammarAccess.getTypeRule()); 
            pushFollow(FOLLOW_1);
            ruleType();

            state._fsp--;

             after(grammarAccess.getTypeRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleType"


    // $ANTLR start "ruleType"
    // InternalGloe.g:237:1: ruleType : ( ( rule__Type__Group__0 ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:241:2: ( ( ( rule__Type__Group__0 ) ) )
            // InternalGloe.g:242:2: ( ( rule__Type__Group__0 ) )
            {
            // InternalGloe.g:242:2: ( ( rule__Type__Group__0 ) )
            // InternalGloe.g:243:3: ( rule__Type__Group__0 )
            {
             before(grammarAccess.getTypeAccess().getGroup()); 
            // InternalGloe.g:244:3: ( rule__Type__Group__0 )
            // InternalGloe.g:244:4: rule__Type__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Type__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "entryRuleValue"
    // InternalGloe.g:253:1: entryRuleValue : ruleValue EOF ;
    public final void entryRuleValue() throws RecognitionException {
        try {
            // InternalGloe.g:254:1: ( ruleValue EOF )
            // InternalGloe.g:255:1: ruleValue EOF
            {
             before(grammarAccess.getValueRule()); 
            pushFollow(FOLLOW_1);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleValue"


    // $ANTLR start "ruleValue"
    // InternalGloe.g:262:1: ruleValue : ( ( rule__Value__Alternatives ) ) ;
    public final void ruleValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:266:2: ( ( ( rule__Value__Alternatives ) ) )
            // InternalGloe.g:267:2: ( ( rule__Value__Alternatives ) )
            {
            // InternalGloe.g:267:2: ( ( rule__Value__Alternatives ) )
            // InternalGloe.g:268:3: ( rule__Value__Alternatives )
            {
             before(grammarAccess.getValueAccess().getAlternatives()); 
            // InternalGloe.g:269:3: ( rule__Value__Alternatives )
            // InternalGloe.g:269:4: rule__Value__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Value__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValue"


    // $ANTLR start "entryRulePluralValue"
    // InternalGloe.g:278:1: entryRulePluralValue : rulePluralValue EOF ;
    public final void entryRulePluralValue() throws RecognitionException {
        try {
            // InternalGloe.g:279:1: ( rulePluralValue EOF )
            // InternalGloe.g:280:1: rulePluralValue EOF
            {
             before(grammarAccess.getPluralValueRule()); 
            pushFollow(FOLLOW_1);
            rulePluralValue();

            state._fsp--;

             after(grammarAccess.getPluralValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePluralValue"


    // $ANTLR start "rulePluralValue"
    // InternalGloe.g:287:1: rulePluralValue : ( ( rule__PluralValue__Alternatives ) ) ;
    public final void rulePluralValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:291:2: ( ( ( rule__PluralValue__Alternatives ) ) )
            // InternalGloe.g:292:2: ( ( rule__PluralValue__Alternatives ) )
            {
            // InternalGloe.g:292:2: ( ( rule__PluralValue__Alternatives ) )
            // InternalGloe.g:293:3: ( rule__PluralValue__Alternatives )
            {
             before(grammarAccess.getPluralValueAccess().getAlternatives()); 
            // InternalGloe.g:294:3: ( rule__PluralValue__Alternatives )
            // InternalGloe.g:294:4: rule__PluralValue__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__PluralValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPluralValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePluralValue"


    // $ANTLR start "entryRuleResultValue"
    // InternalGloe.g:303:1: entryRuleResultValue : ruleResultValue EOF ;
    public final void entryRuleResultValue() throws RecognitionException {
        try {
            // InternalGloe.g:304:1: ( ruleResultValue EOF )
            // InternalGloe.g:305:1: ruleResultValue EOF
            {
             before(grammarAccess.getResultValueRule()); 
            pushFollow(FOLLOW_1);
            ruleResultValue();

            state._fsp--;

             after(grammarAccess.getResultValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleResultValue"


    // $ANTLR start "ruleResultValue"
    // InternalGloe.g:312:1: ruleResultValue : ( ( rule__ResultValue__Group__0 ) ) ;
    public final void ruleResultValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:316:2: ( ( ( rule__ResultValue__Group__0 ) ) )
            // InternalGloe.g:317:2: ( ( rule__ResultValue__Group__0 ) )
            {
            // InternalGloe.g:317:2: ( ( rule__ResultValue__Group__0 ) )
            // InternalGloe.g:318:3: ( rule__ResultValue__Group__0 )
            {
             before(grammarAccess.getResultValueAccess().getGroup()); 
            // InternalGloe.g:319:3: ( rule__ResultValue__Group__0 )
            // InternalGloe.g:319:4: rule__ResultValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ResultValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getResultValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleResultValue"


    // $ANTLR start "entryRulePluralGameResultValue"
    // InternalGloe.g:328:1: entryRulePluralGameResultValue : rulePluralGameResultValue EOF ;
    public final void entryRulePluralGameResultValue() throws RecognitionException {
        try {
            // InternalGloe.g:329:1: ( rulePluralGameResultValue EOF )
            // InternalGloe.g:330:1: rulePluralGameResultValue EOF
            {
             before(grammarAccess.getPluralGameResultValueRule()); 
            pushFollow(FOLLOW_1);
            rulePluralGameResultValue();

            state._fsp--;

             after(grammarAccess.getPluralGameResultValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePluralGameResultValue"


    // $ANTLR start "rulePluralGameResultValue"
    // InternalGloe.g:337:1: rulePluralGameResultValue : ( ( rule__PluralGameResultValue__Group__0 ) ) ;
    public final void rulePluralGameResultValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:341:2: ( ( ( rule__PluralGameResultValue__Group__0 ) ) )
            // InternalGloe.g:342:2: ( ( rule__PluralGameResultValue__Group__0 ) )
            {
            // InternalGloe.g:342:2: ( ( rule__PluralGameResultValue__Group__0 ) )
            // InternalGloe.g:343:3: ( rule__PluralGameResultValue__Group__0 )
            {
             before(grammarAccess.getPluralGameResultValueAccess().getGroup()); 
            // InternalGloe.g:344:3: ( rule__PluralGameResultValue__Group__0 )
            // InternalGloe.g:344:4: rule__PluralGameResultValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__PluralGameResultValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPluralGameResultValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePluralGameResultValue"


    // $ANTLR start "entryRuleTeamValue"
    // InternalGloe.g:353:1: entryRuleTeamValue : ruleTeamValue EOF ;
    public final void entryRuleTeamValue() throws RecognitionException {
        try {
            // InternalGloe.g:354:1: ( ruleTeamValue EOF )
            // InternalGloe.g:355:1: ruleTeamValue EOF
            {
             before(grammarAccess.getTeamValueRule()); 
            pushFollow(FOLLOW_1);
            ruleTeamValue();

            state._fsp--;

             after(grammarAccess.getTeamValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTeamValue"


    // $ANTLR start "ruleTeamValue"
    // InternalGloe.g:362:1: ruleTeamValue : ( ( rule__TeamValue__Group__0 ) ) ;
    public final void ruleTeamValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:366:2: ( ( ( rule__TeamValue__Group__0 ) ) )
            // InternalGloe.g:367:2: ( ( rule__TeamValue__Group__0 ) )
            {
            // InternalGloe.g:367:2: ( ( rule__TeamValue__Group__0 ) )
            // InternalGloe.g:368:3: ( rule__TeamValue__Group__0 )
            {
             before(grammarAccess.getTeamValueAccess().getGroup()); 
            // InternalGloe.g:369:3: ( rule__TeamValue__Group__0 )
            // InternalGloe.g:369:4: rule__TeamValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TeamValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTeamValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTeamValue"


    // $ANTLR start "entryRuleLaneValue"
    // InternalGloe.g:378:1: entryRuleLaneValue : ruleLaneValue EOF ;
    public final void entryRuleLaneValue() throws RecognitionException {
        try {
            // InternalGloe.g:379:1: ( ruleLaneValue EOF )
            // InternalGloe.g:380:1: ruleLaneValue EOF
            {
             before(grammarAccess.getLaneValueRule()); 
            pushFollow(FOLLOW_1);
            ruleLaneValue();

            state._fsp--;

             after(grammarAccess.getLaneValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLaneValue"


    // $ANTLR start "ruleLaneValue"
    // InternalGloe.g:387:1: ruleLaneValue : ( ( rule__LaneValue__Group__0 ) ) ;
    public final void ruleLaneValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:391:2: ( ( ( rule__LaneValue__Group__0 ) ) )
            // InternalGloe.g:392:2: ( ( rule__LaneValue__Group__0 ) )
            {
            // InternalGloe.g:392:2: ( ( rule__LaneValue__Group__0 ) )
            // InternalGloe.g:393:3: ( rule__LaneValue__Group__0 )
            {
             before(grammarAccess.getLaneValueAccess().getGroup()); 
            // InternalGloe.g:394:3: ( rule__LaneValue__Group__0 )
            // InternalGloe.g:394:4: rule__LaneValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LaneValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLaneValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLaneValue"


    // $ANTLR start "entryRuleStatValue"
    // InternalGloe.g:403:1: entryRuleStatValue : ruleStatValue EOF ;
    public final void entryRuleStatValue() throws RecognitionException {
        try {
            // InternalGloe.g:404:1: ( ruleStatValue EOF )
            // InternalGloe.g:405:1: ruleStatValue EOF
            {
             before(grammarAccess.getStatValueRule()); 
            pushFollow(FOLLOW_1);
            ruleStatValue();

            state._fsp--;

             after(grammarAccess.getStatValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStatValue"


    // $ANTLR start "ruleStatValue"
    // InternalGloe.g:412:1: ruleStatValue : ( ( rule__StatValue__Group__0 ) ) ;
    public final void ruleStatValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:416:2: ( ( ( rule__StatValue__Group__0 ) ) )
            // InternalGloe.g:417:2: ( ( rule__StatValue__Group__0 ) )
            {
            // InternalGloe.g:417:2: ( ( rule__StatValue__Group__0 ) )
            // InternalGloe.g:418:3: ( rule__StatValue__Group__0 )
            {
             before(grammarAccess.getStatValueAccess().getGroup()); 
            // InternalGloe.g:419:3: ( rule__StatValue__Group__0 )
            // InternalGloe.g:419:4: rule__StatValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__StatValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getStatValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStatValue"


    // $ANTLR start "entryRuleChampionValue"
    // InternalGloe.g:428:1: entryRuleChampionValue : ruleChampionValue EOF ;
    public final void entryRuleChampionValue() throws RecognitionException {
        try {
            // InternalGloe.g:429:1: ( ruleChampionValue EOF )
            // InternalGloe.g:430:1: ruleChampionValue EOF
            {
             before(grammarAccess.getChampionValueRule()); 
            pushFollow(FOLLOW_1);
            ruleChampionValue();

            state._fsp--;

             after(grammarAccess.getChampionValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChampionValue"


    // $ANTLR start "ruleChampionValue"
    // InternalGloe.g:437:1: ruleChampionValue : ( ( rule__ChampionValue__Group__0 ) ) ;
    public final void ruleChampionValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:441:2: ( ( ( rule__ChampionValue__Group__0 ) ) )
            // InternalGloe.g:442:2: ( ( rule__ChampionValue__Group__0 ) )
            {
            // InternalGloe.g:442:2: ( ( rule__ChampionValue__Group__0 ) )
            // InternalGloe.g:443:3: ( rule__ChampionValue__Group__0 )
            {
             before(grammarAccess.getChampionValueAccess().getGroup()); 
            // InternalGloe.g:444:3: ( rule__ChampionValue__Group__0 )
            // InternalGloe.g:444:4: rule__ChampionValue__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ChampionValue__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getChampionValueAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChampionValue"


    // $ANTLR start "entryRuleTimelapse"
    // InternalGloe.g:453:1: entryRuleTimelapse : ruleTimelapse EOF ;
    public final void entryRuleTimelapse() throws RecognitionException {
        try {
            // InternalGloe.g:454:1: ( ruleTimelapse EOF )
            // InternalGloe.g:455:1: ruleTimelapse EOF
            {
             before(grammarAccess.getTimelapseRule()); 
            pushFollow(FOLLOW_1);
            ruleTimelapse();

            state._fsp--;

             after(grammarAccess.getTimelapseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTimelapse"


    // $ANTLR start "ruleTimelapse"
    // InternalGloe.g:462:1: ruleTimelapse : ( ( rule__Timelapse__Group__0 ) ) ;
    public final void ruleTimelapse() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:466:2: ( ( ( rule__Timelapse__Group__0 ) ) )
            // InternalGloe.g:467:2: ( ( rule__Timelapse__Group__0 ) )
            {
            // InternalGloe.g:467:2: ( ( rule__Timelapse__Group__0 ) )
            // InternalGloe.g:468:3: ( rule__Timelapse__Group__0 )
            {
             before(grammarAccess.getTimelapseAccess().getGroup()); 
            // InternalGloe.g:469:3: ( rule__Timelapse__Group__0 )
            // InternalGloe.g:469:4: rule__Timelapse__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Timelapse__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTimelapseAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTimelapse"


    // $ANTLR start "entryRuleYValueShortcuts"
    // InternalGloe.g:478:1: entryRuleYValueShortcuts : ruleYValueShortcuts EOF ;
    public final void entryRuleYValueShortcuts() throws RecognitionException {
        try {
            // InternalGloe.g:479:1: ( ruleYValueShortcuts EOF )
            // InternalGloe.g:480:1: ruleYValueShortcuts EOF
            {
             before(grammarAccess.getYValueShortcutsRule()); 
            pushFollow(FOLLOW_1);
            ruleYValueShortcuts();

            state._fsp--;

             after(grammarAccess.getYValueShortcutsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleYValueShortcuts"


    // $ANTLR start "ruleYValueShortcuts"
    // InternalGloe.g:487:1: ruleYValueShortcuts : ( ruleWinrate ) ;
    public final void ruleYValueShortcuts() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:491:2: ( ( ruleWinrate ) )
            // InternalGloe.g:492:2: ( ruleWinrate )
            {
            // InternalGloe.g:492:2: ( ruleWinrate )
            // InternalGloe.g:493:3: ruleWinrate
            {
             before(grammarAccess.getYValueShortcutsAccess().getWinrateParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleWinrate();

            state._fsp--;

             after(grammarAccess.getYValueShortcutsAccess().getWinrateParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleYValueShortcuts"


    // $ANTLR start "entryRuleWinrate"
    // InternalGloe.g:503:1: entryRuleWinrate : ruleWinrate EOF ;
    public final void entryRuleWinrate() throws RecognitionException {
        try {
            // InternalGloe.g:504:1: ( ruleWinrate EOF )
            // InternalGloe.g:505:1: ruleWinrate EOF
            {
             before(grammarAccess.getWinrateRule()); 
            pushFollow(FOLLOW_1);
            ruleWinrate();

            state._fsp--;

             after(grammarAccess.getWinrateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWinrate"


    // $ANTLR start "ruleWinrate"
    // InternalGloe.g:512:1: ruleWinrate : ( ( rule__Winrate__Group__0 ) ) ;
    public final void ruleWinrate() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:516:2: ( ( ( rule__Winrate__Group__0 ) ) )
            // InternalGloe.g:517:2: ( ( rule__Winrate__Group__0 ) )
            {
            // InternalGloe.g:517:2: ( ( rule__Winrate__Group__0 ) )
            // InternalGloe.g:518:3: ( rule__Winrate__Group__0 )
            {
             before(grammarAccess.getWinrateAccess().getGroup()); 
            // InternalGloe.g:519:3: ( rule__Winrate__Group__0 )
            // InternalGloe.g:519:4: rule__Winrate__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Winrate__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWinrateAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWinrate"


    // $ANTLR start "entryRuleGameResultValueWinrateShortcut"
    // InternalGloe.g:528:1: entryRuleGameResultValueWinrateShortcut : ruleGameResultValueWinrateShortcut EOF ;
    public final void entryRuleGameResultValueWinrateShortcut() throws RecognitionException {
        try {
            // InternalGloe.g:529:1: ( ruleGameResultValueWinrateShortcut EOF )
            // InternalGloe.g:530:1: ruleGameResultValueWinrateShortcut EOF
            {
             before(grammarAccess.getGameResultValueWinrateShortcutRule()); 
            pushFollow(FOLLOW_1);
            ruleGameResultValueWinrateShortcut();

            state._fsp--;

             after(grammarAccess.getGameResultValueWinrateShortcutRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGameResultValueWinrateShortcut"


    // $ANTLR start "ruleGameResultValueWinrateShortcut"
    // InternalGloe.g:537:1: ruleGameResultValueWinrateShortcut : ( ( rule__GameResultValueWinrateShortcut__Group__0 ) ) ;
    public final void ruleGameResultValueWinrateShortcut() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:541:2: ( ( ( rule__GameResultValueWinrateShortcut__Group__0 ) ) )
            // InternalGloe.g:542:2: ( ( rule__GameResultValueWinrateShortcut__Group__0 ) )
            {
            // InternalGloe.g:542:2: ( ( rule__GameResultValueWinrateShortcut__Group__0 ) )
            // InternalGloe.g:543:3: ( rule__GameResultValueWinrateShortcut__Group__0 )
            {
             before(grammarAccess.getGameResultValueWinrateShortcutAccess().getGroup()); 
            // InternalGloe.g:544:3: ( rule__GameResultValueWinrateShortcut__Group__0 )
            // InternalGloe.g:544:4: rule__GameResultValueWinrateShortcut__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__GameResultValueWinrateShortcut__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getGameResultValueWinrateShortcutAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGameResultValueWinrateShortcut"


    // $ANTLR start "entryRuleFilter"
    // InternalGloe.g:553:1: entryRuleFilter : ruleFilter EOF ;
    public final void entryRuleFilter() throws RecognitionException {
        try {
            // InternalGloe.g:554:1: ( ruleFilter EOF )
            // InternalGloe.g:555:1: ruleFilter EOF
            {
             before(grammarAccess.getFilterRule()); 
            pushFollow(FOLLOW_1);
            ruleFilter();

            state._fsp--;

             after(grammarAccess.getFilterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFilter"


    // $ANTLR start "ruleFilter"
    // InternalGloe.g:562:1: ruleFilter : ( ruleFilterByValue ) ;
    public final void ruleFilter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:566:2: ( ( ruleFilterByValue ) )
            // InternalGloe.g:567:2: ( ruleFilterByValue )
            {
            // InternalGloe.g:567:2: ( ruleFilterByValue )
            // InternalGloe.g:568:3: ruleFilterByValue
            {
             before(grammarAccess.getFilterAccess().getFilterByValueParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleFilterByValue();

            state._fsp--;

             after(grammarAccess.getFilterAccess().getFilterByValueParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFilter"


    // $ANTLR start "entryRuleFilterByValue"
    // InternalGloe.g:578:1: entryRuleFilterByValue : ruleFilterByValue EOF ;
    public final void entryRuleFilterByValue() throws RecognitionException {
        try {
            // InternalGloe.g:579:1: ( ruleFilterByValue EOF )
            // InternalGloe.g:580:1: ruleFilterByValue EOF
            {
             before(grammarAccess.getFilterByValueRule()); 
            pushFollow(FOLLOW_1);
            ruleFilterByValue();

            state._fsp--;

             after(grammarAccess.getFilterByValueRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFilterByValue"


    // $ANTLR start "ruleFilterByValue"
    // InternalGloe.g:587:1: ruleFilterByValue : ( ( rule__FilterByValue__Alternatives ) ) ;
    public final void ruleFilterByValue() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:591:2: ( ( ( rule__FilterByValue__Alternatives ) ) )
            // InternalGloe.g:592:2: ( ( rule__FilterByValue__Alternatives ) )
            {
            // InternalGloe.g:592:2: ( ( rule__FilterByValue__Alternatives ) )
            // InternalGloe.g:593:3: ( rule__FilterByValue__Alternatives )
            {
             before(grammarAccess.getFilterByValueAccess().getAlternatives()); 
            // InternalGloe.g:594:3: ( rule__FilterByValue__Alternatives )
            // InternalGloe.g:594:4: rule__FilterByValue__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getFilterByValueAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFilterByValue"


    // $ANTLR start "entryRuleDiagram"
    // InternalGloe.g:603:1: entryRuleDiagram : ruleDiagram EOF ;
    public final void entryRuleDiagram() throws RecognitionException {
        try {
            // InternalGloe.g:604:1: ( ruleDiagram EOF )
            // InternalGloe.g:605:1: ruleDiagram EOF
            {
             before(grammarAccess.getDiagramRule()); 
            pushFollow(FOLLOW_1);
            ruleDiagram();

            state._fsp--;

             after(grammarAccess.getDiagramRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDiagram"


    // $ANTLR start "ruleDiagram"
    // InternalGloe.g:612:1: ruleDiagram : ( ( rule__Diagram__Group__0 ) ) ;
    public final void ruleDiagram() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:616:2: ( ( ( rule__Diagram__Group__0 ) ) )
            // InternalGloe.g:617:2: ( ( rule__Diagram__Group__0 ) )
            {
            // InternalGloe.g:617:2: ( ( rule__Diagram__Group__0 ) )
            // InternalGloe.g:618:3: ( rule__Diagram__Group__0 )
            {
             before(grammarAccess.getDiagramAccess().getGroup()); 
            // InternalGloe.g:619:3: ( rule__Diagram__Group__0 )
            // InternalGloe.g:619:4: rule__Diagram__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Diagram__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDiagramAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDiagram"


    // $ANTLR start "entryRuleEString"
    // InternalGloe.g:628:1: entryRuleEString : ruleEString EOF ;
    public final void entryRuleEString() throws RecognitionException {
        try {
            // InternalGloe.g:629:1: ( ruleEString EOF )
            // InternalGloe.g:630:1: ruleEString EOF
            {
             before(grammarAccess.getEStringRule()); 
            pushFollow(FOLLOW_1);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getEStringRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEString"


    // $ANTLR start "ruleEString"
    // InternalGloe.g:637:1: ruleEString : ( ( rule__EString__Alternatives ) ) ;
    public final void ruleEString() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:641:2: ( ( ( rule__EString__Alternatives ) ) )
            // InternalGloe.g:642:2: ( ( rule__EString__Alternatives ) )
            {
            // InternalGloe.g:642:2: ( ( rule__EString__Alternatives ) )
            // InternalGloe.g:643:3: ( rule__EString__Alternatives )
            {
             before(grammarAccess.getEStringAccess().getAlternatives()); 
            // InternalGloe.g:644:3: ( rule__EString__Alternatives )
            // InternalGloe.g:644:4: rule__EString__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EString__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEStringAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEString"


    // $ANTLR start "entryRuleEDate"
    // InternalGloe.g:653:1: entryRuleEDate : ruleEDate EOF ;
    public final void entryRuleEDate() throws RecognitionException {
        try {
            // InternalGloe.g:654:1: ( ruleEDate EOF )
            // InternalGloe.g:655:1: ruleEDate EOF
            {
             before(grammarAccess.getEDateRule()); 
            pushFollow(FOLLOW_1);
            ruleEDate();

            state._fsp--;

             after(grammarAccess.getEDateRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleEDate"


    // $ANTLR start "ruleEDate"
    // InternalGloe.g:662:1: ruleEDate : ( ( rule__EDate__Alternatives ) ) ;
    public final void ruleEDate() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:666:2: ( ( ( rule__EDate__Alternatives ) ) )
            // InternalGloe.g:667:2: ( ( rule__EDate__Alternatives ) )
            {
            // InternalGloe.g:667:2: ( ( rule__EDate__Alternatives ) )
            // InternalGloe.g:668:3: ( rule__EDate__Alternatives )
            {
             before(grammarAccess.getEDateAccess().getAlternatives()); 
            // InternalGloe.g:669:3: ( rule__EDate__Alternatives )
            // InternalGloe.g:669:4: rule__EDate__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__EDate__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getEDateAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleEDate"


    // $ANTLR start "ruleValueType"
    // InternalGloe.g:678:1: ruleValueType : ( ( rule__ValueType__Alternatives ) ) ;
    public final void ruleValueType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:682:1: ( ( ( rule__ValueType__Alternatives ) ) )
            // InternalGloe.g:683:2: ( ( rule__ValueType__Alternatives ) )
            {
            // InternalGloe.g:683:2: ( ( rule__ValueType__Alternatives ) )
            // InternalGloe.g:684:3: ( rule__ValueType__Alternatives )
            {
             before(grammarAccess.getValueTypeAccess().getAlternatives()); 
            // InternalGloe.g:685:3: ( rule__ValueType__Alternatives )
            // InternalGloe.g:685:4: rule__ValueType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ValueType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getValueTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleValueType"


    // $ANTLR start "ruleResult"
    // InternalGloe.g:694:1: ruleResult : ( ( rule__Result__Alternatives ) ) ;
    public final void ruleResult() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:698:1: ( ( ( rule__Result__Alternatives ) ) )
            // InternalGloe.g:699:2: ( ( rule__Result__Alternatives ) )
            {
            // InternalGloe.g:699:2: ( ( rule__Result__Alternatives ) )
            // InternalGloe.g:700:3: ( rule__Result__Alternatives )
            {
             before(grammarAccess.getResultAccess().getAlternatives()); 
            // InternalGloe.g:701:3: ( rule__Result__Alternatives )
            // InternalGloe.g:701:4: rule__Result__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Result__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getResultAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleResult"


    // $ANTLR start "rulePluralResult"
    // InternalGloe.g:710:1: rulePluralResult : ( ( rule__PluralResult__Alternatives ) ) ;
    public final void rulePluralResult() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:714:1: ( ( ( rule__PluralResult__Alternatives ) ) )
            // InternalGloe.g:715:2: ( ( rule__PluralResult__Alternatives ) )
            {
            // InternalGloe.g:715:2: ( ( rule__PluralResult__Alternatives ) )
            // InternalGloe.g:716:3: ( rule__PluralResult__Alternatives )
            {
             before(grammarAccess.getPluralResultAccess().getAlternatives()); 
            // InternalGloe.g:717:3: ( rule__PluralResult__Alternatives )
            // InternalGloe.g:717:4: rule__PluralResult__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__PluralResult__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getPluralResultAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePluralResult"


    // $ANTLR start "ruleTeam"
    // InternalGloe.g:726:1: ruleTeam : ( ( rule__Team__Alternatives ) ) ;
    public final void ruleTeam() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:730:1: ( ( ( rule__Team__Alternatives ) ) )
            // InternalGloe.g:731:2: ( ( rule__Team__Alternatives ) )
            {
            // InternalGloe.g:731:2: ( ( rule__Team__Alternatives ) )
            // InternalGloe.g:732:3: ( rule__Team__Alternatives )
            {
             before(grammarAccess.getTeamAccess().getAlternatives()); 
            // InternalGloe.g:733:3: ( rule__Team__Alternatives )
            // InternalGloe.g:733:4: rule__Team__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Team__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTeamAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTeam"


    // $ANTLR start "ruleLane"
    // InternalGloe.g:742:1: ruleLane : ( ( rule__Lane__Alternatives ) ) ;
    public final void ruleLane() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:746:1: ( ( ( rule__Lane__Alternatives ) ) )
            // InternalGloe.g:747:2: ( ( rule__Lane__Alternatives ) )
            {
            // InternalGloe.g:747:2: ( ( rule__Lane__Alternatives ) )
            // InternalGloe.g:748:3: ( rule__Lane__Alternatives )
            {
             before(grammarAccess.getLaneAccess().getAlternatives()); 
            // InternalGloe.g:749:3: ( rule__Lane__Alternatives )
            // InternalGloe.g:749:4: rule__Lane__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Lane__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLaneAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLane"


    // $ANTLR start "ruleStat"
    // InternalGloe.g:758:1: ruleStat : ( ( rule__Stat__Alternatives ) ) ;
    public final void ruleStat() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:762:1: ( ( ( rule__Stat__Alternatives ) ) )
            // InternalGloe.g:763:2: ( ( rule__Stat__Alternatives ) )
            {
            // InternalGloe.g:763:2: ( ( rule__Stat__Alternatives ) )
            // InternalGloe.g:764:3: ( rule__Stat__Alternatives )
            {
             before(grammarAccess.getStatAccess().getAlternatives()); 
            // InternalGloe.g:765:3: ( rule__Stat__Alternatives )
            // InternalGloe.g:765:4: rule__Stat__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Stat__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStatAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStat"


    // $ANTLR start "ruleChampion"
    // InternalGloe.g:774:1: ruleChampion : ( ( rule__Champion__Alternatives ) ) ;
    public final void ruleChampion() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:778:1: ( ( ( rule__Champion__Alternatives ) ) )
            // InternalGloe.g:779:2: ( ( rule__Champion__Alternatives ) )
            {
            // InternalGloe.g:779:2: ( ( rule__Champion__Alternatives ) )
            // InternalGloe.g:780:3: ( rule__Champion__Alternatives )
            {
             before(grammarAccess.getChampionAccess().getAlternatives()); 
            // InternalGloe.g:781:3: ( rule__Champion__Alternatives )
            // InternalGloe.g:781:4: rule__Champion__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Champion__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getChampionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChampion"


    // $ANTLR start "ruleTimeScale"
    // InternalGloe.g:790:1: ruleTimeScale : ( ( rule__TimeScale__Alternatives ) ) ;
    public final void ruleTimeScale() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:794:1: ( ( ( rule__TimeScale__Alternatives ) ) )
            // InternalGloe.g:795:2: ( ( rule__TimeScale__Alternatives ) )
            {
            // InternalGloe.g:795:2: ( ( rule__TimeScale__Alternatives ) )
            // InternalGloe.g:796:3: ( rule__TimeScale__Alternatives )
            {
             before(grammarAccess.getTimeScaleAccess().getAlternatives()); 
            // InternalGloe.g:797:3: ( rule__TimeScale__Alternatives )
            // InternalGloe.g:797:4: rule__TimeScale__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__TimeScale__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTimeScaleAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTimeScale"


    // $ANTLR start "ruleGameResultWinrateShortcut"
    // InternalGloe.g:806:1: ruleGameResultWinrateShortcut : ( ( 'winrate' ) ) ;
    public final void ruleGameResultWinrateShortcut() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:810:1: ( ( ( 'winrate' ) ) )
            // InternalGloe.g:811:2: ( ( 'winrate' ) )
            {
            // InternalGloe.g:811:2: ( ( 'winrate' ) )
            // InternalGloe.g:812:3: ( 'winrate' )
            {
             before(grammarAccess.getGameResultWinrateShortcutAccess().getWINEnumLiteralDeclaration()); 
            // InternalGloe.g:813:3: ( 'winrate' )
            // InternalGloe.g:813:4: 'winrate'
            {
            match(input,11,FOLLOW_2); 

            }

             after(grammarAccess.getGameResultWinrateShortcutAccess().getWINEnumLiteralDeclaration()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGameResultWinrateShortcut"


    // $ANTLR start "ruleDiagramType"
    // InternalGloe.g:822:1: ruleDiagramType : ( ( rule__DiagramType__Alternatives ) ) ;
    public final void ruleDiagramType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:826:1: ( ( ( rule__DiagramType__Alternatives ) ) )
            // InternalGloe.g:827:2: ( ( rule__DiagramType__Alternatives ) )
            {
            // InternalGloe.g:827:2: ( ( rule__DiagramType__Alternatives ) )
            // InternalGloe.g:828:3: ( rule__DiagramType__Alternatives )
            {
             before(grammarAccess.getDiagramTypeAccess().getAlternatives()); 
            // InternalGloe.g:829:3: ( rule__DiagramType__Alternatives )
            // InternalGloe.g:829:4: rule__DiagramType__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DiagramType__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDiagramTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDiagramType"


    // $ANTLR start "rule__YValue__Alternatives"
    // InternalGloe.g:837:1: rule__YValue__Alternatives : ( ( ruleComputedYValue ) | ( ruleYValueShortcuts ) );
    public final void rule__YValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:841:1: ( ( ruleComputedYValue ) | ( ruleYValueShortcuts ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==223||(LA2_0>=225 && LA2_0<=226)) ) {
                alt2=1;
            }
            else if ( (LA2_0==11) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalGloe.g:842:2: ( ruleComputedYValue )
                    {
                    // InternalGloe.g:842:2: ( ruleComputedYValue )
                    // InternalGloe.g:843:3: ruleComputedYValue
                    {
                     before(grammarAccess.getYValueAccess().getComputedYValueParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleComputedYValue();

                    state._fsp--;

                     after(grammarAccess.getYValueAccess().getComputedYValueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:848:2: ( ruleYValueShortcuts )
                    {
                    // InternalGloe.g:848:2: ( ruleYValueShortcuts )
                    // InternalGloe.g:849:3: ruleYValueShortcuts
                    {
                     before(grammarAccess.getYValueAccess().getYValueShortcutsParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleYValueShortcuts();

                    state._fsp--;

                     after(grammarAccess.getYValueAccess().getYValueShortcutsParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__YValue__Alternatives"


    // $ANTLR start "rule__ComputedYValue__Alternatives"
    // InternalGloe.g:858:1: rule__ComputedYValue__Alternatives : ( ( ruleSum ) | ( ruleAverage ) | ( ruleRatio ) );
    public final void rule__ComputedYValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:862:1: ( ( ruleSum ) | ( ruleAverage ) | ( ruleRatio ) )
            int alt3=3;
            switch ( input.LA(1) ) {
            case 223:
                {
                alt3=1;
                }
                break;
            case 225:
                {
                alt3=2;
                }
                break;
            case 226:
                {
                alt3=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }

            switch (alt3) {
                case 1 :
                    // InternalGloe.g:863:2: ( ruleSum )
                    {
                    // InternalGloe.g:863:2: ( ruleSum )
                    // InternalGloe.g:864:3: ruleSum
                    {
                     before(grammarAccess.getComputedYValueAccess().getSumParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleSum();

                    state._fsp--;

                     after(grammarAccess.getComputedYValueAccess().getSumParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:869:2: ( ruleAverage )
                    {
                    // InternalGloe.g:869:2: ( ruleAverage )
                    // InternalGloe.g:870:3: ruleAverage
                    {
                     before(grammarAccess.getComputedYValueAccess().getAverageParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAverage();

                    state._fsp--;

                     after(grammarAccess.getComputedYValueAccess().getAverageParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:875:2: ( ruleRatio )
                    {
                    // InternalGloe.g:875:2: ( ruleRatio )
                    // InternalGloe.g:876:3: ruleRatio
                    {
                     before(grammarAccess.getComputedYValueAccess().getRatioParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleRatio();

                    state._fsp--;

                     after(grammarAccess.getComputedYValueAccess().getRatioParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComputedYValue__Alternatives"


    // $ANTLR start "rule__Value__Alternatives"
    // InternalGloe.g:885:1: rule__Value__Alternatives : ( ( ruleLaneValue ) | ( ruleResultValue ) | ( ruleTeamValue ) | ( ( rule__Value__Group_3__0 ) ) );
    public final void rule__Value__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:889:1: ( ( ruleLaneValue ) | ( ruleResultValue ) | ( ruleTeamValue ) | ( ( rule__Value__Group_3__0 ) ) )
            int alt4=4;
            switch ( input.LA(1) ) {
            case 22:
            case 23:
            case 24:
            case 25:
                {
                alt4=1;
                }
                break;
            case 16:
            case 17:
                {
                alt4=2;
                }
                break;
            case 20:
            case 21:
                {
                alt4=3;
                }
                break;
            case 227:
                {
                alt4=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // InternalGloe.g:890:2: ( ruleLaneValue )
                    {
                    // InternalGloe.g:890:2: ( ruleLaneValue )
                    // InternalGloe.g:891:3: ruleLaneValue
                    {
                     before(grammarAccess.getValueAccess().getLaneValueParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLaneValue();

                    state._fsp--;

                     after(grammarAccess.getValueAccess().getLaneValueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:896:2: ( ruleResultValue )
                    {
                    // InternalGloe.g:896:2: ( ruleResultValue )
                    // InternalGloe.g:897:3: ruleResultValue
                    {
                     before(grammarAccess.getValueAccess().getResultValueParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleResultValue();

                    state._fsp--;

                     after(grammarAccess.getValueAccess().getResultValueParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:902:2: ( ruleTeamValue )
                    {
                    // InternalGloe.g:902:2: ( ruleTeamValue )
                    // InternalGloe.g:903:3: ruleTeamValue
                    {
                     before(grammarAccess.getValueAccess().getTeamValueParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleTeamValue();

                    state._fsp--;

                     after(grammarAccess.getValueAccess().getTeamValueParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:908:2: ( ( rule__Value__Group_3__0 ) )
                    {
                    // InternalGloe.g:908:2: ( ( rule__Value__Group_3__0 ) )
                    // InternalGloe.g:909:3: ( rule__Value__Group_3__0 )
                    {
                     before(grammarAccess.getValueAccess().getGroup_3()); 
                    // InternalGloe.g:910:3: ( rule__Value__Group_3__0 )
                    // InternalGloe.g:910:4: rule__Value__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Value__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getValueAccess().getGroup_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Alternatives"


    // $ANTLR start "rule__PluralValue__Alternatives"
    // InternalGloe.g:918:1: rule__PluralValue__Alternatives : ( ( ruleStatValue ) | ( ( rule__PluralValue__Group_1__0 ) ) | ( rulePluralGameResultValue ) | ( ( rule__PluralValue__Group_3__0 ) ) | ( ( rule__PluralValue__Group_4__0 ) ) );
    public final void rule__PluralValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:922:1: ( ( ruleStatValue ) | ( ( rule__PluralValue__Group_1__0 ) ) | ( rulePluralGameResultValue ) | ( ( rule__PluralValue__Group_3__0 ) ) | ( ( rule__PluralValue__Group_4__0 ) ) )
            int alt5=5;
            switch ( input.LA(1) ) {
            case 26:
            case 27:
            case 28:
            case 29:
            case 30:
            case 31:
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
            case 39:
            case 40:
            case 41:
            case 42:
            case 43:
            case 44:
            case 45:
            case 46:
            case 47:
            case 48:
            case 49:
            case 50:
            case 51:
            case 52:
            case 53:
            case 54:
            case 55:
            case 56:
            case 57:
            case 58:
            case 59:
            case 60:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
                {
                alt5=1;
                }
                break;
            case 227:
                {
                int LA5_2 = input.LA(2);

                if ( (LA5_2==217) ) {
                    int LA5_4 = input.LA(3);

                    if ( ((LA5_4>=20 && LA5_4<=21)) ) {
                        alt5=2;
                    }
                    else if ( ((LA5_4>=22 && LA5_4<=25)) ) {
                        alt5=5;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 4, input);

                        throw nvae;
                    }
                }
                else if ( (LA5_2==228) ) {
                    alt5=4;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 5, 2, input);

                    throw nvae;
                }
                }
                break;
            case 18:
            case 19:
                {
                alt5=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }

            switch (alt5) {
                case 1 :
                    // InternalGloe.g:923:2: ( ruleStatValue )
                    {
                    // InternalGloe.g:923:2: ( ruleStatValue )
                    // InternalGloe.g:924:3: ruleStatValue
                    {
                     before(grammarAccess.getPluralValueAccess().getStatValueParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleStatValue();

                    state._fsp--;

                     after(grammarAccess.getPluralValueAccess().getStatValueParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:929:2: ( ( rule__PluralValue__Group_1__0 ) )
                    {
                    // InternalGloe.g:929:2: ( ( rule__PluralValue__Group_1__0 ) )
                    // InternalGloe.g:930:3: ( rule__PluralValue__Group_1__0 )
                    {
                     before(grammarAccess.getPluralValueAccess().getGroup_1()); 
                    // InternalGloe.g:931:3: ( rule__PluralValue__Group_1__0 )
                    // InternalGloe.g:931:4: rule__PluralValue__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PluralValue__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPluralValueAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:935:2: ( rulePluralGameResultValue )
                    {
                    // InternalGloe.g:935:2: ( rulePluralGameResultValue )
                    // InternalGloe.g:936:3: rulePluralGameResultValue
                    {
                     before(grammarAccess.getPluralValueAccess().getPluralGameResultValueParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    rulePluralGameResultValue();

                    state._fsp--;

                     after(grammarAccess.getPluralValueAccess().getPluralGameResultValueParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:941:2: ( ( rule__PluralValue__Group_3__0 ) )
                    {
                    // InternalGloe.g:941:2: ( ( rule__PluralValue__Group_3__0 ) )
                    // InternalGloe.g:942:3: ( rule__PluralValue__Group_3__0 )
                    {
                     before(grammarAccess.getPluralValueAccess().getGroup_3()); 
                    // InternalGloe.g:943:3: ( rule__PluralValue__Group_3__0 )
                    // InternalGloe.g:943:4: rule__PluralValue__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PluralValue__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPluralValueAccess().getGroup_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGloe.g:947:2: ( ( rule__PluralValue__Group_4__0 ) )
                    {
                    // InternalGloe.g:947:2: ( ( rule__PluralValue__Group_4__0 ) )
                    // InternalGloe.g:948:3: ( rule__PluralValue__Group_4__0 )
                    {
                     before(grammarAccess.getPluralValueAccess().getGroup_4()); 
                    // InternalGloe.g:949:3: ( rule__PluralValue__Group_4__0 )
                    // InternalGloe.g:949:4: rule__PluralValue__Group_4__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__PluralValue__Group_4__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getPluralValueAccess().getGroup_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Alternatives"


    // $ANTLR start "rule__FilterByValue__Alternatives"
    // InternalGloe.g:957:1: rule__FilterByValue__Alternatives : ( ( ( rule__FilterByValue__Group_0__0 ) ) | ( ( rule__FilterByValue__Group_1__0 ) ) | ( ( rule__FilterByValue__Group_2__0 ) ) | ( ( rule__FilterByValue__Group_3__0 ) ) );
    public final void rule__FilterByValue__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:961:1: ( ( ( rule__FilterByValue__Group_0__0 ) ) | ( ( rule__FilterByValue__Group_1__0 ) ) | ( ( rule__FilterByValue__Group_2__0 ) ) | ( ( rule__FilterByValue__Group_3__0 ) ) )
            int alt6=4;
            switch ( input.LA(1) ) {
            case 15:
                {
                alt6=1;
                }
                break;
            case 232:
                {
                alt6=2;
                }
                break;
            case 12:
                {
                alt6=3;
                }
                break;
            case 233:
                {
                alt6=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalGloe.g:962:2: ( ( rule__FilterByValue__Group_0__0 ) )
                    {
                    // InternalGloe.g:962:2: ( ( rule__FilterByValue__Group_0__0 ) )
                    // InternalGloe.g:963:3: ( rule__FilterByValue__Group_0__0 )
                    {
                     before(grammarAccess.getFilterByValueAccess().getGroup_0()); 
                    // InternalGloe.g:964:3: ( rule__FilterByValue__Group_0__0 )
                    // InternalGloe.g:964:4: rule__FilterByValue__Group_0__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FilterByValue__Group_0__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFilterByValueAccess().getGroup_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:968:2: ( ( rule__FilterByValue__Group_1__0 ) )
                    {
                    // InternalGloe.g:968:2: ( ( rule__FilterByValue__Group_1__0 ) )
                    // InternalGloe.g:969:3: ( rule__FilterByValue__Group_1__0 )
                    {
                     before(grammarAccess.getFilterByValueAccess().getGroup_1()); 
                    // InternalGloe.g:970:3: ( rule__FilterByValue__Group_1__0 )
                    // InternalGloe.g:970:4: rule__FilterByValue__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FilterByValue__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFilterByValueAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:974:2: ( ( rule__FilterByValue__Group_2__0 ) )
                    {
                    // InternalGloe.g:974:2: ( ( rule__FilterByValue__Group_2__0 ) )
                    // InternalGloe.g:975:3: ( rule__FilterByValue__Group_2__0 )
                    {
                     before(grammarAccess.getFilterByValueAccess().getGroup_2()); 
                    // InternalGloe.g:976:3: ( rule__FilterByValue__Group_2__0 )
                    // InternalGloe.g:976:4: rule__FilterByValue__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FilterByValue__Group_2__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFilterByValueAccess().getGroup_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:980:2: ( ( rule__FilterByValue__Group_3__0 ) )
                    {
                    // InternalGloe.g:980:2: ( ( rule__FilterByValue__Group_3__0 ) )
                    // InternalGloe.g:981:3: ( rule__FilterByValue__Group_3__0 )
                    {
                     before(grammarAccess.getFilterByValueAccess().getGroup_3()); 
                    // InternalGloe.g:982:3: ( rule__FilterByValue__Group_3__0 )
                    // InternalGloe.g:982:4: rule__FilterByValue__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FilterByValue__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFilterByValueAccess().getGroup_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Alternatives"


    // $ANTLR start "rule__EString__Alternatives"
    // InternalGloe.g:990:1: rule__EString__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EString__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:994:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_STRING) ) {
                alt7=1;
            }
            else if ( (LA7_0==RULE_ID) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalGloe.g:995:2: ( RULE_STRING )
                    {
                    // InternalGloe.g:995:2: ( RULE_STRING )
                    // InternalGloe.g:996:3: RULE_STRING
                    {
                     before(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1001:2: ( RULE_ID )
                    {
                    // InternalGloe.g:1001:2: ( RULE_ID )
                    // InternalGloe.g:1002:3: RULE_ID
                    {
                     before(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEStringAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EString__Alternatives"


    // $ANTLR start "rule__EDate__Alternatives"
    // InternalGloe.g:1011:1: rule__EDate__Alternatives : ( ( RULE_STRING ) | ( RULE_ID ) );
    public final void rule__EDate__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1015:1: ( ( RULE_STRING ) | ( RULE_ID ) )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==RULE_STRING) ) {
                alt8=1;
            }
            else if ( (LA8_0==RULE_ID) ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalGloe.g:1016:2: ( RULE_STRING )
                    {
                    // InternalGloe.g:1016:2: ( RULE_STRING )
                    // InternalGloe.g:1017:3: RULE_STRING
                    {
                     before(grammarAccess.getEDateAccess().getSTRINGTerminalRuleCall_0()); 
                    match(input,RULE_STRING,FOLLOW_2); 
                     after(grammarAccess.getEDateAccess().getSTRINGTerminalRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1022:2: ( RULE_ID )
                    {
                    // InternalGloe.g:1022:2: ( RULE_ID )
                    // InternalGloe.g:1023:3: RULE_ID
                    {
                     before(grammarAccess.getEDateAccess().getIDTerminalRuleCall_1()); 
                    match(input,RULE_ID,FOLLOW_2); 
                     after(grammarAccess.getEDateAccess().getIDTerminalRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__EDate__Alternatives"


    // $ANTLR start "rule__ValueType__Alternatives"
    // InternalGloe.g:1032:1: rule__ValueType__Alternatives : ( ( ( 'lane' ) ) | ( ( 'game result' ) ) | ( ( 'champion' ) ) | ( ( 'team' ) ) );
    public final void rule__ValueType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1036:1: ( ( ( 'lane' ) ) | ( ( 'game result' ) ) | ( ( 'champion' ) ) | ( ( 'team' ) ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case 12:
                {
                alt9=1;
                }
                break;
            case 13:
                {
                alt9=2;
                }
                break;
            case 14:
                {
                alt9=3;
                }
                break;
            case 15:
                {
                alt9=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalGloe.g:1037:2: ( ( 'lane' ) )
                    {
                    // InternalGloe.g:1037:2: ( ( 'lane' ) )
                    // InternalGloe.g:1038:3: ( 'lane' )
                    {
                     before(grammarAccess.getValueTypeAccess().getLANEEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1039:3: ( 'lane' )
                    // InternalGloe.g:1039:4: 'lane'
                    {
                    match(input,12,FOLLOW_2); 

                    }

                     after(grammarAccess.getValueTypeAccess().getLANEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1043:2: ( ( 'game result' ) )
                    {
                    // InternalGloe.g:1043:2: ( ( 'game result' ) )
                    // InternalGloe.g:1044:3: ( 'game result' )
                    {
                     before(grammarAccess.getValueTypeAccess().getGAME_RESULTEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1045:3: ( 'game result' )
                    // InternalGloe.g:1045:4: 'game result'
                    {
                    match(input,13,FOLLOW_2); 

                    }

                     after(grammarAccess.getValueTypeAccess().getGAME_RESULTEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1049:2: ( ( 'champion' ) )
                    {
                    // InternalGloe.g:1049:2: ( ( 'champion' ) )
                    // InternalGloe.g:1050:3: ( 'champion' )
                    {
                     before(grammarAccess.getValueTypeAccess().getCHAMPIONEnumLiteralDeclaration_2()); 
                    // InternalGloe.g:1051:3: ( 'champion' )
                    // InternalGloe.g:1051:4: 'champion'
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getValueTypeAccess().getCHAMPIONEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1055:2: ( ( 'team' ) )
                    {
                    // InternalGloe.g:1055:2: ( ( 'team' ) )
                    // InternalGloe.g:1056:3: ( 'team' )
                    {
                     before(grammarAccess.getValueTypeAccess().getTEAMEnumLiteralDeclaration_3()); 
                    // InternalGloe.g:1057:3: ( 'team' )
                    // InternalGloe.g:1057:4: 'team'
                    {
                    match(input,15,FOLLOW_2); 

                    }

                     after(grammarAccess.getValueTypeAccess().getTEAMEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ValueType__Alternatives"


    // $ANTLR start "rule__Result__Alternatives"
    // InternalGloe.g:1065:1: rule__Result__Alternatives : ( ( ( 'victory' ) ) | ( ( 'defeat' ) ) );
    public final void rule__Result__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1069:1: ( ( ( 'victory' ) ) | ( ( 'defeat' ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==16) ) {
                alt10=1;
            }
            else if ( (LA10_0==17) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalGloe.g:1070:2: ( ( 'victory' ) )
                    {
                    // InternalGloe.g:1070:2: ( ( 'victory' ) )
                    // InternalGloe.g:1071:3: ( 'victory' )
                    {
                     before(grammarAccess.getResultAccess().getWINEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1072:3: ( 'victory' )
                    // InternalGloe.g:1072:4: 'victory'
                    {
                    match(input,16,FOLLOW_2); 

                    }

                     after(grammarAccess.getResultAccess().getWINEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1076:2: ( ( 'defeat' ) )
                    {
                    // InternalGloe.g:1076:2: ( ( 'defeat' ) )
                    // InternalGloe.g:1077:3: ( 'defeat' )
                    {
                     before(grammarAccess.getResultAccess().getLOSEEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1078:3: ( 'defeat' )
                    // InternalGloe.g:1078:4: 'defeat'
                    {
                    match(input,17,FOLLOW_2); 

                    }

                     after(grammarAccess.getResultAccess().getLOSEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Result__Alternatives"


    // $ANTLR start "rule__PluralResult__Alternatives"
    // InternalGloe.g:1086:1: rule__PluralResult__Alternatives : ( ( ( 'victories' ) ) | ( ( 'defeats' ) ) );
    public final void rule__PluralResult__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1090:1: ( ( ( 'victories' ) ) | ( ( 'defeats' ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==18) ) {
                alt11=1;
            }
            else if ( (LA11_0==19) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalGloe.g:1091:2: ( ( 'victories' ) )
                    {
                    // InternalGloe.g:1091:2: ( ( 'victories' ) )
                    // InternalGloe.g:1092:3: ( 'victories' )
                    {
                     before(grammarAccess.getPluralResultAccess().getWINEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1093:3: ( 'victories' )
                    // InternalGloe.g:1093:4: 'victories'
                    {
                    match(input,18,FOLLOW_2); 

                    }

                     after(grammarAccess.getPluralResultAccess().getWINEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1097:2: ( ( 'defeats' ) )
                    {
                    // InternalGloe.g:1097:2: ( ( 'defeats' ) )
                    // InternalGloe.g:1098:3: ( 'defeats' )
                    {
                     before(grammarAccess.getPluralResultAccess().getLOSEEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1099:3: ( 'defeats' )
                    // InternalGloe.g:1099:4: 'defeats'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getPluralResultAccess().getLOSEEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralResult__Alternatives"


    // $ANTLR start "rule__Team__Alternatives"
    // InternalGloe.g:1107:1: rule__Team__Alternatives : ( ( ( 'blue' ) ) | ( ( 'red' ) ) );
    public final void rule__Team__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1111:1: ( ( ( 'blue' ) ) | ( ( 'red' ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==20) ) {
                alt12=1;
            }
            else if ( (LA12_0==21) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalGloe.g:1112:2: ( ( 'blue' ) )
                    {
                    // InternalGloe.g:1112:2: ( ( 'blue' ) )
                    // InternalGloe.g:1113:3: ( 'blue' )
                    {
                     before(grammarAccess.getTeamAccess().getBLUEEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1114:3: ( 'blue' )
                    // InternalGloe.g:1114:4: 'blue'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getTeamAccess().getBLUEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1118:2: ( ( 'red' ) )
                    {
                    // InternalGloe.g:1118:2: ( ( 'red' ) )
                    // InternalGloe.g:1119:3: ( 'red' )
                    {
                     before(grammarAccess.getTeamAccess().getREDEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1120:3: ( 'red' )
                    // InternalGloe.g:1120:4: 'red'
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getTeamAccess().getREDEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Team__Alternatives"


    // $ANTLR start "rule__Lane__Alternatives"
    // InternalGloe.g:1128:1: rule__Lane__Alternatives : ( ( ( 'bot' ) ) | ( ( 'top' ) ) | ( ( 'mid' ) ) | ( ( 'jungle' ) ) );
    public final void rule__Lane__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1132:1: ( ( ( 'bot' ) ) | ( ( 'top' ) ) | ( ( 'mid' ) ) | ( ( 'jungle' ) ) )
            int alt13=4;
            switch ( input.LA(1) ) {
            case 22:
                {
                alt13=1;
                }
                break;
            case 23:
                {
                alt13=2;
                }
                break;
            case 24:
                {
                alt13=3;
                }
                break;
            case 25:
                {
                alt13=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalGloe.g:1133:2: ( ( 'bot' ) )
                    {
                    // InternalGloe.g:1133:2: ( ( 'bot' ) )
                    // InternalGloe.g:1134:3: ( 'bot' )
                    {
                     before(grammarAccess.getLaneAccess().getBOTEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1135:3: ( 'bot' )
                    // InternalGloe.g:1135:4: 'bot'
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getLaneAccess().getBOTEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1139:2: ( ( 'top' ) )
                    {
                    // InternalGloe.g:1139:2: ( ( 'top' ) )
                    // InternalGloe.g:1140:3: ( 'top' )
                    {
                     before(grammarAccess.getLaneAccess().getTOPEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1141:3: ( 'top' )
                    // InternalGloe.g:1141:4: 'top'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getLaneAccess().getTOPEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1145:2: ( ( 'mid' ) )
                    {
                    // InternalGloe.g:1145:2: ( ( 'mid' ) )
                    // InternalGloe.g:1146:3: ( 'mid' )
                    {
                     before(grammarAccess.getLaneAccess().getMIDEnumLiteralDeclaration_2()); 
                    // InternalGloe.g:1147:3: ( 'mid' )
                    // InternalGloe.g:1147:4: 'mid'
                    {
                    match(input,24,FOLLOW_2); 

                    }

                     after(grammarAccess.getLaneAccess().getMIDEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1151:2: ( ( 'jungle' ) )
                    {
                    // InternalGloe.g:1151:2: ( ( 'jungle' ) )
                    // InternalGloe.g:1152:3: ( 'jungle' )
                    {
                     before(grammarAccess.getLaneAccess().getJUNGLEEnumLiteralDeclaration_3()); 
                    // InternalGloe.g:1153:3: ( 'jungle' )
                    // InternalGloe.g:1153:4: 'jungle'
                    {
                    match(input,25,FOLLOW_2); 

                    }

                     after(grammarAccess.getLaneAccess().getJUNGLEEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Lane__Alternatives"


    // $ANTLR start "rule__Stat__Alternatives"
    // InternalGloe.g:1161:1: rule__Stat__Alternatives : ( ( ( 'kills' ) ) | ( ( 'deaths' ) ) | ( ( 'assists' ) ) | ( ( 'double kills' ) ) | ( ( 'triple kills' ) ) | ( ( 'quadra kills' ) ) | ( ( 'penta kills' ) ) | ( ( 'damage dealt' ) ) | ( ( 'magic damage dealt' ) ) | ( ( 'physical damage dealt' ) ) | ( ( 'true damage dealt' ) ) | ( ( 'damage dealt to champions' ) ) | ( ( 'magic damage dealt to champions' ) ) | ( ( 'physical damage dealt to champions' ) ) | ( ( 'true damage dealt to champions' ) ) | ( ( 'heal' ) ) | ( ( 'damage self mitigated' ) ) | ( ( 'damage dealt to objectives' ) ) | ( ( 'damage dealt to turrets' ) ) | ( ( 'time cc' ) ) | ( ( 'damage taken' ) ) | ( ( 'magical damage taken' ) ) | ( ( 'physical damage taken' ) ) | ( ( 'true damage taken' ) ) | ( ( 'gold earned' ) ) | ( ( 'gold spent' ) ) | ( ( 'turret kills' ) ) | ( ( 'inhibitor kills' ) ) | ( ( 'minions killed' ) ) | ( ( 'neutral minions killed' ) ) | ( ( 'neutral minions killed team jungle' ) ) | ( ( 'neutral minions killed enemy jungle' ) ) | ( ( 'time crowd control dealt' ) ) | ( ( 'vision wards bought in game' ) ) | ( ( 'wards placed' ) ) | ( ( 'wards killed' ) ) | ( ( 'first blood kill' ) ) | ( ( 'first blood assist' ) ) | ( ( 'first tower kill' ) ) | ( ( 'first tower assist' ) ) | ( ( 'first inhibitor kill' ) ) | ( ( 'first inhibitor assist' ) ) );
    public final void rule__Stat__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1165:1: ( ( ( 'kills' ) ) | ( ( 'deaths' ) ) | ( ( 'assists' ) ) | ( ( 'double kills' ) ) | ( ( 'triple kills' ) ) | ( ( 'quadra kills' ) ) | ( ( 'penta kills' ) ) | ( ( 'damage dealt' ) ) | ( ( 'magic damage dealt' ) ) | ( ( 'physical damage dealt' ) ) | ( ( 'true damage dealt' ) ) | ( ( 'damage dealt to champions' ) ) | ( ( 'magic damage dealt to champions' ) ) | ( ( 'physical damage dealt to champions' ) ) | ( ( 'true damage dealt to champions' ) ) | ( ( 'heal' ) ) | ( ( 'damage self mitigated' ) ) | ( ( 'damage dealt to objectives' ) ) | ( ( 'damage dealt to turrets' ) ) | ( ( 'time cc' ) ) | ( ( 'damage taken' ) ) | ( ( 'magical damage taken' ) ) | ( ( 'physical damage taken' ) ) | ( ( 'true damage taken' ) ) | ( ( 'gold earned' ) ) | ( ( 'gold spent' ) ) | ( ( 'turret kills' ) ) | ( ( 'inhibitor kills' ) ) | ( ( 'minions killed' ) ) | ( ( 'neutral minions killed' ) ) | ( ( 'neutral minions killed team jungle' ) ) | ( ( 'neutral minions killed enemy jungle' ) ) | ( ( 'time crowd control dealt' ) ) | ( ( 'vision wards bought in game' ) ) | ( ( 'wards placed' ) ) | ( ( 'wards killed' ) ) | ( ( 'first blood kill' ) ) | ( ( 'first blood assist' ) ) | ( ( 'first tower kill' ) ) | ( ( 'first tower assist' ) ) | ( ( 'first inhibitor kill' ) ) | ( ( 'first inhibitor assist' ) ) )
            int alt14=42;
            switch ( input.LA(1) ) {
            case 26:
                {
                alt14=1;
                }
                break;
            case 27:
                {
                alt14=2;
                }
                break;
            case 28:
                {
                alt14=3;
                }
                break;
            case 29:
                {
                alt14=4;
                }
                break;
            case 30:
                {
                alt14=5;
                }
                break;
            case 31:
                {
                alt14=6;
                }
                break;
            case 32:
                {
                alt14=7;
                }
                break;
            case 33:
                {
                alt14=8;
                }
                break;
            case 34:
                {
                alt14=9;
                }
                break;
            case 35:
                {
                alt14=10;
                }
                break;
            case 36:
                {
                alt14=11;
                }
                break;
            case 37:
                {
                alt14=12;
                }
                break;
            case 38:
                {
                alt14=13;
                }
                break;
            case 39:
                {
                alt14=14;
                }
                break;
            case 40:
                {
                alt14=15;
                }
                break;
            case 41:
                {
                alt14=16;
                }
                break;
            case 42:
                {
                alt14=17;
                }
                break;
            case 43:
                {
                alt14=18;
                }
                break;
            case 44:
                {
                alt14=19;
                }
                break;
            case 45:
                {
                alt14=20;
                }
                break;
            case 46:
                {
                alt14=21;
                }
                break;
            case 47:
                {
                alt14=22;
                }
                break;
            case 48:
                {
                alt14=23;
                }
                break;
            case 49:
                {
                alt14=24;
                }
                break;
            case 50:
                {
                alt14=25;
                }
                break;
            case 51:
                {
                alt14=26;
                }
                break;
            case 52:
                {
                alt14=27;
                }
                break;
            case 53:
                {
                alt14=28;
                }
                break;
            case 54:
                {
                alt14=29;
                }
                break;
            case 55:
                {
                alt14=30;
                }
                break;
            case 56:
                {
                alt14=31;
                }
                break;
            case 57:
                {
                alt14=32;
                }
                break;
            case 58:
                {
                alt14=33;
                }
                break;
            case 59:
                {
                alt14=34;
                }
                break;
            case 60:
                {
                alt14=35;
                }
                break;
            case 61:
                {
                alt14=36;
                }
                break;
            case 62:
                {
                alt14=37;
                }
                break;
            case 63:
                {
                alt14=38;
                }
                break;
            case 64:
                {
                alt14=39;
                }
                break;
            case 65:
                {
                alt14=40;
                }
                break;
            case 66:
                {
                alt14=41;
                }
                break;
            case 67:
                {
                alt14=42;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }

            switch (alt14) {
                case 1 :
                    // InternalGloe.g:1166:2: ( ( 'kills' ) )
                    {
                    // InternalGloe.g:1166:2: ( ( 'kills' ) )
                    // InternalGloe.g:1167:3: ( 'kills' )
                    {
                     before(grammarAccess.getStatAccess().getKILLSEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1168:3: ( 'kills' )
                    // InternalGloe.g:1168:4: 'kills'
                    {
                    match(input,26,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getKILLSEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1172:2: ( ( 'deaths' ) )
                    {
                    // InternalGloe.g:1172:2: ( ( 'deaths' ) )
                    // InternalGloe.g:1173:3: ( 'deaths' )
                    {
                     before(grammarAccess.getStatAccess().getDEATHSEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1174:3: ( 'deaths' )
                    // InternalGloe.g:1174:4: 'deaths'
                    {
                    match(input,27,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getDEATHSEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1178:2: ( ( 'assists' ) )
                    {
                    // InternalGloe.g:1178:2: ( ( 'assists' ) )
                    // InternalGloe.g:1179:3: ( 'assists' )
                    {
                     before(grammarAccess.getStatAccess().getASSISTSEnumLiteralDeclaration_2()); 
                    // InternalGloe.g:1180:3: ( 'assists' )
                    // InternalGloe.g:1180:4: 'assists'
                    {
                    match(input,28,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getASSISTSEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1184:2: ( ( 'double kills' ) )
                    {
                    // InternalGloe.g:1184:2: ( ( 'double kills' ) )
                    // InternalGloe.g:1185:3: ( 'double kills' )
                    {
                     before(grammarAccess.getStatAccess().getDOUBLE_KILLSEnumLiteralDeclaration_3()); 
                    // InternalGloe.g:1186:3: ( 'double kills' )
                    // InternalGloe.g:1186:4: 'double kills'
                    {
                    match(input,29,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getDOUBLE_KILLSEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGloe.g:1190:2: ( ( 'triple kills' ) )
                    {
                    // InternalGloe.g:1190:2: ( ( 'triple kills' ) )
                    // InternalGloe.g:1191:3: ( 'triple kills' )
                    {
                     before(grammarAccess.getStatAccess().getTRIPLE_KILLSEnumLiteralDeclaration_4()); 
                    // InternalGloe.g:1192:3: ( 'triple kills' )
                    // InternalGloe.g:1192:4: 'triple kills'
                    {
                    match(input,30,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTRIPLE_KILLSEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGloe.g:1196:2: ( ( 'quadra kills' ) )
                    {
                    // InternalGloe.g:1196:2: ( ( 'quadra kills' ) )
                    // InternalGloe.g:1197:3: ( 'quadra kills' )
                    {
                     before(grammarAccess.getStatAccess().getQUADRA_KILLSEnumLiteralDeclaration_5()); 
                    // InternalGloe.g:1198:3: ( 'quadra kills' )
                    // InternalGloe.g:1198:4: 'quadra kills'
                    {
                    match(input,31,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getQUADRA_KILLSEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalGloe.g:1202:2: ( ( 'penta kills' ) )
                    {
                    // InternalGloe.g:1202:2: ( ( 'penta kills' ) )
                    // InternalGloe.g:1203:3: ( 'penta kills' )
                    {
                     before(grammarAccess.getStatAccess().getPENTA_KILLSEnumLiteralDeclaration_6()); 
                    // InternalGloe.g:1204:3: ( 'penta kills' )
                    // InternalGloe.g:1204:4: 'penta kills'
                    {
                    match(input,32,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getPENTA_KILLSEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalGloe.g:1208:2: ( ( 'damage dealt' ) )
                    {
                    // InternalGloe.g:1208:2: ( ( 'damage dealt' ) )
                    // InternalGloe.g:1209:3: ( 'damage dealt' )
                    {
                     before(grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALTEnumLiteralDeclaration_7()); 
                    // InternalGloe.g:1210:3: ( 'damage dealt' )
                    // InternalGloe.g:1210:4: 'damage dealt'
                    {
                    match(input,33,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALTEnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalGloe.g:1214:2: ( ( 'magic damage dealt' ) )
                    {
                    // InternalGloe.g:1214:2: ( ( 'magic damage dealt' ) )
                    // InternalGloe.g:1215:3: ( 'magic damage dealt' )
                    {
                     before(grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALTEnumLiteralDeclaration_8()); 
                    // InternalGloe.g:1216:3: ( 'magic damage dealt' )
                    // InternalGloe.g:1216:4: 'magic damage dealt'
                    {
                    match(input,34,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALTEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalGloe.g:1220:2: ( ( 'physical damage dealt' ) )
                    {
                    // InternalGloe.g:1220:2: ( ( 'physical damage dealt' ) )
                    // InternalGloe.g:1221:3: ( 'physical damage dealt' )
                    {
                     before(grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALTEnumLiteralDeclaration_9()); 
                    // InternalGloe.g:1222:3: ( 'physical damage dealt' )
                    // InternalGloe.g:1222:4: 'physical damage dealt'
                    {
                    match(input,35,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALTEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalGloe.g:1226:2: ( ( 'true damage dealt' ) )
                    {
                    // InternalGloe.g:1226:2: ( ( 'true damage dealt' ) )
                    // InternalGloe.g:1227:3: ( 'true damage dealt' )
                    {
                     before(grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALTEnumLiteralDeclaration_10()); 
                    // InternalGloe.g:1228:3: ( 'true damage dealt' )
                    // InternalGloe.g:1228:4: 'true damage dealt'
                    {
                    match(input,36,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALTEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // InternalGloe.g:1232:2: ( ( 'damage dealt to champions' ) )
                    {
                    // InternalGloe.g:1232:2: ( ( 'damage dealt to champions' ) )
                    // InternalGloe.g:1233:3: ( 'damage dealt to champions' )
                    {
                     before(grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_11()); 
                    // InternalGloe.g:1234:3: ( 'damage dealt to champions' )
                    // InternalGloe.g:1234:4: 'damage dealt to champions'
                    {
                    match(input,37,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTOTAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_11()); 

                    }


                    }
                    break;
                case 13 :
                    // InternalGloe.g:1238:2: ( ( 'magic damage dealt to champions' ) )
                    {
                    // InternalGloe.g:1238:2: ( ( 'magic damage dealt to champions' ) )
                    // InternalGloe.g:1239:3: ( 'magic damage dealt to champions' )
                    {
                     before(grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_12()); 
                    // InternalGloe.g:1240:3: ( 'magic damage dealt to champions' )
                    // InternalGloe.g:1240:4: 'magic damage dealt to champions'
                    {
                    match(input,38,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getMAGIC_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_12()); 

                    }


                    }
                    break;
                case 14 :
                    // InternalGloe.g:1244:2: ( ( 'physical damage dealt to champions' ) )
                    {
                    // InternalGloe.g:1244:2: ( ( 'physical damage dealt to champions' ) )
                    // InternalGloe.g:1245:3: ( 'physical damage dealt to champions' )
                    {
                     before(grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_13()); 
                    // InternalGloe.g:1246:3: ( 'physical damage dealt to champions' )
                    // InternalGloe.g:1246:4: 'physical damage dealt to champions'
                    {
                    match(input,39,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_13()); 

                    }


                    }
                    break;
                case 15 :
                    // InternalGloe.g:1250:2: ( ( 'true damage dealt to champions' ) )
                    {
                    // InternalGloe.g:1250:2: ( ( 'true damage dealt to champions' ) )
                    // InternalGloe.g:1251:3: ( 'true damage dealt to champions' )
                    {
                     before(grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_14()); 
                    // InternalGloe.g:1252:3: ( 'true damage dealt to champions' )
                    // InternalGloe.g:1252:4: 'true damage dealt to champions'
                    {
                    match(input,40,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTRUE_DAMAGE_DEALT_TO_CHAMPIONSEnumLiteralDeclaration_14()); 

                    }


                    }
                    break;
                case 16 :
                    // InternalGloe.g:1256:2: ( ( 'heal' ) )
                    {
                    // InternalGloe.g:1256:2: ( ( 'heal' ) )
                    // InternalGloe.g:1257:3: ( 'heal' )
                    {
                     before(grammarAccess.getStatAccess().getTOTAL_HEALEnumLiteralDeclaration_15()); 
                    // InternalGloe.g:1258:3: ( 'heal' )
                    // InternalGloe.g:1258:4: 'heal'
                    {
                    match(input,41,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTOTAL_HEALEnumLiteralDeclaration_15()); 

                    }


                    }
                    break;
                case 17 :
                    // InternalGloe.g:1262:2: ( ( 'damage self mitigated' ) )
                    {
                    // InternalGloe.g:1262:2: ( ( 'damage self mitigated' ) )
                    // InternalGloe.g:1263:3: ( 'damage self mitigated' )
                    {
                     before(grammarAccess.getStatAccess().getDAMAGE_SELF_MITIGATEDEnumLiteralDeclaration_16()); 
                    // InternalGloe.g:1264:3: ( 'damage self mitigated' )
                    // InternalGloe.g:1264:4: 'damage self mitigated'
                    {
                    match(input,42,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getDAMAGE_SELF_MITIGATEDEnumLiteralDeclaration_16()); 

                    }


                    }
                    break;
                case 18 :
                    // InternalGloe.g:1268:2: ( ( 'damage dealt to objectives' ) )
                    {
                    // InternalGloe.g:1268:2: ( ( 'damage dealt to objectives' ) )
                    // InternalGloe.g:1269:3: ( 'damage dealt to objectives' )
                    {
                     before(grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_OBJECTIVESEnumLiteralDeclaration_17()); 
                    // InternalGloe.g:1270:3: ( 'damage dealt to objectives' )
                    // InternalGloe.g:1270:4: 'damage dealt to objectives'
                    {
                    match(input,43,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_OBJECTIVESEnumLiteralDeclaration_17()); 

                    }


                    }
                    break;
                case 19 :
                    // InternalGloe.g:1274:2: ( ( 'damage dealt to turrets' ) )
                    {
                    // InternalGloe.g:1274:2: ( ( 'damage dealt to turrets' ) )
                    // InternalGloe.g:1275:3: ( 'damage dealt to turrets' )
                    {
                     before(grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_TURRETSEnumLiteralDeclaration_18()); 
                    // InternalGloe.g:1276:3: ( 'damage dealt to turrets' )
                    // InternalGloe.g:1276:4: 'damage dealt to turrets'
                    {
                    match(input,44,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getDAMAGE_DEALT_TO_TURRETSEnumLiteralDeclaration_18()); 

                    }


                    }
                    break;
                case 20 :
                    // InternalGloe.g:1280:2: ( ( 'time cc' ) )
                    {
                    // InternalGloe.g:1280:2: ( ( 'time cc' ) )
                    // InternalGloe.g:1281:3: ( 'time cc' )
                    {
                     before(grammarAccess.getStatAccess().getTIME_C_CING_OTHERSEnumLiteralDeclaration_19()); 
                    // InternalGloe.g:1282:3: ( 'time cc' )
                    // InternalGloe.g:1282:4: 'time cc'
                    {
                    match(input,45,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTIME_C_CING_OTHERSEnumLiteralDeclaration_19()); 

                    }


                    }
                    break;
                case 21 :
                    // InternalGloe.g:1286:2: ( ( 'damage taken' ) )
                    {
                    // InternalGloe.g:1286:2: ( ( 'damage taken' ) )
                    // InternalGloe.g:1287:3: ( 'damage taken' )
                    {
                     before(grammarAccess.getStatAccess().getTOTAL_DAMAGE_TAKENEnumLiteralDeclaration_20()); 
                    // InternalGloe.g:1288:3: ( 'damage taken' )
                    // InternalGloe.g:1288:4: 'damage taken'
                    {
                    match(input,46,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTOTAL_DAMAGE_TAKENEnumLiteralDeclaration_20()); 

                    }


                    }
                    break;
                case 22 :
                    // InternalGloe.g:1292:2: ( ( 'magical damage taken' ) )
                    {
                    // InternalGloe.g:1292:2: ( ( 'magical damage taken' ) )
                    // InternalGloe.g:1293:3: ( 'magical damage taken' )
                    {
                     before(grammarAccess.getStatAccess().getMAGICAL_DAMAGE_TAKENEnumLiteralDeclaration_21()); 
                    // InternalGloe.g:1294:3: ( 'magical damage taken' )
                    // InternalGloe.g:1294:4: 'magical damage taken'
                    {
                    match(input,47,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getMAGICAL_DAMAGE_TAKENEnumLiteralDeclaration_21()); 

                    }


                    }
                    break;
                case 23 :
                    // InternalGloe.g:1298:2: ( ( 'physical damage taken' ) )
                    {
                    // InternalGloe.g:1298:2: ( ( 'physical damage taken' ) )
                    // InternalGloe.g:1299:3: ( 'physical damage taken' )
                    {
                     before(grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_TAKENEnumLiteralDeclaration_22()); 
                    // InternalGloe.g:1300:3: ( 'physical damage taken' )
                    // InternalGloe.g:1300:4: 'physical damage taken'
                    {
                    match(input,48,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getPHYSICAL_DAMAGE_TAKENEnumLiteralDeclaration_22()); 

                    }


                    }
                    break;
                case 24 :
                    // InternalGloe.g:1304:2: ( ( 'true damage taken' ) )
                    {
                    // InternalGloe.g:1304:2: ( ( 'true damage taken' ) )
                    // InternalGloe.g:1305:3: ( 'true damage taken' )
                    {
                     before(grammarAccess.getStatAccess().getTRUE_DAMAGE_TAKENEnumLiteralDeclaration_23()); 
                    // InternalGloe.g:1306:3: ( 'true damage taken' )
                    // InternalGloe.g:1306:4: 'true damage taken'
                    {
                    match(input,49,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTRUE_DAMAGE_TAKENEnumLiteralDeclaration_23()); 

                    }


                    }
                    break;
                case 25 :
                    // InternalGloe.g:1310:2: ( ( 'gold earned' ) )
                    {
                    // InternalGloe.g:1310:2: ( ( 'gold earned' ) )
                    // InternalGloe.g:1311:3: ( 'gold earned' )
                    {
                     before(grammarAccess.getStatAccess().getGOLD_EARNEDEnumLiteralDeclaration_24()); 
                    // InternalGloe.g:1312:3: ( 'gold earned' )
                    // InternalGloe.g:1312:4: 'gold earned'
                    {
                    match(input,50,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getGOLD_EARNEDEnumLiteralDeclaration_24()); 

                    }


                    }
                    break;
                case 26 :
                    // InternalGloe.g:1316:2: ( ( 'gold spent' ) )
                    {
                    // InternalGloe.g:1316:2: ( ( 'gold spent' ) )
                    // InternalGloe.g:1317:3: ( 'gold spent' )
                    {
                     before(grammarAccess.getStatAccess().getGOLD_SPENTEnumLiteralDeclaration_25()); 
                    // InternalGloe.g:1318:3: ( 'gold spent' )
                    // InternalGloe.g:1318:4: 'gold spent'
                    {
                    match(input,51,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getGOLD_SPENTEnumLiteralDeclaration_25()); 

                    }


                    }
                    break;
                case 27 :
                    // InternalGloe.g:1322:2: ( ( 'turret kills' ) )
                    {
                    // InternalGloe.g:1322:2: ( ( 'turret kills' ) )
                    // InternalGloe.g:1323:3: ( 'turret kills' )
                    {
                     before(grammarAccess.getStatAccess().getTURRET_KILLSEnumLiteralDeclaration_26()); 
                    // InternalGloe.g:1324:3: ( 'turret kills' )
                    // InternalGloe.g:1324:4: 'turret kills'
                    {
                    match(input,52,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTURRET_KILLSEnumLiteralDeclaration_26()); 

                    }


                    }
                    break;
                case 28 :
                    // InternalGloe.g:1328:2: ( ( 'inhibitor kills' ) )
                    {
                    // InternalGloe.g:1328:2: ( ( 'inhibitor kills' ) )
                    // InternalGloe.g:1329:3: ( 'inhibitor kills' )
                    {
                     before(grammarAccess.getStatAccess().getINHIBITOR_KILLSEnumLiteralDeclaration_27()); 
                    // InternalGloe.g:1330:3: ( 'inhibitor kills' )
                    // InternalGloe.g:1330:4: 'inhibitor kills'
                    {
                    match(input,53,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getINHIBITOR_KILLSEnumLiteralDeclaration_27()); 

                    }


                    }
                    break;
                case 29 :
                    // InternalGloe.g:1334:2: ( ( 'minions killed' ) )
                    {
                    // InternalGloe.g:1334:2: ( ( 'minions killed' ) )
                    // InternalGloe.g:1335:3: ( 'minions killed' )
                    {
                     before(grammarAccess.getStatAccess().getTOTAL_MINIONS_KILLEDEnumLiteralDeclaration_28()); 
                    // InternalGloe.g:1336:3: ( 'minions killed' )
                    // InternalGloe.g:1336:4: 'minions killed'
                    {
                    match(input,54,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTOTAL_MINIONS_KILLEDEnumLiteralDeclaration_28()); 

                    }


                    }
                    break;
                case 30 :
                    // InternalGloe.g:1340:2: ( ( 'neutral minions killed' ) )
                    {
                    // InternalGloe.g:1340:2: ( ( 'neutral minions killed' ) )
                    // InternalGloe.g:1341:3: ( 'neutral minions killed' )
                    {
                     before(grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLEDEnumLiteralDeclaration_29()); 
                    // InternalGloe.g:1342:3: ( 'neutral minions killed' )
                    // InternalGloe.g:1342:4: 'neutral minions killed'
                    {
                    match(input,55,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLEDEnumLiteralDeclaration_29()); 

                    }


                    }
                    break;
                case 31 :
                    // InternalGloe.g:1346:2: ( ( 'neutral minions killed team jungle' ) )
                    {
                    // InternalGloe.g:1346:2: ( ( 'neutral minions killed team jungle' ) )
                    // InternalGloe.g:1347:3: ( 'neutral minions killed team jungle' )
                    {
                     before(grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_TEAM_JUNGLEEnumLiteralDeclaration_30()); 
                    // InternalGloe.g:1348:3: ( 'neutral minions killed team jungle' )
                    // InternalGloe.g:1348:4: 'neutral minions killed team jungle'
                    {
                    match(input,56,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_TEAM_JUNGLEEnumLiteralDeclaration_30()); 

                    }


                    }
                    break;
                case 32 :
                    // InternalGloe.g:1352:2: ( ( 'neutral minions killed enemy jungle' ) )
                    {
                    // InternalGloe.g:1352:2: ( ( 'neutral minions killed enemy jungle' ) )
                    // InternalGloe.g:1353:3: ( 'neutral minions killed enemy jungle' )
                    {
                     before(grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_ENEMY_JUNGLEEnumLiteralDeclaration_31()); 
                    // InternalGloe.g:1354:3: ( 'neutral minions killed enemy jungle' )
                    // InternalGloe.g:1354:4: 'neutral minions killed enemy jungle'
                    {
                    match(input,57,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getNEUTRAL_MINIONS_KILLED_ENEMY_JUNGLEEnumLiteralDeclaration_31()); 

                    }


                    }
                    break;
                case 33 :
                    // InternalGloe.g:1358:2: ( ( 'time crowd control dealt' ) )
                    {
                    // InternalGloe.g:1358:2: ( ( 'time crowd control dealt' ) )
                    // InternalGloe.g:1359:3: ( 'time crowd control dealt' )
                    {
                     before(grammarAccess.getStatAccess().getTOTAL_TIME_CROWD_CONTROL_DEALTEnumLiteralDeclaration_32()); 
                    // InternalGloe.g:1360:3: ( 'time crowd control dealt' )
                    // InternalGloe.g:1360:4: 'time crowd control dealt'
                    {
                    match(input,58,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getTOTAL_TIME_CROWD_CONTROL_DEALTEnumLiteralDeclaration_32()); 

                    }


                    }
                    break;
                case 34 :
                    // InternalGloe.g:1364:2: ( ( 'vision wards bought in game' ) )
                    {
                    // InternalGloe.g:1364:2: ( ( 'vision wards bought in game' ) )
                    // InternalGloe.g:1365:3: ( 'vision wards bought in game' )
                    {
                     before(grammarAccess.getStatAccess().getVISION_WARDS_BOUGHT_IN_GAMEEnumLiteralDeclaration_33()); 
                    // InternalGloe.g:1366:3: ( 'vision wards bought in game' )
                    // InternalGloe.g:1366:4: 'vision wards bought in game'
                    {
                    match(input,59,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getVISION_WARDS_BOUGHT_IN_GAMEEnumLiteralDeclaration_33()); 

                    }


                    }
                    break;
                case 35 :
                    // InternalGloe.g:1370:2: ( ( 'wards placed' ) )
                    {
                    // InternalGloe.g:1370:2: ( ( 'wards placed' ) )
                    // InternalGloe.g:1371:3: ( 'wards placed' )
                    {
                     before(grammarAccess.getStatAccess().getWARDS_PLACEDEnumLiteralDeclaration_34()); 
                    // InternalGloe.g:1372:3: ( 'wards placed' )
                    // InternalGloe.g:1372:4: 'wards placed'
                    {
                    match(input,60,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getWARDS_PLACEDEnumLiteralDeclaration_34()); 

                    }


                    }
                    break;
                case 36 :
                    // InternalGloe.g:1376:2: ( ( 'wards killed' ) )
                    {
                    // InternalGloe.g:1376:2: ( ( 'wards killed' ) )
                    // InternalGloe.g:1377:3: ( 'wards killed' )
                    {
                     before(grammarAccess.getStatAccess().getWARDS_KILLEDEnumLiteralDeclaration_35()); 
                    // InternalGloe.g:1378:3: ( 'wards killed' )
                    // InternalGloe.g:1378:4: 'wards killed'
                    {
                    match(input,61,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getWARDS_KILLEDEnumLiteralDeclaration_35()); 

                    }


                    }
                    break;
                case 37 :
                    // InternalGloe.g:1382:2: ( ( 'first blood kill' ) )
                    {
                    // InternalGloe.g:1382:2: ( ( 'first blood kill' ) )
                    // InternalGloe.g:1383:3: ( 'first blood kill' )
                    {
                     before(grammarAccess.getStatAccess().getFIRST_BLOOD_KILLEnumLiteralDeclaration_36()); 
                    // InternalGloe.g:1384:3: ( 'first blood kill' )
                    // InternalGloe.g:1384:4: 'first blood kill'
                    {
                    match(input,62,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getFIRST_BLOOD_KILLEnumLiteralDeclaration_36()); 

                    }


                    }
                    break;
                case 38 :
                    // InternalGloe.g:1388:2: ( ( 'first blood assist' ) )
                    {
                    // InternalGloe.g:1388:2: ( ( 'first blood assist' ) )
                    // InternalGloe.g:1389:3: ( 'first blood assist' )
                    {
                     before(grammarAccess.getStatAccess().getFIRST_BLOOD_ASSISTEnumLiteralDeclaration_37()); 
                    // InternalGloe.g:1390:3: ( 'first blood assist' )
                    // InternalGloe.g:1390:4: 'first blood assist'
                    {
                    match(input,63,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getFIRST_BLOOD_ASSISTEnumLiteralDeclaration_37()); 

                    }


                    }
                    break;
                case 39 :
                    // InternalGloe.g:1394:2: ( ( 'first tower kill' ) )
                    {
                    // InternalGloe.g:1394:2: ( ( 'first tower kill' ) )
                    // InternalGloe.g:1395:3: ( 'first tower kill' )
                    {
                     before(grammarAccess.getStatAccess().getFIRST_TOWER_KILLEnumLiteralDeclaration_38()); 
                    // InternalGloe.g:1396:3: ( 'first tower kill' )
                    // InternalGloe.g:1396:4: 'first tower kill'
                    {
                    match(input,64,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getFIRST_TOWER_KILLEnumLiteralDeclaration_38()); 

                    }


                    }
                    break;
                case 40 :
                    // InternalGloe.g:1400:2: ( ( 'first tower assist' ) )
                    {
                    // InternalGloe.g:1400:2: ( ( 'first tower assist' ) )
                    // InternalGloe.g:1401:3: ( 'first tower assist' )
                    {
                     before(grammarAccess.getStatAccess().getFIRST_TOWER_ASSISTEnumLiteralDeclaration_39()); 
                    // InternalGloe.g:1402:3: ( 'first tower assist' )
                    // InternalGloe.g:1402:4: 'first tower assist'
                    {
                    match(input,65,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getFIRST_TOWER_ASSISTEnumLiteralDeclaration_39()); 

                    }


                    }
                    break;
                case 41 :
                    // InternalGloe.g:1406:2: ( ( 'first inhibitor kill' ) )
                    {
                    // InternalGloe.g:1406:2: ( ( 'first inhibitor kill' ) )
                    // InternalGloe.g:1407:3: ( 'first inhibitor kill' )
                    {
                     before(grammarAccess.getStatAccess().getFIRST_INHIBITOR_KILLEnumLiteralDeclaration_40()); 
                    // InternalGloe.g:1408:3: ( 'first inhibitor kill' )
                    // InternalGloe.g:1408:4: 'first inhibitor kill'
                    {
                    match(input,66,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getFIRST_INHIBITOR_KILLEnumLiteralDeclaration_40()); 

                    }


                    }
                    break;
                case 42 :
                    // InternalGloe.g:1412:2: ( ( 'first inhibitor assist' ) )
                    {
                    // InternalGloe.g:1412:2: ( ( 'first inhibitor assist' ) )
                    // InternalGloe.g:1413:3: ( 'first inhibitor assist' )
                    {
                     before(grammarAccess.getStatAccess().getFIRST_INHIBITOR_ASSISTEnumLiteralDeclaration_41()); 
                    // InternalGloe.g:1414:3: ( 'first inhibitor assist' )
                    // InternalGloe.g:1414:4: 'first inhibitor assist'
                    {
                    match(input,67,FOLLOW_2); 

                    }

                     after(grammarAccess.getStatAccess().getFIRST_INHIBITOR_ASSISTEnumLiteralDeclaration_41()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Stat__Alternatives"


    // $ANTLR start "rule__Champion__Alternatives"
    // InternalGloe.g:1422:1: rule__Champion__Alternatives : ( ( ( 'Annie' ) ) | ( ( 'Olaf' ) ) | ( ( 'Galio' ) ) | ( ( 'Twisted Fate' ) ) | ( ( 'Xin Zhao' ) ) | ( ( 'Urgot' ) ) | ( ( 'Leblanc' ) ) | ( ( 'Vladimir' ) ) | ( ( 'Fiddlesticks' ) ) | ( ( 'Kayle' ) ) | ( ( 'Master Yi' ) ) | ( ( 'Alistar' ) ) | ( ( 'Ryze' ) ) | ( ( 'Sion' ) ) | ( ( 'Sivir' ) ) | ( ( 'Soraka' ) ) | ( ( 'Teemo' ) ) | ( ( 'Tristana' ) ) | ( ( 'Warwick' ) ) | ( ( 'Nunu' ) ) | ( ( 'Miss Fortune' ) ) | ( ( 'Ashe' ) ) | ( ( 'Tryndamere' ) ) | ( ( 'Jax' ) ) | ( ( 'Morgana' ) ) | ( ( 'Zilean' ) ) | ( ( 'Singed' ) ) | ( ( 'Evelynn' ) ) | ( ( 'Twitch' ) ) | ( ( 'Karthus' ) ) | ( ( 'Cho\\'Gath' ) ) | ( ( 'Amumu' ) ) | ( ( 'Rammus' ) ) | ( ( 'Anivia' ) ) | ( ( 'Shaco' ) ) | ( ( 'Dr. Mundo' ) ) | ( ( 'Sona' ) ) | ( ( 'Kassadin' ) ) | ( ( 'Irelia' ) ) | ( ( 'Janna' ) ) | ( ( 'Gangplank' ) ) | ( ( 'Corki' ) ) | ( ( 'Karma' ) ) | ( ( 'Taric' ) ) | ( ( 'Veigar' ) ) | ( ( 'Trundle' ) ) | ( ( 'Swain' ) ) | ( ( 'Caitlyn' ) ) | ( ( 'Blitzcrank' ) ) | ( ( 'Malphite' ) ) | ( ( 'Katarina' ) ) | ( ( 'Nocturne' ) ) | ( ( 'Maokai' ) ) | ( ( 'Renekton' ) ) | ( ( 'Jarvan IV' ) ) | ( ( 'Elise' ) ) | ( ( 'Orianna' ) ) | ( ( 'Wukong' ) ) | ( ( 'Brand' ) ) | ( ( 'Lee Sin' ) ) | ( ( 'Vayne' ) ) | ( ( 'Rumble' ) ) | ( ( 'Cassiopeia' ) ) | ( ( 'Skarner' ) ) | ( ( 'Heimerdinger' ) ) | ( ( 'Nasus' ) ) | ( ( 'Nidalee' ) ) | ( ( 'Udyr' ) ) | ( ( 'Poppy' ) ) | ( ( 'Gragas' ) ) | ( ( 'Pantheon' ) ) | ( ( 'Ezreal' ) ) | ( ( 'Mordekaiser' ) ) | ( ( 'Yorick' ) ) | ( ( 'Akali' ) ) | ( ( 'Kennen' ) ) | ( ( 'Garen' ) ) | ( ( 'Leona' ) ) | ( ( 'Malzahar' ) ) | ( ( 'Talon' ) ) | ( ( 'Riven' ) ) | ( ( 'Kog\\'Maw' ) ) | ( ( 'Shen' ) ) | ( ( 'Lux' ) ) | ( ( 'Xerath' ) ) | ( ( 'Shyvana' ) ) | ( ( 'Ahri' ) ) | ( ( 'Graves' ) ) | ( ( 'Fizz' ) ) | ( ( 'Volibear' ) ) | ( ( 'Rengar' ) ) | ( ( 'Varus' ) ) | ( ( 'Nautilus' ) ) | ( ( 'Viktor' ) ) | ( ( 'Sejuani' ) ) | ( ( 'Fiora' ) ) | ( ( 'Ziggs' ) ) | ( ( 'Lulu' ) ) | ( ( 'Draven' ) ) | ( ( 'Hecarim' ) ) | ( ( 'Kha\\'Zix' ) ) | ( ( 'Darius' ) ) | ( ( 'Jayce' ) ) | ( ( 'Lissandra' ) ) | ( ( 'Diana' ) ) | ( ( 'Quinn' ) ) | ( ( 'Syndra' ) ) | ( ( 'Aurelion Sol' ) ) | ( ( 'Kayn' ) ) | ( ( 'Zoe' ) ) | ( ( 'Zyra' ) ) | ( ( 'Kai\\'Sa' ) ) | ( ( 'Gnar' ) ) | ( ( 'Zac' ) ) | ( ( 'Yasuo' ) ) | ( ( 'Vel\\'Koz' ) ) | ( ( 'Taliyah' ) ) | ( ( 'Camille' ) ) | ( ( 'Braum' ) ) | ( ( 'Jhin' ) ) | ( ( 'Kindred' ) ) | ( ( 'Jinx' ) ) | ( ( 'Tahm Kench' ) ) | ( ( 'Lucian' ) ) | ( ( 'Zed' ) ) | ( ( 'Kled' ) ) | ( ( 'Ekko' ) ) | ( ( 'Vi' ) ) | ( ( 'Aatrox' ) ) | ( ( 'Nami' ) ) | ( ( 'Azir' ) ) | ( ( 'Thresh' ) ) | ( ( 'Illaoi' ) ) | ( ( 'Rek\\'Sai' ) ) | ( ( 'Ivern' ) ) | ( ( 'Kalista' ) ) | ( ( 'Bard' ) ) | ( ( 'Rakan' ) ) | ( ( 'Xayah' ) ) | ( ( 'Ornn' ) ) );
    public final void rule__Champion__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:1426:1: ( ( ( 'Annie' ) ) | ( ( 'Olaf' ) ) | ( ( 'Galio' ) ) | ( ( 'Twisted Fate' ) ) | ( ( 'Xin Zhao' ) ) | ( ( 'Urgot' ) ) | ( ( 'Leblanc' ) ) | ( ( 'Vladimir' ) ) | ( ( 'Fiddlesticks' ) ) | ( ( 'Kayle' ) ) | ( ( 'Master Yi' ) ) | ( ( 'Alistar' ) ) | ( ( 'Ryze' ) ) | ( ( 'Sion' ) ) | ( ( 'Sivir' ) ) | ( ( 'Soraka' ) ) | ( ( 'Teemo' ) ) | ( ( 'Tristana' ) ) | ( ( 'Warwick' ) ) | ( ( 'Nunu' ) ) | ( ( 'Miss Fortune' ) ) | ( ( 'Ashe' ) ) | ( ( 'Tryndamere' ) ) | ( ( 'Jax' ) ) | ( ( 'Morgana' ) ) | ( ( 'Zilean' ) ) | ( ( 'Singed' ) ) | ( ( 'Evelynn' ) ) | ( ( 'Twitch' ) ) | ( ( 'Karthus' ) ) | ( ( 'Cho\\'Gath' ) ) | ( ( 'Amumu' ) ) | ( ( 'Rammus' ) ) | ( ( 'Anivia' ) ) | ( ( 'Shaco' ) ) | ( ( 'Dr. Mundo' ) ) | ( ( 'Sona' ) ) | ( ( 'Kassadin' ) ) | ( ( 'Irelia' ) ) | ( ( 'Janna' ) ) | ( ( 'Gangplank' ) ) | ( ( 'Corki' ) ) | ( ( 'Karma' ) ) | ( ( 'Taric' ) ) | ( ( 'Veigar' ) ) | ( ( 'Trundle' ) ) | ( ( 'Swain' ) ) | ( ( 'Caitlyn' ) ) | ( ( 'Blitzcrank' ) ) | ( ( 'Malphite' ) ) | ( ( 'Katarina' ) ) | ( ( 'Nocturne' ) ) | ( ( 'Maokai' ) ) | ( ( 'Renekton' ) ) | ( ( 'Jarvan IV' ) ) | ( ( 'Elise' ) ) | ( ( 'Orianna' ) ) | ( ( 'Wukong' ) ) | ( ( 'Brand' ) ) | ( ( 'Lee Sin' ) ) | ( ( 'Vayne' ) ) | ( ( 'Rumble' ) ) | ( ( 'Cassiopeia' ) ) | ( ( 'Skarner' ) ) | ( ( 'Heimerdinger' ) ) | ( ( 'Nasus' ) ) | ( ( 'Nidalee' ) ) | ( ( 'Udyr' ) ) | ( ( 'Poppy' ) ) | ( ( 'Gragas' ) ) | ( ( 'Pantheon' ) ) | ( ( 'Ezreal' ) ) | ( ( 'Mordekaiser' ) ) | ( ( 'Yorick' ) ) | ( ( 'Akali' ) ) | ( ( 'Kennen' ) ) | ( ( 'Garen' ) ) | ( ( 'Leona' ) ) | ( ( 'Malzahar' ) ) | ( ( 'Talon' ) ) | ( ( 'Riven' ) ) | ( ( 'Kog\\'Maw' ) ) | ( ( 'Shen' ) ) | ( ( 'Lux' ) ) | ( ( 'Xerath' ) ) | ( ( 'Shyvana' ) ) | ( ( 'Ahri' ) ) | ( ( 'Graves' ) ) | ( ( 'Fizz' ) ) | ( ( 'Volibear' ) ) | ( ( 'Rengar' ) ) | ( ( 'Varus' ) ) | ( ( 'Nautilus' ) ) | ( ( 'Viktor' ) ) | ( ( 'Sejuani' ) ) | ( ( 'Fiora' ) ) | ( ( 'Ziggs' ) ) | ( ( 'Lulu' ) ) | ( ( 'Draven' ) ) | ( ( 'Hecarim' ) ) | ( ( 'Kha\\'Zix' ) ) | ( ( 'Darius' ) ) | ( ( 'Jayce' ) ) | ( ( 'Lissandra' ) ) | ( ( 'Diana' ) ) | ( ( 'Quinn' ) ) | ( ( 'Syndra' ) ) | ( ( 'Aurelion Sol' ) ) | ( ( 'Kayn' ) ) | ( ( 'Zoe' ) ) | ( ( 'Zyra' ) ) | ( ( 'Kai\\'Sa' ) ) | ( ( 'Gnar' ) ) | ( ( 'Zac' ) ) | ( ( 'Yasuo' ) ) | ( ( 'Vel\\'Koz' ) ) | ( ( 'Taliyah' ) ) | ( ( 'Camille' ) ) | ( ( 'Braum' ) ) | ( ( 'Jhin' ) ) | ( ( 'Kindred' ) ) | ( ( 'Jinx' ) ) | ( ( 'Tahm Kench' ) ) | ( ( 'Lucian' ) ) | ( ( 'Zed' ) ) | ( ( 'Kled' ) ) | ( ( 'Ekko' ) ) | ( ( 'Vi' ) ) | ( ( 'Aatrox' ) ) | ( ( 'Nami' ) ) | ( ( 'Azir' ) ) | ( ( 'Thresh' ) ) | ( ( 'Illaoi' ) ) | ( ( 'Rek\\'Sai' ) ) | ( ( 'Ivern' ) ) | ( ( 'Kalista' ) ) | ( ( 'Bard' ) ) | ( ( 'Rakan' ) ) | ( ( 'Xayah' ) ) | ( ( 'Ornn' ) ) )
            int alt15=140;
            switch ( input.LA(1) ) {
            case 68:
                {
                alt15=1;
                }
                break;
            case 69:
                {
                alt15=2;
                }
                break;
            case 70:
                {
                alt15=3;
                }
                break;
            case 71:
                {
                alt15=4;
                }
                break;
            case 72:
                {
                alt15=5;
                }
                break;
            case 73:
                {
                alt15=6;
                }
                break;
            case 74:
                {
                alt15=7;
                }
                break;
            case 75:
                {
                alt15=8;
                }
                break;
            case 76:
                {
                alt15=9;
                }
                break;
            case 77:
                {
                alt15=10;
                }
                break;
            case 78:
                {
                alt15=11;
                }
                break;
            case 79:
                {
                alt15=12;
                }
                break;
            case 80:
                {
                alt15=13;
                }
                break;
            case 81:
                {
                alt15=14;
                }
                break;
            case 82:
                {
                alt15=15;
                }
                break;
            case 83:
                {
                alt15=16;
                }
                break;
            case 84:
                {
                alt15=17;
                }
                break;
            case 85:
                {
                alt15=18;
                }
                break;
            case 86:
                {
                alt15=19;
                }
                break;
            case 87:
                {
                alt15=20;
                }
                break;
            case 88:
                {
                alt15=21;
                }
                break;
            case 89:
                {
                alt15=22;
                }
                break;
            case 90:
                {
                alt15=23;
                }
                break;
            case 91:
                {
                alt15=24;
                }
                break;
            case 92:
                {
                alt15=25;
                }
                break;
            case 93:
                {
                alt15=26;
                }
                break;
            case 94:
                {
                alt15=27;
                }
                break;
            case 95:
                {
                alt15=28;
                }
                break;
            case 96:
                {
                alt15=29;
                }
                break;
            case 97:
                {
                alt15=30;
                }
                break;
            case 98:
                {
                alt15=31;
                }
                break;
            case 99:
                {
                alt15=32;
                }
                break;
            case 100:
                {
                alt15=33;
                }
                break;
            case 101:
                {
                alt15=34;
                }
                break;
            case 102:
                {
                alt15=35;
                }
                break;
            case 103:
                {
                alt15=36;
                }
                break;
            case 104:
                {
                alt15=37;
                }
                break;
            case 105:
                {
                alt15=38;
                }
                break;
            case 106:
                {
                alt15=39;
                }
                break;
            case 107:
                {
                alt15=40;
                }
                break;
            case 108:
                {
                alt15=41;
                }
                break;
            case 109:
                {
                alt15=42;
                }
                break;
            case 110:
                {
                alt15=43;
                }
                break;
            case 111:
                {
                alt15=44;
                }
                break;
            case 112:
                {
                alt15=45;
                }
                break;
            case 113:
                {
                alt15=46;
                }
                break;
            case 114:
                {
                alt15=47;
                }
                break;
            case 115:
                {
                alt15=48;
                }
                break;
            case 116:
                {
                alt15=49;
                }
                break;
            case 117:
                {
                alt15=50;
                }
                break;
            case 118:
                {
                alt15=51;
                }
                break;
            case 119:
                {
                alt15=52;
                }
                break;
            case 120:
                {
                alt15=53;
                }
                break;
            case 121:
                {
                alt15=54;
                }
                break;
            case 122:
                {
                alt15=55;
                }
                break;
            case 123:
                {
                alt15=56;
                }
                break;
            case 124:
                {
                alt15=57;
                }
                break;
            case 125:
                {
                alt15=58;
                }
                break;
            case 126:
                {
                alt15=59;
                }
                break;
            case 127:
                {
                alt15=60;
                }
                break;
            case 128:
                {
                alt15=61;
                }
                break;
            case 129:
                {
                alt15=62;
                }
                break;
            case 130:
                {
                alt15=63;
                }
                break;
            case 131:
                {
                alt15=64;
                }
                break;
            case 132:
                {
                alt15=65;
                }
                break;
            case 133:
                {
                alt15=66;
                }
                break;
            case 134:
                {
                alt15=67;
                }
                break;
            case 135:
                {
                alt15=68;
                }
                break;
            case 136:
                {
                alt15=69;
                }
                break;
            case 137:
                {
                alt15=70;
                }
                break;
            case 138:
                {
                alt15=71;
                }
                break;
            case 139:
                {
                alt15=72;
                }
                break;
            case 140:
                {
                alt15=73;
                }
                break;
            case 141:
                {
                alt15=74;
                }
                break;
            case 142:
                {
                alt15=75;
                }
                break;
            case 143:
                {
                alt15=76;
                }
                break;
            case 144:
                {
                alt15=77;
                }
                break;
            case 145:
                {
                alt15=78;
                }
                break;
            case 146:
                {
                alt15=79;
                }
                break;
            case 147:
                {
                alt15=80;
                }
                break;
            case 148:
                {
                alt15=81;
                }
                break;
            case 149:
                {
                alt15=82;
                }
                break;
            case 150:
                {
                alt15=83;
                }
                break;
            case 151:
                {
                alt15=84;
                }
                break;
            case 152:
                {
                alt15=85;
                }
                break;
            case 153:
                {
                alt15=86;
                }
                break;
            case 154:
                {
                alt15=87;
                }
                break;
            case 155:
                {
                alt15=88;
                }
                break;
            case 156:
                {
                alt15=89;
                }
                break;
            case 157:
                {
                alt15=90;
                }
                break;
            case 158:
                {
                alt15=91;
                }
                break;
            case 159:
                {
                alt15=92;
                }
                break;
            case 160:
                {
                alt15=93;
                }
                break;
            case 161:
                {
                alt15=94;
                }
                break;
            case 162:
                {
                alt15=95;
                }
                break;
            case 163:
                {
                alt15=96;
                }
                break;
            case 164:
                {
                alt15=97;
                }
                break;
            case 165:
                {
                alt15=98;
                }
                break;
            case 166:
                {
                alt15=99;
                }
                break;
            case 167:
                {
                alt15=100;
                }
                break;
            case 168:
                {
                alt15=101;
                }
                break;
            case 169:
                {
                alt15=102;
                }
                break;
            case 170:
                {
                alt15=103;
                }
                break;
            case 171:
                {
                alt15=104;
                }
                break;
            case 172:
                {
                alt15=105;
                }
                break;
            case 173:
                {
                alt15=106;
                }
                break;
            case 174:
                {
                alt15=107;
                }
                break;
            case 175:
                {
                alt15=108;
                }
                break;
            case 176:
                {
                alt15=109;
                }
                break;
            case 177:
                {
                alt15=110;
                }
                break;
            case 178:
                {
                alt15=111;
                }
                break;
            case 179:
                {
                alt15=112;
                }
                break;
            case 180:
                {
                alt15=113;
                }
                break;
            case 181:
                {
                alt15=114;
                }
                break;
            case 182:
                {
                alt15=115;
                }
                break;
            case 183:
                {
                alt15=116;
                }
                break;
            case 184:
                {
                alt15=117;
                }
                break;
            case 185:
                {
                alt15=118;
                }
                break;
            case 186:
                {
                alt15=119;
                }
                break;
            case 187:
                {
                alt15=120;
                }
                break;
            case 188:
                {
                alt15=121;
                }
                break;
            case 189:
                {
                alt15=122;
                }
                break;
            case 190:
                {
                alt15=123;
                }
                break;
            case 191:
                {
                alt15=124;
                }
                break;
            case 192:
                {
                alt15=125;
                }
                break;
            case 193:
                {
                alt15=126;
                }
                break;
            case 194:
                {
                alt15=127;
                }
                break;
            case 195:
                {
                alt15=128;
                }
                break;
            case 196:
                {
                alt15=129;
                }
                break;
            case 197:
                {
                alt15=130;
                }
                break;
            case 198:
                {
                alt15=131;
                }
                break;
            case 199:
                {
                alt15=132;
                }
                break;
            case 200:
                {
                alt15=133;
                }
                break;
            case 201:
                {
                alt15=134;
                }
                break;
            case 202:
                {
                alt15=135;
                }
                break;
            case 203:
                {
                alt15=136;
                }
                break;
            case 204:
                {
                alt15=137;
                }
                break;
            case 205:
                {
                alt15=138;
                }
                break;
            case 206:
                {
                alt15=139;
                }
                break;
            case 207:
                {
                alt15=140;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalGloe.g:1427:2: ( ( 'Annie' ) )
                    {
                    // InternalGloe.g:1427:2: ( ( 'Annie' ) )
                    // InternalGloe.g:1428:3: ( 'Annie' )
                    {
                     before(grammarAccess.getChampionAccess().getANNIEEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:1429:3: ( 'Annie' )
                    // InternalGloe.g:1429:4: 'Annie'
                    {
                    match(input,68,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getANNIEEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:1433:2: ( ( 'Olaf' ) )
                    {
                    // InternalGloe.g:1433:2: ( ( 'Olaf' ) )
                    // InternalGloe.g:1434:3: ( 'Olaf' )
                    {
                     before(grammarAccess.getChampionAccess().getOLAFEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:1435:3: ( 'Olaf' )
                    // InternalGloe.g:1435:4: 'Olaf'
                    {
                    match(input,69,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getOLAFEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:1439:2: ( ( 'Galio' ) )
                    {
                    // InternalGloe.g:1439:2: ( ( 'Galio' ) )
                    // InternalGloe.g:1440:3: ( 'Galio' )
                    {
                     before(grammarAccess.getChampionAccess().getGALIOEnumLiteralDeclaration_2()); 
                    // InternalGloe.g:1441:3: ( 'Galio' )
                    // InternalGloe.g:1441:4: 'Galio'
                    {
                    match(input,70,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getGALIOEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:1445:2: ( ( 'Twisted Fate' ) )
                    {
                    // InternalGloe.g:1445:2: ( ( 'Twisted Fate' ) )
                    // InternalGloe.g:1446:3: ( 'Twisted Fate' )
                    {
                     before(grammarAccess.getChampionAccess().getTWISTEDFATEEnumLiteralDeclaration_3()); 
                    // InternalGloe.g:1447:3: ( 'Twisted Fate' )
                    // InternalGloe.g:1447:4: 'Twisted Fate'
                    {
                    match(input,71,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTWISTEDFATEEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGloe.g:1451:2: ( ( 'Xin Zhao' ) )
                    {
                    // InternalGloe.g:1451:2: ( ( 'Xin Zhao' ) )
                    // InternalGloe.g:1452:3: ( 'Xin Zhao' )
                    {
                     before(grammarAccess.getChampionAccess().getXINZHAOEnumLiteralDeclaration_4()); 
                    // InternalGloe.g:1453:3: ( 'Xin Zhao' )
                    // InternalGloe.g:1453:4: 'Xin Zhao'
                    {
                    match(input,72,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getXINZHAOEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGloe.g:1457:2: ( ( 'Urgot' ) )
                    {
                    // InternalGloe.g:1457:2: ( ( 'Urgot' ) )
                    // InternalGloe.g:1458:3: ( 'Urgot' )
                    {
                     before(grammarAccess.getChampionAccess().getURGOTEnumLiteralDeclaration_5()); 
                    // InternalGloe.g:1459:3: ( 'Urgot' )
                    // InternalGloe.g:1459:4: 'Urgot'
                    {
                    match(input,73,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getURGOTEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalGloe.g:1463:2: ( ( 'Leblanc' ) )
                    {
                    // InternalGloe.g:1463:2: ( ( 'Leblanc' ) )
                    // InternalGloe.g:1464:3: ( 'Leblanc' )
                    {
                     before(grammarAccess.getChampionAccess().getLEBLANCEnumLiteralDeclaration_6()); 
                    // InternalGloe.g:1465:3: ( 'Leblanc' )
                    // InternalGloe.g:1465:4: 'Leblanc'
                    {
                    match(input,74,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLEBLANCEnumLiteralDeclaration_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalGloe.g:1469:2: ( ( 'Vladimir' ) )
                    {
                    // InternalGloe.g:1469:2: ( ( 'Vladimir' ) )
                    // InternalGloe.g:1470:3: ( 'Vladimir' )
                    {
                     before(grammarAccess.getChampionAccess().getVLADIMIREnumLiteralDeclaration_7()); 
                    // InternalGloe.g:1471:3: ( 'Vladimir' )
                    // InternalGloe.g:1471:4: 'Vladimir'
                    {
                    match(input,75,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVLADIMIREnumLiteralDeclaration_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalGloe.g:1475:2: ( ( 'Fiddlesticks' ) )
                    {
                    // InternalGloe.g:1475:2: ( ( 'Fiddlesticks' ) )
                    // InternalGloe.g:1476:3: ( 'Fiddlesticks' )
                    {
                     before(grammarAccess.getChampionAccess().getFIDDLESTICKSEnumLiteralDeclaration_8()); 
                    // InternalGloe.g:1477:3: ( 'Fiddlesticks' )
                    // InternalGloe.g:1477:4: 'Fiddlesticks'
                    {
                    match(input,76,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getFIDDLESTICKSEnumLiteralDeclaration_8()); 

                    }


                    }
                    break;
                case 10 :
                    // InternalGloe.g:1481:2: ( ( 'Kayle' ) )
                    {
                    // InternalGloe.g:1481:2: ( ( 'Kayle' ) )
                    // InternalGloe.g:1482:3: ( 'Kayle' )
                    {
                     before(grammarAccess.getChampionAccess().getKAYLEEnumLiteralDeclaration_9()); 
                    // InternalGloe.g:1483:3: ( 'Kayle' )
                    // InternalGloe.g:1483:4: 'Kayle'
                    {
                    match(input,77,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKAYLEEnumLiteralDeclaration_9()); 

                    }


                    }
                    break;
                case 11 :
                    // InternalGloe.g:1487:2: ( ( 'Master Yi' ) )
                    {
                    // InternalGloe.g:1487:2: ( ( 'Master Yi' ) )
                    // InternalGloe.g:1488:3: ( 'Master Yi' )
                    {
                     before(grammarAccess.getChampionAccess().getMASTERYIEnumLiteralDeclaration_10()); 
                    // InternalGloe.g:1489:3: ( 'Master Yi' )
                    // InternalGloe.g:1489:4: 'Master Yi'
                    {
                    match(input,78,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMASTERYIEnumLiteralDeclaration_10()); 

                    }


                    }
                    break;
                case 12 :
                    // InternalGloe.g:1493:2: ( ( 'Alistar' ) )
                    {
                    // InternalGloe.g:1493:2: ( ( 'Alistar' ) )
                    // InternalGloe.g:1494:3: ( 'Alistar' )
                    {
                     before(grammarAccess.getChampionAccess().getALISTAREnumLiteralDeclaration_11()); 
                    // InternalGloe.g:1495:3: ( 'Alistar' )
                    // InternalGloe.g:1495:4: 'Alistar'
                    {
                    match(input,79,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getALISTAREnumLiteralDeclaration_11()); 

                    }


                    }
                    break;
                case 13 :
                    // InternalGloe.g:1499:2: ( ( 'Ryze' ) )
                    {
                    // InternalGloe.g:1499:2: ( ( 'Ryze' ) )
                    // InternalGloe.g:1500:3: ( 'Ryze' )
                    {
                     before(grammarAccess.getChampionAccess().getRYZEEnumLiteralDeclaration_12()); 
                    // InternalGloe.g:1501:3: ( 'Ryze' )
                    // InternalGloe.g:1501:4: 'Ryze'
                    {
                    match(input,80,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRYZEEnumLiteralDeclaration_12()); 

                    }


                    }
                    break;
                case 14 :
                    // InternalGloe.g:1505:2: ( ( 'Sion' ) )
                    {
                    // InternalGloe.g:1505:2: ( ( 'Sion' ) )
                    // InternalGloe.g:1506:3: ( 'Sion' )
                    {
                     before(grammarAccess.getChampionAccess().getSIONEnumLiteralDeclaration_13()); 
                    // InternalGloe.g:1507:3: ( 'Sion' )
                    // InternalGloe.g:1507:4: 'Sion'
                    {
                    match(input,81,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSIONEnumLiteralDeclaration_13()); 

                    }


                    }
                    break;
                case 15 :
                    // InternalGloe.g:1511:2: ( ( 'Sivir' ) )
                    {
                    // InternalGloe.g:1511:2: ( ( 'Sivir' ) )
                    // InternalGloe.g:1512:3: ( 'Sivir' )
                    {
                     before(grammarAccess.getChampionAccess().getSIVIREnumLiteralDeclaration_14()); 
                    // InternalGloe.g:1513:3: ( 'Sivir' )
                    // InternalGloe.g:1513:4: 'Sivir'
                    {
                    match(input,82,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSIVIREnumLiteralDeclaration_14()); 

                    }


                    }
                    break;
                case 16 :
                    // InternalGloe.g:1517:2: ( ( 'Soraka' ) )
                    {
                    // InternalGloe.g:1517:2: ( ( 'Soraka' ) )
                    // InternalGloe.g:1518:3: ( 'Soraka' )
                    {
                     before(grammarAccess.getChampionAccess().getSORAKAEnumLiteralDeclaration_15()); 
                    // InternalGloe.g:1519:3: ( 'Soraka' )
                    // InternalGloe.g:1519:4: 'Soraka'
                    {
                    match(input,83,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSORAKAEnumLiteralDeclaration_15()); 

                    }


                    }
                    break;
                case 17 :
                    // InternalGloe.g:1523:2: ( ( 'Teemo' ) )
                    {
                    // InternalGloe.g:1523:2: ( ( 'Teemo' ) )
                    // InternalGloe.g:1524:3: ( 'Teemo' )
                    {
                     before(grammarAccess.getChampionAccess().getTEEMOEnumLiteralDeclaration_16()); 
                    // InternalGloe.g:1525:3: ( 'Teemo' )
                    // InternalGloe.g:1525:4: 'Teemo'
                    {
                    match(input,84,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTEEMOEnumLiteralDeclaration_16()); 

                    }


                    }
                    break;
                case 18 :
                    // InternalGloe.g:1529:2: ( ( 'Tristana' ) )
                    {
                    // InternalGloe.g:1529:2: ( ( 'Tristana' ) )
                    // InternalGloe.g:1530:3: ( 'Tristana' )
                    {
                     before(grammarAccess.getChampionAccess().getTRISTANAEnumLiteralDeclaration_17()); 
                    // InternalGloe.g:1531:3: ( 'Tristana' )
                    // InternalGloe.g:1531:4: 'Tristana'
                    {
                    match(input,85,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTRISTANAEnumLiteralDeclaration_17()); 

                    }


                    }
                    break;
                case 19 :
                    // InternalGloe.g:1535:2: ( ( 'Warwick' ) )
                    {
                    // InternalGloe.g:1535:2: ( ( 'Warwick' ) )
                    // InternalGloe.g:1536:3: ( 'Warwick' )
                    {
                     before(grammarAccess.getChampionAccess().getWARWICKEnumLiteralDeclaration_18()); 
                    // InternalGloe.g:1537:3: ( 'Warwick' )
                    // InternalGloe.g:1537:4: 'Warwick'
                    {
                    match(input,86,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getWARWICKEnumLiteralDeclaration_18()); 

                    }


                    }
                    break;
                case 20 :
                    // InternalGloe.g:1541:2: ( ( 'Nunu' ) )
                    {
                    // InternalGloe.g:1541:2: ( ( 'Nunu' ) )
                    // InternalGloe.g:1542:3: ( 'Nunu' )
                    {
                     before(grammarAccess.getChampionAccess().getNUNUEnumLiteralDeclaration_19()); 
                    // InternalGloe.g:1543:3: ( 'Nunu' )
                    // InternalGloe.g:1543:4: 'Nunu'
                    {
                    match(input,87,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getNUNUEnumLiteralDeclaration_19()); 

                    }


                    }
                    break;
                case 21 :
                    // InternalGloe.g:1547:2: ( ( 'Miss Fortune' ) )
                    {
                    // InternalGloe.g:1547:2: ( ( 'Miss Fortune' ) )
                    // InternalGloe.g:1548:3: ( 'Miss Fortune' )
                    {
                     before(grammarAccess.getChampionAccess().getMISSFORTUNEEnumLiteralDeclaration_20()); 
                    // InternalGloe.g:1549:3: ( 'Miss Fortune' )
                    // InternalGloe.g:1549:4: 'Miss Fortune'
                    {
                    match(input,88,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMISSFORTUNEEnumLiteralDeclaration_20()); 

                    }


                    }
                    break;
                case 22 :
                    // InternalGloe.g:1553:2: ( ( 'Ashe' ) )
                    {
                    // InternalGloe.g:1553:2: ( ( 'Ashe' ) )
                    // InternalGloe.g:1554:3: ( 'Ashe' )
                    {
                     before(grammarAccess.getChampionAccess().getASHEEnumLiteralDeclaration_21()); 
                    // InternalGloe.g:1555:3: ( 'Ashe' )
                    // InternalGloe.g:1555:4: 'Ashe'
                    {
                    match(input,89,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getASHEEnumLiteralDeclaration_21()); 

                    }


                    }
                    break;
                case 23 :
                    // InternalGloe.g:1559:2: ( ( 'Tryndamere' ) )
                    {
                    // InternalGloe.g:1559:2: ( ( 'Tryndamere' ) )
                    // InternalGloe.g:1560:3: ( 'Tryndamere' )
                    {
                     before(grammarAccess.getChampionAccess().getTRYNDAMEREEnumLiteralDeclaration_22()); 
                    // InternalGloe.g:1561:3: ( 'Tryndamere' )
                    // InternalGloe.g:1561:4: 'Tryndamere'
                    {
                    match(input,90,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTRYNDAMEREEnumLiteralDeclaration_22()); 

                    }


                    }
                    break;
                case 24 :
                    // InternalGloe.g:1565:2: ( ( 'Jax' ) )
                    {
                    // InternalGloe.g:1565:2: ( ( 'Jax' ) )
                    // InternalGloe.g:1566:3: ( 'Jax' )
                    {
                     before(grammarAccess.getChampionAccess().getJAXEnumLiteralDeclaration_23()); 
                    // InternalGloe.g:1567:3: ( 'Jax' )
                    // InternalGloe.g:1567:4: 'Jax'
                    {
                    match(input,91,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getJAXEnumLiteralDeclaration_23()); 

                    }


                    }
                    break;
                case 25 :
                    // InternalGloe.g:1571:2: ( ( 'Morgana' ) )
                    {
                    // InternalGloe.g:1571:2: ( ( 'Morgana' ) )
                    // InternalGloe.g:1572:3: ( 'Morgana' )
                    {
                     before(grammarAccess.getChampionAccess().getMORGANAEnumLiteralDeclaration_24()); 
                    // InternalGloe.g:1573:3: ( 'Morgana' )
                    // InternalGloe.g:1573:4: 'Morgana'
                    {
                    match(input,92,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMORGANAEnumLiteralDeclaration_24()); 

                    }


                    }
                    break;
                case 26 :
                    // InternalGloe.g:1577:2: ( ( 'Zilean' ) )
                    {
                    // InternalGloe.g:1577:2: ( ( 'Zilean' ) )
                    // InternalGloe.g:1578:3: ( 'Zilean' )
                    {
                     before(grammarAccess.getChampionAccess().getZILEANEnumLiteralDeclaration_25()); 
                    // InternalGloe.g:1579:3: ( 'Zilean' )
                    // InternalGloe.g:1579:4: 'Zilean'
                    {
                    match(input,93,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getZILEANEnumLiteralDeclaration_25()); 

                    }


                    }
                    break;
                case 27 :
                    // InternalGloe.g:1583:2: ( ( 'Singed' ) )
                    {
                    // InternalGloe.g:1583:2: ( ( 'Singed' ) )
                    // InternalGloe.g:1584:3: ( 'Singed' )
                    {
                     before(grammarAccess.getChampionAccess().getSINGEDEnumLiteralDeclaration_26()); 
                    // InternalGloe.g:1585:3: ( 'Singed' )
                    // InternalGloe.g:1585:4: 'Singed'
                    {
                    match(input,94,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSINGEDEnumLiteralDeclaration_26()); 

                    }


                    }
                    break;
                case 28 :
                    // InternalGloe.g:1589:2: ( ( 'Evelynn' ) )
                    {
                    // InternalGloe.g:1589:2: ( ( 'Evelynn' ) )
                    // InternalGloe.g:1590:3: ( 'Evelynn' )
                    {
                     before(grammarAccess.getChampionAccess().getEVELYNNEnumLiteralDeclaration_27()); 
                    // InternalGloe.g:1591:3: ( 'Evelynn' )
                    // InternalGloe.g:1591:4: 'Evelynn'
                    {
                    match(input,95,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getEVELYNNEnumLiteralDeclaration_27()); 

                    }


                    }
                    break;
                case 29 :
                    // InternalGloe.g:1595:2: ( ( 'Twitch' ) )
                    {
                    // InternalGloe.g:1595:2: ( ( 'Twitch' ) )
                    // InternalGloe.g:1596:3: ( 'Twitch' )
                    {
                     before(grammarAccess.getChampionAccess().getTWITCHEnumLiteralDeclaration_28()); 
                    // InternalGloe.g:1597:3: ( 'Twitch' )
                    // InternalGloe.g:1597:4: 'Twitch'
                    {
                    match(input,96,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTWITCHEnumLiteralDeclaration_28()); 

                    }


                    }
                    break;
                case 30 :
                    // InternalGloe.g:1601:2: ( ( 'Karthus' ) )
                    {
                    // InternalGloe.g:1601:2: ( ( 'Karthus' ) )
                    // InternalGloe.g:1602:3: ( 'Karthus' )
                    {
                     before(grammarAccess.getChampionAccess().getKARTHUSEnumLiteralDeclaration_29()); 
                    // InternalGloe.g:1603:3: ( 'Karthus' )
                    // InternalGloe.g:1603:4: 'Karthus'
                    {
                    match(input,97,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKARTHUSEnumLiteralDeclaration_29()); 

                    }


                    }
                    break;
                case 31 :
                    // InternalGloe.g:1607:2: ( ( 'Cho\\'Gath' ) )
                    {
                    // InternalGloe.g:1607:2: ( ( 'Cho\\'Gath' ) )
                    // InternalGloe.g:1608:3: ( 'Cho\\'Gath' )
                    {
                     before(grammarAccess.getChampionAccess().getCHOGATHEnumLiteralDeclaration_30()); 
                    // InternalGloe.g:1609:3: ( 'Cho\\'Gath' )
                    // InternalGloe.g:1609:4: 'Cho\\'Gath'
                    {
                    match(input,98,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getCHOGATHEnumLiteralDeclaration_30()); 

                    }


                    }
                    break;
                case 32 :
                    // InternalGloe.g:1613:2: ( ( 'Amumu' ) )
                    {
                    // InternalGloe.g:1613:2: ( ( 'Amumu' ) )
                    // InternalGloe.g:1614:3: ( 'Amumu' )
                    {
                     before(grammarAccess.getChampionAccess().getAMUMUEnumLiteralDeclaration_31()); 
                    // InternalGloe.g:1615:3: ( 'Amumu' )
                    // InternalGloe.g:1615:4: 'Amumu'
                    {
                    match(input,99,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getAMUMUEnumLiteralDeclaration_31()); 

                    }


                    }
                    break;
                case 33 :
                    // InternalGloe.g:1619:2: ( ( 'Rammus' ) )
                    {
                    // InternalGloe.g:1619:2: ( ( 'Rammus' ) )
                    // InternalGloe.g:1620:3: ( 'Rammus' )
                    {
                     before(grammarAccess.getChampionAccess().getRAMMUSEnumLiteralDeclaration_32()); 
                    // InternalGloe.g:1621:3: ( 'Rammus' )
                    // InternalGloe.g:1621:4: 'Rammus'
                    {
                    match(input,100,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRAMMUSEnumLiteralDeclaration_32()); 

                    }


                    }
                    break;
                case 34 :
                    // InternalGloe.g:1625:2: ( ( 'Anivia' ) )
                    {
                    // InternalGloe.g:1625:2: ( ( 'Anivia' ) )
                    // InternalGloe.g:1626:3: ( 'Anivia' )
                    {
                     before(grammarAccess.getChampionAccess().getANIVIAEnumLiteralDeclaration_33()); 
                    // InternalGloe.g:1627:3: ( 'Anivia' )
                    // InternalGloe.g:1627:4: 'Anivia'
                    {
                    match(input,101,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getANIVIAEnumLiteralDeclaration_33()); 

                    }


                    }
                    break;
                case 35 :
                    // InternalGloe.g:1631:2: ( ( 'Shaco' ) )
                    {
                    // InternalGloe.g:1631:2: ( ( 'Shaco' ) )
                    // InternalGloe.g:1632:3: ( 'Shaco' )
                    {
                     before(grammarAccess.getChampionAccess().getSHACOEnumLiteralDeclaration_34()); 
                    // InternalGloe.g:1633:3: ( 'Shaco' )
                    // InternalGloe.g:1633:4: 'Shaco'
                    {
                    match(input,102,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSHACOEnumLiteralDeclaration_34()); 

                    }


                    }
                    break;
                case 36 :
                    // InternalGloe.g:1637:2: ( ( 'Dr. Mundo' ) )
                    {
                    // InternalGloe.g:1637:2: ( ( 'Dr. Mundo' ) )
                    // InternalGloe.g:1638:3: ( 'Dr. Mundo' )
                    {
                     before(grammarAccess.getChampionAccess().getDRMUNDOEnumLiteralDeclaration_35()); 
                    // InternalGloe.g:1639:3: ( 'Dr. Mundo' )
                    // InternalGloe.g:1639:4: 'Dr. Mundo'
                    {
                    match(input,103,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getDRMUNDOEnumLiteralDeclaration_35()); 

                    }


                    }
                    break;
                case 37 :
                    // InternalGloe.g:1643:2: ( ( 'Sona' ) )
                    {
                    // InternalGloe.g:1643:2: ( ( 'Sona' ) )
                    // InternalGloe.g:1644:3: ( 'Sona' )
                    {
                     before(grammarAccess.getChampionAccess().getSONAEnumLiteralDeclaration_36()); 
                    // InternalGloe.g:1645:3: ( 'Sona' )
                    // InternalGloe.g:1645:4: 'Sona'
                    {
                    match(input,104,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSONAEnumLiteralDeclaration_36()); 

                    }


                    }
                    break;
                case 38 :
                    // InternalGloe.g:1649:2: ( ( 'Kassadin' ) )
                    {
                    // InternalGloe.g:1649:2: ( ( 'Kassadin' ) )
                    // InternalGloe.g:1650:3: ( 'Kassadin' )
                    {
                     before(grammarAccess.getChampionAccess().getKASSADINEnumLiteralDeclaration_37()); 
                    // InternalGloe.g:1651:3: ( 'Kassadin' )
                    // InternalGloe.g:1651:4: 'Kassadin'
                    {
                    match(input,105,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKASSADINEnumLiteralDeclaration_37()); 

                    }


                    }
                    break;
                case 39 :
                    // InternalGloe.g:1655:2: ( ( 'Irelia' ) )
                    {
                    // InternalGloe.g:1655:2: ( ( 'Irelia' ) )
                    // InternalGloe.g:1656:3: ( 'Irelia' )
                    {
                     before(grammarAccess.getChampionAccess().getIRELIAEnumLiteralDeclaration_38()); 
                    // InternalGloe.g:1657:3: ( 'Irelia' )
                    // InternalGloe.g:1657:4: 'Irelia'
                    {
                    match(input,106,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getIRELIAEnumLiteralDeclaration_38()); 

                    }


                    }
                    break;
                case 40 :
                    // InternalGloe.g:1661:2: ( ( 'Janna' ) )
                    {
                    // InternalGloe.g:1661:2: ( ( 'Janna' ) )
                    // InternalGloe.g:1662:3: ( 'Janna' )
                    {
                     before(grammarAccess.getChampionAccess().getJANNAEnumLiteralDeclaration_39()); 
                    // InternalGloe.g:1663:3: ( 'Janna' )
                    // InternalGloe.g:1663:4: 'Janna'
                    {
                    match(input,107,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getJANNAEnumLiteralDeclaration_39()); 

                    }


                    }
                    break;
                case 41 :
                    // InternalGloe.g:1667:2: ( ( 'Gangplank' ) )
                    {
                    // InternalGloe.g:1667:2: ( ( 'Gangplank' ) )
                    // InternalGloe.g:1668:3: ( 'Gangplank' )
                    {
                     before(grammarAccess.getChampionAccess().getGANGPLANKEnumLiteralDeclaration_40()); 
                    // InternalGloe.g:1669:3: ( 'Gangplank' )
                    // InternalGloe.g:1669:4: 'Gangplank'
                    {
                    match(input,108,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getGANGPLANKEnumLiteralDeclaration_40()); 

                    }


                    }
                    break;
                case 42 :
                    // InternalGloe.g:1673:2: ( ( 'Corki' ) )
                    {
                    // InternalGloe.g:1673:2: ( ( 'Corki' ) )
                    // InternalGloe.g:1674:3: ( 'Corki' )
                    {
                     before(grammarAccess.getChampionAccess().getCORKIEnumLiteralDeclaration_41()); 
                    // InternalGloe.g:1675:3: ( 'Corki' )
                    // InternalGloe.g:1675:4: 'Corki'
                    {
                    match(input,109,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getCORKIEnumLiteralDeclaration_41()); 

                    }


                    }
                    break;
                case 43 :
                    // InternalGloe.g:1679:2: ( ( 'Karma' ) )
                    {
                    // InternalGloe.g:1679:2: ( ( 'Karma' ) )
                    // InternalGloe.g:1680:3: ( 'Karma' )
                    {
                     before(grammarAccess.getChampionAccess().getKARMAEnumLiteralDeclaration_42()); 
                    // InternalGloe.g:1681:3: ( 'Karma' )
                    // InternalGloe.g:1681:4: 'Karma'
                    {
                    match(input,110,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKARMAEnumLiteralDeclaration_42()); 

                    }


                    }
                    break;
                case 44 :
                    // InternalGloe.g:1685:2: ( ( 'Taric' ) )
                    {
                    // InternalGloe.g:1685:2: ( ( 'Taric' ) )
                    // InternalGloe.g:1686:3: ( 'Taric' )
                    {
                     before(grammarAccess.getChampionAccess().getTARICEnumLiteralDeclaration_43()); 
                    // InternalGloe.g:1687:3: ( 'Taric' )
                    // InternalGloe.g:1687:4: 'Taric'
                    {
                    match(input,111,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTARICEnumLiteralDeclaration_43()); 

                    }


                    }
                    break;
                case 45 :
                    // InternalGloe.g:1691:2: ( ( 'Veigar' ) )
                    {
                    // InternalGloe.g:1691:2: ( ( 'Veigar' ) )
                    // InternalGloe.g:1692:3: ( 'Veigar' )
                    {
                     before(grammarAccess.getChampionAccess().getVEIGAREnumLiteralDeclaration_44()); 
                    // InternalGloe.g:1693:3: ( 'Veigar' )
                    // InternalGloe.g:1693:4: 'Veigar'
                    {
                    match(input,112,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVEIGAREnumLiteralDeclaration_44()); 

                    }


                    }
                    break;
                case 46 :
                    // InternalGloe.g:1697:2: ( ( 'Trundle' ) )
                    {
                    // InternalGloe.g:1697:2: ( ( 'Trundle' ) )
                    // InternalGloe.g:1698:3: ( 'Trundle' )
                    {
                     before(grammarAccess.getChampionAccess().getTRUNDLEEnumLiteralDeclaration_45()); 
                    // InternalGloe.g:1699:3: ( 'Trundle' )
                    // InternalGloe.g:1699:4: 'Trundle'
                    {
                    match(input,113,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTRUNDLEEnumLiteralDeclaration_45()); 

                    }


                    }
                    break;
                case 47 :
                    // InternalGloe.g:1703:2: ( ( 'Swain' ) )
                    {
                    // InternalGloe.g:1703:2: ( ( 'Swain' ) )
                    // InternalGloe.g:1704:3: ( 'Swain' )
                    {
                     before(grammarAccess.getChampionAccess().getSWAINEnumLiteralDeclaration_46()); 
                    // InternalGloe.g:1705:3: ( 'Swain' )
                    // InternalGloe.g:1705:4: 'Swain'
                    {
                    match(input,114,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSWAINEnumLiteralDeclaration_46()); 

                    }


                    }
                    break;
                case 48 :
                    // InternalGloe.g:1709:2: ( ( 'Caitlyn' ) )
                    {
                    // InternalGloe.g:1709:2: ( ( 'Caitlyn' ) )
                    // InternalGloe.g:1710:3: ( 'Caitlyn' )
                    {
                     before(grammarAccess.getChampionAccess().getCAITLYNEnumLiteralDeclaration_47()); 
                    // InternalGloe.g:1711:3: ( 'Caitlyn' )
                    // InternalGloe.g:1711:4: 'Caitlyn'
                    {
                    match(input,115,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getCAITLYNEnumLiteralDeclaration_47()); 

                    }


                    }
                    break;
                case 49 :
                    // InternalGloe.g:1715:2: ( ( 'Blitzcrank' ) )
                    {
                    // InternalGloe.g:1715:2: ( ( 'Blitzcrank' ) )
                    // InternalGloe.g:1716:3: ( 'Blitzcrank' )
                    {
                     before(grammarAccess.getChampionAccess().getBLITZCRANKEnumLiteralDeclaration_48()); 
                    // InternalGloe.g:1717:3: ( 'Blitzcrank' )
                    // InternalGloe.g:1717:4: 'Blitzcrank'
                    {
                    match(input,116,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getBLITZCRANKEnumLiteralDeclaration_48()); 

                    }


                    }
                    break;
                case 50 :
                    // InternalGloe.g:1721:2: ( ( 'Malphite' ) )
                    {
                    // InternalGloe.g:1721:2: ( ( 'Malphite' ) )
                    // InternalGloe.g:1722:3: ( 'Malphite' )
                    {
                     before(grammarAccess.getChampionAccess().getMALPHITEEnumLiteralDeclaration_49()); 
                    // InternalGloe.g:1723:3: ( 'Malphite' )
                    // InternalGloe.g:1723:4: 'Malphite'
                    {
                    match(input,117,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMALPHITEEnumLiteralDeclaration_49()); 

                    }


                    }
                    break;
                case 51 :
                    // InternalGloe.g:1727:2: ( ( 'Katarina' ) )
                    {
                    // InternalGloe.g:1727:2: ( ( 'Katarina' ) )
                    // InternalGloe.g:1728:3: ( 'Katarina' )
                    {
                     before(grammarAccess.getChampionAccess().getKATARINAEnumLiteralDeclaration_50()); 
                    // InternalGloe.g:1729:3: ( 'Katarina' )
                    // InternalGloe.g:1729:4: 'Katarina'
                    {
                    match(input,118,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKATARINAEnumLiteralDeclaration_50()); 

                    }


                    }
                    break;
                case 52 :
                    // InternalGloe.g:1733:2: ( ( 'Nocturne' ) )
                    {
                    // InternalGloe.g:1733:2: ( ( 'Nocturne' ) )
                    // InternalGloe.g:1734:3: ( 'Nocturne' )
                    {
                     before(grammarAccess.getChampionAccess().getNOCTURNEEnumLiteralDeclaration_51()); 
                    // InternalGloe.g:1735:3: ( 'Nocturne' )
                    // InternalGloe.g:1735:4: 'Nocturne'
                    {
                    match(input,119,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getNOCTURNEEnumLiteralDeclaration_51()); 

                    }


                    }
                    break;
                case 53 :
                    // InternalGloe.g:1739:2: ( ( 'Maokai' ) )
                    {
                    // InternalGloe.g:1739:2: ( ( 'Maokai' ) )
                    // InternalGloe.g:1740:3: ( 'Maokai' )
                    {
                     before(grammarAccess.getChampionAccess().getMAOKAIEnumLiteralDeclaration_52()); 
                    // InternalGloe.g:1741:3: ( 'Maokai' )
                    // InternalGloe.g:1741:4: 'Maokai'
                    {
                    match(input,120,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMAOKAIEnumLiteralDeclaration_52()); 

                    }


                    }
                    break;
                case 54 :
                    // InternalGloe.g:1745:2: ( ( 'Renekton' ) )
                    {
                    // InternalGloe.g:1745:2: ( ( 'Renekton' ) )
                    // InternalGloe.g:1746:3: ( 'Renekton' )
                    {
                     before(grammarAccess.getChampionAccess().getRENEKTONEnumLiteralDeclaration_53()); 
                    // InternalGloe.g:1747:3: ( 'Renekton' )
                    // InternalGloe.g:1747:4: 'Renekton'
                    {
                    match(input,121,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRENEKTONEnumLiteralDeclaration_53()); 

                    }


                    }
                    break;
                case 55 :
                    // InternalGloe.g:1751:2: ( ( 'Jarvan IV' ) )
                    {
                    // InternalGloe.g:1751:2: ( ( 'Jarvan IV' ) )
                    // InternalGloe.g:1752:3: ( 'Jarvan IV' )
                    {
                     before(grammarAccess.getChampionAccess().getJARVANIVEnumLiteralDeclaration_54()); 
                    // InternalGloe.g:1753:3: ( 'Jarvan IV' )
                    // InternalGloe.g:1753:4: 'Jarvan IV'
                    {
                    match(input,122,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getJARVANIVEnumLiteralDeclaration_54()); 

                    }


                    }
                    break;
                case 56 :
                    // InternalGloe.g:1757:2: ( ( 'Elise' ) )
                    {
                    // InternalGloe.g:1757:2: ( ( 'Elise' ) )
                    // InternalGloe.g:1758:3: ( 'Elise' )
                    {
                     before(grammarAccess.getChampionAccess().getELISEEnumLiteralDeclaration_55()); 
                    // InternalGloe.g:1759:3: ( 'Elise' )
                    // InternalGloe.g:1759:4: 'Elise'
                    {
                    match(input,123,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getELISEEnumLiteralDeclaration_55()); 

                    }


                    }
                    break;
                case 57 :
                    // InternalGloe.g:1763:2: ( ( 'Orianna' ) )
                    {
                    // InternalGloe.g:1763:2: ( ( 'Orianna' ) )
                    // InternalGloe.g:1764:3: ( 'Orianna' )
                    {
                     before(grammarAccess.getChampionAccess().getORIANNAEnumLiteralDeclaration_56()); 
                    // InternalGloe.g:1765:3: ( 'Orianna' )
                    // InternalGloe.g:1765:4: 'Orianna'
                    {
                    match(input,124,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getORIANNAEnumLiteralDeclaration_56()); 

                    }


                    }
                    break;
                case 58 :
                    // InternalGloe.g:1769:2: ( ( 'Wukong' ) )
                    {
                    // InternalGloe.g:1769:2: ( ( 'Wukong' ) )
                    // InternalGloe.g:1770:3: ( 'Wukong' )
                    {
                     before(grammarAccess.getChampionAccess().getMONKEYKINGEnumLiteralDeclaration_57()); 
                    // InternalGloe.g:1771:3: ( 'Wukong' )
                    // InternalGloe.g:1771:4: 'Wukong'
                    {
                    match(input,125,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMONKEYKINGEnumLiteralDeclaration_57()); 

                    }


                    }
                    break;
                case 59 :
                    // InternalGloe.g:1775:2: ( ( 'Brand' ) )
                    {
                    // InternalGloe.g:1775:2: ( ( 'Brand' ) )
                    // InternalGloe.g:1776:3: ( 'Brand' )
                    {
                     before(grammarAccess.getChampionAccess().getBRANDEnumLiteralDeclaration_58()); 
                    // InternalGloe.g:1777:3: ( 'Brand' )
                    // InternalGloe.g:1777:4: 'Brand'
                    {
                    match(input,126,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getBRANDEnumLiteralDeclaration_58()); 

                    }


                    }
                    break;
                case 60 :
                    // InternalGloe.g:1781:2: ( ( 'Lee Sin' ) )
                    {
                    // InternalGloe.g:1781:2: ( ( 'Lee Sin' ) )
                    // InternalGloe.g:1782:3: ( 'Lee Sin' )
                    {
                     before(grammarAccess.getChampionAccess().getLEESINEnumLiteralDeclaration_59()); 
                    // InternalGloe.g:1783:3: ( 'Lee Sin' )
                    // InternalGloe.g:1783:4: 'Lee Sin'
                    {
                    match(input,127,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLEESINEnumLiteralDeclaration_59()); 

                    }


                    }
                    break;
                case 61 :
                    // InternalGloe.g:1787:2: ( ( 'Vayne' ) )
                    {
                    // InternalGloe.g:1787:2: ( ( 'Vayne' ) )
                    // InternalGloe.g:1788:3: ( 'Vayne' )
                    {
                     before(grammarAccess.getChampionAccess().getVAYNEEnumLiteralDeclaration_60()); 
                    // InternalGloe.g:1789:3: ( 'Vayne' )
                    // InternalGloe.g:1789:4: 'Vayne'
                    {
                    match(input,128,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVAYNEEnumLiteralDeclaration_60()); 

                    }


                    }
                    break;
                case 62 :
                    // InternalGloe.g:1793:2: ( ( 'Rumble' ) )
                    {
                    // InternalGloe.g:1793:2: ( ( 'Rumble' ) )
                    // InternalGloe.g:1794:3: ( 'Rumble' )
                    {
                     before(grammarAccess.getChampionAccess().getRUMBLEEnumLiteralDeclaration_61()); 
                    // InternalGloe.g:1795:3: ( 'Rumble' )
                    // InternalGloe.g:1795:4: 'Rumble'
                    {
                    match(input,129,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRUMBLEEnumLiteralDeclaration_61()); 

                    }


                    }
                    break;
                case 63 :
                    // InternalGloe.g:1799:2: ( ( 'Cassiopeia' ) )
                    {
                    // InternalGloe.g:1799:2: ( ( 'Cassiopeia' ) )
                    // InternalGloe.g:1800:3: ( 'Cassiopeia' )
                    {
                     before(grammarAccess.getChampionAccess().getCASSIOPEIAEnumLiteralDeclaration_62()); 
                    // InternalGloe.g:1801:3: ( 'Cassiopeia' )
                    // InternalGloe.g:1801:4: 'Cassiopeia'
                    {
                    match(input,130,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getCASSIOPEIAEnumLiteralDeclaration_62()); 

                    }


                    }
                    break;
                case 64 :
                    // InternalGloe.g:1805:2: ( ( 'Skarner' ) )
                    {
                    // InternalGloe.g:1805:2: ( ( 'Skarner' ) )
                    // InternalGloe.g:1806:3: ( 'Skarner' )
                    {
                     before(grammarAccess.getChampionAccess().getSKARNEREnumLiteralDeclaration_63()); 
                    // InternalGloe.g:1807:3: ( 'Skarner' )
                    // InternalGloe.g:1807:4: 'Skarner'
                    {
                    match(input,131,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSKARNEREnumLiteralDeclaration_63()); 

                    }


                    }
                    break;
                case 65 :
                    // InternalGloe.g:1811:2: ( ( 'Heimerdinger' ) )
                    {
                    // InternalGloe.g:1811:2: ( ( 'Heimerdinger' ) )
                    // InternalGloe.g:1812:3: ( 'Heimerdinger' )
                    {
                     before(grammarAccess.getChampionAccess().getHEIMERDINGEREnumLiteralDeclaration_64()); 
                    // InternalGloe.g:1813:3: ( 'Heimerdinger' )
                    // InternalGloe.g:1813:4: 'Heimerdinger'
                    {
                    match(input,132,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getHEIMERDINGEREnumLiteralDeclaration_64()); 

                    }


                    }
                    break;
                case 66 :
                    // InternalGloe.g:1817:2: ( ( 'Nasus' ) )
                    {
                    // InternalGloe.g:1817:2: ( ( 'Nasus' ) )
                    // InternalGloe.g:1818:3: ( 'Nasus' )
                    {
                     before(grammarAccess.getChampionAccess().getNASUSEnumLiteralDeclaration_65()); 
                    // InternalGloe.g:1819:3: ( 'Nasus' )
                    // InternalGloe.g:1819:4: 'Nasus'
                    {
                    match(input,133,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getNASUSEnumLiteralDeclaration_65()); 

                    }


                    }
                    break;
                case 67 :
                    // InternalGloe.g:1823:2: ( ( 'Nidalee' ) )
                    {
                    // InternalGloe.g:1823:2: ( ( 'Nidalee' ) )
                    // InternalGloe.g:1824:3: ( 'Nidalee' )
                    {
                     before(grammarAccess.getChampionAccess().getNIDALEEEnumLiteralDeclaration_66()); 
                    // InternalGloe.g:1825:3: ( 'Nidalee' )
                    // InternalGloe.g:1825:4: 'Nidalee'
                    {
                    match(input,134,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getNIDALEEEnumLiteralDeclaration_66()); 

                    }


                    }
                    break;
                case 68 :
                    // InternalGloe.g:1829:2: ( ( 'Udyr' ) )
                    {
                    // InternalGloe.g:1829:2: ( ( 'Udyr' ) )
                    // InternalGloe.g:1830:3: ( 'Udyr' )
                    {
                     before(grammarAccess.getChampionAccess().getUDYREnumLiteralDeclaration_67()); 
                    // InternalGloe.g:1831:3: ( 'Udyr' )
                    // InternalGloe.g:1831:4: 'Udyr'
                    {
                    match(input,135,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getUDYREnumLiteralDeclaration_67()); 

                    }


                    }
                    break;
                case 69 :
                    // InternalGloe.g:1835:2: ( ( 'Poppy' ) )
                    {
                    // InternalGloe.g:1835:2: ( ( 'Poppy' ) )
                    // InternalGloe.g:1836:3: ( 'Poppy' )
                    {
                     before(grammarAccess.getChampionAccess().getPOPPYEnumLiteralDeclaration_68()); 
                    // InternalGloe.g:1837:3: ( 'Poppy' )
                    // InternalGloe.g:1837:4: 'Poppy'
                    {
                    match(input,136,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getPOPPYEnumLiteralDeclaration_68()); 

                    }


                    }
                    break;
                case 70 :
                    // InternalGloe.g:1841:2: ( ( 'Gragas' ) )
                    {
                    // InternalGloe.g:1841:2: ( ( 'Gragas' ) )
                    // InternalGloe.g:1842:3: ( 'Gragas' )
                    {
                     before(grammarAccess.getChampionAccess().getGRAGASEnumLiteralDeclaration_69()); 
                    // InternalGloe.g:1843:3: ( 'Gragas' )
                    // InternalGloe.g:1843:4: 'Gragas'
                    {
                    match(input,137,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getGRAGASEnumLiteralDeclaration_69()); 

                    }


                    }
                    break;
                case 71 :
                    // InternalGloe.g:1847:2: ( ( 'Pantheon' ) )
                    {
                    // InternalGloe.g:1847:2: ( ( 'Pantheon' ) )
                    // InternalGloe.g:1848:3: ( 'Pantheon' )
                    {
                     before(grammarAccess.getChampionAccess().getPANTHEONEnumLiteralDeclaration_70()); 
                    // InternalGloe.g:1849:3: ( 'Pantheon' )
                    // InternalGloe.g:1849:4: 'Pantheon'
                    {
                    match(input,138,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getPANTHEONEnumLiteralDeclaration_70()); 

                    }


                    }
                    break;
                case 72 :
                    // InternalGloe.g:1853:2: ( ( 'Ezreal' ) )
                    {
                    // InternalGloe.g:1853:2: ( ( 'Ezreal' ) )
                    // InternalGloe.g:1854:3: ( 'Ezreal' )
                    {
                     before(grammarAccess.getChampionAccess().getEZREALEnumLiteralDeclaration_71()); 
                    // InternalGloe.g:1855:3: ( 'Ezreal' )
                    // InternalGloe.g:1855:4: 'Ezreal'
                    {
                    match(input,139,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getEZREALEnumLiteralDeclaration_71()); 

                    }


                    }
                    break;
                case 73 :
                    // InternalGloe.g:1859:2: ( ( 'Mordekaiser' ) )
                    {
                    // InternalGloe.g:1859:2: ( ( 'Mordekaiser' ) )
                    // InternalGloe.g:1860:3: ( 'Mordekaiser' )
                    {
                     before(grammarAccess.getChampionAccess().getMORDEKAISEREnumLiteralDeclaration_72()); 
                    // InternalGloe.g:1861:3: ( 'Mordekaiser' )
                    // InternalGloe.g:1861:4: 'Mordekaiser'
                    {
                    match(input,140,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMORDEKAISEREnumLiteralDeclaration_72()); 

                    }


                    }
                    break;
                case 74 :
                    // InternalGloe.g:1865:2: ( ( 'Yorick' ) )
                    {
                    // InternalGloe.g:1865:2: ( ( 'Yorick' ) )
                    // InternalGloe.g:1866:3: ( 'Yorick' )
                    {
                     before(grammarAccess.getChampionAccess().getYORICKEnumLiteralDeclaration_73()); 
                    // InternalGloe.g:1867:3: ( 'Yorick' )
                    // InternalGloe.g:1867:4: 'Yorick'
                    {
                    match(input,141,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getYORICKEnumLiteralDeclaration_73()); 

                    }


                    }
                    break;
                case 75 :
                    // InternalGloe.g:1871:2: ( ( 'Akali' ) )
                    {
                    // InternalGloe.g:1871:2: ( ( 'Akali' ) )
                    // InternalGloe.g:1872:3: ( 'Akali' )
                    {
                     before(grammarAccess.getChampionAccess().getAKALIEnumLiteralDeclaration_74()); 
                    // InternalGloe.g:1873:3: ( 'Akali' )
                    // InternalGloe.g:1873:4: 'Akali'
                    {
                    match(input,142,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getAKALIEnumLiteralDeclaration_74()); 

                    }


                    }
                    break;
                case 76 :
                    // InternalGloe.g:1877:2: ( ( 'Kennen' ) )
                    {
                    // InternalGloe.g:1877:2: ( ( 'Kennen' ) )
                    // InternalGloe.g:1878:3: ( 'Kennen' )
                    {
                     before(grammarAccess.getChampionAccess().getKENNENEnumLiteralDeclaration_75()); 
                    // InternalGloe.g:1879:3: ( 'Kennen' )
                    // InternalGloe.g:1879:4: 'Kennen'
                    {
                    match(input,143,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKENNENEnumLiteralDeclaration_75()); 

                    }


                    }
                    break;
                case 77 :
                    // InternalGloe.g:1883:2: ( ( 'Garen' ) )
                    {
                    // InternalGloe.g:1883:2: ( ( 'Garen' ) )
                    // InternalGloe.g:1884:3: ( 'Garen' )
                    {
                     before(grammarAccess.getChampionAccess().getGARENEnumLiteralDeclaration_76()); 
                    // InternalGloe.g:1885:3: ( 'Garen' )
                    // InternalGloe.g:1885:4: 'Garen'
                    {
                    match(input,144,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getGARENEnumLiteralDeclaration_76()); 

                    }


                    }
                    break;
                case 78 :
                    // InternalGloe.g:1889:2: ( ( 'Leona' ) )
                    {
                    // InternalGloe.g:1889:2: ( ( 'Leona' ) )
                    // InternalGloe.g:1890:3: ( 'Leona' )
                    {
                     before(grammarAccess.getChampionAccess().getLEONAEnumLiteralDeclaration_77()); 
                    // InternalGloe.g:1891:3: ( 'Leona' )
                    // InternalGloe.g:1891:4: 'Leona'
                    {
                    match(input,145,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLEONAEnumLiteralDeclaration_77()); 

                    }


                    }
                    break;
                case 79 :
                    // InternalGloe.g:1895:2: ( ( 'Malzahar' ) )
                    {
                    // InternalGloe.g:1895:2: ( ( 'Malzahar' ) )
                    // InternalGloe.g:1896:3: ( 'Malzahar' )
                    {
                     before(grammarAccess.getChampionAccess().getMALZAHAREnumLiteralDeclaration_78()); 
                    // InternalGloe.g:1897:3: ( 'Malzahar' )
                    // InternalGloe.g:1897:4: 'Malzahar'
                    {
                    match(input,146,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getMALZAHAREnumLiteralDeclaration_78()); 

                    }


                    }
                    break;
                case 80 :
                    // InternalGloe.g:1901:2: ( ( 'Talon' ) )
                    {
                    // InternalGloe.g:1901:2: ( ( 'Talon' ) )
                    // InternalGloe.g:1902:3: ( 'Talon' )
                    {
                     before(grammarAccess.getChampionAccess().getTALONEnumLiteralDeclaration_79()); 
                    // InternalGloe.g:1903:3: ( 'Talon' )
                    // InternalGloe.g:1903:4: 'Talon'
                    {
                    match(input,147,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTALONEnumLiteralDeclaration_79()); 

                    }


                    }
                    break;
                case 81 :
                    // InternalGloe.g:1907:2: ( ( 'Riven' ) )
                    {
                    // InternalGloe.g:1907:2: ( ( 'Riven' ) )
                    // InternalGloe.g:1908:3: ( 'Riven' )
                    {
                     before(grammarAccess.getChampionAccess().getRIVENEnumLiteralDeclaration_80()); 
                    // InternalGloe.g:1909:3: ( 'Riven' )
                    // InternalGloe.g:1909:4: 'Riven'
                    {
                    match(input,148,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRIVENEnumLiteralDeclaration_80()); 

                    }


                    }
                    break;
                case 82 :
                    // InternalGloe.g:1913:2: ( ( 'Kog\\'Maw' ) )
                    {
                    // InternalGloe.g:1913:2: ( ( 'Kog\\'Maw' ) )
                    // InternalGloe.g:1914:3: ( 'Kog\\'Maw' )
                    {
                     before(grammarAccess.getChampionAccess().getKOGMAWEnumLiteralDeclaration_81()); 
                    // InternalGloe.g:1915:3: ( 'Kog\\'Maw' )
                    // InternalGloe.g:1915:4: 'Kog\\'Maw'
                    {
                    match(input,149,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKOGMAWEnumLiteralDeclaration_81()); 

                    }


                    }
                    break;
                case 83 :
                    // InternalGloe.g:1919:2: ( ( 'Shen' ) )
                    {
                    // InternalGloe.g:1919:2: ( ( 'Shen' ) )
                    // InternalGloe.g:1920:3: ( 'Shen' )
                    {
                     before(grammarAccess.getChampionAccess().getSHENEnumLiteralDeclaration_82()); 
                    // InternalGloe.g:1921:3: ( 'Shen' )
                    // InternalGloe.g:1921:4: 'Shen'
                    {
                    match(input,150,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSHENEnumLiteralDeclaration_82()); 

                    }


                    }
                    break;
                case 84 :
                    // InternalGloe.g:1925:2: ( ( 'Lux' ) )
                    {
                    // InternalGloe.g:1925:2: ( ( 'Lux' ) )
                    // InternalGloe.g:1926:3: ( 'Lux' )
                    {
                     before(grammarAccess.getChampionAccess().getLUXEnumLiteralDeclaration_83()); 
                    // InternalGloe.g:1927:3: ( 'Lux' )
                    // InternalGloe.g:1927:4: 'Lux'
                    {
                    match(input,151,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLUXEnumLiteralDeclaration_83()); 

                    }


                    }
                    break;
                case 85 :
                    // InternalGloe.g:1931:2: ( ( 'Xerath' ) )
                    {
                    // InternalGloe.g:1931:2: ( ( 'Xerath' ) )
                    // InternalGloe.g:1932:3: ( 'Xerath' )
                    {
                     before(grammarAccess.getChampionAccess().getXERATHEnumLiteralDeclaration_84()); 
                    // InternalGloe.g:1933:3: ( 'Xerath' )
                    // InternalGloe.g:1933:4: 'Xerath'
                    {
                    match(input,152,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getXERATHEnumLiteralDeclaration_84()); 

                    }


                    }
                    break;
                case 86 :
                    // InternalGloe.g:1937:2: ( ( 'Shyvana' ) )
                    {
                    // InternalGloe.g:1937:2: ( ( 'Shyvana' ) )
                    // InternalGloe.g:1938:3: ( 'Shyvana' )
                    {
                     before(grammarAccess.getChampionAccess().getSHYVANAEnumLiteralDeclaration_85()); 
                    // InternalGloe.g:1939:3: ( 'Shyvana' )
                    // InternalGloe.g:1939:4: 'Shyvana'
                    {
                    match(input,153,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSHYVANAEnumLiteralDeclaration_85()); 

                    }


                    }
                    break;
                case 87 :
                    // InternalGloe.g:1943:2: ( ( 'Ahri' ) )
                    {
                    // InternalGloe.g:1943:2: ( ( 'Ahri' ) )
                    // InternalGloe.g:1944:3: ( 'Ahri' )
                    {
                     before(grammarAccess.getChampionAccess().getAHRIEnumLiteralDeclaration_86()); 
                    // InternalGloe.g:1945:3: ( 'Ahri' )
                    // InternalGloe.g:1945:4: 'Ahri'
                    {
                    match(input,154,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getAHRIEnumLiteralDeclaration_86()); 

                    }


                    }
                    break;
                case 88 :
                    // InternalGloe.g:1949:2: ( ( 'Graves' ) )
                    {
                    // InternalGloe.g:1949:2: ( ( 'Graves' ) )
                    // InternalGloe.g:1950:3: ( 'Graves' )
                    {
                     before(grammarAccess.getChampionAccess().getGRAVESEnumLiteralDeclaration_87()); 
                    // InternalGloe.g:1951:3: ( 'Graves' )
                    // InternalGloe.g:1951:4: 'Graves'
                    {
                    match(input,155,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getGRAVESEnumLiteralDeclaration_87()); 

                    }


                    }
                    break;
                case 89 :
                    // InternalGloe.g:1955:2: ( ( 'Fizz' ) )
                    {
                    // InternalGloe.g:1955:2: ( ( 'Fizz' ) )
                    // InternalGloe.g:1956:3: ( 'Fizz' )
                    {
                     before(grammarAccess.getChampionAccess().getFIZZEnumLiteralDeclaration_88()); 
                    // InternalGloe.g:1957:3: ( 'Fizz' )
                    // InternalGloe.g:1957:4: 'Fizz'
                    {
                    match(input,156,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getFIZZEnumLiteralDeclaration_88()); 

                    }


                    }
                    break;
                case 90 :
                    // InternalGloe.g:1961:2: ( ( 'Volibear' ) )
                    {
                    // InternalGloe.g:1961:2: ( ( 'Volibear' ) )
                    // InternalGloe.g:1962:3: ( 'Volibear' )
                    {
                     before(grammarAccess.getChampionAccess().getVOLIBEAREnumLiteralDeclaration_89()); 
                    // InternalGloe.g:1963:3: ( 'Volibear' )
                    // InternalGloe.g:1963:4: 'Volibear'
                    {
                    match(input,157,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVOLIBEAREnumLiteralDeclaration_89()); 

                    }


                    }
                    break;
                case 91 :
                    // InternalGloe.g:1967:2: ( ( 'Rengar' ) )
                    {
                    // InternalGloe.g:1967:2: ( ( 'Rengar' ) )
                    // InternalGloe.g:1968:3: ( 'Rengar' )
                    {
                     before(grammarAccess.getChampionAccess().getRENGAREnumLiteralDeclaration_90()); 
                    // InternalGloe.g:1969:3: ( 'Rengar' )
                    // InternalGloe.g:1969:4: 'Rengar'
                    {
                    match(input,158,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRENGAREnumLiteralDeclaration_90()); 

                    }


                    }
                    break;
                case 92 :
                    // InternalGloe.g:1973:2: ( ( 'Varus' ) )
                    {
                    // InternalGloe.g:1973:2: ( ( 'Varus' ) )
                    // InternalGloe.g:1974:3: ( 'Varus' )
                    {
                     before(grammarAccess.getChampionAccess().getVARUSEnumLiteralDeclaration_91()); 
                    // InternalGloe.g:1975:3: ( 'Varus' )
                    // InternalGloe.g:1975:4: 'Varus'
                    {
                    match(input,159,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVARUSEnumLiteralDeclaration_91()); 

                    }


                    }
                    break;
                case 93 :
                    // InternalGloe.g:1979:2: ( ( 'Nautilus' ) )
                    {
                    // InternalGloe.g:1979:2: ( ( 'Nautilus' ) )
                    // InternalGloe.g:1980:3: ( 'Nautilus' )
                    {
                     before(grammarAccess.getChampionAccess().getNAUTILUSEnumLiteralDeclaration_92()); 
                    // InternalGloe.g:1981:3: ( 'Nautilus' )
                    // InternalGloe.g:1981:4: 'Nautilus'
                    {
                    match(input,160,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getNAUTILUSEnumLiteralDeclaration_92()); 

                    }


                    }
                    break;
                case 94 :
                    // InternalGloe.g:1985:2: ( ( 'Viktor' ) )
                    {
                    // InternalGloe.g:1985:2: ( ( 'Viktor' ) )
                    // InternalGloe.g:1986:3: ( 'Viktor' )
                    {
                     before(grammarAccess.getChampionAccess().getVIKTOREnumLiteralDeclaration_93()); 
                    // InternalGloe.g:1987:3: ( 'Viktor' )
                    // InternalGloe.g:1987:4: 'Viktor'
                    {
                    match(input,161,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVIKTOREnumLiteralDeclaration_93()); 

                    }


                    }
                    break;
                case 95 :
                    // InternalGloe.g:1991:2: ( ( 'Sejuani' ) )
                    {
                    // InternalGloe.g:1991:2: ( ( 'Sejuani' ) )
                    // InternalGloe.g:1992:3: ( 'Sejuani' )
                    {
                     before(grammarAccess.getChampionAccess().getSEJUANIEnumLiteralDeclaration_94()); 
                    // InternalGloe.g:1993:3: ( 'Sejuani' )
                    // InternalGloe.g:1993:4: 'Sejuani'
                    {
                    match(input,162,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSEJUANIEnumLiteralDeclaration_94()); 

                    }


                    }
                    break;
                case 96 :
                    // InternalGloe.g:1997:2: ( ( 'Fiora' ) )
                    {
                    // InternalGloe.g:1997:2: ( ( 'Fiora' ) )
                    // InternalGloe.g:1998:3: ( 'Fiora' )
                    {
                     before(grammarAccess.getChampionAccess().getFIORAEnumLiteralDeclaration_95()); 
                    // InternalGloe.g:1999:3: ( 'Fiora' )
                    // InternalGloe.g:1999:4: 'Fiora'
                    {
                    match(input,163,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getFIORAEnumLiteralDeclaration_95()); 

                    }


                    }
                    break;
                case 97 :
                    // InternalGloe.g:2003:2: ( ( 'Ziggs' ) )
                    {
                    // InternalGloe.g:2003:2: ( ( 'Ziggs' ) )
                    // InternalGloe.g:2004:3: ( 'Ziggs' )
                    {
                     before(grammarAccess.getChampionAccess().getZIGGSEnumLiteralDeclaration_96()); 
                    // InternalGloe.g:2005:3: ( 'Ziggs' )
                    // InternalGloe.g:2005:4: 'Ziggs'
                    {
                    match(input,164,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getZIGGSEnumLiteralDeclaration_96()); 

                    }


                    }
                    break;
                case 98 :
                    // InternalGloe.g:2009:2: ( ( 'Lulu' ) )
                    {
                    // InternalGloe.g:2009:2: ( ( 'Lulu' ) )
                    // InternalGloe.g:2010:3: ( 'Lulu' )
                    {
                     before(grammarAccess.getChampionAccess().getLULUEnumLiteralDeclaration_97()); 
                    // InternalGloe.g:2011:3: ( 'Lulu' )
                    // InternalGloe.g:2011:4: 'Lulu'
                    {
                    match(input,165,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLULUEnumLiteralDeclaration_97()); 

                    }


                    }
                    break;
                case 99 :
                    // InternalGloe.g:2015:2: ( ( 'Draven' ) )
                    {
                    // InternalGloe.g:2015:2: ( ( 'Draven' ) )
                    // InternalGloe.g:2016:3: ( 'Draven' )
                    {
                     before(grammarAccess.getChampionAccess().getDRAVENEnumLiteralDeclaration_98()); 
                    // InternalGloe.g:2017:3: ( 'Draven' )
                    // InternalGloe.g:2017:4: 'Draven'
                    {
                    match(input,166,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getDRAVENEnumLiteralDeclaration_98()); 

                    }


                    }
                    break;
                case 100 :
                    // InternalGloe.g:2021:2: ( ( 'Hecarim' ) )
                    {
                    // InternalGloe.g:2021:2: ( ( 'Hecarim' ) )
                    // InternalGloe.g:2022:3: ( 'Hecarim' )
                    {
                     before(grammarAccess.getChampionAccess().getHECARIMEnumLiteralDeclaration_99()); 
                    // InternalGloe.g:2023:3: ( 'Hecarim' )
                    // InternalGloe.g:2023:4: 'Hecarim'
                    {
                    match(input,167,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getHECARIMEnumLiteralDeclaration_99()); 

                    }


                    }
                    break;
                case 101 :
                    // InternalGloe.g:2027:2: ( ( 'Kha\\'Zix' ) )
                    {
                    // InternalGloe.g:2027:2: ( ( 'Kha\\'Zix' ) )
                    // InternalGloe.g:2028:3: ( 'Kha\\'Zix' )
                    {
                     before(grammarAccess.getChampionAccess().getKHAZIXEnumLiteralDeclaration_100()); 
                    // InternalGloe.g:2029:3: ( 'Kha\\'Zix' )
                    // InternalGloe.g:2029:4: 'Kha\\'Zix'
                    {
                    match(input,168,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKHAZIXEnumLiteralDeclaration_100()); 

                    }


                    }
                    break;
                case 102 :
                    // InternalGloe.g:2033:2: ( ( 'Darius' ) )
                    {
                    // InternalGloe.g:2033:2: ( ( 'Darius' ) )
                    // InternalGloe.g:2034:3: ( 'Darius' )
                    {
                     before(grammarAccess.getChampionAccess().getDARIUSEnumLiteralDeclaration_101()); 
                    // InternalGloe.g:2035:3: ( 'Darius' )
                    // InternalGloe.g:2035:4: 'Darius'
                    {
                    match(input,169,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getDARIUSEnumLiteralDeclaration_101()); 

                    }


                    }
                    break;
                case 103 :
                    // InternalGloe.g:2039:2: ( ( 'Jayce' ) )
                    {
                    // InternalGloe.g:2039:2: ( ( 'Jayce' ) )
                    // InternalGloe.g:2040:3: ( 'Jayce' )
                    {
                     before(grammarAccess.getChampionAccess().getJAYCEEnumLiteralDeclaration_102()); 
                    // InternalGloe.g:2041:3: ( 'Jayce' )
                    // InternalGloe.g:2041:4: 'Jayce'
                    {
                    match(input,170,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getJAYCEEnumLiteralDeclaration_102()); 

                    }


                    }
                    break;
                case 104 :
                    // InternalGloe.g:2045:2: ( ( 'Lissandra' ) )
                    {
                    // InternalGloe.g:2045:2: ( ( 'Lissandra' ) )
                    // InternalGloe.g:2046:3: ( 'Lissandra' )
                    {
                     before(grammarAccess.getChampionAccess().getLISSANDRAEnumLiteralDeclaration_103()); 
                    // InternalGloe.g:2047:3: ( 'Lissandra' )
                    // InternalGloe.g:2047:4: 'Lissandra'
                    {
                    match(input,171,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLISSANDRAEnumLiteralDeclaration_103()); 

                    }


                    }
                    break;
                case 105 :
                    // InternalGloe.g:2051:2: ( ( 'Diana' ) )
                    {
                    // InternalGloe.g:2051:2: ( ( 'Diana' ) )
                    // InternalGloe.g:2052:3: ( 'Diana' )
                    {
                     before(grammarAccess.getChampionAccess().getDIANAEnumLiteralDeclaration_104()); 
                    // InternalGloe.g:2053:3: ( 'Diana' )
                    // InternalGloe.g:2053:4: 'Diana'
                    {
                    match(input,172,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getDIANAEnumLiteralDeclaration_104()); 

                    }


                    }
                    break;
                case 106 :
                    // InternalGloe.g:2057:2: ( ( 'Quinn' ) )
                    {
                    // InternalGloe.g:2057:2: ( ( 'Quinn' ) )
                    // InternalGloe.g:2058:3: ( 'Quinn' )
                    {
                     before(grammarAccess.getChampionAccess().getQUINNEnumLiteralDeclaration_105()); 
                    // InternalGloe.g:2059:3: ( 'Quinn' )
                    // InternalGloe.g:2059:4: 'Quinn'
                    {
                    match(input,173,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getQUINNEnumLiteralDeclaration_105()); 

                    }


                    }
                    break;
                case 107 :
                    // InternalGloe.g:2063:2: ( ( 'Syndra' ) )
                    {
                    // InternalGloe.g:2063:2: ( ( 'Syndra' ) )
                    // InternalGloe.g:2064:3: ( 'Syndra' )
                    {
                     before(grammarAccess.getChampionAccess().getSYNDRAEnumLiteralDeclaration_106()); 
                    // InternalGloe.g:2065:3: ( 'Syndra' )
                    // InternalGloe.g:2065:4: 'Syndra'
                    {
                    match(input,174,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getSYNDRAEnumLiteralDeclaration_106()); 

                    }


                    }
                    break;
                case 108 :
                    // InternalGloe.g:2069:2: ( ( 'Aurelion Sol' ) )
                    {
                    // InternalGloe.g:2069:2: ( ( 'Aurelion Sol' ) )
                    // InternalGloe.g:2070:3: ( 'Aurelion Sol' )
                    {
                     before(grammarAccess.getChampionAccess().getAURELIONSOLEnumLiteralDeclaration_107()); 
                    // InternalGloe.g:2071:3: ( 'Aurelion Sol' )
                    // InternalGloe.g:2071:4: 'Aurelion Sol'
                    {
                    match(input,175,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getAURELIONSOLEnumLiteralDeclaration_107()); 

                    }


                    }
                    break;
                case 109 :
                    // InternalGloe.g:2075:2: ( ( 'Kayn' ) )
                    {
                    // InternalGloe.g:2075:2: ( ( 'Kayn' ) )
                    // InternalGloe.g:2076:3: ( 'Kayn' )
                    {
                     before(grammarAccess.getChampionAccess().getKAYNEnumLiteralDeclaration_108()); 
                    // InternalGloe.g:2077:3: ( 'Kayn' )
                    // InternalGloe.g:2077:4: 'Kayn'
                    {
                    match(input,176,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKAYNEnumLiteralDeclaration_108()); 

                    }


                    }
                    break;
                case 110 :
                    // InternalGloe.g:2081:2: ( ( 'Zoe' ) )
                    {
                    // InternalGloe.g:2081:2: ( ( 'Zoe' ) )
                    // InternalGloe.g:2082:3: ( 'Zoe' )
                    {
                     before(grammarAccess.getChampionAccess().getZOEEnumLiteralDeclaration_109()); 
                    // InternalGloe.g:2083:3: ( 'Zoe' )
                    // InternalGloe.g:2083:4: 'Zoe'
                    {
                    match(input,177,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getZOEEnumLiteralDeclaration_109()); 

                    }


                    }
                    break;
                case 111 :
                    // InternalGloe.g:2087:2: ( ( 'Zyra' ) )
                    {
                    // InternalGloe.g:2087:2: ( ( 'Zyra' ) )
                    // InternalGloe.g:2088:3: ( 'Zyra' )
                    {
                     before(grammarAccess.getChampionAccess().getZYRAEnumLiteralDeclaration_110()); 
                    // InternalGloe.g:2089:3: ( 'Zyra' )
                    // InternalGloe.g:2089:4: 'Zyra'
                    {
                    match(input,178,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getZYRAEnumLiteralDeclaration_110()); 

                    }


                    }
                    break;
                case 112 :
                    // InternalGloe.g:2093:2: ( ( 'Kai\\'Sa' ) )
                    {
                    // InternalGloe.g:2093:2: ( ( 'Kai\\'Sa' ) )
                    // InternalGloe.g:2094:3: ( 'Kai\\'Sa' )
                    {
                     before(grammarAccess.getChampionAccess().getKAISAEnumLiteralDeclaration_111()); 
                    // InternalGloe.g:2095:3: ( 'Kai\\'Sa' )
                    // InternalGloe.g:2095:4: 'Kai\\'Sa'
                    {
                    match(input,179,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKAISAEnumLiteralDeclaration_111()); 

                    }


                    }
                    break;
                case 113 :
                    // InternalGloe.g:2099:2: ( ( 'Gnar' ) )
                    {
                    // InternalGloe.g:2099:2: ( ( 'Gnar' ) )
                    // InternalGloe.g:2100:3: ( 'Gnar' )
                    {
                     before(grammarAccess.getChampionAccess().getGNAREnumLiteralDeclaration_112()); 
                    // InternalGloe.g:2101:3: ( 'Gnar' )
                    // InternalGloe.g:2101:4: 'Gnar'
                    {
                    match(input,180,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getGNAREnumLiteralDeclaration_112()); 

                    }


                    }
                    break;
                case 114 :
                    // InternalGloe.g:2105:2: ( ( 'Zac' ) )
                    {
                    // InternalGloe.g:2105:2: ( ( 'Zac' ) )
                    // InternalGloe.g:2106:3: ( 'Zac' )
                    {
                     before(grammarAccess.getChampionAccess().getZACEnumLiteralDeclaration_113()); 
                    // InternalGloe.g:2107:3: ( 'Zac' )
                    // InternalGloe.g:2107:4: 'Zac'
                    {
                    match(input,181,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getZACEnumLiteralDeclaration_113()); 

                    }


                    }
                    break;
                case 115 :
                    // InternalGloe.g:2111:2: ( ( 'Yasuo' ) )
                    {
                    // InternalGloe.g:2111:2: ( ( 'Yasuo' ) )
                    // InternalGloe.g:2112:3: ( 'Yasuo' )
                    {
                     before(grammarAccess.getChampionAccess().getYASUOEnumLiteralDeclaration_114()); 
                    // InternalGloe.g:2113:3: ( 'Yasuo' )
                    // InternalGloe.g:2113:4: 'Yasuo'
                    {
                    match(input,182,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getYASUOEnumLiteralDeclaration_114()); 

                    }


                    }
                    break;
                case 116 :
                    // InternalGloe.g:2117:2: ( ( 'Vel\\'Koz' ) )
                    {
                    // InternalGloe.g:2117:2: ( ( 'Vel\\'Koz' ) )
                    // InternalGloe.g:2118:3: ( 'Vel\\'Koz' )
                    {
                     before(grammarAccess.getChampionAccess().getVELKOZEnumLiteralDeclaration_115()); 
                    // InternalGloe.g:2119:3: ( 'Vel\\'Koz' )
                    // InternalGloe.g:2119:4: 'Vel\\'Koz'
                    {
                    match(input,183,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVELKOZEnumLiteralDeclaration_115()); 

                    }


                    }
                    break;
                case 117 :
                    // InternalGloe.g:2123:2: ( ( 'Taliyah' ) )
                    {
                    // InternalGloe.g:2123:2: ( ( 'Taliyah' ) )
                    // InternalGloe.g:2124:3: ( 'Taliyah' )
                    {
                     before(grammarAccess.getChampionAccess().getTALIYAHEnumLiteralDeclaration_116()); 
                    // InternalGloe.g:2125:3: ( 'Taliyah' )
                    // InternalGloe.g:2125:4: 'Taliyah'
                    {
                    match(input,184,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTALIYAHEnumLiteralDeclaration_116()); 

                    }


                    }
                    break;
                case 118 :
                    // InternalGloe.g:2129:2: ( ( 'Camille' ) )
                    {
                    // InternalGloe.g:2129:2: ( ( 'Camille' ) )
                    // InternalGloe.g:2130:3: ( 'Camille' )
                    {
                     before(grammarAccess.getChampionAccess().getCAMILLEEnumLiteralDeclaration_117()); 
                    // InternalGloe.g:2131:3: ( 'Camille' )
                    // InternalGloe.g:2131:4: 'Camille'
                    {
                    match(input,185,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getCAMILLEEnumLiteralDeclaration_117()); 

                    }


                    }
                    break;
                case 119 :
                    // InternalGloe.g:2135:2: ( ( 'Braum' ) )
                    {
                    // InternalGloe.g:2135:2: ( ( 'Braum' ) )
                    // InternalGloe.g:2136:3: ( 'Braum' )
                    {
                     before(grammarAccess.getChampionAccess().getBRAUMEnumLiteralDeclaration_118()); 
                    // InternalGloe.g:2137:3: ( 'Braum' )
                    // InternalGloe.g:2137:4: 'Braum'
                    {
                    match(input,186,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getBRAUMEnumLiteralDeclaration_118()); 

                    }


                    }
                    break;
                case 120 :
                    // InternalGloe.g:2141:2: ( ( 'Jhin' ) )
                    {
                    // InternalGloe.g:2141:2: ( ( 'Jhin' ) )
                    // InternalGloe.g:2142:3: ( 'Jhin' )
                    {
                     before(grammarAccess.getChampionAccess().getJHINEnumLiteralDeclaration_119()); 
                    // InternalGloe.g:2143:3: ( 'Jhin' )
                    // InternalGloe.g:2143:4: 'Jhin'
                    {
                    match(input,187,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getJHINEnumLiteralDeclaration_119()); 

                    }


                    }
                    break;
                case 121 :
                    // InternalGloe.g:2147:2: ( ( 'Kindred' ) )
                    {
                    // InternalGloe.g:2147:2: ( ( 'Kindred' ) )
                    // InternalGloe.g:2148:3: ( 'Kindred' )
                    {
                     before(grammarAccess.getChampionAccess().getKINDREDEnumLiteralDeclaration_120()); 
                    // InternalGloe.g:2149:3: ( 'Kindred' )
                    // InternalGloe.g:2149:4: 'Kindred'
                    {
                    match(input,188,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKINDREDEnumLiteralDeclaration_120()); 

                    }


                    }
                    break;
                case 122 :
                    // InternalGloe.g:2153:2: ( ( 'Jinx' ) )
                    {
                    // InternalGloe.g:2153:2: ( ( 'Jinx' ) )
                    // InternalGloe.g:2154:3: ( 'Jinx' )
                    {
                     before(grammarAccess.getChampionAccess().getJINXEnumLiteralDeclaration_121()); 
                    // InternalGloe.g:2155:3: ( 'Jinx' )
                    // InternalGloe.g:2155:4: 'Jinx'
                    {
                    match(input,189,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getJINXEnumLiteralDeclaration_121()); 

                    }


                    }
                    break;
                case 123 :
                    // InternalGloe.g:2159:2: ( ( 'Tahm Kench' ) )
                    {
                    // InternalGloe.g:2159:2: ( ( 'Tahm Kench' ) )
                    // InternalGloe.g:2160:3: ( 'Tahm Kench' )
                    {
                     before(grammarAccess.getChampionAccess().getTAHMKENCHEnumLiteralDeclaration_122()); 
                    // InternalGloe.g:2161:3: ( 'Tahm Kench' )
                    // InternalGloe.g:2161:4: 'Tahm Kench'
                    {
                    match(input,190,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTAHMKENCHEnumLiteralDeclaration_122()); 

                    }


                    }
                    break;
                case 124 :
                    // InternalGloe.g:2165:2: ( ( 'Lucian' ) )
                    {
                    // InternalGloe.g:2165:2: ( ( 'Lucian' ) )
                    // InternalGloe.g:2166:3: ( 'Lucian' )
                    {
                     before(grammarAccess.getChampionAccess().getLUCIANEnumLiteralDeclaration_123()); 
                    // InternalGloe.g:2167:3: ( 'Lucian' )
                    // InternalGloe.g:2167:4: 'Lucian'
                    {
                    match(input,191,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getLUCIANEnumLiteralDeclaration_123()); 

                    }


                    }
                    break;
                case 125 :
                    // InternalGloe.g:2171:2: ( ( 'Zed' ) )
                    {
                    // InternalGloe.g:2171:2: ( ( 'Zed' ) )
                    // InternalGloe.g:2172:3: ( 'Zed' )
                    {
                     before(grammarAccess.getChampionAccess().getZEDEnumLiteralDeclaration_124()); 
                    // InternalGloe.g:2173:3: ( 'Zed' )
                    // InternalGloe.g:2173:4: 'Zed'
                    {
                    match(input,192,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getZEDEnumLiteralDeclaration_124()); 

                    }


                    }
                    break;
                case 126 :
                    // InternalGloe.g:2177:2: ( ( 'Kled' ) )
                    {
                    // InternalGloe.g:2177:2: ( ( 'Kled' ) )
                    // InternalGloe.g:2178:3: ( 'Kled' )
                    {
                     before(grammarAccess.getChampionAccess().getKLEDEnumLiteralDeclaration_125()); 
                    // InternalGloe.g:2179:3: ( 'Kled' )
                    // InternalGloe.g:2179:4: 'Kled'
                    {
                    match(input,193,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKLEDEnumLiteralDeclaration_125()); 

                    }


                    }
                    break;
                case 127 :
                    // InternalGloe.g:2183:2: ( ( 'Ekko' ) )
                    {
                    // InternalGloe.g:2183:2: ( ( 'Ekko' ) )
                    // InternalGloe.g:2184:3: ( 'Ekko' )
                    {
                     before(grammarAccess.getChampionAccess().getEKKOEnumLiteralDeclaration_126()); 
                    // InternalGloe.g:2185:3: ( 'Ekko' )
                    // InternalGloe.g:2185:4: 'Ekko'
                    {
                    match(input,194,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getEKKOEnumLiteralDeclaration_126()); 

                    }


                    }
                    break;
                case 128 :
                    // InternalGloe.g:2189:2: ( ( 'Vi' ) )
                    {
                    // InternalGloe.g:2189:2: ( ( 'Vi' ) )
                    // InternalGloe.g:2190:3: ( 'Vi' )
                    {
                     before(grammarAccess.getChampionAccess().getVIEnumLiteralDeclaration_127()); 
                    // InternalGloe.g:2191:3: ( 'Vi' )
                    // InternalGloe.g:2191:4: 'Vi'
                    {
                    match(input,195,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getVIEnumLiteralDeclaration_127()); 

                    }


                    }
                    break;
                case 129 :
                    // InternalGloe.g:2195:2: ( ( 'Aatrox' ) )
                    {
                    // InternalGloe.g:2195:2: ( ( 'Aatrox' ) )
                    // InternalGloe.g:2196:3: ( 'Aatrox' )
                    {
                     before(grammarAccess.getChampionAccess().getAATROXEnumLiteralDeclaration_128()); 
                    // InternalGloe.g:2197:3: ( 'Aatrox' )
                    // InternalGloe.g:2197:4: 'Aatrox'
                    {
                    match(input,196,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getAATROXEnumLiteralDeclaration_128()); 

                    }


                    }
                    break;
                case 130 :
                    // InternalGloe.g:2201:2: ( ( 'Nami' ) )
                    {
                    // InternalGloe.g:2201:2: ( ( 'Nami' ) )
                    // InternalGloe.g:2202:3: ( 'Nami' )
                    {
                     before(grammarAccess.getChampionAccess().getNAMIEnumLiteralDeclaration_129()); 
                    // InternalGloe.g:2203:3: ( 'Nami' )
                    // InternalGloe.g:2203:4: 'Nami'
                    {
                    match(input,197,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getNAMIEnumLiteralDeclaration_129()); 

                    }


                    }
                    break;
                case 131 :
                    // InternalGloe.g:2207:2: ( ( 'Azir' ) )
                    {
                    // InternalGloe.g:2207:2: ( ( 'Azir' ) )
                    // InternalGloe.g:2208:3: ( 'Azir' )
                    {
                     before(grammarAccess.getChampionAccess().getAZIREnumLiteralDeclaration_130()); 
                    // InternalGloe.g:2209:3: ( 'Azir' )
                    // InternalGloe.g:2209:4: 'Azir'
                    {
                    match(input,198,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getAZIREnumLiteralDeclaration_130()); 

                    }


                    }
                    break;
                case 132 :
                    // InternalGloe.g:2213:2: ( ( 'Thresh' ) )
                    {
                    // InternalGloe.g:2213:2: ( ( 'Thresh' ) )
                    // InternalGloe.g:2214:3: ( 'Thresh' )
                    {
                     before(grammarAccess.getChampionAccess().getTHRESHEnumLiteralDeclaration_131()); 
                    // InternalGloe.g:2215:3: ( 'Thresh' )
                    // InternalGloe.g:2215:4: 'Thresh'
                    {
                    match(input,199,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getTHRESHEnumLiteralDeclaration_131()); 

                    }


                    }
                    break;
                case 133 :
                    // InternalGloe.g:2219:2: ( ( 'Illaoi' ) )
                    {
                    // InternalGloe.g:2219:2: ( ( 'Illaoi' ) )
                    // InternalGloe.g:2220:3: ( 'Illaoi' )
                    {
                     before(grammarAccess.getChampionAccess().getILLAOIEnumLiteralDeclaration_132()); 
                    // InternalGloe.g:2221:3: ( 'Illaoi' )
                    // InternalGloe.g:2221:4: 'Illaoi'
                    {
                    match(input,200,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getILLAOIEnumLiteralDeclaration_132()); 

                    }


                    }
                    break;
                case 134 :
                    // InternalGloe.g:2225:2: ( ( 'Rek\\'Sai' ) )
                    {
                    // InternalGloe.g:2225:2: ( ( 'Rek\\'Sai' ) )
                    // InternalGloe.g:2226:3: ( 'Rek\\'Sai' )
                    {
                     before(grammarAccess.getChampionAccess().getREKSAIEnumLiteralDeclaration_133()); 
                    // InternalGloe.g:2227:3: ( 'Rek\\'Sai' )
                    // InternalGloe.g:2227:4: 'Rek\\'Sai'
                    {
                    match(input,201,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getREKSAIEnumLiteralDeclaration_133()); 

                    }


                    }
                    break;
                case 135 :
                    // InternalGloe.g:2231:2: ( ( 'Ivern' ) )
                    {
                    // InternalGloe.g:2231:2: ( ( 'Ivern' ) )
                    // InternalGloe.g:2232:3: ( 'Ivern' )
                    {
                     before(grammarAccess.getChampionAccess().getIVERNEnumLiteralDeclaration_134()); 
                    // InternalGloe.g:2233:3: ( 'Ivern' )
                    // InternalGloe.g:2233:4: 'Ivern'
                    {
                    match(input,202,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getIVERNEnumLiteralDeclaration_134()); 

                    }


                    }
                    break;
                case 136 :
                    // InternalGloe.g:2237:2: ( ( 'Kalista' ) )
                    {
                    // InternalGloe.g:2237:2: ( ( 'Kalista' ) )
                    // InternalGloe.g:2238:3: ( 'Kalista' )
                    {
                     before(grammarAccess.getChampionAccess().getKALISTAEnumLiteralDeclaration_135()); 
                    // InternalGloe.g:2239:3: ( 'Kalista' )
                    // InternalGloe.g:2239:4: 'Kalista'
                    {
                    match(input,203,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getKALISTAEnumLiteralDeclaration_135()); 

                    }


                    }
                    break;
                case 137 :
                    // InternalGloe.g:2243:2: ( ( 'Bard' ) )
                    {
                    // InternalGloe.g:2243:2: ( ( 'Bard' ) )
                    // InternalGloe.g:2244:3: ( 'Bard' )
                    {
                     before(grammarAccess.getChampionAccess().getBARDEnumLiteralDeclaration_136()); 
                    // InternalGloe.g:2245:3: ( 'Bard' )
                    // InternalGloe.g:2245:4: 'Bard'
                    {
                    match(input,204,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getBARDEnumLiteralDeclaration_136()); 

                    }


                    }
                    break;
                case 138 :
                    // InternalGloe.g:2249:2: ( ( 'Rakan' ) )
                    {
                    // InternalGloe.g:2249:2: ( ( 'Rakan' ) )
                    // InternalGloe.g:2250:3: ( 'Rakan' )
                    {
                     before(grammarAccess.getChampionAccess().getRAKANEnumLiteralDeclaration_137()); 
                    // InternalGloe.g:2251:3: ( 'Rakan' )
                    // InternalGloe.g:2251:4: 'Rakan'
                    {
                    match(input,205,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getRAKANEnumLiteralDeclaration_137()); 

                    }


                    }
                    break;
                case 139 :
                    // InternalGloe.g:2255:2: ( ( 'Xayah' ) )
                    {
                    // InternalGloe.g:2255:2: ( ( 'Xayah' ) )
                    // InternalGloe.g:2256:3: ( 'Xayah' )
                    {
                     before(grammarAccess.getChampionAccess().getXAYAHEnumLiteralDeclaration_138()); 
                    // InternalGloe.g:2257:3: ( 'Xayah' )
                    // InternalGloe.g:2257:4: 'Xayah'
                    {
                    match(input,206,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getXAYAHEnumLiteralDeclaration_138()); 

                    }


                    }
                    break;
                case 140 :
                    // InternalGloe.g:2261:2: ( ( 'Ornn' ) )
                    {
                    // InternalGloe.g:2261:2: ( ( 'Ornn' ) )
                    // InternalGloe.g:2262:3: ( 'Ornn' )
                    {
                     before(grammarAccess.getChampionAccess().getORNNEnumLiteralDeclaration_139()); 
                    // InternalGloe.g:2263:3: ( 'Ornn' )
                    // InternalGloe.g:2263:4: 'Ornn'
                    {
                    match(input,207,FOLLOW_2); 

                    }

                     after(grammarAccess.getChampionAccess().getORNNEnumLiteralDeclaration_139()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Champion__Alternatives"


    // $ANTLR start "rule__TimeScale__Alternatives"
    // InternalGloe.g:2271:1: rule__TimeScale__Alternatives : ( ( ( 'day' ) ) | ( ( 'month' ) ) | ( ( 'year' ) ) );
    public final void rule__TimeScale__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2275:1: ( ( ( 'day' ) ) | ( ( 'month' ) ) | ( ( 'year' ) ) )
            int alt16=3;
            switch ( input.LA(1) ) {
            case 208:
                {
                alt16=1;
                }
                break;
            case 209:
                {
                alt16=2;
                }
                break;
            case 210:
                {
                alt16=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }

            switch (alt16) {
                case 1 :
                    // InternalGloe.g:2276:2: ( ( 'day' ) )
                    {
                    // InternalGloe.g:2276:2: ( ( 'day' ) )
                    // InternalGloe.g:2277:3: ( 'day' )
                    {
                     before(grammarAccess.getTimeScaleAccess().getDAYEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:2278:3: ( 'day' )
                    // InternalGloe.g:2278:4: 'day'
                    {
                    match(input,208,FOLLOW_2); 

                    }

                     after(grammarAccess.getTimeScaleAccess().getDAYEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:2282:2: ( ( 'month' ) )
                    {
                    // InternalGloe.g:2282:2: ( ( 'month' ) )
                    // InternalGloe.g:2283:3: ( 'month' )
                    {
                     before(grammarAccess.getTimeScaleAccess().getMONTHEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:2284:3: ( 'month' )
                    // InternalGloe.g:2284:4: 'month'
                    {
                    match(input,209,FOLLOW_2); 

                    }

                     after(grammarAccess.getTimeScaleAccess().getMONTHEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:2288:2: ( ( 'year' ) )
                    {
                    // InternalGloe.g:2288:2: ( ( 'year' ) )
                    // InternalGloe.g:2289:3: ( 'year' )
                    {
                     before(grammarAccess.getTimeScaleAccess().getYEAREnumLiteralDeclaration_2()); 
                    // InternalGloe.g:2290:3: ( 'year' )
                    // InternalGloe.g:2290:4: 'year'
                    {
                    match(input,210,FOLLOW_2); 

                    }

                     after(grammarAccess.getTimeScaleAccess().getYEAREnumLiteralDeclaration_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TimeScale__Alternatives"


    // $ANTLR start "rule__DiagramType__Alternatives"
    // InternalGloe.g:2298:1: rule__DiagramType__Alternatives : ( ( ( 'pie chart' ) ) | ( ( '3D pie chart' ) ) | ( ( 'line chart' ) ) | ( ( 'bar chart' ) ) );
    public final void rule__DiagramType__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2302:1: ( ( ( 'pie chart' ) ) | ( ( '3D pie chart' ) ) | ( ( 'line chart' ) ) | ( ( 'bar chart' ) ) )
            int alt17=4;
            switch ( input.LA(1) ) {
            case 211:
                {
                alt17=1;
                }
                break;
            case 212:
                {
                alt17=2;
                }
                break;
            case 213:
                {
                alt17=3;
                }
                break;
            case 214:
                {
                alt17=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalGloe.g:2303:2: ( ( 'pie chart' ) )
                    {
                    // InternalGloe.g:2303:2: ( ( 'pie chart' ) )
                    // InternalGloe.g:2304:3: ( 'pie chart' )
                    {
                     before(grammarAccess.getDiagramTypeAccess().getPIE_CHARTEnumLiteralDeclaration_0()); 
                    // InternalGloe.g:2305:3: ( 'pie chart' )
                    // InternalGloe.g:2305:4: 'pie chart'
                    {
                    match(input,211,FOLLOW_2); 

                    }

                     after(grammarAccess.getDiagramTypeAccess().getPIE_CHARTEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGloe.g:2309:2: ( ( '3D pie chart' ) )
                    {
                    // InternalGloe.g:2309:2: ( ( '3D pie chart' ) )
                    // InternalGloe.g:2310:3: ( '3D pie chart' )
                    {
                     before(grammarAccess.getDiagramTypeAccess().getPIE_CHART_3DEnumLiteralDeclaration_1()); 
                    // InternalGloe.g:2311:3: ( '3D pie chart' )
                    // InternalGloe.g:2311:4: '3D pie chart'
                    {
                    match(input,212,FOLLOW_2); 

                    }

                     after(grammarAccess.getDiagramTypeAccess().getPIE_CHART_3DEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGloe.g:2315:2: ( ( 'line chart' ) )
                    {
                    // InternalGloe.g:2315:2: ( ( 'line chart' ) )
                    // InternalGloe.g:2316:3: ( 'line chart' )
                    {
                     before(grammarAccess.getDiagramTypeAccess().getLINE_CHARTEnumLiteralDeclaration_2()); 
                    // InternalGloe.g:2317:3: ( 'line chart' )
                    // InternalGloe.g:2317:4: 'line chart'
                    {
                    match(input,213,FOLLOW_2); 

                    }

                     after(grammarAccess.getDiagramTypeAccess().getLINE_CHARTEnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGloe.g:2321:2: ( ( 'bar chart' ) )
                    {
                    // InternalGloe.g:2321:2: ( ( 'bar chart' ) )
                    // InternalGloe.g:2322:3: ( 'bar chart' )
                    {
                     before(grammarAccess.getDiagramTypeAccess().getBAR_CHARTEnumLiteralDeclaration_3()); 
                    // InternalGloe.g:2323:3: ( 'bar chart' )
                    // InternalGloe.g:2323:4: 'bar chart'
                    {
                    match(input,214,FOLLOW_2); 

                    }

                     after(grammarAccess.getDiagramTypeAccess().getBAR_CHARTEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DiagramType__Alternatives"


    // $ANTLR start "rule__Request__Group__0"
    // InternalGloe.g:2331:1: rule__Request__Group__0 : rule__Request__Group__0__Impl rule__Request__Group__1 ;
    public final void rule__Request__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2335:1: ( rule__Request__Group__0__Impl rule__Request__Group__1 )
            // InternalGloe.g:2336:2: rule__Request__Group__0__Impl rule__Request__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Request__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__0"


    // $ANTLR start "rule__Request__Group__0__Impl"
    // InternalGloe.g:2343:1: rule__Request__Group__0__Impl : ( 'summoners' ) ;
    public final void rule__Request__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2347:1: ( ( 'summoners' ) )
            // InternalGloe.g:2348:1: ( 'summoners' )
            {
            // InternalGloe.g:2348:1: ( 'summoners' )
            // InternalGloe.g:2349:2: 'summoners'
            {
             before(grammarAccess.getRequestAccess().getSummonersKeyword_0()); 
            match(input,215,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getSummonersKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__0__Impl"


    // $ANTLR start "rule__Request__Group__1"
    // InternalGloe.g:2358:1: rule__Request__Group__1 : rule__Request__Group__1__Impl rule__Request__Group__2 ;
    public final void rule__Request__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2362:1: ( rule__Request__Group__1__Impl rule__Request__Group__2 )
            // InternalGloe.g:2363:2: rule__Request__Group__1__Impl rule__Request__Group__2
            {
            pushFollow(FOLLOW_4);
            rule__Request__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__1"


    // $ANTLR start "rule__Request__Group__1__Impl"
    // InternalGloe.g:2370:1: rule__Request__Group__1__Impl : ( ( rule__Request__SummonersAssignment_1 ) ) ;
    public final void rule__Request__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2374:1: ( ( ( rule__Request__SummonersAssignment_1 ) ) )
            // InternalGloe.g:2375:1: ( ( rule__Request__SummonersAssignment_1 ) )
            {
            // InternalGloe.g:2375:1: ( ( rule__Request__SummonersAssignment_1 ) )
            // InternalGloe.g:2376:2: ( rule__Request__SummonersAssignment_1 )
            {
             before(grammarAccess.getRequestAccess().getSummonersAssignment_1()); 
            // InternalGloe.g:2377:2: ( rule__Request__SummonersAssignment_1 )
            // InternalGloe.g:2377:3: rule__Request__SummonersAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__SummonersAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getSummonersAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__1__Impl"


    // $ANTLR start "rule__Request__Group__2"
    // InternalGloe.g:2385:1: rule__Request__Group__2 : rule__Request__Group__2__Impl rule__Request__Group__3 ;
    public final void rule__Request__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2389:1: ( rule__Request__Group__2__Impl rule__Request__Group__3 )
            // InternalGloe.g:2390:2: rule__Request__Group__2__Impl rule__Request__Group__3
            {
            pushFollow(FOLLOW_4);
            rule__Request__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__2"


    // $ANTLR start "rule__Request__Group__2__Impl"
    // InternalGloe.g:2397:1: rule__Request__Group__2__Impl : ( ( rule__Request__Group_2__0 )* ) ;
    public final void rule__Request__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2401:1: ( ( ( rule__Request__Group_2__0 )* ) )
            // InternalGloe.g:2402:1: ( ( rule__Request__Group_2__0 )* )
            {
            // InternalGloe.g:2402:1: ( ( rule__Request__Group_2__0 )* )
            // InternalGloe.g:2403:2: ( rule__Request__Group_2__0 )*
            {
             before(grammarAccess.getRequestAccess().getGroup_2()); 
            // InternalGloe.g:2404:2: ( rule__Request__Group_2__0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==219) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalGloe.g:2404:3: rule__Request__Group_2__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Request__Group_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getRequestAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__2__Impl"


    // $ANTLR start "rule__Request__Group__3"
    // InternalGloe.g:2412:1: rule__Request__Group__3 : rule__Request__Group__3__Impl rule__Request__Group__4 ;
    public final void rule__Request__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2416:1: ( rule__Request__Group__3__Impl rule__Request__Group__4 )
            // InternalGloe.g:2417:2: rule__Request__Group__3__Impl rule__Request__Group__4
            {
            pushFollow(FOLLOW_6);
            rule__Request__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__3"


    // $ANTLR start "rule__Request__Group__3__Impl"
    // InternalGloe.g:2424:1: rule__Request__Group__3__Impl : ( 'display' ) ;
    public final void rule__Request__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2428:1: ( ( 'display' ) )
            // InternalGloe.g:2429:1: ( 'display' )
            {
            // InternalGloe.g:2429:1: ( 'display' )
            // InternalGloe.g:2430:2: 'display'
            {
             before(grammarAccess.getRequestAccess().getDisplayKeyword_3()); 
            match(input,216,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getDisplayKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__3__Impl"


    // $ANTLR start "rule__Request__Group__4"
    // InternalGloe.g:2439:1: rule__Request__Group__4 : rule__Request__Group__4__Impl rule__Request__Group__5 ;
    public final void rule__Request__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2443:1: ( rule__Request__Group__4__Impl rule__Request__Group__5 )
            // InternalGloe.g:2444:2: rule__Request__Group__4__Impl rule__Request__Group__5
            {
            pushFollow(FOLLOW_7);
            rule__Request__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__4"


    // $ANTLR start "rule__Request__Group__4__Impl"
    // InternalGloe.g:2451:1: rule__Request__Group__4__Impl : ( ( rule__Request__YvaluesAssignment_4 ) ) ;
    public final void rule__Request__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2455:1: ( ( ( rule__Request__YvaluesAssignment_4 ) ) )
            // InternalGloe.g:2456:1: ( ( rule__Request__YvaluesAssignment_4 ) )
            {
            // InternalGloe.g:2456:1: ( ( rule__Request__YvaluesAssignment_4 ) )
            // InternalGloe.g:2457:2: ( rule__Request__YvaluesAssignment_4 )
            {
             before(grammarAccess.getRequestAccess().getYvaluesAssignment_4()); 
            // InternalGloe.g:2458:2: ( rule__Request__YvaluesAssignment_4 )
            // InternalGloe.g:2458:3: rule__Request__YvaluesAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__Request__YvaluesAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getYvaluesAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__4__Impl"


    // $ANTLR start "rule__Request__Group__5"
    // InternalGloe.g:2466:1: rule__Request__Group__5 : rule__Request__Group__5__Impl rule__Request__Group__6 ;
    public final void rule__Request__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2470:1: ( rule__Request__Group__5__Impl rule__Request__Group__6 )
            // InternalGloe.g:2471:2: rule__Request__Group__5__Impl rule__Request__Group__6
            {
            pushFollow(FOLLOW_7);
            rule__Request__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__5"


    // $ANTLR start "rule__Request__Group__5__Impl"
    // InternalGloe.g:2478:1: rule__Request__Group__5__Impl : ( ( rule__Request__Group_5__0 )* ) ;
    public final void rule__Request__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2482:1: ( ( ( rule__Request__Group_5__0 )* ) )
            // InternalGloe.g:2483:1: ( ( rule__Request__Group_5__0 )* )
            {
            // InternalGloe.g:2483:1: ( ( rule__Request__Group_5__0 )* )
            // InternalGloe.g:2484:2: ( rule__Request__Group_5__0 )*
            {
             before(grammarAccess.getRequestAccess().getGroup_5()); 
            // InternalGloe.g:2485:2: ( rule__Request__Group_5__0 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==219) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalGloe.g:2485:3: rule__Request__Group_5__0
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__Request__Group_5__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getRequestAccess().getGroup_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__5__Impl"


    // $ANTLR start "rule__Request__Group__6"
    // InternalGloe.g:2493:1: rule__Request__Group__6 : rule__Request__Group__6__Impl rule__Request__Group__7 ;
    public final void rule__Request__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2497:1: ( rule__Request__Group__6__Impl rule__Request__Group__7 )
            // InternalGloe.g:2498:2: rule__Request__Group__6__Impl rule__Request__Group__7
            {
            pushFollow(FOLLOW_7);
            rule__Request__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__6"


    // $ANTLR start "rule__Request__Group__6__Impl"
    // InternalGloe.g:2505:1: rule__Request__Group__6__Impl : ( ( rule__Request__Group_6__0 )? ) ;
    public final void rule__Request__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2509:1: ( ( ( rule__Request__Group_6__0 )? ) )
            // InternalGloe.g:2510:1: ( ( rule__Request__Group_6__0 )? )
            {
            // InternalGloe.g:2510:1: ( ( rule__Request__Group_6__0 )? )
            // InternalGloe.g:2511:2: ( rule__Request__Group_6__0 )?
            {
             before(grammarAccess.getRequestAccess().getGroup_6()); 
            // InternalGloe.g:2512:2: ( rule__Request__Group_6__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==220) ) {
                int LA20_1 = input.LA(2);

                if ( ((LA20_1>=12 && LA20_1<=15)) ) {
                    alt20=1;
                }
            }
            switch (alt20) {
                case 1 :
                    // InternalGloe.g:2512:3: rule__Request__Group_6__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Request__Group_6__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRequestAccess().getGroup_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__6__Impl"


    // $ANTLR start "rule__Request__Group__7"
    // InternalGloe.g:2520:1: rule__Request__Group__7 : rule__Request__Group__7__Impl rule__Request__Group__8 ;
    public final void rule__Request__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2524:1: ( rule__Request__Group__7__Impl rule__Request__Group__8 )
            // InternalGloe.g:2525:2: rule__Request__Group__7__Impl rule__Request__Group__8
            {
            pushFollow(FOLLOW_7);
            rule__Request__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__7"


    // $ANTLR start "rule__Request__Group__7__Impl"
    // InternalGloe.g:2532:1: rule__Request__Group__7__Impl : ( ( rule__Request__Group_7__0 )? ) ;
    public final void rule__Request__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2536:1: ( ( ( rule__Request__Group_7__0 )? ) )
            // InternalGloe.g:2537:1: ( ( rule__Request__Group_7__0 )? )
            {
            // InternalGloe.g:2537:1: ( ( rule__Request__Group_7__0 )? )
            // InternalGloe.g:2538:2: ( rule__Request__Group_7__0 )?
            {
             before(grammarAccess.getRequestAccess().getGroup_7()); 
            // InternalGloe.g:2539:2: ( rule__Request__Group_7__0 )?
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( (LA21_0==220) ) {
                alt21=1;
            }
            switch (alt21) {
                case 1 :
                    // InternalGloe.g:2539:3: rule__Request__Group_7__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Request__Group_7__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRequestAccess().getGroup_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__7__Impl"


    // $ANTLR start "rule__Request__Group__8"
    // InternalGloe.g:2547:1: rule__Request__Group__8 : rule__Request__Group__8__Impl rule__Request__Group__9 ;
    public final void rule__Request__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2551:1: ( rule__Request__Group__8__Impl rule__Request__Group__9 )
            // InternalGloe.g:2552:2: rule__Request__Group__8__Impl rule__Request__Group__9
            {
            pushFollow(FOLLOW_7);
            rule__Request__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__8"


    // $ANTLR start "rule__Request__Group__8__Impl"
    // InternalGloe.g:2559:1: rule__Request__Group__8__Impl : ( ( rule__Request__Group_8__0 )? ) ;
    public final void rule__Request__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2563:1: ( ( ( rule__Request__Group_8__0 )? ) )
            // InternalGloe.g:2564:1: ( ( rule__Request__Group_8__0 )? )
            {
            // InternalGloe.g:2564:1: ( ( rule__Request__Group_8__0 )? )
            // InternalGloe.g:2565:2: ( rule__Request__Group_8__0 )?
            {
             before(grammarAccess.getRequestAccess().getGroup_8()); 
            // InternalGloe.g:2566:2: ( rule__Request__Group_8__0 )?
            int alt22=2;
            int LA22_0 = input.LA(1);

            if ( (LA22_0==221) ) {
                alt22=1;
            }
            switch (alt22) {
                case 1 :
                    // InternalGloe.g:2566:3: rule__Request__Group_8__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Request__Group_8__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRequestAccess().getGroup_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__8__Impl"


    // $ANTLR start "rule__Request__Group__9"
    // InternalGloe.g:2574:1: rule__Request__Group__9 : rule__Request__Group__9__Impl rule__Request__Group__10 ;
    public final void rule__Request__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2578:1: ( rule__Request__Group__9__Impl rule__Request__Group__10 )
            // InternalGloe.g:2579:2: rule__Request__Group__9__Impl rule__Request__Group__10
            {
            pushFollow(FOLLOW_7);
            rule__Request__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__9"


    // $ANTLR start "rule__Request__Group__9__Impl"
    // InternalGloe.g:2586:1: rule__Request__Group__9__Impl : ( ( rule__Request__TimelapseAssignment_9 )? ) ;
    public final void rule__Request__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2590:1: ( ( ( rule__Request__TimelapseAssignment_9 )? ) )
            // InternalGloe.g:2591:1: ( ( rule__Request__TimelapseAssignment_9 )? )
            {
            // InternalGloe.g:2591:1: ( ( rule__Request__TimelapseAssignment_9 )? )
            // InternalGloe.g:2592:2: ( rule__Request__TimelapseAssignment_9 )?
            {
             before(grammarAccess.getRequestAccess().getTimelapseAssignment_9()); 
            // InternalGloe.g:2593:2: ( rule__Request__TimelapseAssignment_9 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==229) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalGloe.g:2593:3: rule__Request__TimelapseAssignment_9
                    {
                    pushFollow(FOLLOW_2);
                    rule__Request__TimelapseAssignment_9();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getRequestAccess().getTimelapseAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__9__Impl"


    // $ANTLR start "rule__Request__Group__10"
    // InternalGloe.g:2601:1: rule__Request__Group__10 : rule__Request__Group__10__Impl rule__Request__Group__11 ;
    public final void rule__Request__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2605:1: ( rule__Request__Group__10__Impl rule__Request__Group__11 )
            // InternalGloe.g:2606:2: rule__Request__Group__10__Impl rule__Request__Group__11
            {
            pushFollow(FOLLOW_8);
            rule__Request__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__10"


    // $ANTLR start "rule__Request__Group__10__Impl"
    // InternalGloe.g:2613:1: rule__Request__Group__10__Impl : ( 'in' ) ;
    public final void rule__Request__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2617:1: ( ( 'in' ) )
            // InternalGloe.g:2618:1: ( 'in' )
            {
            // InternalGloe.g:2618:1: ( 'in' )
            // InternalGloe.g:2619:2: 'in'
            {
             before(grammarAccess.getRequestAccess().getInKeyword_10()); 
            match(input,217,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getInKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__10__Impl"


    // $ANTLR start "rule__Request__Group__11"
    // InternalGloe.g:2628:1: rule__Request__Group__11 : rule__Request__Group__11__Impl rule__Request__Group__12 ;
    public final void rule__Request__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2632:1: ( rule__Request__Group__11__Impl rule__Request__Group__12 )
            // InternalGloe.g:2633:2: rule__Request__Group__11__Impl rule__Request__Group__12
            {
            pushFollow(FOLLOW_9);
            rule__Request__Group__11__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group__12();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__11"


    // $ANTLR start "rule__Request__Group__11__Impl"
    // InternalGloe.g:2640:1: rule__Request__Group__11__Impl : ( 'a' ) ;
    public final void rule__Request__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2644:1: ( ( 'a' ) )
            // InternalGloe.g:2645:1: ( 'a' )
            {
            // InternalGloe.g:2645:1: ( 'a' )
            // InternalGloe.g:2646:2: 'a'
            {
             before(grammarAccess.getRequestAccess().getAKeyword_11()); 
            match(input,218,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getAKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__11__Impl"


    // $ANTLR start "rule__Request__Group__12"
    // InternalGloe.g:2655:1: rule__Request__Group__12 : rule__Request__Group__12__Impl ;
    public final void rule__Request__Group__12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2659:1: ( rule__Request__Group__12__Impl )
            // InternalGloe.g:2660:2: rule__Request__Group__12__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group__12__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__12"


    // $ANTLR start "rule__Request__Group__12__Impl"
    // InternalGloe.g:2666:1: rule__Request__Group__12__Impl : ( ( rule__Request__DiagramAssignment_12 ) ) ;
    public final void rule__Request__Group__12__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2670:1: ( ( ( rule__Request__DiagramAssignment_12 ) ) )
            // InternalGloe.g:2671:1: ( ( rule__Request__DiagramAssignment_12 ) )
            {
            // InternalGloe.g:2671:1: ( ( rule__Request__DiagramAssignment_12 ) )
            // InternalGloe.g:2672:2: ( rule__Request__DiagramAssignment_12 )
            {
             before(grammarAccess.getRequestAccess().getDiagramAssignment_12()); 
            // InternalGloe.g:2673:2: ( rule__Request__DiagramAssignment_12 )
            // InternalGloe.g:2673:3: rule__Request__DiagramAssignment_12
            {
            pushFollow(FOLLOW_2);
            rule__Request__DiagramAssignment_12();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getDiagramAssignment_12()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group__12__Impl"


    // $ANTLR start "rule__Request__Group_2__0"
    // InternalGloe.g:2682:1: rule__Request__Group_2__0 : rule__Request__Group_2__0__Impl rule__Request__Group_2__1 ;
    public final void rule__Request__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2686:1: ( rule__Request__Group_2__0__Impl rule__Request__Group_2__1 )
            // InternalGloe.g:2687:2: rule__Request__Group_2__0__Impl rule__Request__Group_2__1
            {
            pushFollow(FOLLOW_3);
            rule__Request__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_2__0"


    // $ANTLR start "rule__Request__Group_2__0__Impl"
    // InternalGloe.g:2694:1: rule__Request__Group_2__0__Impl : ( ',' ) ;
    public final void rule__Request__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2698:1: ( ( ',' ) )
            // InternalGloe.g:2699:1: ( ',' )
            {
            // InternalGloe.g:2699:1: ( ',' )
            // InternalGloe.g:2700:2: ','
            {
             before(grammarAccess.getRequestAccess().getCommaKeyword_2_0()); 
            match(input,219,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_2__0__Impl"


    // $ANTLR start "rule__Request__Group_2__1"
    // InternalGloe.g:2709:1: rule__Request__Group_2__1 : rule__Request__Group_2__1__Impl ;
    public final void rule__Request__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2713:1: ( rule__Request__Group_2__1__Impl )
            // InternalGloe.g:2714:2: rule__Request__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_2__1"


    // $ANTLR start "rule__Request__Group_2__1__Impl"
    // InternalGloe.g:2720:1: rule__Request__Group_2__1__Impl : ( ( rule__Request__SummonersAssignment_2_1 ) ) ;
    public final void rule__Request__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2724:1: ( ( ( rule__Request__SummonersAssignment_2_1 ) ) )
            // InternalGloe.g:2725:1: ( ( rule__Request__SummonersAssignment_2_1 ) )
            {
            // InternalGloe.g:2725:1: ( ( rule__Request__SummonersAssignment_2_1 ) )
            // InternalGloe.g:2726:2: ( rule__Request__SummonersAssignment_2_1 )
            {
             before(grammarAccess.getRequestAccess().getSummonersAssignment_2_1()); 
            // InternalGloe.g:2727:2: ( rule__Request__SummonersAssignment_2_1 )
            // InternalGloe.g:2727:3: rule__Request__SummonersAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__SummonersAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getSummonersAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_2__1__Impl"


    // $ANTLR start "rule__Request__Group_5__0"
    // InternalGloe.g:2736:1: rule__Request__Group_5__0 : rule__Request__Group_5__0__Impl rule__Request__Group_5__1 ;
    public final void rule__Request__Group_5__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2740:1: ( rule__Request__Group_5__0__Impl rule__Request__Group_5__1 )
            // InternalGloe.g:2741:2: rule__Request__Group_5__0__Impl rule__Request__Group_5__1
            {
            pushFollow(FOLLOW_6);
            rule__Request__Group_5__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_5__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_5__0"


    // $ANTLR start "rule__Request__Group_5__0__Impl"
    // InternalGloe.g:2748:1: rule__Request__Group_5__0__Impl : ( ',' ) ;
    public final void rule__Request__Group_5__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2752:1: ( ( ',' ) )
            // InternalGloe.g:2753:1: ( ',' )
            {
            // InternalGloe.g:2753:1: ( ',' )
            // InternalGloe.g:2754:2: ','
            {
             before(grammarAccess.getRequestAccess().getCommaKeyword_5_0()); 
            match(input,219,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getCommaKeyword_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_5__0__Impl"


    // $ANTLR start "rule__Request__Group_5__1"
    // InternalGloe.g:2763:1: rule__Request__Group_5__1 : rule__Request__Group_5__1__Impl ;
    public final void rule__Request__Group_5__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2767:1: ( rule__Request__Group_5__1__Impl )
            // InternalGloe.g:2768:2: rule__Request__Group_5__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group_5__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_5__1"


    // $ANTLR start "rule__Request__Group_5__1__Impl"
    // InternalGloe.g:2774:1: rule__Request__Group_5__1__Impl : ( ( rule__Request__YvaluesAssignment_5_1 ) ) ;
    public final void rule__Request__Group_5__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2778:1: ( ( ( rule__Request__YvaluesAssignment_5_1 ) ) )
            // InternalGloe.g:2779:1: ( ( rule__Request__YvaluesAssignment_5_1 ) )
            {
            // InternalGloe.g:2779:1: ( ( rule__Request__YvaluesAssignment_5_1 ) )
            // InternalGloe.g:2780:2: ( rule__Request__YvaluesAssignment_5_1 )
            {
             before(grammarAccess.getRequestAccess().getYvaluesAssignment_5_1()); 
            // InternalGloe.g:2781:2: ( rule__Request__YvaluesAssignment_5_1 )
            // InternalGloe.g:2781:3: rule__Request__YvaluesAssignment_5_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__YvaluesAssignment_5_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getYvaluesAssignment_5_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_5__1__Impl"


    // $ANTLR start "rule__Request__Group_6__0"
    // InternalGloe.g:2790:1: rule__Request__Group_6__0 : rule__Request__Group_6__0__Impl rule__Request__Group_6__1 ;
    public final void rule__Request__Group_6__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2794:1: ( rule__Request__Group_6__0__Impl rule__Request__Group_6__1 )
            // InternalGloe.g:2795:2: rule__Request__Group_6__0__Impl rule__Request__Group_6__1
            {
            pushFollow(FOLLOW_10);
            rule__Request__Group_6__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_6__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_6__0"


    // $ANTLR start "rule__Request__Group_6__0__Impl"
    // InternalGloe.g:2802:1: rule__Request__Group_6__0__Impl : ( 'by' ) ;
    public final void rule__Request__Group_6__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2806:1: ( ( 'by' ) )
            // InternalGloe.g:2807:1: ( 'by' )
            {
            // InternalGloe.g:2807:1: ( 'by' )
            // InternalGloe.g:2808:2: 'by'
            {
             before(grammarAccess.getRequestAccess().getByKeyword_6_0()); 
            match(input,220,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getByKeyword_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_6__0__Impl"


    // $ANTLR start "rule__Request__Group_6__1"
    // InternalGloe.g:2817:1: rule__Request__Group_6__1 : rule__Request__Group_6__1__Impl ;
    public final void rule__Request__Group_6__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2821:1: ( rule__Request__Group_6__1__Impl )
            // InternalGloe.g:2822:2: rule__Request__Group_6__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group_6__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_6__1"


    // $ANTLR start "rule__Request__Group_6__1__Impl"
    // InternalGloe.g:2828:1: rule__Request__Group_6__1__Impl : ( ( rule__Request__XUnitAssignment_6_1 ) ) ;
    public final void rule__Request__Group_6__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2832:1: ( ( ( rule__Request__XUnitAssignment_6_1 ) ) )
            // InternalGloe.g:2833:1: ( ( rule__Request__XUnitAssignment_6_1 ) )
            {
            // InternalGloe.g:2833:1: ( ( rule__Request__XUnitAssignment_6_1 ) )
            // InternalGloe.g:2834:2: ( rule__Request__XUnitAssignment_6_1 )
            {
             before(grammarAccess.getRequestAccess().getXUnitAssignment_6_1()); 
            // InternalGloe.g:2835:2: ( rule__Request__XUnitAssignment_6_1 )
            // InternalGloe.g:2835:3: rule__Request__XUnitAssignment_6_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__XUnitAssignment_6_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getXUnitAssignment_6_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_6__1__Impl"


    // $ANTLR start "rule__Request__Group_7__0"
    // InternalGloe.g:2844:1: rule__Request__Group_7__0 : rule__Request__Group_7__0__Impl rule__Request__Group_7__1 ;
    public final void rule__Request__Group_7__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2848:1: ( rule__Request__Group_7__0__Impl rule__Request__Group_7__1 )
            // InternalGloe.g:2849:2: rule__Request__Group_7__0__Impl rule__Request__Group_7__1
            {
            pushFollow(FOLLOW_11);
            rule__Request__Group_7__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_7__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_7__0"


    // $ANTLR start "rule__Request__Group_7__0__Impl"
    // InternalGloe.g:2856:1: rule__Request__Group_7__0__Impl : ( 'by' ) ;
    public final void rule__Request__Group_7__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2860:1: ( ( 'by' ) )
            // InternalGloe.g:2861:1: ( 'by' )
            {
            // InternalGloe.g:2861:1: ( 'by' )
            // InternalGloe.g:2862:2: 'by'
            {
             before(grammarAccess.getRequestAccess().getByKeyword_7_0()); 
            match(input,220,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getByKeyword_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_7__0__Impl"


    // $ANTLR start "rule__Request__Group_7__1"
    // InternalGloe.g:2871:1: rule__Request__Group_7__1 : rule__Request__Group_7__1__Impl ;
    public final void rule__Request__Group_7__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2875:1: ( rule__Request__Group_7__1__Impl )
            // InternalGloe.g:2876:2: rule__Request__Group_7__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group_7__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_7__1"


    // $ANTLR start "rule__Request__Group_7__1__Impl"
    // InternalGloe.g:2882:1: rule__Request__Group_7__1__Impl : ( ( rule__Request__TimeScaleAssignment_7_1 ) ) ;
    public final void rule__Request__Group_7__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2886:1: ( ( ( rule__Request__TimeScaleAssignment_7_1 ) ) )
            // InternalGloe.g:2887:1: ( ( rule__Request__TimeScaleAssignment_7_1 ) )
            {
            // InternalGloe.g:2887:1: ( ( rule__Request__TimeScaleAssignment_7_1 ) )
            // InternalGloe.g:2888:2: ( rule__Request__TimeScaleAssignment_7_1 )
            {
             before(grammarAccess.getRequestAccess().getTimeScaleAssignment_7_1()); 
            // InternalGloe.g:2889:2: ( rule__Request__TimeScaleAssignment_7_1 )
            // InternalGloe.g:2889:3: rule__Request__TimeScaleAssignment_7_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__TimeScaleAssignment_7_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getTimeScaleAssignment_7_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_7__1__Impl"


    // $ANTLR start "rule__Request__Group_8__0"
    // InternalGloe.g:2898:1: rule__Request__Group_8__0 : rule__Request__Group_8__0__Impl rule__Request__Group_8__1 ;
    public final void rule__Request__Group_8__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2902:1: ( rule__Request__Group_8__0__Impl rule__Request__Group_8__1 )
            // InternalGloe.g:2903:2: rule__Request__Group_8__0__Impl rule__Request__Group_8__1
            {
            pushFollow(FOLLOW_12);
            rule__Request__Group_8__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_8__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8__0"


    // $ANTLR start "rule__Request__Group_8__0__Impl"
    // InternalGloe.g:2910:1: rule__Request__Group_8__0__Impl : ( 'when' ) ;
    public final void rule__Request__Group_8__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2914:1: ( ( 'when' ) )
            // InternalGloe.g:2915:1: ( 'when' )
            {
            // InternalGloe.g:2915:1: ( 'when' )
            // InternalGloe.g:2916:2: 'when'
            {
             before(grammarAccess.getRequestAccess().getWhenKeyword_8_0()); 
            match(input,221,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getWhenKeyword_8_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8__0__Impl"


    // $ANTLR start "rule__Request__Group_8__1"
    // InternalGloe.g:2925:1: rule__Request__Group_8__1 : rule__Request__Group_8__1__Impl rule__Request__Group_8__2 ;
    public final void rule__Request__Group_8__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2929:1: ( rule__Request__Group_8__1__Impl rule__Request__Group_8__2 )
            // InternalGloe.g:2930:2: rule__Request__Group_8__1__Impl rule__Request__Group_8__2
            {
            pushFollow(FOLLOW_13);
            rule__Request__Group_8__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_8__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8__1"


    // $ANTLR start "rule__Request__Group_8__1__Impl"
    // InternalGloe.g:2937:1: rule__Request__Group_8__1__Impl : ( ( rule__Request__FiltersAssignment_8_1 ) ) ;
    public final void rule__Request__Group_8__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2941:1: ( ( ( rule__Request__FiltersAssignment_8_1 ) ) )
            // InternalGloe.g:2942:1: ( ( rule__Request__FiltersAssignment_8_1 ) )
            {
            // InternalGloe.g:2942:1: ( ( rule__Request__FiltersAssignment_8_1 ) )
            // InternalGloe.g:2943:2: ( rule__Request__FiltersAssignment_8_1 )
            {
             before(grammarAccess.getRequestAccess().getFiltersAssignment_8_1()); 
            // InternalGloe.g:2944:2: ( rule__Request__FiltersAssignment_8_1 )
            // InternalGloe.g:2944:3: rule__Request__FiltersAssignment_8_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__FiltersAssignment_8_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getFiltersAssignment_8_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8__1__Impl"


    // $ANTLR start "rule__Request__Group_8__2"
    // InternalGloe.g:2952:1: rule__Request__Group_8__2 : rule__Request__Group_8__2__Impl ;
    public final void rule__Request__Group_8__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2956:1: ( rule__Request__Group_8__2__Impl )
            // InternalGloe.g:2957:2: rule__Request__Group_8__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group_8__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8__2"


    // $ANTLR start "rule__Request__Group_8__2__Impl"
    // InternalGloe.g:2963:1: rule__Request__Group_8__2__Impl : ( ( rule__Request__Group_8_2__0 )* ) ;
    public final void rule__Request__Group_8__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2967:1: ( ( ( rule__Request__Group_8_2__0 )* ) )
            // InternalGloe.g:2968:1: ( ( rule__Request__Group_8_2__0 )* )
            {
            // InternalGloe.g:2968:1: ( ( rule__Request__Group_8_2__0 )* )
            // InternalGloe.g:2969:2: ( rule__Request__Group_8_2__0 )*
            {
             before(grammarAccess.getRequestAccess().getGroup_8_2()); 
            // InternalGloe.g:2970:2: ( rule__Request__Group_8_2__0 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==222) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalGloe.g:2970:3: rule__Request__Group_8_2__0
            	    {
            	    pushFollow(FOLLOW_14);
            	    rule__Request__Group_8_2__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getRequestAccess().getGroup_8_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8__2__Impl"


    // $ANTLR start "rule__Request__Group_8_2__0"
    // InternalGloe.g:2979:1: rule__Request__Group_8_2__0 : rule__Request__Group_8_2__0__Impl rule__Request__Group_8_2__1 ;
    public final void rule__Request__Group_8_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2983:1: ( rule__Request__Group_8_2__0__Impl rule__Request__Group_8_2__1 )
            // InternalGloe.g:2984:2: rule__Request__Group_8_2__0__Impl rule__Request__Group_8_2__1
            {
            pushFollow(FOLLOW_12);
            rule__Request__Group_8_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Request__Group_8_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8_2__0"


    // $ANTLR start "rule__Request__Group_8_2__0__Impl"
    // InternalGloe.g:2991:1: rule__Request__Group_8_2__0__Impl : ( 'and' ) ;
    public final void rule__Request__Group_8_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:2995:1: ( ( 'and' ) )
            // InternalGloe.g:2996:1: ( 'and' )
            {
            // InternalGloe.g:2996:1: ( 'and' )
            // InternalGloe.g:2997:2: 'and'
            {
             before(grammarAccess.getRequestAccess().getAndKeyword_8_2_0()); 
            match(input,222,FOLLOW_2); 
             after(grammarAccess.getRequestAccess().getAndKeyword_8_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8_2__0__Impl"


    // $ANTLR start "rule__Request__Group_8_2__1"
    // InternalGloe.g:3006:1: rule__Request__Group_8_2__1 : rule__Request__Group_8_2__1__Impl ;
    public final void rule__Request__Group_8_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3010:1: ( rule__Request__Group_8_2__1__Impl )
            // InternalGloe.g:3011:2: rule__Request__Group_8_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Request__Group_8_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8_2__1"


    // $ANTLR start "rule__Request__Group_8_2__1__Impl"
    // InternalGloe.g:3017:1: rule__Request__Group_8_2__1__Impl : ( ( rule__Request__FiltersAssignment_8_2_1 ) ) ;
    public final void rule__Request__Group_8_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3021:1: ( ( ( rule__Request__FiltersAssignment_8_2_1 ) ) )
            // InternalGloe.g:3022:1: ( ( rule__Request__FiltersAssignment_8_2_1 ) )
            {
            // InternalGloe.g:3022:1: ( ( rule__Request__FiltersAssignment_8_2_1 ) )
            // InternalGloe.g:3023:2: ( rule__Request__FiltersAssignment_8_2_1 )
            {
             before(grammarAccess.getRequestAccess().getFiltersAssignment_8_2_1()); 
            // InternalGloe.g:3024:2: ( rule__Request__FiltersAssignment_8_2_1 )
            // InternalGloe.g:3024:3: rule__Request__FiltersAssignment_8_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Request__FiltersAssignment_8_2_1();

            state._fsp--;


            }

             after(grammarAccess.getRequestAccess().getFiltersAssignment_8_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__Group_8_2__1__Impl"


    // $ANTLR start "rule__Summoner__Group__0"
    // InternalGloe.g:3033:1: rule__Summoner__Group__0 : rule__Summoner__Group__0__Impl rule__Summoner__Group__1 ;
    public final void rule__Summoner__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3037:1: ( rule__Summoner__Group__0__Impl rule__Summoner__Group__1 )
            // InternalGloe.g:3038:2: rule__Summoner__Group__0__Impl rule__Summoner__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__Summoner__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Summoner__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Summoner__Group__0"


    // $ANTLR start "rule__Summoner__Group__0__Impl"
    // InternalGloe.g:3045:1: rule__Summoner__Group__0__Impl : ( () ) ;
    public final void rule__Summoner__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3049:1: ( ( () ) )
            // InternalGloe.g:3050:1: ( () )
            {
            // InternalGloe.g:3050:1: ( () )
            // InternalGloe.g:3051:2: ()
            {
             before(grammarAccess.getSummonerAccess().getSummonerAction_0()); 
            // InternalGloe.g:3052:2: ()
            // InternalGloe.g:3052:3: 
            {
            }

             after(grammarAccess.getSummonerAccess().getSummonerAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Summoner__Group__0__Impl"


    // $ANTLR start "rule__Summoner__Group__1"
    // InternalGloe.g:3060:1: rule__Summoner__Group__1 : rule__Summoner__Group__1__Impl ;
    public final void rule__Summoner__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3064:1: ( rule__Summoner__Group__1__Impl )
            // InternalGloe.g:3065:2: rule__Summoner__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Summoner__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Summoner__Group__1"


    // $ANTLR start "rule__Summoner__Group__1__Impl"
    // InternalGloe.g:3071:1: rule__Summoner__Group__1__Impl : ( ( rule__Summoner__NameAssignment_1 ) ) ;
    public final void rule__Summoner__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3075:1: ( ( ( rule__Summoner__NameAssignment_1 ) ) )
            // InternalGloe.g:3076:1: ( ( rule__Summoner__NameAssignment_1 ) )
            {
            // InternalGloe.g:3076:1: ( ( rule__Summoner__NameAssignment_1 ) )
            // InternalGloe.g:3077:2: ( rule__Summoner__NameAssignment_1 )
            {
             before(grammarAccess.getSummonerAccess().getNameAssignment_1()); 
            // InternalGloe.g:3078:2: ( rule__Summoner__NameAssignment_1 )
            // InternalGloe.g:3078:3: rule__Summoner__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Summoner__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getSummonerAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Summoner__Group__1__Impl"


    // $ANTLR start "rule__Sum__Group__0"
    // InternalGloe.g:3087:1: rule__Sum__Group__0 : rule__Sum__Group__0__Impl rule__Sum__Group__1 ;
    public final void rule__Sum__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3091:1: ( rule__Sum__Group__0__Impl rule__Sum__Group__1 )
            // InternalGloe.g:3092:2: rule__Sum__Group__0__Impl rule__Sum__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Sum__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Sum__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__0"


    // $ANTLR start "rule__Sum__Group__0__Impl"
    // InternalGloe.g:3099:1: rule__Sum__Group__0__Impl : ( () ) ;
    public final void rule__Sum__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3103:1: ( ( () ) )
            // InternalGloe.g:3104:1: ( () )
            {
            // InternalGloe.g:3104:1: ( () )
            // InternalGloe.g:3105:2: ()
            {
             before(grammarAccess.getSumAccess().getSumAction_0()); 
            // InternalGloe.g:3106:2: ()
            // InternalGloe.g:3106:3: 
            {
            }

             after(grammarAccess.getSumAccess().getSumAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__0__Impl"


    // $ANTLR start "rule__Sum__Group__1"
    // InternalGloe.g:3114:1: rule__Sum__Group__1 : rule__Sum__Group__1__Impl rule__Sum__Group__2 ;
    public final void rule__Sum__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3118:1: ( rule__Sum__Group__1__Impl rule__Sum__Group__2 )
            // InternalGloe.g:3119:2: rule__Sum__Group__1__Impl rule__Sum__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Sum__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Sum__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__1"


    // $ANTLR start "rule__Sum__Group__1__Impl"
    // InternalGloe.g:3126:1: rule__Sum__Group__1__Impl : ( 'total' ) ;
    public final void rule__Sum__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3130:1: ( ( 'total' ) )
            // InternalGloe.g:3131:1: ( 'total' )
            {
            // InternalGloe.g:3131:1: ( 'total' )
            // InternalGloe.g:3132:2: 'total'
            {
             before(grammarAccess.getSumAccess().getTotalKeyword_1()); 
            match(input,223,FOLLOW_2); 
             after(grammarAccess.getSumAccess().getTotalKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__1__Impl"


    // $ANTLR start "rule__Sum__Group__2"
    // InternalGloe.g:3141:1: rule__Sum__Group__2 : rule__Sum__Group__2__Impl rule__Sum__Group__3 ;
    public final void rule__Sum__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3145:1: ( rule__Sum__Group__2__Impl rule__Sum__Group__3 )
            // InternalGloe.g:3146:2: rule__Sum__Group__2__Impl rule__Sum__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__Sum__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Sum__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__2"


    // $ANTLR start "rule__Sum__Group__2__Impl"
    // InternalGloe.g:3153:1: rule__Sum__Group__2__Impl : ( 'of' ) ;
    public final void rule__Sum__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3157:1: ( ( 'of' ) )
            // InternalGloe.g:3158:1: ( 'of' )
            {
            // InternalGloe.g:3158:1: ( 'of' )
            // InternalGloe.g:3159:2: 'of'
            {
             before(grammarAccess.getSumAccess().getOfKeyword_2()); 
            match(input,224,FOLLOW_2); 
             after(grammarAccess.getSumAccess().getOfKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__2__Impl"


    // $ANTLR start "rule__Sum__Group__3"
    // InternalGloe.g:3168:1: rule__Sum__Group__3 : rule__Sum__Group__3__Impl ;
    public final void rule__Sum__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3172:1: ( rule__Sum__Group__3__Impl )
            // InternalGloe.g:3173:2: rule__Sum__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Sum__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__3"


    // $ANTLR start "rule__Sum__Group__3__Impl"
    // InternalGloe.g:3179:1: rule__Sum__Group__3__Impl : ( ( rule__Sum__ValueAssignment_3 ) ) ;
    public final void rule__Sum__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3183:1: ( ( ( rule__Sum__ValueAssignment_3 ) ) )
            // InternalGloe.g:3184:1: ( ( rule__Sum__ValueAssignment_3 ) )
            {
            // InternalGloe.g:3184:1: ( ( rule__Sum__ValueAssignment_3 ) )
            // InternalGloe.g:3185:2: ( rule__Sum__ValueAssignment_3 )
            {
             before(grammarAccess.getSumAccess().getValueAssignment_3()); 
            // InternalGloe.g:3186:2: ( rule__Sum__ValueAssignment_3 )
            // InternalGloe.g:3186:3: rule__Sum__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Sum__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getSumAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__Group__3__Impl"


    // $ANTLR start "rule__Average__Group__0"
    // InternalGloe.g:3195:1: rule__Average__Group__0 : rule__Average__Group__0__Impl rule__Average__Group__1 ;
    public final void rule__Average__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3199:1: ( rule__Average__Group__0__Impl rule__Average__Group__1 )
            // InternalGloe.g:3200:2: rule__Average__Group__0__Impl rule__Average__Group__1
            {
            pushFollow(FOLLOW_18);
            rule__Average__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Average__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__0"


    // $ANTLR start "rule__Average__Group__0__Impl"
    // InternalGloe.g:3207:1: rule__Average__Group__0__Impl : ( () ) ;
    public final void rule__Average__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3211:1: ( ( () ) )
            // InternalGloe.g:3212:1: ( () )
            {
            // InternalGloe.g:3212:1: ( () )
            // InternalGloe.g:3213:2: ()
            {
             before(grammarAccess.getAverageAccess().getAverageAction_0()); 
            // InternalGloe.g:3214:2: ()
            // InternalGloe.g:3214:3: 
            {
            }

             after(grammarAccess.getAverageAccess().getAverageAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__0__Impl"


    // $ANTLR start "rule__Average__Group__1"
    // InternalGloe.g:3222:1: rule__Average__Group__1 : rule__Average__Group__1__Impl rule__Average__Group__2 ;
    public final void rule__Average__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3226:1: ( rule__Average__Group__1__Impl rule__Average__Group__2 )
            // InternalGloe.g:3227:2: rule__Average__Group__1__Impl rule__Average__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Average__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Average__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__1"


    // $ANTLR start "rule__Average__Group__1__Impl"
    // InternalGloe.g:3234:1: rule__Average__Group__1__Impl : ( 'average' ) ;
    public final void rule__Average__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3238:1: ( ( 'average' ) )
            // InternalGloe.g:3239:1: ( 'average' )
            {
            // InternalGloe.g:3239:1: ( 'average' )
            // InternalGloe.g:3240:2: 'average'
            {
             before(grammarAccess.getAverageAccess().getAverageKeyword_1()); 
            match(input,225,FOLLOW_2); 
             after(grammarAccess.getAverageAccess().getAverageKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__1__Impl"


    // $ANTLR start "rule__Average__Group__2"
    // InternalGloe.g:3249:1: rule__Average__Group__2 : rule__Average__Group__2__Impl rule__Average__Group__3 ;
    public final void rule__Average__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3253:1: ( rule__Average__Group__2__Impl rule__Average__Group__3 )
            // InternalGloe.g:3254:2: rule__Average__Group__2__Impl rule__Average__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__Average__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Average__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__2"


    // $ANTLR start "rule__Average__Group__2__Impl"
    // InternalGloe.g:3261:1: rule__Average__Group__2__Impl : ( 'of' ) ;
    public final void rule__Average__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3265:1: ( ( 'of' ) )
            // InternalGloe.g:3266:1: ( 'of' )
            {
            // InternalGloe.g:3266:1: ( 'of' )
            // InternalGloe.g:3267:2: 'of'
            {
             before(grammarAccess.getAverageAccess().getOfKeyword_2()); 
            match(input,224,FOLLOW_2); 
             after(grammarAccess.getAverageAccess().getOfKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__2__Impl"


    // $ANTLR start "rule__Average__Group__3"
    // InternalGloe.g:3276:1: rule__Average__Group__3 : rule__Average__Group__3__Impl ;
    public final void rule__Average__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3280:1: ( rule__Average__Group__3__Impl )
            // InternalGloe.g:3281:2: rule__Average__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Average__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__3"


    // $ANTLR start "rule__Average__Group__3__Impl"
    // InternalGloe.g:3287:1: rule__Average__Group__3__Impl : ( ( rule__Average__ValueAssignment_3 ) ) ;
    public final void rule__Average__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3291:1: ( ( ( rule__Average__ValueAssignment_3 ) ) )
            // InternalGloe.g:3292:1: ( ( rule__Average__ValueAssignment_3 ) )
            {
            // InternalGloe.g:3292:1: ( ( rule__Average__ValueAssignment_3 ) )
            // InternalGloe.g:3293:2: ( rule__Average__ValueAssignment_3 )
            {
             before(grammarAccess.getAverageAccess().getValueAssignment_3()); 
            // InternalGloe.g:3294:2: ( rule__Average__ValueAssignment_3 )
            // InternalGloe.g:3294:3: rule__Average__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Average__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getAverageAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__Group__3__Impl"


    // $ANTLR start "rule__Ratio__Group__0"
    // InternalGloe.g:3303:1: rule__Ratio__Group__0 : rule__Ratio__Group__0__Impl rule__Ratio__Group__1 ;
    public final void rule__Ratio__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3307:1: ( rule__Ratio__Group__0__Impl rule__Ratio__Group__1 )
            // InternalGloe.g:3308:2: rule__Ratio__Group__0__Impl rule__Ratio__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__Ratio__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ratio__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__0"


    // $ANTLR start "rule__Ratio__Group__0__Impl"
    // InternalGloe.g:3315:1: rule__Ratio__Group__0__Impl : ( () ) ;
    public final void rule__Ratio__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3319:1: ( ( () ) )
            // InternalGloe.g:3320:1: ( () )
            {
            // InternalGloe.g:3320:1: ( () )
            // InternalGloe.g:3321:2: ()
            {
             before(grammarAccess.getRatioAccess().getRatioAction_0()); 
            // InternalGloe.g:3322:2: ()
            // InternalGloe.g:3322:3: 
            {
            }

             after(grammarAccess.getRatioAccess().getRatioAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__0__Impl"


    // $ANTLR start "rule__Ratio__Group__1"
    // InternalGloe.g:3330:1: rule__Ratio__Group__1 : rule__Ratio__Group__1__Impl rule__Ratio__Group__2 ;
    public final void rule__Ratio__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3334:1: ( rule__Ratio__Group__1__Impl rule__Ratio__Group__2 )
            // InternalGloe.g:3335:2: rule__Ratio__Group__1__Impl rule__Ratio__Group__2
            {
            pushFollow(FOLLOW_16);
            rule__Ratio__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ratio__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__1"


    // $ANTLR start "rule__Ratio__Group__1__Impl"
    // InternalGloe.g:3342:1: rule__Ratio__Group__1__Impl : ( 'ratio' ) ;
    public final void rule__Ratio__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3346:1: ( ( 'ratio' ) )
            // InternalGloe.g:3347:1: ( 'ratio' )
            {
            // InternalGloe.g:3347:1: ( 'ratio' )
            // InternalGloe.g:3348:2: 'ratio'
            {
             before(grammarAccess.getRatioAccess().getRatioKeyword_1()); 
            match(input,226,FOLLOW_2); 
             after(grammarAccess.getRatioAccess().getRatioKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__1__Impl"


    // $ANTLR start "rule__Ratio__Group__2"
    // InternalGloe.g:3357:1: rule__Ratio__Group__2 : rule__Ratio__Group__2__Impl rule__Ratio__Group__3 ;
    public final void rule__Ratio__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3361:1: ( rule__Ratio__Group__2__Impl rule__Ratio__Group__3 )
            // InternalGloe.g:3362:2: rule__Ratio__Group__2__Impl rule__Ratio__Group__3
            {
            pushFollow(FOLLOW_20);
            rule__Ratio__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Ratio__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__2"


    // $ANTLR start "rule__Ratio__Group__2__Impl"
    // InternalGloe.g:3369:1: rule__Ratio__Group__2__Impl : ( 'of' ) ;
    public final void rule__Ratio__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3373:1: ( ( 'of' ) )
            // InternalGloe.g:3374:1: ( 'of' )
            {
            // InternalGloe.g:3374:1: ( 'of' )
            // InternalGloe.g:3375:2: 'of'
            {
             before(grammarAccess.getRatioAccess().getOfKeyword_2()); 
            match(input,224,FOLLOW_2); 
             after(grammarAccess.getRatioAccess().getOfKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__2__Impl"


    // $ANTLR start "rule__Ratio__Group__3"
    // InternalGloe.g:3384:1: rule__Ratio__Group__3 : rule__Ratio__Group__3__Impl ;
    public final void rule__Ratio__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3388:1: ( rule__Ratio__Group__3__Impl )
            // InternalGloe.g:3389:2: rule__Ratio__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Ratio__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__3"


    // $ANTLR start "rule__Ratio__Group__3__Impl"
    // InternalGloe.g:3395:1: rule__Ratio__Group__3__Impl : ( ( rule__Ratio__ValueAssignment_3 ) ) ;
    public final void rule__Ratio__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3399:1: ( ( ( rule__Ratio__ValueAssignment_3 ) ) )
            // InternalGloe.g:3400:1: ( ( rule__Ratio__ValueAssignment_3 ) )
            {
            // InternalGloe.g:3400:1: ( ( rule__Ratio__ValueAssignment_3 ) )
            // InternalGloe.g:3401:2: ( rule__Ratio__ValueAssignment_3 )
            {
             before(grammarAccess.getRatioAccess().getValueAssignment_3()); 
            // InternalGloe.g:3402:2: ( rule__Ratio__ValueAssignment_3 )
            // InternalGloe.g:3402:3: rule__Ratio__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__Ratio__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getRatioAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__Group__3__Impl"


    // $ANTLR start "rule__Type__Group__0"
    // InternalGloe.g:3411:1: rule__Type__Group__0 : rule__Type__Group__0__Impl rule__Type__Group__1 ;
    public final void rule__Type__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3415:1: ( rule__Type__Group__0__Impl rule__Type__Group__1 )
            // InternalGloe.g:3416:2: rule__Type__Group__0__Impl rule__Type__Group__1
            {
            pushFollow(FOLLOW_10);
            rule__Type__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Type__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0"


    // $ANTLR start "rule__Type__Group__0__Impl"
    // InternalGloe.g:3423:1: rule__Type__Group__0__Impl : ( () ) ;
    public final void rule__Type__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3427:1: ( ( () ) )
            // InternalGloe.g:3428:1: ( () )
            {
            // InternalGloe.g:3428:1: ( () )
            // InternalGloe.g:3429:2: ()
            {
             before(grammarAccess.getTypeAccess().getTypeAction_0()); 
            // InternalGloe.g:3430:2: ()
            // InternalGloe.g:3430:3: 
            {
            }

             after(grammarAccess.getTypeAccess().getTypeAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__0__Impl"


    // $ANTLR start "rule__Type__Group__1"
    // InternalGloe.g:3438:1: rule__Type__Group__1 : rule__Type__Group__1__Impl ;
    public final void rule__Type__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3442:1: ( rule__Type__Group__1__Impl )
            // InternalGloe.g:3443:2: rule__Type__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Type__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1"


    // $ANTLR start "rule__Type__Group__1__Impl"
    // InternalGloe.g:3449:1: rule__Type__Group__1__Impl : ( ( rule__Type__TypeAssignment_1 ) ) ;
    public final void rule__Type__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3453:1: ( ( ( rule__Type__TypeAssignment_1 ) ) )
            // InternalGloe.g:3454:1: ( ( rule__Type__TypeAssignment_1 ) )
            {
            // InternalGloe.g:3454:1: ( ( rule__Type__TypeAssignment_1 ) )
            // InternalGloe.g:3455:2: ( rule__Type__TypeAssignment_1 )
            {
             before(grammarAccess.getTypeAccess().getTypeAssignment_1()); 
            // InternalGloe.g:3456:2: ( rule__Type__TypeAssignment_1 )
            // InternalGloe.g:3456:3: rule__Type__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Type__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Group__1__Impl"


    // $ANTLR start "rule__Value__Group_3__0"
    // InternalGloe.g:3465:1: rule__Value__Group_3__0 : rule__Value__Group_3__0__Impl rule__Value__Group_3__1 ;
    public final void rule__Value__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3469:1: ( rule__Value__Group_3__0__Impl rule__Value__Group_3__1 )
            // InternalGloe.g:3470:2: rule__Value__Group_3__0__Impl rule__Value__Group_3__1
            {
            pushFollow(FOLLOW_21);
            rule__Value__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Value__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Group_3__0"


    // $ANTLR start "rule__Value__Group_3__0__Impl"
    // InternalGloe.g:3477:1: rule__Value__Group_3__0__Impl : ( 'games' ) ;
    public final void rule__Value__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3481:1: ( ( 'games' ) )
            // InternalGloe.g:3482:1: ( 'games' )
            {
            // InternalGloe.g:3482:1: ( 'games' )
            // InternalGloe.g:3483:2: 'games'
            {
             before(grammarAccess.getValueAccess().getGamesKeyword_3_0()); 
            match(input,227,FOLLOW_2); 
             after(grammarAccess.getValueAccess().getGamesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Group_3__0__Impl"


    // $ANTLR start "rule__Value__Group_3__1"
    // InternalGloe.g:3492:1: rule__Value__Group_3__1 : rule__Value__Group_3__1__Impl rule__Value__Group_3__2 ;
    public final void rule__Value__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3496:1: ( rule__Value__Group_3__1__Impl rule__Value__Group_3__2 )
            // InternalGloe.g:3497:2: rule__Value__Group_3__1__Impl rule__Value__Group_3__2
            {
            pushFollow(FOLLOW_22);
            rule__Value__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Value__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Group_3__1"


    // $ANTLR start "rule__Value__Group_3__1__Impl"
    // InternalGloe.g:3504:1: rule__Value__Group_3__1__Impl : ( 'with' ) ;
    public final void rule__Value__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3508:1: ( ( 'with' ) )
            // InternalGloe.g:3509:1: ( 'with' )
            {
            // InternalGloe.g:3509:1: ( 'with' )
            // InternalGloe.g:3510:2: 'with'
            {
             before(grammarAccess.getValueAccess().getWithKeyword_3_1()); 
            match(input,228,FOLLOW_2); 
             after(grammarAccess.getValueAccess().getWithKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Group_3__1__Impl"


    // $ANTLR start "rule__Value__Group_3__2"
    // InternalGloe.g:3519:1: rule__Value__Group_3__2 : rule__Value__Group_3__2__Impl ;
    public final void rule__Value__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3523:1: ( rule__Value__Group_3__2__Impl )
            // InternalGloe.g:3524:2: rule__Value__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Value__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Group_3__2"


    // $ANTLR start "rule__Value__Group_3__2__Impl"
    // InternalGloe.g:3530:1: rule__Value__Group_3__2__Impl : ( ruleChampionValue ) ;
    public final void rule__Value__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3534:1: ( ( ruleChampionValue ) )
            // InternalGloe.g:3535:1: ( ruleChampionValue )
            {
            // InternalGloe.g:3535:1: ( ruleChampionValue )
            // InternalGloe.g:3536:2: ruleChampionValue
            {
             before(grammarAccess.getValueAccess().getChampionValueParserRuleCall_3_2()); 
            pushFollow(FOLLOW_2);
            ruleChampionValue();

            state._fsp--;

             after(grammarAccess.getValueAccess().getChampionValueParserRuleCall_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Value__Group_3__2__Impl"


    // $ANTLR start "rule__PluralValue__Group_1__0"
    // InternalGloe.g:3546:1: rule__PluralValue__Group_1__0 : rule__PluralValue__Group_1__0__Impl rule__PluralValue__Group_1__1 ;
    public final void rule__PluralValue__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3550:1: ( rule__PluralValue__Group_1__0__Impl rule__PluralValue__Group_1__1 )
            // InternalGloe.g:3551:2: rule__PluralValue__Group_1__0__Impl rule__PluralValue__Group_1__1
            {
            pushFollow(FOLLOW_23);
            rule__PluralValue__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__0"


    // $ANTLR start "rule__PluralValue__Group_1__0__Impl"
    // InternalGloe.g:3558:1: rule__PluralValue__Group_1__0__Impl : ( 'games' ) ;
    public final void rule__PluralValue__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3562:1: ( ( 'games' ) )
            // InternalGloe.g:3563:1: ( 'games' )
            {
            // InternalGloe.g:3563:1: ( 'games' )
            // InternalGloe.g:3564:2: 'games'
            {
             before(grammarAccess.getPluralValueAccess().getGamesKeyword_1_0()); 
            match(input,227,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getGamesKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__0__Impl"


    // $ANTLR start "rule__PluralValue__Group_1__1"
    // InternalGloe.g:3573:1: rule__PluralValue__Group_1__1 : rule__PluralValue__Group_1__1__Impl rule__PluralValue__Group_1__2 ;
    public final void rule__PluralValue__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3577:1: ( rule__PluralValue__Group_1__1__Impl rule__PluralValue__Group_1__2 )
            // InternalGloe.g:3578:2: rule__PluralValue__Group_1__1__Impl rule__PluralValue__Group_1__2
            {
            pushFollow(FOLLOW_24);
            rule__PluralValue__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__1"


    // $ANTLR start "rule__PluralValue__Group_1__1__Impl"
    // InternalGloe.g:3585:1: rule__PluralValue__Group_1__1__Impl : ( 'in' ) ;
    public final void rule__PluralValue__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3589:1: ( ( 'in' ) )
            // InternalGloe.g:3590:1: ( 'in' )
            {
            // InternalGloe.g:3590:1: ( 'in' )
            // InternalGloe.g:3591:2: 'in'
            {
             before(grammarAccess.getPluralValueAccess().getInKeyword_1_1()); 
            match(input,217,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getInKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__1__Impl"


    // $ANTLR start "rule__PluralValue__Group_1__2"
    // InternalGloe.g:3600:1: rule__PluralValue__Group_1__2 : rule__PluralValue__Group_1__2__Impl rule__PluralValue__Group_1__3 ;
    public final void rule__PluralValue__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3604:1: ( rule__PluralValue__Group_1__2__Impl rule__PluralValue__Group_1__3 )
            // InternalGloe.g:3605:2: rule__PluralValue__Group_1__2__Impl rule__PluralValue__Group_1__3
            {
            pushFollow(FOLLOW_25);
            rule__PluralValue__Group_1__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_1__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__2"


    // $ANTLR start "rule__PluralValue__Group_1__2__Impl"
    // InternalGloe.g:3612:1: rule__PluralValue__Group_1__2__Impl : ( ruleTeamValue ) ;
    public final void rule__PluralValue__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3616:1: ( ( ruleTeamValue ) )
            // InternalGloe.g:3617:1: ( ruleTeamValue )
            {
            // InternalGloe.g:3617:1: ( ruleTeamValue )
            // InternalGloe.g:3618:2: ruleTeamValue
            {
             before(grammarAccess.getPluralValueAccess().getTeamValueParserRuleCall_1_2()); 
            pushFollow(FOLLOW_2);
            ruleTeamValue();

            state._fsp--;

             after(grammarAccess.getPluralValueAccess().getTeamValueParserRuleCall_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__2__Impl"


    // $ANTLR start "rule__PluralValue__Group_1__3"
    // InternalGloe.g:3627:1: rule__PluralValue__Group_1__3 : rule__PluralValue__Group_1__3__Impl ;
    public final void rule__PluralValue__Group_1__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3631:1: ( rule__PluralValue__Group_1__3__Impl )
            // InternalGloe.g:3632:2: rule__PluralValue__Group_1__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_1__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__3"


    // $ANTLR start "rule__PluralValue__Group_1__3__Impl"
    // InternalGloe.g:3638:1: rule__PluralValue__Group_1__3__Impl : ( 'team' ) ;
    public final void rule__PluralValue__Group_1__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3642:1: ( ( 'team' ) )
            // InternalGloe.g:3643:1: ( 'team' )
            {
            // InternalGloe.g:3643:1: ( 'team' )
            // InternalGloe.g:3644:2: 'team'
            {
             before(grammarAccess.getPluralValueAccess().getTeamKeyword_1_3()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getTeamKeyword_1_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_1__3__Impl"


    // $ANTLR start "rule__PluralValue__Group_3__0"
    // InternalGloe.g:3654:1: rule__PluralValue__Group_3__0 : rule__PluralValue__Group_3__0__Impl rule__PluralValue__Group_3__1 ;
    public final void rule__PluralValue__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3658:1: ( rule__PluralValue__Group_3__0__Impl rule__PluralValue__Group_3__1 )
            // InternalGloe.g:3659:2: rule__PluralValue__Group_3__0__Impl rule__PluralValue__Group_3__1
            {
            pushFollow(FOLLOW_21);
            rule__PluralValue__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_3__0"


    // $ANTLR start "rule__PluralValue__Group_3__0__Impl"
    // InternalGloe.g:3666:1: rule__PluralValue__Group_3__0__Impl : ( 'games' ) ;
    public final void rule__PluralValue__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3670:1: ( ( 'games' ) )
            // InternalGloe.g:3671:1: ( 'games' )
            {
            // InternalGloe.g:3671:1: ( 'games' )
            // InternalGloe.g:3672:2: 'games'
            {
             before(grammarAccess.getPluralValueAccess().getGamesKeyword_3_0()); 
            match(input,227,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getGamesKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_3__0__Impl"


    // $ANTLR start "rule__PluralValue__Group_3__1"
    // InternalGloe.g:3681:1: rule__PluralValue__Group_3__1 : rule__PluralValue__Group_3__1__Impl rule__PluralValue__Group_3__2 ;
    public final void rule__PluralValue__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3685:1: ( rule__PluralValue__Group_3__1__Impl rule__PluralValue__Group_3__2 )
            // InternalGloe.g:3686:2: rule__PluralValue__Group_3__1__Impl rule__PluralValue__Group_3__2
            {
            pushFollow(FOLLOW_22);
            rule__PluralValue__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_3__1"


    // $ANTLR start "rule__PluralValue__Group_3__1__Impl"
    // InternalGloe.g:3693:1: rule__PluralValue__Group_3__1__Impl : ( 'with' ) ;
    public final void rule__PluralValue__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3697:1: ( ( 'with' ) )
            // InternalGloe.g:3698:1: ( 'with' )
            {
            // InternalGloe.g:3698:1: ( 'with' )
            // InternalGloe.g:3699:2: 'with'
            {
             before(grammarAccess.getPluralValueAccess().getWithKeyword_3_1()); 
            match(input,228,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getWithKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_3__1__Impl"


    // $ANTLR start "rule__PluralValue__Group_3__2"
    // InternalGloe.g:3708:1: rule__PluralValue__Group_3__2 : rule__PluralValue__Group_3__2__Impl ;
    public final void rule__PluralValue__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3712:1: ( rule__PluralValue__Group_3__2__Impl )
            // InternalGloe.g:3713:2: rule__PluralValue__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_3__2"


    // $ANTLR start "rule__PluralValue__Group_3__2__Impl"
    // InternalGloe.g:3719:1: rule__PluralValue__Group_3__2__Impl : ( ruleChampionValue ) ;
    public final void rule__PluralValue__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3723:1: ( ( ruleChampionValue ) )
            // InternalGloe.g:3724:1: ( ruleChampionValue )
            {
            // InternalGloe.g:3724:1: ( ruleChampionValue )
            // InternalGloe.g:3725:2: ruleChampionValue
            {
             before(grammarAccess.getPluralValueAccess().getChampionValueParserRuleCall_3_2()); 
            pushFollow(FOLLOW_2);
            ruleChampionValue();

            state._fsp--;

             after(grammarAccess.getPluralValueAccess().getChampionValueParserRuleCall_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_3__2__Impl"


    // $ANTLR start "rule__PluralValue__Group_4__0"
    // InternalGloe.g:3735:1: rule__PluralValue__Group_4__0 : rule__PluralValue__Group_4__0__Impl rule__PluralValue__Group_4__1 ;
    public final void rule__PluralValue__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3739:1: ( rule__PluralValue__Group_4__0__Impl rule__PluralValue__Group_4__1 )
            // InternalGloe.g:3740:2: rule__PluralValue__Group_4__0__Impl rule__PluralValue__Group_4__1
            {
            pushFollow(FOLLOW_23);
            rule__PluralValue__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_4__0"


    // $ANTLR start "rule__PluralValue__Group_4__0__Impl"
    // InternalGloe.g:3747:1: rule__PluralValue__Group_4__0__Impl : ( 'games' ) ;
    public final void rule__PluralValue__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3751:1: ( ( 'games' ) )
            // InternalGloe.g:3752:1: ( 'games' )
            {
            // InternalGloe.g:3752:1: ( 'games' )
            // InternalGloe.g:3753:2: 'games'
            {
             before(grammarAccess.getPluralValueAccess().getGamesKeyword_4_0()); 
            match(input,227,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getGamesKeyword_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_4__0__Impl"


    // $ANTLR start "rule__PluralValue__Group_4__1"
    // InternalGloe.g:3762:1: rule__PluralValue__Group_4__1 : rule__PluralValue__Group_4__1__Impl rule__PluralValue__Group_4__2 ;
    public final void rule__PluralValue__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3766:1: ( rule__PluralValue__Group_4__1__Impl rule__PluralValue__Group_4__2 )
            // InternalGloe.g:3767:2: rule__PluralValue__Group_4__1__Impl rule__PluralValue__Group_4__2
            {
            pushFollow(FOLLOW_26);
            rule__PluralValue__Group_4__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_4__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_4__1"


    // $ANTLR start "rule__PluralValue__Group_4__1__Impl"
    // InternalGloe.g:3774:1: rule__PluralValue__Group_4__1__Impl : ( 'in' ) ;
    public final void rule__PluralValue__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3778:1: ( ( 'in' ) )
            // InternalGloe.g:3779:1: ( 'in' )
            {
            // InternalGloe.g:3779:1: ( 'in' )
            // InternalGloe.g:3780:2: 'in'
            {
             before(grammarAccess.getPluralValueAccess().getInKeyword_4_1()); 
            match(input,217,FOLLOW_2); 
             after(grammarAccess.getPluralValueAccess().getInKeyword_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_4__1__Impl"


    // $ANTLR start "rule__PluralValue__Group_4__2"
    // InternalGloe.g:3789:1: rule__PluralValue__Group_4__2 : rule__PluralValue__Group_4__2__Impl ;
    public final void rule__PluralValue__Group_4__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3793:1: ( rule__PluralValue__Group_4__2__Impl )
            // InternalGloe.g:3794:2: rule__PluralValue__Group_4__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PluralValue__Group_4__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_4__2"


    // $ANTLR start "rule__PluralValue__Group_4__2__Impl"
    // InternalGloe.g:3800:1: rule__PluralValue__Group_4__2__Impl : ( ruleLaneValue ) ;
    public final void rule__PluralValue__Group_4__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3804:1: ( ( ruleLaneValue ) )
            // InternalGloe.g:3805:1: ( ruleLaneValue )
            {
            // InternalGloe.g:3805:1: ( ruleLaneValue )
            // InternalGloe.g:3806:2: ruleLaneValue
            {
             before(grammarAccess.getPluralValueAccess().getLaneValueParserRuleCall_4_2()); 
            pushFollow(FOLLOW_2);
            ruleLaneValue();

            state._fsp--;

             after(grammarAccess.getPluralValueAccess().getLaneValueParserRuleCall_4_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralValue__Group_4__2__Impl"


    // $ANTLR start "rule__ResultValue__Group__0"
    // InternalGloe.g:3816:1: rule__ResultValue__Group__0 : rule__ResultValue__Group__0__Impl rule__ResultValue__Group__1 ;
    public final void rule__ResultValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3820:1: ( rule__ResultValue__Group__0__Impl rule__ResultValue__Group__1 )
            // InternalGloe.g:3821:2: rule__ResultValue__Group__0__Impl rule__ResultValue__Group__1
            {
            pushFollow(FOLLOW_27);
            rule__ResultValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ResultValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResultValue__Group__0"


    // $ANTLR start "rule__ResultValue__Group__0__Impl"
    // InternalGloe.g:3828:1: rule__ResultValue__Group__0__Impl : ( () ) ;
    public final void rule__ResultValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3832:1: ( ( () ) )
            // InternalGloe.g:3833:1: ( () )
            {
            // InternalGloe.g:3833:1: ( () )
            // InternalGloe.g:3834:2: ()
            {
             before(grammarAccess.getResultValueAccess().getResultValueAction_0()); 
            // InternalGloe.g:3835:2: ()
            // InternalGloe.g:3835:3: 
            {
            }

             after(grammarAccess.getResultValueAccess().getResultValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResultValue__Group__0__Impl"


    // $ANTLR start "rule__ResultValue__Group__1"
    // InternalGloe.g:3843:1: rule__ResultValue__Group__1 : rule__ResultValue__Group__1__Impl ;
    public final void rule__ResultValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3847:1: ( rule__ResultValue__Group__1__Impl )
            // InternalGloe.g:3848:2: rule__ResultValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ResultValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResultValue__Group__1"


    // $ANTLR start "rule__ResultValue__Group__1__Impl"
    // InternalGloe.g:3854:1: rule__ResultValue__Group__1__Impl : ( ( rule__ResultValue__ResultAssignment_1 ) ) ;
    public final void rule__ResultValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3858:1: ( ( ( rule__ResultValue__ResultAssignment_1 ) ) )
            // InternalGloe.g:3859:1: ( ( rule__ResultValue__ResultAssignment_1 ) )
            {
            // InternalGloe.g:3859:1: ( ( rule__ResultValue__ResultAssignment_1 ) )
            // InternalGloe.g:3860:2: ( rule__ResultValue__ResultAssignment_1 )
            {
             before(grammarAccess.getResultValueAccess().getResultAssignment_1()); 
            // InternalGloe.g:3861:2: ( rule__ResultValue__ResultAssignment_1 )
            // InternalGloe.g:3861:3: rule__ResultValue__ResultAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ResultValue__ResultAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getResultValueAccess().getResultAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResultValue__Group__1__Impl"


    // $ANTLR start "rule__PluralGameResultValue__Group__0"
    // InternalGloe.g:3870:1: rule__PluralGameResultValue__Group__0 : rule__PluralGameResultValue__Group__0__Impl rule__PluralGameResultValue__Group__1 ;
    public final void rule__PluralGameResultValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3874:1: ( rule__PluralGameResultValue__Group__0__Impl rule__PluralGameResultValue__Group__1 )
            // InternalGloe.g:3875:2: rule__PluralGameResultValue__Group__0__Impl rule__PluralGameResultValue__Group__1
            {
            pushFollow(FOLLOW_28);
            rule__PluralGameResultValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__PluralGameResultValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralGameResultValue__Group__0"


    // $ANTLR start "rule__PluralGameResultValue__Group__0__Impl"
    // InternalGloe.g:3882:1: rule__PluralGameResultValue__Group__0__Impl : ( () ) ;
    public final void rule__PluralGameResultValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3886:1: ( ( () ) )
            // InternalGloe.g:3887:1: ( () )
            {
            // InternalGloe.g:3887:1: ( () )
            // InternalGloe.g:3888:2: ()
            {
             before(grammarAccess.getPluralGameResultValueAccess().getResultValueAction_0()); 
            // InternalGloe.g:3889:2: ()
            // InternalGloe.g:3889:3: 
            {
            }

             after(grammarAccess.getPluralGameResultValueAccess().getResultValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralGameResultValue__Group__0__Impl"


    // $ANTLR start "rule__PluralGameResultValue__Group__1"
    // InternalGloe.g:3897:1: rule__PluralGameResultValue__Group__1 : rule__PluralGameResultValue__Group__1__Impl ;
    public final void rule__PluralGameResultValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3901:1: ( rule__PluralGameResultValue__Group__1__Impl )
            // InternalGloe.g:3902:2: rule__PluralGameResultValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__PluralGameResultValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralGameResultValue__Group__1"


    // $ANTLR start "rule__PluralGameResultValue__Group__1__Impl"
    // InternalGloe.g:3908:1: rule__PluralGameResultValue__Group__1__Impl : ( ( rule__PluralGameResultValue__ResultAssignment_1 ) ) ;
    public final void rule__PluralGameResultValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3912:1: ( ( ( rule__PluralGameResultValue__ResultAssignment_1 ) ) )
            // InternalGloe.g:3913:1: ( ( rule__PluralGameResultValue__ResultAssignment_1 ) )
            {
            // InternalGloe.g:3913:1: ( ( rule__PluralGameResultValue__ResultAssignment_1 ) )
            // InternalGloe.g:3914:2: ( rule__PluralGameResultValue__ResultAssignment_1 )
            {
             before(grammarAccess.getPluralGameResultValueAccess().getResultAssignment_1()); 
            // InternalGloe.g:3915:2: ( rule__PluralGameResultValue__ResultAssignment_1 )
            // InternalGloe.g:3915:3: rule__PluralGameResultValue__ResultAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__PluralGameResultValue__ResultAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getPluralGameResultValueAccess().getResultAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralGameResultValue__Group__1__Impl"


    // $ANTLR start "rule__TeamValue__Group__0"
    // InternalGloe.g:3924:1: rule__TeamValue__Group__0 : rule__TeamValue__Group__0__Impl rule__TeamValue__Group__1 ;
    public final void rule__TeamValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3928:1: ( rule__TeamValue__Group__0__Impl rule__TeamValue__Group__1 )
            // InternalGloe.g:3929:2: rule__TeamValue__Group__0__Impl rule__TeamValue__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__TeamValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TeamValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TeamValue__Group__0"


    // $ANTLR start "rule__TeamValue__Group__0__Impl"
    // InternalGloe.g:3936:1: rule__TeamValue__Group__0__Impl : ( () ) ;
    public final void rule__TeamValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3940:1: ( ( () ) )
            // InternalGloe.g:3941:1: ( () )
            {
            // InternalGloe.g:3941:1: ( () )
            // InternalGloe.g:3942:2: ()
            {
             before(grammarAccess.getTeamValueAccess().getTeamValueAction_0()); 
            // InternalGloe.g:3943:2: ()
            // InternalGloe.g:3943:3: 
            {
            }

             after(grammarAccess.getTeamValueAccess().getTeamValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TeamValue__Group__0__Impl"


    // $ANTLR start "rule__TeamValue__Group__1"
    // InternalGloe.g:3951:1: rule__TeamValue__Group__1 : rule__TeamValue__Group__1__Impl ;
    public final void rule__TeamValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3955:1: ( rule__TeamValue__Group__1__Impl )
            // InternalGloe.g:3956:2: rule__TeamValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TeamValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TeamValue__Group__1"


    // $ANTLR start "rule__TeamValue__Group__1__Impl"
    // InternalGloe.g:3962:1: rule__TeamValue__Group__1__Impl : ( ( rule__TeamValue__TeamAssignment_1 ) ) ;
    public final void rule__TeamValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3966:1: ( ( ( rule__TeamValue__TeamAssignment_1 ) ) )
            // InternalGloe.g:3967:1: ( ( rule__TeamValue__TeamAssignment_1 ) )
            {
            // InternalGloe.g:3967:1: ( ( rule__TeamValue__TeamAssignment_1 ) )
            // InternalGloe.g:3968:2: ( rule__TeamValue__TeamAssignment_1 )
            {
             before(grammarAccess.getTeamValueAccess().getTeamAssignment_1()); 
            // InternalGloe.g:3969:2: ( rule__TeamValue__TeamAssignment_1 )
            // InternalGloe.g:3969:3: rule__TeamValue__TeamAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__TeamValue__TeamAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTeamValueAccess().getTeamAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TeamValue__Group__1__Impl"


    // $ANTLR start "rule__LaneValue__Group__0"
    // InternalGloe.g:3978:1: rule__LaneValue__Group__0 : rule__LaneValue__Group__0__Impl rule__LaneValue__Group__1 ;
    public final void rule__LaneValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3982:1: ( rule__LaneValue__Group__0__Impl rule__LaneValue__Group__1 )
            // InternalGloe.g:3983:2: rule__LaneValue__Group__0__Impl rule__LaneValue__Group__1
            {
            pushFollow(FOLLOW_26);
            rule__LaneValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LaneValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LaneValue__Group__0"


    // $ANTLR start "rule__LaneValue__Group__0__Impl"
    // InternalGloe.g:3990:1: rule__LaneValue__Group__0__Impl : ( () ) ;
    public final void rule__LaneValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:3994:1: ( ( () ) )
            // InternalGloe.g:3995:1: ( () )
            {
            // InternalGloe.g:3995:1: ( () )
            // InternalGloe.g:3996:2: ()
            {
             before(grammarAccess.getLaneValueAccess().getLaneValueAction_0()); 
            // InternalGloe.g:3997:2: ()
            // InternalGloe.g:3997:3: 
            {
            }

             after(grammarAccess.getLaneValueAccess().getLaneValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LaneValue__Group__0__Impl"


    // $ANTLR start "rule__LaneValue__Group__1"
    // InternalGloe.g:4005:1: rule__LaneValue__Group__1 : rule__LaneValue__Group__1__Impl ;
    public final void rule__LaneValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4009:1: ( rule__LaneValue__Group__1__Impl )
            // InternalGloe.g:4010:2: rule__LaneValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LaneValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LaneValue__Group__1"


    // $ANTLR start "rule__LaneValue__Group__1__Impl"
    // InternalGloe.g:4016:1: rule__LaneValue__Group__1__Impl : ( ( rule__LaneValue__LaneAssignment_1 ) ) ;
    public final void rule__LaneValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4020:1: ( ( ( rule__LaneValue__LaneAssignment_1 ) ) )
            // InternalGloe.g:4021:1: ( ( rule__LaneValue__LaneAssignment_1 ) )
            {
            // InternalGloe.g:4021:1: ( ( rule__LaneValue__LaneAssignment_1 ) )
            // InternalGloe.g:4022:2: ( rule__LaneValue__LaneAssignment_1 )
            {
             before(grammarAccess.getLaneValueAccess().getLaneAssignment_1()); 
            // InternalGloe.g:4023:2: ( rule__LaneValue__LaneAssignment_1 )
            // InternalGloe.g:4023:3: rule__LaneValue__LaneAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__LaneValue__LaneAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLaneValueAccess().getLaneAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LaneValue__Group__1__Impl"


    // $ANTLR start "rule__StatValue__Group__0"
    // InternalGloe.g:4032:1: rule__StatValue__Group__0 : rule__StatValue__Group__0__Impl rule__StatValue__Group__1 ;
    public final void rule__StatValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4036:1: ( rule__StatValue__Group__0__Impl rule__StatValue__Group__1 )
            // InternalGloe.g:4037:2: rule__StatValue__Group__0__Impl rule__StatValue__Group__1
            {
            pushFollow(FOLLOW_29);
            rule__StatValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__StatValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StatValue__Group__0"


    // $ANTLR start "rule__StatValue__Group__0__Impl"
    // InternalGloe.g:4044:1: rule__StatValue__Group__0__Impl : ( () ) ;
    public final void rule__StatValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4048:1: ( ( () ) )
            // InternalGloe.g:4049:1: ( () )
            {
            // InternalGloe.g:4049:1: ( () )
            // InternalGloe.g:4050:2: ()
            {
             before(grammarAccess.getStatValueAccess().getStatValueAction_0()); 
            // InternalGloe.g:4051:2: ()
            // InternalGloe.g:4051:3: 
            {
            }

             after(grammarAccess.getStatValueAccess().getStatValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StatValue__Group__0__Impl"


    // $ANTLR start "rule__StatValue__Group__1"
    // InternalGloe.g:4059:1: rule__StatValue__Group__1 : rule__StatValue__Group__1__Impl ;
    public final void rule__StatValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4063:1: ( rule__StatValue__Group__1__Impl )
            // InternalGloe.g:4064:2: rule__StatValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__StatValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StatValue__Group__1"


    // $ANTLR start "rule__StatValue__Group__1__Impl"
    // InternalGloe.g:4070:1: rule__StatValue__Group__1__Impl : ( ( rule__StatValue__StatAssignment_1 ) ) ;
    public final void rule__StatValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4074:1: ( ( ( rule__StatValue__StatAssignment_1 ) ) )
            // InternalGloe.g:4075:1: ( ( rule__StatValue__StatAssignment_1 ) )
            {
            // InternalGloe.g:4075:1: ( ( rule__StatValue__StatAssignment_1 ) )
            // InternalGloe.g:4076:2: ( rule__StatValue__StatAssignment_1 )
            {
             before(grammarAccess.getStatValueAccess().getStatAssignment_1()); 
            // InternalGloe.g:4077:2: ( rule__StatValue__StatAssignment_1 )
            // InternalGloe.g:4077:3: rule__StatValue__StatAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__StatValue__StatAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getStatValueAccess().getStatAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StatValue__Group__1__Impl"


    // $ANTLR start "rule__ChampionValue__Group__0"
    // InternalGloe.g:4086:1: rule__ChampionValue__Group__0 : rule__ChampionValue__Group__0__Impl rule__ChampionValue__Group__1 ;
    public final void rule__ChampionValue__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4090:1: ( rule__ChampionValue__Group__0__Impl rule__ChampionValue__Group__1 )
            // InternalGloe.g:4091:2: rule__ChampionValue__Group__0__Impl rule__ChampionValue__Group__1
            {
            pushFollow(FOLLOW_22);
            rule__ChampionValue__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ChampionValue__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChampionValue__Group__0"


    // $ANTLR start "rule__ChampionValue__Group__0__Impl"
    // InternalGloe.g:4098:1: rule__ChampionValue__Group__0__Impl : ( () ) ;
    public final void rule__ChampionValue__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4102:1: ( ( () ) )
            // InternalGloe.g:4103:1: ( () )
            {
            // InternalGloe.g:4103:1: ( () )
            // InternalGloe.g:4104:2: ()
            {
             before(grammarAccess.getChampionValueAccess().getChampionValueAction_0()); 
            // InternalGloe.g:4105:2: ()
            // InternalGloe.g:4105:3: 
            {
            }

             after(grammarAccess.getChampionValueAccess().getChampionValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChampionValue__Group__0__Impl"


    // $ANTLR start "rule__ChampionValue__Group__1"
    // InternalGloe.g:4113:1: rule__ChampionValue__Group__1 : rule__ChampionValue__Group__1__Impl ;
    public final void rule__ChampionValue__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4117:1: ( rule__ChampionValue__Group__1__Impl )
            // InternalGloe.g:4118:2: rule__ChampionValue__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ChampionValue__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChampionValue__Group__1"


    // $ANTLR start "rule__ChampionValue__Group__1__Impl"
    // InternalGloe.g:4124:1: rule__ChampionValue__Group__1__Impl : ( ( rule__ChampionValue__ChampionAssignment_1 ) ) ;
    public final void rule__ChampionValue__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4128:1: ( ( ( rule__ChampionValue__ChampionAssignment_1 ) ) )
            // InternalGloe.g:4129:1: ( ( rule__ChampionValue__ChampionAssignment_1 ) )
            {
            // InternalGloe.g:4129:1: ( ( rule__ChampionValue__ChampionAssignment_1 ) )
            // InternalGloe.g:4130:2: ( rule__ChampionValue__ChampionAssignment_1 )
            {
             before(grammarAccess.getChampionValueAccess().getChampionAssignment_1()); 
            // InternalGloe.g:4131:2: ( rule__ChampionValue__ChampionAssignment_1 )
            // InternalGloe.g:4131:3: rule__ChampionValue__ChampionAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ChampionValue__ChampionAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getChampionValueAccess().getChampionAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChampionValue__Group__1__Impl"


    // $ANTLR start "rule__Timelapse__Group__0"
    // InternalGloe.g:4140:1: rule__Timelapse__Group__0 : rule__Timelapse__Group__0__Impl rule__Timelapse__Group__1 ;
    public final void rule__Timelapse__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4144:1: ( rule__Timelapse__Group__0__Impl rule__Timelapse__Group__1 )
            // InternalGloe.g:4145:2: rule__Timelapse__Group__0__Impl rule__Timelapse__Group__1
            {
            pushFollow(FOLLOW_30);
            rule__Timelapse__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timelapse__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__0"


    // $ANTLR start "rule__Timelapse__Group__0__Impl"
    // InternalGloe.g:4152:1: rule__Timelapse__Group__0__Impl : ( () ) ;
    public final void rule__Timelapse__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4156:1: ( ( () ) )
            // InternalGloe.g:4157:1: ( () )
            {
            // InternalGloe.g:4157:1: ( () )
            // InternalGloe.g:4158:2: ()
            {
             before(grammarAccess.getTimelapseAccess().getTimelapseAction_0()); 
            // InternalGloe.g:4159:2: ()
            // InternalGloe.g:4159:3: 
            {
            }

             after(grammarAccess.getTimelapseAccess().getTimelapseAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__0__Impl"


    // $ANTLR start "rule__Timelapse__Group__1"
    // InternalGloe.g:4167:1: rule__Timelapse__Group__1 : rule__Timelapse__Group__1__Impl rule__Timelapse__Group__2 ;
    public final void rule__Timelapse__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4171:1: ( rule__Timelapse__Group__1__Impl rule__Timelapse__Group__2 )
            // InternalGloe.g:4172:2: rule__Timelapse__Group__1__Impl rule__Timelapse__Group__2
            {
            pushFollow(FOLLOW_3);
            rule__Timelapse__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timelapse__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__1"


    // $ANTLR start "rule__Timelapse__Group__1__Impl"
    // InternalGloe.g:4179:1: rule__Timelapse__Group__1__Impl : ( 'from' ) ;
    public final void rule__Timelapse__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4183:1: ( ( 'from' ) )
            // InternalGloe.g:4184:1: ( 'from' )
            {
            // InternalGloe.g:4184:1: ( 'from' )
            // InternalGloe.g:4185:2: 'from'
            {
             before(grammarAccess.getTimelapseAccess().getFromKeyword_1()); 
            match(input,229,FOLLOW_2); 
             after(grammarAccess.getTimelapseAccess().getFromKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__1__Impl"


    // $ANTLR start "rule__Timelapse__Group__2"
    // InternalGloe.g:4194:1: rule__Timelapse__Group__2 : rule__Timelapse__Group__2__Impl rule__Timelapse__Group__3 ;
    public final void rule__Timelapse__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4198:1: ( rule__Timelapse__Group__2__Impl rule__Timelapse__Group__3 )
            // InternalGloe.g:4199:2: rule__Timelapse__Group__2__Impl rule__Timelapse__Group__3
            {
            pushFollow(FOLLOW_31);
            rule__Timelapse__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timelapse__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__2"


    // $ANTLR start "rule__Timelapse__Group__2__Impl"
    // InternalGloe.g:4206:1: rule__Timelapse__Group__2__Impl : ( ( rule__Timelapse__FromAssignment_2 ) ) ;
    public final void rule__Timelapse__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4210:1: ( ( ( rule__Timelapse__FromAssignment_2 ) ) )
            // InternalGloe.g:4211:1: ( ( rule__Timelapse__FromAssignment_2 ) )
            {
            // InternalGloe.g:4211:1: ( ( rule__Timelapse__FromAssignment_2 ) )
            // InternalGloe.g:4212:2: ( rule__Timelapse__FromAssignment_2 )
            {
             before(grammarAccess.getTimelapseAccess().getFromAssignment_2()); 
            // InternalGloe.g:4213:2: ( rule__Timelapse__FromAssignment_2 )
            // InternalGloe.g:4213:3: rule__Timelapse__FromAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Timelapse__FromAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTimelapseAccess().getFromAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__2__Impl"


    // $ANTLR start "rule__Timelapse__Group__3"
    // InternalGloe.g:4221:1: rule__Timelapse__Group__3 : rule__Timelapse__Group__3__Impl ;
    public final void rule__Timelapse__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4225:1: ( rule__Timelapse__Group__3__Impl )
            // InternalGloe.g:4226:2: rule__Timelapse__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Timelapse__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__3"


    // $ANTLR start "rule__Timelapse__Group__3__Impl"
    // InternalGloe.g:4232:1: rule__Timelapse__Group__3__Impl : ( ( rule__Timelapse__Group_3__0 )? ) ;
    public final void rule__Timelapse__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4236:1: ( ( ( rule__Timelapse__Group_3__0 )? ) )
            // InternalGloe.g:4237:1: ( ( rule__Timelapse__Group_3__0 )? )
            {
            // InternalGloe.g:4237:1: ( ( rule__Timelapse__Group_3__0 )? )
            // InternalGloe.g:4238:2: ( rule__Timelapse__Group_3__0 )?
            {
             before(grammarAccess.getTimelapseAccess().getGroup_3()); 
            // InternalGloe.g:4239:2: ( rule__Timelapse__Group_3__0 )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==230) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalGloe.g:4239:3: rule__Timelapse__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Timelapse__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getTimelapseAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group__3__Impl"


    // $ANTLR start "rule__Timelapse__Group_3__0"
    // InternalGloe.g:4248:1: rule__Timelapse__Group_3__0 : rule__Timelapse__Group_3__0__Impl rule__Timelapse__Group_3__1 ;
    public final void rule__Timelapse__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4252:1: ( rule__Timelapse__Group_3__0__Impl rule__Timelapse__Group_3__1 )
            // InternalGloe.g:4253:2: rule__Timelapse__Group_3__0__Impl rule__Timelapse__Group_3__1
            {
            pushFollow(FOLLOW_3);
            rule__Timelapse__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Timelapse__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group_3__0"


    // $ANTLR start "rule__Timelapse__Group_3__0__Impl"
    // InternalGloe.g:4260:1: rule__Timelapse__Group_3__0__Impl : ( 'to' ) ;
    public final void rule__Timelapse__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4264:1: ( ( 'to' ) )
            // InternalGloe.g:4265:1: ( 'to' )
            {
            // InternalGloe.g:4265:1: ( 'to' )
            // InternalGloe.g:4266:2: 'to'
            {
             before(grammarAccess.getTimelapseAccess().getToKeyword_3_0()); 
            match(input,230,FOLLOW_2); 
             after(grammarAccess.getTimelapseAccess().getToKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group_3__0__Impl"


    // $ANTLR start "rule__Timelapse__Group_3__1"
    // InternalGloe.g:4275:1: rule__Timelapse__Group_3__1 : rule__Timelapse__Group_3__1__Impl ;
    public final void rule__Timelapse__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4279:1: ( rule__Timelapse__Group_3__1__Impl )
            // InternalGloe.g:4280:2: rule__Timelapse__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Timelapse__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group_3__1"


    // $ANTLR start "rule__Timelapse__Group_3__1__Impl"
    // InternalGloe.g:4286:1: rule__Timelapse__Group_3__1__Impl : ( ( rule__Timelapse__ToAssignment_3_1 ) ) ;
    public final void rule__Timelapse__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4290:1: ( ( ( rule__Timelapse__ToAssignment_3_1 ) ) )
            // InternalGloe.g:4291:1: ( ( rule__Timelapse__ToAssignment_3_1 ) )
            {
            // InternalGloe.g:4291:1: ( ( rule__Timelapse__ToAssignment_3_1 ) )
            // InternalGloe.g:4292:2: ( rule__Timelapse__ToAssignment_3_1 )
            {
             before(grammarAccess.getTimelapseAccess().getToAssignment_3_1()); 
            // InternalGloe.g:4293:2: ( rule__Timelapse__ToAssignment_3_1 )
            // InternalGloe.g:4293:3: rule__Timelapse__ToAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Timelapse__ToAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getTimelapseAccess().getToAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__Group_3__1__Impl"


    // $ANTLR start "rule__Winrate__Group__0"
    // InternalGloe.g:4302:1: rule__Winrate__Group__0 : rule__Winrate__Group__0__Impl rule__Winrate__Group__1 ;
    public final void rule__Winrate__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4306:1: ( rule__Winrate__Group__0__Impl rule__Winrate__Group__1 )
            // InternalGloe.g:4307:2: rule__Winrate__Group__0__Impl rule__Winrate__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__Winrate__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Winrate__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Winrate__Group__0"


    // $ANTLR start "rule__Winrate__Group__0__Impl"
    // InternalGloe.g:4314:1: rule__Winrate__Group__0__Impl : ( () ) ;
    public final void rule__Winrate__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4318:1: ( ( () ) )
            // InternalGloe.g:4319:1: ( () )
            {
            // InternalGloe.g:4319:1: ( () )
            // InternalGloe.g:4320:2: ()
            {
             before(grammarAccess.getWinrateAccess().getRatioAction_0()); 
            // InternalGloe.g:4321:2: ()
            // InternalGloe.g:4321:3: 
            {
            }

             after(grammarAccess.getWinrateAccess().getRatioAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Winrate__Group__0__Impl"


    // $ANTLR start "rule__Winrate__Group__1"
    // InternalGloe.g:4329:1: rule__Winrate__Group__1 : rule__Winrate__Group__1__Impl ;
    public final void rule__Winrate__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4333:1: ( rule__Winrate__Group__1__Impl )
            // InternalGloe.g:4334:2: rule__Winrate__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Winrate__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Winrate__Group__1"


    // $ANTLR start "rule__Winrate__Group__1__Impl"
    // InternalGloe.g:4340:1: rule__Winrate__Group__1__Impl : ( ( rule__Winrate__ValueAssignment_1 ) ) ;
    public final void rule__Winrate__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4344:1: ( ( ( rule__Winrate__ValueAssignment_1 ) ) )
            // InternalGloe.g:4345:1: ( ( rule__Winrate__ValueAssignment_1 ) )
            {
            // InternalGloe.g:4345:1: ( ( rule__Winrate__ValueAssignment_1 ) )
            // InternalGloe.g:4346:2: ( rule__Winrate__ValueAssignment_1 )
            {
             before(grammarAccess.getWinrateAccess().getValueAssignment_1()); 
            // InternalGloe.g:4347:2: ( rule__Winrate__ValueAssignment_1 )
            // InternalGloe.g:4347:3: rule__Winrate__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Winrate__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getWinrateAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Winrate__Group__1__Impl"


    // $ANTLR start "rule__GameResultValueWinrateShortcut__Group__0"
    // InternalGloe.g:4356:1: rule__GameResultValueWinrateShortcut__Group__0 : rule__GameResultValueWinrateShortcut__Group__0__Impl rule__GameResultValueWinrateShortcut__Group__1 ;
    public final void rule__GameResultValueWinrateShortcut__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4360:1: ( rule__GameResultValueWinrateShortcut__Group__0__Impl rule__GameResultValueWinrateShortcut__Group__1 )
            // InternalGloe.g:4361:2: rule__GameResultValueWinrateShortcut__Group__0__Impl rule__GameResultValueWinrateShortcut__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__GameResultValueWinrateShortcut__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__GameResultValueWinrateShortcut__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameResultValueWinrateShortcut__Group__0"


    // $ANTLR start "rule__GameResultValueWinrateShortcut__Group__0__Impl"
    // InternalGloe.g:4368:1: rule__GameResultValueWinrateShortcut__Group__0__Impl : ( () ) ;
    public final void rule__GameResultValueWinrateShortcut__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4372:1: ( ( () ) )
            // InternalGloe.g:4373:1: ( () )
            {
            // InternalGloe.g:4373:1: ( () )
            // InternalGloe.g:4374:2: ()
            {
             before(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultValueAction_0()); 
            // InternalGloe.g:4375:2: ()
            // InternalGloe.g:4375:3: 
            {
            }

             after(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultValueAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameResultValueWinrateShortcut__Group__0__Impl"


    // $ANTLR start "rule__GameResultValueWinrateShortcut__Group__1"
    // InternalGloe.g:4383:1: rule__GameResultValueWinrateShortcut__Group__1 : rule__GameResultValueWinrateShortcut__Group__1__Impl ;
    public final void rule__GameResultValueWinrateShortcut__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4387:1: ( rule__GameResultValueWinrateShortcut__Group__1__Impl )
            // InternalGloe.g:4388:2: rule__GameResultValueWinrateShortcut__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__GameResultValueWinrateShortcut__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameResultValueWinrateShortcut__Group__1"


    // $ANTLR start "rule__GameResultValueWinrateShortcut__Group__1__Impl"
    // InternalGloe.g:4394:1: rule__GameResultValueWinrateShortcut__Group__1__Impl : ( ( rule__GameResultValueWinrateShortcut__ResultAssignment_1 ) ) ;
    public final void rule__GameResultValueWinrateShortcut__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4398:1: ( ( ( rule__GameResultValueWinrateShortcut__ResultAssignment_1 ) ) )
            // InternalGloe.g:4399:1: ( ( rule__GameResultValueWinrateShortcut__ResultAssignment_1 ) )
            {
            // InternalGloe.g:4399:1: ( ( rule__GameResultValueWinrateShortcut__ResultAssignment_1 ) )
            // InternalGloe.g:4400:2: ( rule__GameResultValueWinrateShortcut__ResultAssignment_1 )
            {
             before(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultAssignment_1()); 
            // InternalGloe.g:4401:2: ( rule__GameResultValueWinrateShortcut__ResultAssignment_1 )
            // InternalGloe.g:4401:3: rule__GameResultValueWinrateShortcut__ResultAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__GameResultValueWinrateShortcut__ResultAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameResultValueWinrateShortcut__Group__1__Impl"


    // $ANTLR start "rule__FilterByValue__Group_0__0"
    // InternalGloe.g:4410:1: rule__FilterByValue__Group_0__0 : rule__FilterByValue__Group_0__0__Impl rule__FilterByValue__Group_0__1 ;
    public final void rule__FilterByValue__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4414:1: ( rule__FilterByValue__Group_0__0__Impl rule__FilterByValue__Group_0__1 )
            // InternalGloe.g:4415:2: rule__FilterByValue__Group_0__0__Impl rule__FilterByValue__Group_0__1
            {
            pushFollow(FOLLOW_25);
            rule__FilterByValue__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0__0"


    // $ANTLR start "rule__FilterByValue__Group_0__0__Impl"
    // InternalGloe.g:4422:1: rule__FilterByValue__Group_0__0__Impl : ( () ) ;
    public final void rule__FilterByValue__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4426:1: ( ( () ) )
            // InternalGloe.g:4427:1: ( () )
            {
            // InternalGloe.g:4427:1: ( () )
            // InternalGloe.g:4428:2: ()
            {
             before(grammarAccess.getFilterByValueAccess().getFilterByValueAction_0_0()); 
            // InternalGloe.g:4429:2: ()
            // InternalGloe.g:4429:3: 
            {
            }

             after(grammarAccess.getFilterByValueAccess().getFilterByValueAction_0_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0__0__Impl"


    // $ANTLR start "rule__FilterByValue__Group_0__1"
    // InternalGloe.g:4437:1: rule__FilterByValue__Group_0__1 : rule__FilterByValue__Group_0__1__Impl ;
    public final void rule__FilterByValue__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4441:1: ( rule__FilterByValue__Group_0__1__Impl )
            // InternalGloe.g:4442:2: rule__FilterByValue__Group_0__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0__1"


    // $ANTLR start "rule__FilterByValue__Group_0__1__Impl"
    // InternalGloe.g:4448:1: rule__FilterByValue__Group_0__1__Impl : ( ( rule__FilterByValue__Group_0_1__0 ) ) ;
    public final void rule__FilterByValue__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4452:1: ( ( ( rule__FilterByValue__Group_0_1__0 ) ) )
            // InternalGloe.g:4453:1: ( ( rule__FilterByValue__Group_0_1__0 ) )
            {
            // InternalGloe.g:4453:1: ( ( rule__FilterByValue__Group_0_1__0 ) )
            // InternalGloe.g:4454:2: ( rule__FilterByValue__Group_0_1__0 )
            {
             before(grammarAccess.getFilterByValueAccess().getGroup_0_1()); 
            // InternalGloe.g:4455:2: ( rule__FilterByValue__Group_0_1__0 )
            // InternalGloe.g:4455:3: rule__FilterByValue__Group_0_1__0
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_0_1__0();

            state._fsp--;


            }

             after(grammarAccess.getFilterByValueAccess().getGroup_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0__1__Impl"


    // $ANTLR start "rule__FilterByValue__Group_0_1__0"
    // InternalGloe.g:4464:1: rule__FilterByValue__Group_0_1__0 : rule__FilterByValue__Group_0_1__0__Impl rule__FilterByValue__Group_0_1__1 ;
    public final void rule__FilterByValue__Group_0_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4468:1: ( rule__FilterByValue__Group_0_1__0__Impl rule__FilterByValue__Group_0_1__1 )
            // InternalGloe.g:4469:2: rule__FilterByValue__Group_0_1__0__Impl rule__FilterByValue__Group_0_1__1
            {
            pushFollow(FOLLOW_32);
            rule__FilterByValue__Group_0_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_0_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0_1__0"


    // $ANTLR start "rule__FilterByValue__Group_0_1__0__Impl"
    // InternalGloe.g:4476:1: rule__FilterByValue__Group_0_1__0__Impl : ( 'team' ) ;
    public final void rule__FilterByValue__Group_0_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4480:1: ( ( 'team' ) )
            // InternalGloe.g:4481:1: ( 'team' )
            {
            // InternalGloe.g:4481:1: ( 'team' )
            // InternalGloe.g:4482:2: 'team'
            {
             before(grammarAccess.getFilterByValueAccess().getTeamKeyword_0_1_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getTeamKeyword_0_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0_1__0__Impl"


    // $ANTLR start "rule__FilterByValue__Group_0_1__1"
    // InternalGloe.g:4491:1: rule__FilterByValue__Group_0_1__1 : rule__FilterByValue__Group_0_1__1__Impl rule__FilterByValue__Group_0_1__2 ;
    public final void rule__FilterByValue__Group_0_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4495:1: ( rule__FilterByValue__Group_0_1__1__Impl rule__FilterByValue__Group_0_1__2 )
            // InternalGloe.g:4496:2: rule__FilterByValue__Group_0_1__1__Impl rule__FilterByValue__Group_0_1__2
            {
            pushFollow(FOLLOW_24);
            rule__FilterByValue__Group_0_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_0_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0_1__1"


    // $ANTLR start "rule__FilterByValue__Group_0_1__1__Impl"
    // InternalGloe.g:4503:1: rule__FilterByValue__Group_0_1__1__Impl : ( 'is' ) ;
    public final void rule__FilterByValue__Group_0_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4507:1: ( ( 'is' ) )
            // InternalGloe.g:4508:1: ( 'is' )
            {
            // InternalGloe.g:4508:1: ( 'is' )
            // InternalGloe.g:4509:2: 'is'
            {
             before(grammarAccess.getFilterByValueAccess().getIsKeyword_0_1_1()); 
            match(input,231,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getIsKeyword_0_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0_1__1__Impl"


    // $ANTLR start "rule__FilterByValue__Group_0_1__2"
    // InternalGloe.g:4518:1: rule__FilterByValue__Group_0_1__2 : rule__FilterByValue__Group_0_1__2__Impl ;
    public final void rule__FilterByValue__Group_0_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4522:1: ( rule__FilterByValue__Group_0_1__2__Impl )
            // InternalGloe.g:4523:2: rule__FilterByValue__Group_0_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_0_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0_1__2"


    // $ANTLR start "rule__FilterByValue__Group_0_1__2__Impl"
    // InternalGloe.g:4529:1: rule__FilterByValue__Group_0_1__2__Impl : ( ( rule__FilterByValue__ValueAssignment_0_1_2 ) ) ;
    public final void rule__FilterByValue__Group_0_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4533:1: ( ( ( rule__FilterByValue__ValueAssignment_0_1_2 ) ) )
            // InternalGloe.g:4534:1: ( ( rule__FilterByValue__ValueAssignment_0_1_2 ) )
            {
            // InternalGloe.g:4534:1: ( ( rule__FilterByValue__ValueAssignment_0_1_2 ) )
            // InternalGloe.g:4535:2: ( rule__FilterByValue__ValueAssignment_0_1_2 )
            {
             before(grammarAccess.getFilterByValueAccess().getValueAssignment_0_1_2()); 
            // InternalGloe.g:4536:2: ( rule__FilterByValue__ValueAssignment_0_1_2 )
            // InternalGloe.g:4536:3: rule__FilterByValue__ValueAssignment_0_1_2
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__ValueAssignment_0_1_2();

            state._fsp--;


            }

             after(grammarAccess.getFilterByValueAccess().getValueAssignment_0_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_0_1__2__Impl"


    // $ANTLR start "rule__FilterByValue__Group_1__0"
    // InternalGloe.g:4545:1: rule__FilterByValue__Group_1__0 : rule__FilterByValue__Group_1__0__Impl rule__FilterByValue__Group_1__1 ;
    public final void rule__FilterByValue__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4549:1: ( rule__FilterByValue__Group_1__0__Impl rule__FilterByValue__Group_1__1 )
            // InternalGloe.g:4550:2: rule__FilterByValue__Group_1__0__Impl rule__FilterByValue__Group_1__1
            {
            pushFollow(FOLLOW_22);
            rule__FilterByValue__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_1__0"


    // $ANTLR start "rule__FilterByValue__Group_1__0__Impl"
    // InternalGloe.g:4557:1: rule__FilterByValue__Group_1__0__Impl : ( 'playing' ) ;
    public final void rule__FilterByValue__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4561:1: ( ( 'playing' ) )
            // InternalGloe.g:4562:1: ( 'playing' )
            {
            // InternalGloe.g:4562:1: ( 'playing' )
            // InternalGloe.g:4563:2: 'playing'
            {
             before(grammarAccess.getFilterByValueAccess().getPlayingKeyword_1_0()); 
            match(input,232,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getPlayingKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_1__0__Impl"


    // $ANTLR start "rule__FilterByValue__Group_1__1"
    // InternalGloe.g:4572:1: rule__FilterByValue__Group_1__1 : rule__FilterByValue__Group_1__1__Impl ;
    public final void rule__FilterByValue__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4576:1: ( rule__FilterByValue__Group_1__1__Impl )
            // InternalGloe.g:4577:2: rule__FilterByValue__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_1__1"


    // $ANTLR start "rule__FilterByValue__Group_1__1__Impl"
    // InternalGloe.g:4583:1: rule__FilterByValue__Group_1__1__Impl : ( ( rule__FilterByValue__ValueAssignment_1_1 ) ) ;
    public final void rule__FilterByValue__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4587:1: ( ( ( rule__FilterByValue__ValueAssignment_1_1 ) ) )
            // InternalGloe.g:4588:1: ( ( rule__FilterByValue__ValueAssignment_1_1 ) )
            {
            // InternalGloe.g:4588:1: ( ( rule__FilterByValue__ValueAssignment_1_1 ) )
            // InternalGloe.g:4589:2: ( rule__FilterByValue__ValueAssignment_1_1 )
            {
             before(grammarAccess.getFilterByValueAccess().getValueAssignment_1_1()); 
            // InternalGloe.g:4590:2: ( rule__FilterByValue__ValueAssignment_1_1 )
            // InternalGloe.g:4590:3: rule__FilterByValue__ValueAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__ValueAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFilterByValueAccess().getValueAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_1__1__Impl"


    // $ANTLR start "rule__FilterByValue__Group_2__0"
    // InternalGloe.g:4599:1: rule__FilterByValue__Group_2__0 : rule__FilterByValue__Group_2__0__Impl rule__FilterByValue__Group_2__1 ;
    public final void rule__FilterByValue__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4603:1: ( rule__FilterByValue__Group_2__0__Impl rule__FilterByValue__Group_2__1 )
            // InternalGloe.g:4604:2: rule__FilterByValue__Group_2__0__Impl rule__FilterByValue__Group_2__1
            {
            pushFollow(FOLLOW_32);
            rule__FilterByValue__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_2__0"


    // $ANTLR start "rule__FilterByValue__Group_2__0__Impl"
    // InternalGloe.g:4611:1: rule__FilterByValue__Group_2__0__Impl : ( 'lane' ) ;
    public final void rule__FilterByValue__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4615:1: ( ( 'lane' ) )
            // InternalGloe.g:4616:1: ( 'lane' )
            {
            // InternalGloe.g:4616:1: ( 'lane' )
            // InternalGloe.g:4617:2: 'lane'
            {
             before(grammarAccess.getFilterByValueAccess().getLaneKeyword_2_0()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getLaneKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_2__0__Impl"


    // $ANTLR start "rule__FilterByValue__Group_2__1"
    // InternalGloe.g:4626:1: rule__FilterByValue__Group_2__1 : rule__FilterByValue__Group_2__1__Impl rule__FilterByValue__Group_2__2 ;
    public final void rule__FilterByValue__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4630:1: ( rule__FilterByValue__Group_2__1__Impl rule__FilterByValue__Group_2__2 )
            // InternalGloe.g:4631:2: rule__FilterByValue__Group_2__1__Impl rule__FilterByValue__Group_2__2
            {
            pushFollow(FOLLOW_26);
            rule__FilterByValue__Group_2__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_2__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_2__1"


    // $ANTLR start "rule__FilterByValue__Group_2__1__Impl"
    // InternalGloe.g:4638:1: rule__FilterByValue__Group_2__1__Impl : ( 'is' ) ;
    public final void rule__FilterByValue__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4642:1: ( ( 'is' ) )
            // InternalGloe.g:4643:1: ( 'is' )
            {
            // InternalGloe.g:4643:1: ( 'is' )
            // InternalGloe.g:4644:2: 'is'
            {
             before(grammarAccess.getFilterByValueAccess().getIsKeyword_2_1()); 
            match(input,231,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getIsKeyword_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_2__1__Impl"


    // $ANTLR start "rule__FilterByValue__Group_2__2"
    // InternalGloe.g:4653:1: rule__FilterByValue__Group_2__2 : rule__FilterByValue__Group_2__2__Impl ;
    public final void rule__FilterByValue__Group_2__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4657:1: ( rule__FilterByValue__Group_2__2__Impl )
            // InternalGloe.g:4658:2: rule__FilterByValue__Group_2__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_2__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_2__2"


    // $ANTLR start "rule__FilterByValue__Group_2__2__Impl"
    // InternalGloe.g:4664:1: rule__FilterByValue__Group_2__2__Impl : ( ( rule__FilterByValue__ValueAssignment_2_2 ) ) ;
    public final void rule__FilterByValue__Group_2__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4668:1: ( ( ( rule__FilterByValue__ValueAssignment_2_2 ) ) )
            // InternalGloe.g:4669:1: ( ( rule__FilterByValue__ValueAssignment_2_2 ) )
            {
            // InternalGloe.g:4669:1: ( ( rule__FilterByValue__ValueAssignment_2_2 ) )
            // InternalGloe.g:4670:2: ( rule__FilterByValue__ValueAssignment_2_2 )
            {
             before(grammarAccess.getFilterByValueAccess().getValueAssignment_2_2()); 
            // InternalGloe.g:4671:2: ( rule__FilterByValue__ValueAssignment_2_2 )
            // InternalGloe.g:4671:3: rule__FilterByValue__ValueAssignment_2_2
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__ValueAssignment_2_2();

            state._fsp--;


            }

             after(grammarAccess.getFilterByValueAccess().getValueAssignment_2_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_2__2__Impl"


    // $ANTLR start "rule__FilterByValue__Group_3__0"
    // InternalGloe.g:4680:1: rule__FilterByValue__Group_3__0 : rule__FilterByValue__Group_3__0__Impl rule__FilterByValue__Group_3__1 ;
    public final void rule__FilterByValue__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4684:1: ( rule__FilterByValue__Group_3__0__Impl rule__FilterByValue__Group_3__1 )
            // InternalGloe.g:4685:2: rule__FilterByValue__Group_3__0__Impl rule__FilterByValue__Group_3__1
            {
            pushFollow(FOLLOW_21);
            rule__FilterByValue__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_3__0"


    // $ANTLR start "rule__FilterByValue__Group_3__0__Impl"
    // InternalGloe.g:4692:1: rule__FilterByValue__Group_3__0__Impl : ( 'ends' ) ;
    public final void rule__FilterByValue__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4696:1: ( ( 'ends' ) )
            // InternalGloe.g:4697:1: ( 'ends' )
            {
            // InternalGloe.g:4697:1: ( 'ends' )
            // InternalGloe.g:4698:2: 'ends'
            {
             before(grammarAccess.getFilterByValueAccess().getEndsKeyword_3_0()); 
            match(input,233,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getEndsKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_3__0__Impl"


    // $ANTLR start "rule__FilterByValue__Group_3__1"
    // InternalGloe.g:4707:1: rule__FilterByValue__Group_3__1 : rule__FilterByValue__Group_3__1__Impl rule__FilterByValue__Group_3__2 ;
    public final void rule__FilterByValue__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4711:1: ( rule__FilterByValue__Group_3__1__Impl rule__FilterByValue__Group_3__2 )
            // InternalGloe.g:4712:2: rule__FilterByValue__Group_3__1__Impl rule__FilterByValue__Group_3__2
            {
            pushFollow(FOLLOW_27);
            rule__FilterByValue__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_3__1"


    // $ANTLR start "rule__FilterByValue__Group_3__1__Impl"
    // InternalGloe.g:4719:1: rule__FilterByValue__Group_3__1__Impl : ( 'with' ) ;
    public final void rule__FilterByValue__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4723:1: ( ( 'with' ) )
            // InternalGloe.g:4724:1: ( 'with' )
            {
            // InternalGloe.g:4724:1: ( 'with' )
            // InternalGloe.g:4725:2: 'with'
            {
             before(grammarAccess.getFilterByValueAccess().getWithKeyword_3_1()); 
            match(input,228,FOLLOW_2); 
             after(grammarAccess.getFilterByValueAccess().getWithKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_3__1__Impl"


    // $ANTLR start "rule__FilterByValue__Group_3__2"
    // InternalGloe.g:4734:1: rule__FilterByValue__Group_3__2 : rule__FilterByValue__Group_3__2__Impl ;
    public final void rule__FilterByValue__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4738:1: ( rule__FilterByValue__Group_3__2__Impl )
            // InternalGloe.g:4739:2: rule__FilterByValue__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_3__2"


    // $ANTLR start "rule__FilterByValue__Group_3__2__Impl"
    // InternalGloe.g:4745:1: rule__FilterByValue__Group_3__2__Impl : ( ( rule__FilterByValue__ValueAssignment_3_2 ) ) ;
    public final void rule__FilterByValue__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4749:1: ( ( ( rule__FilterByValue__ValueAssignment_3_2 ) ) )
            // InternalGloe.g:4750:1: ( ( rule__FilterByValue__ValueAssignment_3_2 ) )
            {
            // InternalGloe.g:4750:1: ( ( rule__FilterByValue__ValueAssignment_3_2 ) )
            // InternalGloe.g:4751:2: ( rule__FilterByValue__ValueAssignment_3_2 )
            {
             before(grammarAccess.getFilterByValueAccess().getValueAssignment_3_2()); 
            // InternalGloe.g:4752:2: ( rule__FilterByValue__ValueAssignment_3_2 )
            // InternalGloe.g:4752:3: rule__FilterByValue__ValueAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__FilterByValue__ValueAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getFilterByValueAccess().getValueAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__Group_3__2__Impl"


    // $ANTLR start "rule__Diagram__Group__0"
    // InternalGloe.g:4761:1: rule__Diagram__Group__0 : rule__Diagram__Group__0__Impl rule__Diagram__Group__1 ;
    public final void rule__Diagram__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4765:1: ( rule__Diagram__Group__0__Impl rule__Diagram__Group__1 )
            // InternalGloe.g:4766:2: rule__Diagram__Group__0__Impl rule__Diagram__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Diagram__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Diagram__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group__0"


    // $ANTLR start "rule__Diagram__Group__0__Impl"
    // InternalGloe.g:4773:1: rule__Diagram__Group__0__Impl : ( () ) ;
    public final void rule__Diagram__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4777:1: ( ( () ) )
            // InternalGloe.g:4778:1: ( () )
            {
            // InternalGloe.g:4778:1: ( () )
            // InternalGloe.g:4779:2: ()
            {
             before(grammarAccess.getDiagramAccess().getDiagramAction_0()); 
            // InternalGloe.g:4780:2: ()
            // InternalGloe.g:4780:3: 
            {
            }

             after(grammarAccess.getDiagramAccess().getDiagramAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group__0__Impl"


    // $ANTLR start "rule__Diagram__Group__1"
    // InternalGloe.g:4788:1: rule__Diagram__Group__1 : rule__Diagram__Group__1__Impl rule__Diagram__Group__2 ;
    public final void rule__Diagram__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4792:1: ( rule__Diagram__Group__1__Impl rule__Diagram__Group__2 )
            // InternalGloe.g:4793:2: rule__Diagram__Group__1__Impl rule__Diagram__Group__2
            {
            pushFollow(FOLLOW_33);
            rule__Diagram__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Diagram__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group__1"


    // $ANTLR start "rule__Diagram__Group__1__Impl"
    // InternalGloe.g:4800:1: rule__Diagram__Group__1__Impl : ( ( rule__Diagram__TypeAssignment_1 ) ) ;
    public final void rule__Diagram__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4804:1: ( ( ( rule__Diagram__TypeAssignment_1 ) ) )
            // InternalGloe.g:4805:1: ( ( rule__Diagram__TypeAssignment_1 ) )
            {
            // InternalGloe.g:4805:1: ( ( rule__Diagram__TypeAssignment_1 ) )
            // InternalGloe.g:4806:2: ( rule__Diagram__TypeAssignment_1 )
            {
             before(grammarAccess.getDiagramAccess().getTypeAssignment_1()); 
            // InternalGloe.g:4807:2: ( rule__Diagram__TypeAssignment_1 )
            // InternalGloe.g:4807:3: rule__Diagram__TypeAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Diagram__TypeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDiagramAccess().getTypeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group__1__Impl"


    // $ANTLR start "rule__Diagram__Group__2"
    // InternalGloe.g:4815:1: rule__Diagram__Group__2 : rule__Diagram__Group__2__Impl ;
    public final void rule__Diagram__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4819:1: ( rule__Diagram__Group__2__Impl )
            // InternalGloe.g:4820:2: rule__Diagram__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Diagram__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group__2"


    // $ANTLR start "rule__Diagram__Group__2__Impl"
    // InternalGloe.g:4826:1: rule__Diagram__Group__2__Impl : ( ( rule__Diagram__Group_2__0 )? ) ;
    public final void rule__Diagram__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4830:1: ( ( ( rule__Diagram__Group_2__0 )? ) )
            // InternalGloe.g:4831:1: ( ( rule__Diagram__Group_2__0 )? )
            {
            // InternalGloe.g:4831:1: ( ( rule__Diagram__Group_2__0 )? )
            // InternalGloe.g:4832:2: ( rule__Diagram__Group_2__0 )?
            {
             before(grammarAccess.getDiagramAccess().getGroup_2()); 
            // InternalGloe.g:4833:2: ( rule__Diagram__Group_2__0 )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( (LA26_0==234) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalGloe.g:4833:3: rule__Diagram__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Diagram__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getDiagramAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group__2__Impl"


    // $ANTLR start "rule__Diagram__Group_2__0"
    // InternalGloe.g:4842:1: rule__Diagram__Group_2__0 : rule__Diagram__Group_2__0__Impl rule__Diagram__Group_2__1 ;
    public final void rule__Diagram__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4846:1: ( rule__Diagram__Group_2__0__Impl rule__Diagram__Group_2__1 )
            // InternalGloe.g:4847:2: rule__Diagram__Group_2__0__Impl rule__Diagram__Group_2__1
            {
            pushFollow(FOLLOW_3);
            rule__Diagram__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Diagram__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group_2__0"


    // $ANTLR start "rule__Diagram__Group_2__0__Impl"
    // InternalGloe.g:4854:1: rule__Diagram__Group_2__0__Impl : ( 'called' ) ;
    public final void rule__Diagram__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4858:1: ( ( 'called' ) )
            // InternalGloe.g:4859:1: ( 'called' )
            {
            // InternalGloe.g:4859:1: ( 'called' )
            // InternalGloe.g:4860:2: 'called'
            {
             before(grammarAccess.getDiagramAccess().getCalledKeyword_2_0()); 
            match(input,234,FOLLOW_2); 
             after(grammarAccess.getDiagramAccess().getCalledKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group_2__0__Impl"


    // $ANTLR start "rule__Diagram__Group_2__1"
    // InternalGloe.g:4869:1: rule__Diagram__Group_2__1 : rule__Diagram__Group_2__1__Impl ;
    public final void rule__Diagram__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4873:1: ( rule__Diagram__Group_2__1__Impl )
            // InternalGloe.g:4874:2: rule__Diagram__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Diagram__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group_2__1"


    // $ANTLR start "rule__Diagram__Group_2__1__Impl"
    // InternalGloe.g:4880:1: rule__Diagram__Group_2__1__Impl : ( ( rule__Diagram__NameAssignment_2_1 ) ) ;
    public final void rule__Diagram__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4884:1: ( ( ( rule__Diagram__NameAssignment_2_1 ) ) )
            // InternalGloe.g:4885:1: ( ( rule__Diagram__NameAssignment_2_1 ) )
            {
            // InternalGloe.g:4885:1: ( ( rule__Diagram__NameAssignment_2_1 ) )
            // InternalGloe.g:4886:2: ( rule__Diagram__NameAssignment_2_1 )
            {
             before(grammarAccess.getDiagramAccess().getNameAssignment_2_1()); 
            // InternalGloe.g:4887:2: ( rule__Diagram__NameAssignment_2_1 )
            // InternalGloe.g:4887:3: rule__Diagram__NameAssignment_2_1
            {
            pushFollow(FOLLOW_2);
            rule__Diagram__NameAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getDiagramAccess().getNameAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__Group_2__1__Impl"


    // $ANTLR start "rule__Request__SummonersAssignment_1"
    // InternalGloe.g:4896:1: rule__Request__SummonersAssignment_1 : ( ruleSummoner ) ;
    public final void rule__Request__SummonersAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4900:1: ( ( ruleSummoner ) )
            // InternalGloe.g:4901:2: ( ruleSummoner )
            {
            // InternalGloe.g:4901:2: ( ruleSummoner )
            // InternalGloe.g:4902:3: ruleSummoner
            {
             before(grammarAccess.getRequestAccess().getSummonersSummonerParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSummoner();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getSummonersSummonerParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__SummonersAssignment_1"


    // $ANTLR start "rule__Request__SummonersAssignment_2_1"
    // InternalGloe.g:4911:1: rule__Request__SummonersAssignment_2_1 : ( ruleSummoner ) ;
    public final void rule__Request__SummonersAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4915:1: ( ( ruleSummoner ) )
            // InternalGloe.g:4916:2: ( ruleSummoner )
            {
            // InternalGloe.g:4916:2: ( ruleSummoner )
            // InternalGloe.g:4917:3: ruleSummoner
            {
             before(grammarAccess.getRequestAccess().getSummonersSummonerParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleSummoner();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getSummonersSummonerParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__SummonersAssignment_2_1"


    // $ANTLR start "rule__Request__YvaluesAssignment_4"
    // InternalGloe.g:4926:1: rule__Request__YvaluesAssignment_4 : ( ruleYValue ) ;
    public final void rule__Request__YvaluesAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4930:1: ( ( ruleYValue ) )
            // InternalGloe.g:4931:2: ( ruleYValue )
            {
            // InternalGloe.g:4931:2: ( ruleYValue )
            // InternalGloe.g:4932:3: ruleYValue
            {
             before(grammarAccess.getRequestAccess().getYvaluesYValueParserRuleCall_4_0()); 
            pushFollow(FOLLOW_2);
            ruleYValue();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getYvaluesYValueParserRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__YvaluesAssignment_4"


    // $ANTLR start "rule__Request__YvaluesAssignment_5_1"
    // InternalGloe.g:4941:1: rule__Request__YvaluesAssignment_5_1 : ( ruleYValue ) ;
    public final void rule__Request__YvaluesAssignment_5_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4945:1: ( ( ruleYValue ) )
            // InternalGloe.g:4946:2: ( ruleYValue )
            {
            // InternalGloe.g:4946:2: ( ruleYValue )
            // InternalGloe.g:4947:3: ruleYValue
            {
             before(grammarAccess.getRequestAccess().getYvaluesYValueParserRuleCall_5_1_0()); 
            pushFollow(FOLLOW_2);
            ruleYValue();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getYvaluesYValueParserRuleCall_5_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__YvaluesAssignment_5_1"


    // $ANTLR start "rule__Request__XUnitAssignment_6_1"
    // InternalGloe.g:4956:1: rule__Request__XUnitAssignment_6_1 : ( ruleValueType ) ;
    public final void rule__Request__XUnitAssignment_6_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4960:1: ( ( ruleValueType ) )
            // InternalGloe.g:4961:2: ( ruleValueType )
            {
            // InternalGloe.g:4961:2: ( ruleValueType )
            // InternalGloe.g:4962:3: ruleValueType
            {
             before(grammarAccess.getRequestAccess().getXUnitValueTypeEnumRuleCall_6_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValueType();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getXUnitValueTypeEnumRuleCall_6_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__XUnitAssignment_6_1"


    // $ANTLR start "rule__Request__TimeScaleAssignment_7_1"
    // InternalGloe.g:4971:1: rule__Request__TimeScaleAssignment_7_1 : ( ruleTimeScale ) ;
    public final void rule__Request__TimeScaleAssignment_7_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4975:1: ( ( ruleTimeScale ) )
            // InternalGloe.g:4976:2: ( ruleTimeScale )
            {
            // InternalGloe.g:4976:2: ( ruleTimeScale )
            // InternalGloe.g:4977:3: ruleTimeScale
            {
             before(grammarAccess.getRequestAccess().getTimeScaleTimeScaleEnumRuleCall_7_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTimeScale();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getTimeScaleTimeScaleEnumRuleCall_7_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__TimeScaleAssignment_7_1"


    // $ANTLR start "rule__Request__FiltersAssignment_8_1"
    // InternalGloe.g:4986:1: rule__Request__FiltersAssignment_8_1 : ( ruleFilter ) ;
    public final void rule__Request__FiltersAssignment_8_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:4990:1: ( ( ruleFilter ) )
            // InternalGloe.g:4991:2: ( ruleFilter )
            {
            // InternalGloe.g:4991:2: ( ruleFilter )
            // InternalGloe.g:4992:3: ruleFilter
            {
             before(grammarAccess.getRequestAccess().getFiltersFilterParserRuleCall_8_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFilter();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getFiltersFilterParserRuleCall_8_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__FiltersAssignment_8_1"


    // $ANTLR start "rule__Request__FiltersAssignment_8_2_1"
    // InternalGloe.g:5001:1: rule__Request__FiltersAssignment_8_2_1 : ( ruleFilter ) ;
    public final void rule__Request__FiltersAssignment_8_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5005:1: ( ( ruleFilter ) )
            // InternalGloe.g:5006:2: ( ruleFilter )
            {
            // InternalGloe.g:5006:2: ( ruleFilter )
            // InternalGloe.g:5007:3: ruleFilter
            {
             before(grammarAccess.getRequestAccess().getFiltersFilterParserRuleCall_8_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFilter();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getFiltersFilterParserRuleCall_8_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__FiltersAssignment_8_2_1"


    // $ANTLR start "rule__Request__TimelapseAssignment_9"
    // InternalGloe.g:5016:1: rule__Request__TimelapseAssignment_9 : ( ruleTimelapse ) ;
    public final void rule__Request__TimelapseAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5020:1: ( ( ruleTimelapse ) )
            // InternalGloe.g:5021:2: ( ruleTimelapse )
            {
            // InternalGloe.g:5021:2: ( ruleTimelapse )
            // InternalGloe.g:5022:3: ruleTimelapse
            {
             before(grammarAccess.getRequestAccess().getTimelapseTimelapseParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleTimelapse();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getTimelapseTimelapseParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__TimelapseAssignment_9"


    // $ANTLR start "rule__Request__DiagramAssignment_12"
    // InternalGloe.g:5031:1: rule__Request__DiagramAssignment_12 : ( ruleDiagram ) ;
    public final void rule__Request__DiagramAssignment_12() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5035:1: ( ( ruleDiagram ) )
            // InternalGloe.g:5036:2: ( ruleDiagram )
            {
            // InternalGloe.g:5036:2: ( ruleDiagram )
            // InternalGloe.g:5037:3: ruleDiagram
            {
             before(grammarAccess.getRequestAccess().getDiagramDiagramParserRuleCall_12_0()); 
            pushFollow(FOLLOW_2);
            ruleDiagram();

            state._fsp--;

             after(grammarAccess.getRequestAccess().getDiagramDiagramParserRuleCall_12_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Request__DiagramAssignment_12"


    // $ANTLR start "rule__Summoner__NameAssignment_1"
    // InternalGloe.g:5046:1: rule__Summoner__NameAssignment_1 : ( ruleEString ) ;
    public final void rule__Summoner__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5050:1: ( ( ruleEString ) )
            // InternalGloe.g:5051:2: ( ruleEString )
            {
            // InternalGloe.g:5051:2: ( ruleEString )
            // InternalGloe.g:5052:3: ruleEString
            {
             before(grammarAccess.getSummonerAccess().getNameEStringParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getSummonerAccess().getNameEStringParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Summoner__NameAssignment_1"


    // $ANTLR start "rule__Sum__ValueAssignment_3"
    // InternalGloe.g:5061:1: rule__Sum__ValueAssignment_3 : ( rulePluralValue ) ;
    public final void rule__Sum__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5065:1: ( ( rulePluralValue ) )
            // InternalGloe.g:5066:2: ( rulePluralValue )
            {
            // InternalGloe.g:5066:2: ( rulePluralValue )
            // InternalGloe.g:5067:3: rulePluralValue
            {
             before(grammarAccess.getSumAccess().getValuePluralValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            rulePluralValue();

            state._fsp--;

             after(grammarAccess.getSumAccess().getValuePluralValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Sum__ValueAssignment_3"


    // $ANTLR start "rule__Average__ValueAssignment_3"
    // InternalGloe.g:5076:1: rule__Average__ValueAssignment_3 : ( rulePluralValue ) ;
    public final void rule__Average__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5080:1: ( ( rulePluralValue ) )
            // InternalGloe.g:5081:2: ( rulePluralValue )
            {
            // InternalGloe.g:5081:2: ( rulePluralValue )
            // InternalGloe.g:5082:3: rulePluralValue
            {
             before(grammarAccess.getAverageAccess().getValuePluralValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            rulePluralValue();

            state._fsp--;

             after(grammarAccess.getAverageAccess().getValuePluralValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Average__ValueAssignment_3"


    // $ANTLR start "rule__Ratio__ValueAssignment_3"
    // InternalGloe.g:5091:1: rule__Ratio__ValueAssignment_3 : ( ruleValue ) ;
    public final void rule__Ratio__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5095:1: ( ( ruleValue ) )
            // InternalGloe.g:5096:2: ( ruleValue )
            {
            // InternalGloe.g:5096:2: ( ruleValue )
            // InternalGloe.g:5097:3: ruleValue
            {
             before(grammarAccess.getRatioAccess().getValueValueParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleValue();

            state._fsp--;

             after(grammarAccess.getRatioAccess().getValueValueParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Ratio__ValueAssignment_3"


    // $ANTLR start "rule__Type__TypeAssignment_1"
    // InternalGloe.g:5106:1: rule__Type__TypeAssignment_1 : ( ruleValueType ) ;
    public final void rule__Type__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5110:1: ( ( ruleValueType ) )
            // InternalGloe.g:5111:2: ( ruleValueType )
            {
            // InternalGloe.g:5111:2: ( ruleValueType )
            // InternalGloe.g:5112:3: ruleValueType
            {
             before(grammarAccess.getTypeAccess().getTypeValueTypeEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleValueType();

            state._fsp--;

             after(grammarAccess.getTypeAccess().getTypeValueTypeEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__TypeAssignment_1"


    // $ANTLR start "rule__ResultValue__ResultAssignment_1"
    // InternalGloe.g:5121:1: rule__ResultValue__ResultAssignment_1 : ( ruleResult ) ;
    public final void rule__ResultValue__ResultAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5125:1: ( ( ruleResult ) )
            // InternalGloe.g:5126:2: ( ruleResult )
            {
            // InternalGloe.g:5126:2: ( ruleResult )
            // InternalGloe.g:5127:3: ruleResult
            {
             before(grammarAccess.getResultValueAccess().getResultResultEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleResult();

            state._fsp--;

             after(grammarAccess.getResultValueAccess().getResultResultEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ResultValue__ResultAssignment_1"


    // $ANTLR start "rule__PluralGameResultValue__ResultAssignment_1"
    // InternalGloe.g:5136:1: rule__PluralGameResultValue__ResultAssignment_1 : ( rulePluralResult ) ;
    public final void rule__PluralGameResultValue__ResultAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5140:1: ( ( rulePluralResult ) )
            // InternalGloe.g:5141:2: ( rulePluralResult )
            {
            // InternalGloe.g:5141:2: ( rulePluralResult )
            // InternalGloe.g:5142:3: rulePluralResult
            {
             before(grammarAccess.getPluralGameResultValueAccess().getResultPluralResultEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            rulePluralResult();

            state._fsp--;

             after(grammarAccess.getPluralGameResultValueAccess().getResultPluralResultEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PluralGameResultValue__ResultAssignment_1"


    // $ANTLR start "rule__TeamValue__TeamAssignment_1"
    // InternalGloe.g:5151:1: rule__TeamValue__TeamAssignment_1 : ( ruleTeam ) ;
    public final void rule__TeamValue__TeamAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5155:1: ( ( ruleTeam ) )
            // InternalGloe.g:5156:2: ( ruleTeam )
            {
            // InternalGloe.g:5156:2: ( ruleTeam )
            // InternalGloe.g:5157:3: ruleTeam
            {
             before(grammarAccess.getTeamValueAccess().getTeamTeamEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleTeam();

            state._fsp--;

             after(grammarAccess.getTeamValueAccess().getTeamTeamEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TeamValue__TeamAssignment_1"


    // $ANTLR start "rule__LaneValue__LaneAssignment_1"
    // InternalGloe.g:5166:1: rule__LaneValue__LaneAssignment_1 : ( ruleLane ) ;
    public final void rule__LaneValue__LaneAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5170:1: ( ( ruleLane ) )
            // InternalGloe.g:5171:2: ( ruleLane )
            {
            // InternalGloe.g:5171:2: ( ruleLane )
            // InternalGloe.g:5172:3: ruleLane
            {
             before(grammarAccess.getLaneValueAccess().getLaneLaneEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLane();

            state._fsp--;

             after(grammarAccess.getLaneValueAccess().getLaneLaneEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LaneValue__LaneAssignment_1"


    // $ANTLR start "rule__StatValue__StatAssignment_1"
    // InternalGloe.g:5181:1: rule__StatValue__StatAssignment_1 : ( ruleStat ) ;
    public final void rule__StatValue__StatAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5185:1: ( ( ruleStat ) )
            // InternalGloe.g:5186:2: ( ruleStat )
            {
            // InternalGloe.g:5186:2: ( ruleStat )
            // InternalGloe.g:5187:3: ruleStat
            {
             before(grammarAccess.getStatValueAccess().getStatStatEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleStat();

            state._fsp--;

             after(grammarAccess.getStatValueAccess().getStatStatEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StatValue__StatAssignment_1"


    // $ANTLR start "rule__ChampionValue__ChampionAssignment_1"
    // InternalGloe.g:5196:1: rule__ChampionValue__ChampionAssignment_1 : ( ruleChampion ) ;
    public final void rule__ChampionValue__ChampionAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5200:1: ( ( ruleChampion ) )
            // InternalGloe.g:5201:2: ( ruleChampion )
            {
            // InternalGloe.g:5201:2: ( ruleChampion )
            // InternalGloe.g:5202:3: ruleChampion
            {
             before(grammarAccess.getChampionValueAccess().getChampionChampionEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleChampion();

            state._fsp--;

             after(grammarAccess.getChampionValueAccess().getChampionChampionEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ChampionValue__ChampionAssignment_1"


    // $ANTLR start "rule__Timelapse__FromAssignment_2"
    // InternalGloe.g:5211:1: rule__Timelapse__FromAssignment_2 : ( ruleEDate ) ;
    public final void rule__Timelapse__FromAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5215:1: ( ( ruleEDate ) )
            // InternalGloe.g:5216:2: ( ruleEDate )
            {
            // InternalGloe.g:5216:2: ( ruleEDate )
            // InternalGloe.g:5217:3: ruleEDate
            {
             before(grammarAccess.getTimelapseAccess().getFromEDateParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleEDate();

            state._fsp--;

             after(grammarAccess.getTimelapseAccess().getFromEDateParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__FromAssignment_2"


    // $ANTLR start "rule__Timelapse__ToAssignment_3_1"
    // InternalGloe.g:5226:1: rule__Timelapse__ToAssignment_3_1 : ( ruleEDate ) ;
    public final void rule__Timelapse__ToAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5230:1: ( ( ruleEDate ) )
            // InternalGloe.g:5231:2: ( ruleEDate )
            {
            // InternalGloe.g:5231:2: ( ruleEDate )
            // InternalGloe.g:5232:3: ruleEDate
            {
             before(grammarAccess.getTimelapseAccess().getToEDateParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEDate();

            state._fsp--;

             after(grammarAccess.getTimelapseAccess().getToEDateParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Timelapse__ToAssignment_3_1"


    // $ANTLR start "rule__Winrate__ValueAssignment_1"
    // InternalGloe.g:5241:1: rule__Winrate__ValueAssignment_1 : ( ruleGameResultValueWinrateShortcut ) ;
    public final void rule__Winrate__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5245:1: ( ( ruleGameResultValueWinrateShortcut ) )
            // InternalGloe.g:5246:2: ( ruleGameResultValueWinrateShortcut )
            {
            // InternalGloe.g:5246:2: ( ruleGameResultValueWinrateShortcut )
            // InternalGloe.g:5247:3: ruleGameResultValueWinrateShortcut
            {
             before(grammarAccess.getWinrateAccess().getValueGameResultValueWinrateShortcutParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGameResultValueWinrateShortcut();

            state._fsp--;

             after(grammarAccess.getWinrateAccess().getValueGameResultValueWinrateShortcutParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Winrate__ValueAssignment_1"


    // $ANTLR start "rule__GameResultValueWinrateShortcut__ResultAssignment_1"
    // InternalGloe.g:5256:1: rule__GameResultValueWinrateShortcut__ResultAssignment_1 : ( ruleGameResultWinrateShortcut ) ;
    public final void rule__GameResultValueWinrateShortcut__ResultAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5260:1: ( ( ruleGameResultWinrateShortcut ) )
            // InternalGloe.g:5261:2: ( ruleGameResultWinrateShortcut )
            {
            // InternalGloe.g:5261:2: ( ruleGameResultWinrateShortcut )
            // InternalGloe.g:5262:3: ruleGameResultWinrateShortcut
            {
             before(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultGameResultWinrateShortcutEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleGameResultWinrateShortcut();

            state._fsp--;

             after(grammarAccess.getGameResultValueWinrateShortcutAccess().getResultGameResultWinrateShortcutEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__GameResultValueWinrateShortcut__ResultAssignment_1"


    // $ANTLR start "rule__FilterByValue__ValueAssignment_0_1_2"
    // InternalGloe.g:5271:1: rule__FilterByValue__ValueAssignment_0_1_2 : ( ruleTeamValue ) ;
    public final void rule__FilterByValue__ValueAssignment_0_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5275:1: ( ( ruleTeamValue ) )
            // InternalGloe.g:5276:2: ( ruleTeamValue )
            {
            // InternalGloe.g:5276:2: ( ruleTeamValue )
            // InternalGloe.g:5277:3: ruleTeamValue
            {
             before(grammarAccess.getFilterByValueAccess().getValueTeamValueParserRuleCall_0_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleTeamValue();

            state._fsp--;

             after(grammarAccess.getFilterByValueAccess().getValueTeamValueParserRuleCall_0_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__ValueAssignment_0_1_2"


    // $ANTLR start "rule__FilterByValue__ValueAssignment_1_1"
    // InternalGloe.g:5286:1: rule__FilterByValue__ValueAssignment_1_1 : ( ruleChampionValue ) ;
    public final void rule__FilterByValue__ValueAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5290:1: ( ( ruleChampionValue ) )
            // InternalGloe.g:5291:2: ( ruleChampionValue )
            {
            // InternalGloe.g:5291:2: ( ruleChampionValue )
            // InternalGloe.g:5292:3: ruleChampionValue
            {
             before(grammarAccess.getFilterByValueAccess().getValueChampionValueParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleChampionValue();

            state._fsp--;

             after(grammarAccess.getFilterByValueAccess().getValueChampionValueParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__ValueAssignment_1_1"


    // $ANTLR start "rule__FilterByValue__ValueAssignment_2_2"
    // InternalGloe.g:5301:1: rule__FilterByValue__ValueAssignment_2_2 : ( ruleLaneValue ) ;
    public final void rule__FilterByValue__ValueAssignment_2_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5305:1: ( ( ruleLaneValue ) )
            // InternalGloe.g:5306:2: ( ruleLaneValue )
            {
            // InternalGloe.g:5306:2: ( ruleLaneValue )
            // InternalGloe.g:5307:3: ruleLaneValue
            {
             before(grammarAccess.getFilterByValueAccess().getValueLaneValueParserRuleCall_2_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLaneValue();

            state._fsp--;

             after(grammarAccess.getFilterByValueAccess().getValueLaneValueParserRuleCall_2_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__ValueAssignment_2_2"


    // $ANTLR start "rule__FilterByValue__ValueAssignment_3_2"
    // InternalGloe.g:5316:1: rule__FilterByValue__ValueAssignment_3_2 : ( ruleResultValue ) ;
    public final void rule__FilterByValue__ValueAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5320:1: ( ( ruleResultValue ) )
            // InternalGloe.g:5321:2: ( ruleResultValue )
            {
            // InternalGloe.g:5321:2: ( ruleResultValue )
            // InternalGloe.g:5322:3: ruleResultValue
            {
             before(grammarAccess.getFilterByValueAccess().getValueResultValueParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleResultValue();

            state._fsp--;

             after(grammarAccess.getFilterByValueAccess().getValueResultValueParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FilterByValue__ValueAssignment_3_2"


    // $ANTLR start "rule__Diagram__TypeAssignment_1"
    // InternalGloe.g:5331:1: rule__Diagram__TypeAssignment_1 : ( ruleDiagramType ) ;
    public final void rule__Diagram__TypeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5335:1: ( ( ruleDiagramType ) )
            // InternalGloe.g:5336:2: ( ruleDiagramType )
            {
            // InternalGloe.g:5336:2: ( ruleDiagramType )
            // InternalGloe.g:5337:3: ruleDiagramType
            {
             before(grammarAccess.getDiagramAccess().getTypeDiagramTypeEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleDiagramType();

            state._fsp--;

             after(grammarAccess.getDiagramAccess().getTypeDiagramTypeEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__TypeAssignment_1"


    // $ANTLR start "rule__Diagram__NameAssignment_2_1"
    // InternalGloe.g:5346:1: rule__Diagram__NameAssignment_2_1 : ( ruleEString ) ;
    public final void rule__Diagram__NameAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGloe.g:5350:1: ( ( ruleEString ) )
            // InternalGloe.g:5351:2: ( ruleEString )
            {
            // InternalGloe.g:5351:2: ( ruleEString )
            // InternalGloe.g:5352:3: ruleEString
            {
             before(grammarAccess.getDiagramAccess().getNameEStringParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_2);
            ruleEString();

            state._fsp--;

             after(grammarAccess.getDiagramAccess().getNameEStringParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Diagram__NameAssignment_2_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000009000000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000008000000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000800L,0x0000000000000000L,0x0000000000000000L,0x0000000680000000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x000000203A000000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000780000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x000000000000F000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000070000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000000009000L,0x0000000000000000L,0x0000000000000000L,0x0000030000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000040000000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000040000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000080000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000100000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0xFFFFFFFFFC0C0000L,0x000000000000000FL,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000200000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000680000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000003F30000L,0x0000000000000000L,0x0000000000000000L,0x0000000800000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000001000000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000000L,0xFFFFFFFFFFFFFFF0L,0xFFFFFFFFFFFFFFFFL,0x000000000000FFFFL});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000002000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000300000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000003C00000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x00000000000C0000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0xFFFFFFFFFC000000L,0x000000000000000FL});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000002000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000004000000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000008000000000L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000040000000000L});

}