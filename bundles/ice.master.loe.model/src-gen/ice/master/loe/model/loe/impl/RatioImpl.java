/**
 */
package ice.master.loe.model.loe.impl;

import ice.master.loe.model.loe.Computation;
import ice.master.loe.model.loe.LoePackage;
import ice.master.loe.model.loe.Ratio;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Ratio</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RatioImpl extends ComputedYValueImpl implements Ratio {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RatioImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LoePackage.Literals.RATIO;
	}

	@Override
	public Computation getComputation() {
		return Computation.PERCENTAGE;
	}

} //RatioImpl
