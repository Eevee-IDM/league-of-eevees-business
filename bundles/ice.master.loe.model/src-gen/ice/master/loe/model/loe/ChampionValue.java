/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Champion Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.ChampionValue#getChampion <em>Champion</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getChampionValue()
 * @model
 * @generated
 */
public interface ChampionValue extends Value {
	/**
	 * Returns the value of the '<em><b>Champion</b></em>' attribute.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.Champion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Champion</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Champion</em>' attribute.
	 * @see ice.master.loe.model.loe.Champion
	 * @see #setChampion(Champion)
	 * @see ice.master.loe.model.loe.LoePackage#getChampionValue_Champion()
	 * @model
	 * @generated
	 */
	Champion getChampion();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.ChampionValue#getChampion <em>Champion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Champion</em>' attribute.
	 * @see ice.master.loe.model.loe.Champion
	 * @see #getChampion()
	 * @generated
	 */
	void setChampion(Champion value);

} // ChampionValue
