/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Team Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.TeamValue#getTeam <em>Team</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getTeamValue()
 * @model
 * @generated
 */
public interface TeamValue extends Value {
	/**
	 * Returns the value of the '<em><b>Team</b></em>' attribute.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.Team}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Team</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Team</em>' attribute.
	 * @see ice.master.loe.model.loe.Team
	 * @see #setTeam(Team)
	 * @see ice.master.loe.model.loe.LoePackage#getTeamValue_Team()
	 * @model
	 * @generated
	 */
	Team getTeam();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.TeamValue#getTeam <em>Team</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Team</em>' attribute.
	 * @see ice.master.loe.model.loe.Team
	 * @see #getTeam()
	 * @generated
	 */
	void setTeam(Team value);

} // TeamValue
