/**
 */
package ice.master.loe.model.loe;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.Computable#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getComputable()
 * @model abstract="true"
 * @generated
 */
public interface Computable extends EObject {

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see ice.master.loe.model.loe.ValueType
	 * @see #setType(ValueType)
	 * @see ice.master.loe.model.loe.LoePackage#getComputable_Type()
	 * @model
	 * @generated
	 */
	ValueType getType();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.Computable#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see ice.master.loe.model.loe.ValueType
	 * @see #getType()
	 * @generated
	 */
	void setType(ValueType value);
} // Computable
