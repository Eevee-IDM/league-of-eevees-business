/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Result Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.ResultValue#getResult <em>Result</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getResultValue()
 * @model
 * @generated
 */
public interface ResultValue extends Value {
	/**
	 * Returns the value of the '<em><b>Result</b></em>' attribute.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.Result}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' attribute.
	 * @see ice.master.loe.model.loe.Result
	 * @see #setResult(Result)
	 * @see ice.master.loe.model.loe.LoePackage#getResultValue_Result()
	 * @model
	 * @generated
	 */
	Result getResult();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.ResultValue#getResult <em>Result</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' attribute.
	 * @see ice.master.loe.model.loe.Result
	 * @see #getResult()
	 * @generated
	 */
	void setResult(Result value);

} // ResultValue
