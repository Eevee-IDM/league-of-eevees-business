/**
 */
package ice.master.loe.model.loe.util;

import ice.master.loe.model.loe.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see ice.master.loe.model.loe.LoePackage
 * @generated
 */
public class LoeAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LoePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoeAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = LoePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LoeSwitch<Adapter> modelSwitch = new LoeSwitch<Adapter>() {
		@Override
		public Adapter caseSummoner(Summoner object) {
			return createSummonerAdapter();
		}

		@Override
		public Adapter caseRequest(Request object) {
			return createRequestAdapter();
		}

		@Override
		public Adapter caseFilter(Filter object) {
			return createFilterAdapter();
		}

		@Override
		public Adapter caseTimelapse(Timelapse object) {
			return createTimelapseAdapter();
		}

		@Override
		public Adapter caseValue(Value object) {
			return createValueAdapter();
		}

		@Override
		public Adapter caseChampionValue(ChampionValue object) {
			return createChampionValueAdapter();
		}

		@Override
		public Adapter caseTeamValue(TeamValue object) {
			return createTeamValueAdapter();
		}

		@Override
		public Adapter caseResultValue(ResultValue object) {
			return createResultValueAdapter();
		}

		@Override
		public Adapter caseLaneValue(LaneValue object) {
			return createLaneValueAdapter();
		}

		@Override
		public Adapter caseStatValue(StatValue object) {
			return createStatValueAdapter();
		}

		@Override
		public Adapter caseYValue(YValue object) {
			return createYValueAdapter();
		}

		@Override
		public Adapter caseComputedYValue(ComputedYValue object) {
			return createComputedYValueAdapter();
		}

		@Override
		public Adapter caseSum(Sum object) {
			return createSumAdapter();
		}

		@Override
		public Adapter caseAverage(Average object) {
			return createAverageAdapter();
		}

		@Override
		public Adapter caseRatio(Ratio object) {
			return createRatioAdapter();
		}

		@Override
		public Adapter caseType(Type object) {
			return createTypeAdapter();
		}

		@Override
		public Adapter caseComputable(Computable object) {
			return createComputableAdapter();
		}

		@Override
		public Adapter caseFilterByValue(FilterByValue object) {
			return createFilterByValueAdapter();
		}

		@Override
		public Adapter caseDiagram(Diagram object) {
			return createDiagramAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Summoner <em>Summoner</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Summoner
	 * @generated
	 */
	public Adapter createSummonerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Request <em>Request</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Request
	 * @generated
	 */
	public Adapter createRequestAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Filter
	 * @generated
	 */
	public Adapter createFilterAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Timelapse <em>Timelapse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Timelapse
	 * @generated
	 */
	public Adapter createTimelapseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Value
	 * @generated
	 */
	public Adapter createValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.ChampionValue <em>Champion Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.ChampionValue
	 * @generated
	 */
	public Adapter createChampionValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.TeamValue <em>Team Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.TeamValue
	 * @generated
	 */
	public Adapter createTeamValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.ResultValue <em>Result Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.ResultValue
	 * @generated
	 */
	public Adapter createResultValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.LaneValue <em>Lane Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.LaneValue
	 * @generated
	 */
	public Adapter createLaneValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.StatValue <em>Stat Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.StatValue
	 * @generated
	 */
	public Adapter createStatValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.YValue <em>YValue</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.YValue
	 * @generated
	 */
	public Adapter createYValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.ComputedYValue <em>Computed YValue</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.ComputedYValue
	 * @generated
	 */
	public Adapter createComputedYValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Sum <em>Sum</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Sum
	 * @generated
	 */
	public Adapter createSumAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Average <em>Average</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Average
	 * @generated
	 */
	public Adapter createAverageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Ratio <em>Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Ratio
	 * @generated
	 */
	public Adapter createRatioAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Type
	 * @generated
	 */
	public Adapter createTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Computable <em>Computable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Computable
	 * @generated
	 */
	public Adapter createComputableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.FilterByValue <em>Filter By Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.FilterByValue
	 * @generated
	 */
	public Adapter createFilterByValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link ice.master.loe.model.loe.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see ice.master.loe.model.loe.Diagram
	 * @generated
	 */
	public Adapter createDiagramAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //LoeAdapterFactory
