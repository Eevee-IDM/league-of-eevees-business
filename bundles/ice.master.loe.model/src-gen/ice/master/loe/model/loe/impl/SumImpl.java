/**
 */
package ice.master.loe.model.loe.impl;

import ice.master.loe.model.loe.Computation;
import ice.master.loe.model.loe.LoePackage;
import ice.master.loe.model.loe.Sum;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sum</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SumImpl extends ComputedYValueImpl implements Sum {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SumImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LoePackage.Literals.SUM;
	}

	@Override
	public Computation getComputation() {
		return Computation.SUM;
	}

} //SumImpl
