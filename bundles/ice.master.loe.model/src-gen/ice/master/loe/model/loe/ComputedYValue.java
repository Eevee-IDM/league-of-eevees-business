/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Computed YValue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.ComputedYValue#getComputation <em>Computation</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.ComputedYValue#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getComputedYValue()
 * @model
 * @generated
 */
public interface ComputedYValue extends YValue {
	/**
	 * Returns the value of the '<em><b>Computation</b></em>' attribute.
	 * The default value is <code>"SUM"</code>.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.Computation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Computation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Computation</em>' attribute.
	 * @see ice.master.loe.model.loe.Computation
	 * @see #isSetComputation()
	 * @see ice.master.loe.model.loe.LoePackage#getComputedYValue_Computation()
	 * @model default="SUM" unsettable="true" changeable="false"
	 * @generated
	 */
	Computation getComputation();

	/**
	 * Returns whether the value of the '{@link ice.master.loe.model.loe.ComputedYValue#getComputation <em>Computation</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Computation</em>' attribute is set.
	 * @see #getComputation()
	 * @generated
	 */
	boolean isSetComputation();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Computable)
	 * @see ice.master.loe.model.loe.LoePackage#getComputedYValue_Value()
	 * @model containment="true"
	 * @generated
	 */
	Computable getValue();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.ComputedYValue#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Computable value);

} // ComputedYValue
