/**
 */
package ice.master.loe.model.loe;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Diagram Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ice.master.loe.model.loe.LoePackage#getDiagramType()
 * @model
 * @generated NOT
 */
public enum DiagramType implements Enumerator {
	/**
	 * The '<em><b>PIE CHART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PIE_CHART_VALUE
	 * @generated NOT
	 * @ordered
	 */
	PIE_CHART(0, "PIE_CHART", "PIE_CHART", "pie2d"),

	/**
	 * The '<em><b>PIE CHART 3D</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PIE_CHART_3D_VALUE
	 * @generated NOT
	 * @ordered
	 */
	PIE_CHART_3D(1, "PIE_CHART_3D", "PIE_CHART_3D", "pie3d"),

	/**
	 * The '<em><b>LINE CHART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LINE_CHART_VALUE
	 * @generated NOT
	 * @ordered
	 */
	LINE_CHART(2, "LINE_CHART", "LINE_CHART", "line"),

	/**
	 * The '<em><b>BAR CHART</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BAR_CHART_VALUE
	 * @generated NOT
	 * @ordered
	 */
	BAR_CHART(3, "BAR_CHART", "BAR_CHART", "column2d");

	/**
	 * The '<em><b>PIE CHART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PIE CHART</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PIE_CHART
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PIE_CHART_VALUE = 0;

	/**
	 * The '<em><b>PIE CHART 3D</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PIE CHART 3D</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PIE_CHART_3D
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PIE_CHART_3D_VALUE = 1;

	/**
	 * The '<em><b>LINE CHART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LINE CHART</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LINE_CHART
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LINE_CHART_VALUE = 2;

	/**
	 * The '<em><b>BAR CHART</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BAR CHART</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BAR_CHART
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BAR_CHART_VALUE = 3;

	/**
	 * An array of all the '<em><b>Diagram Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final DiagramType[] VALUES_ARRAY = new DiagramType[] { PIE_CHART, PIE_CHART_3D, LINE_CHART,
			BAR_CHART, };

	/**
	 * A public read-only list of all the '<em><b>Diagram Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<DiagramType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Diagram Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DiagramType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DiagramType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Diagram Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DiagramType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			DiagramType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Diagram Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static DiagramType get(int value) {
		switch (value) {
		case PIE_CHART_VALUE:
			return PIE_CHART;
		case PIE_CHART_3D_VALUE:
			return PIE_CHART_3D;
		case LINE_CHART_VALUE:
			return LINE_CHART;
		case BAR_CHART_VALUE:
			return BAR_CHART;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	private final String id;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private DiagramType(int value, String name, String literal, String id) {
		this.value = value;
		this.name = name;
		this.literal = literal;
		this.id = id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	public String getId() {
		return id;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //DiagramType
