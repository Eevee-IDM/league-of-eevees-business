/**
 */
package ice.master.loe.model.loe.impl;

import ice.master.loe.model.loe.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LoeFactoryImpl extends EFactoryImpl implements LoeFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LoeFactory init() {
		try {
			LoeFactory theLoeFactory = (LoeFactory) EPackage.Registry.INSTANCE.getEFactory(LoePackage.eNS_URI);
			if (theLoeFactory != null) {
				return theLoeFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LoeFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoeFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case LoePackage.SUMMONER:
			return createSummoner();
		case LoePackage.REQUEST:
			return createRequest();
		case LoePackage.TIMELAPSE:
			return createTimelapse();
		case LoePackage.CHAMPION_VALUE:
			return createChampionValue();
		case LoePackage.TEAM_VALUE:
			return createTeamValue();
		case LoePackage.RESULT_VALUE:
			return createResultValue();
		case LoePackage.LANE_VALUE:
			return createLaneValue();
		case LoePackage.STAT_VALUE:
			return createStatValue();
		case LoePackage.COMPUTED_YVALUE:
			return createComputedYValue();
		case LoePackage.SUM:
			return createSum();
		case LoePackage.AVERAGE:
			return createAverage();
		case LoePackage.RATIO:
			return createRatio();
		case LoePackage.TYPE:
			return createType();
		case LoePackage.FILTER_BY_VALUE:
			return createFilterByValue();
		case LoePackage.DIAGRAM:
			return createDiagram();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case LoePackage.RESULT:
			return createResultFromString(eDataType, initialValue);
		case LoePackage.TEAM:
			return createTeamFromString(eDataType, initialValue);
		case LoePackage.CHAMPION:
			return createChampionFromString(eDataType, initialValue);
		case LoePackage.LANE:
			return createLaneFromString(eDataType, initialValue);
		case LoePackage.STAT:
			return createStatFromString(eDataType, initialValue);
		case LoePackage.COMPUTATION:
			return createComputationFromString(eDataType, initialValue);
		case LoePackage.VALUE_TYPE:
			return createValueTypeFromString(eDataType, initialValue);
		case LoePackage.DIAGRAM_TYPE:
			return createDiagramTypeFromString(eDataType, initialValue);
		case LoePackage.TIME_SCALE:
			return createTimeScaleFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case LoePackage.RESULT:
			return convertResultToString(eDataType, instanceValue);
		case LoePackage.TEAM:
			return convertTeamToString(eDataType, instanceValue);
		case LoePackage.CHAMPION:
			return convertChampionToString(eDataType, instanceValue);
		case LoePackage.LANE:
			return convertLaneToString(eDataType, instanceValue);
		case LoePackage.STAT:
			return convertStatToString(eDataType, instanceValue);
		case LoePackage.COMPUTATION:
			return convertComputationToString(eDataType, instanceValue);
		case LoePackage.VALUE_TYPE:
			return convertValueTypeToString(eDataType, instanceValue);
		case LoePackage.DIAGRAM_TYPE:
			return convertDiagramTypeToString(eDataType, instanceValue);
		case LoePackage.TIME_SCALE:
			return convertTimeScaleToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Summoner createSummoner() {
		SummonerImpl summoner = new SummonerImpl();
		return summoner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Request createRequest() {
		RequestImpl request = new RequestImpl();
		return request;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timelapse createTimelapse() {
		TimelapseImpl timelapse = new TimelapseImpl();
		return timelapse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChampionValue createChampionValue() {
		ChampionValueImpl championValue = new ChampionValueImpl();
		return championValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TeamValue createTeamValue() {
		TeamValueImpl teamValue = new TeamValueImpl();
		return teamValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResultValue createResultValue() {
		ResultValueImpl resultValue = new ResultValueImpl();
		return resultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LaneValue createLaneValue() {
		LaneValueImpl laneValue = new LaneValueImpl();
		return laneValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatValue createStatValue() {
		StatValueImpl statValue = new StatValueImpl();
		return statValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComputedYValue createComputedYValue() {
		ComputedYValueImpl computedYValue = new ComputedYValueImpl();
		return computedYValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sum createSum() {
		SumImpl sum = new SumImpl();
		return sum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Average createAverage() {
		AverageImpl average = new AverageImpl();
		return average;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Ratio createRatio() {
		RatioImpl ratio = new RatioImpl();
		return ratio;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type createType() {
		TypeImpl type = new TypeImpl();
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FilterByValue createFilterByValue() {
		FilterByValueImpl filterByValue = new FilterByValueImpl();
		return filterByValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Diagram createDiagram() {
		DiagramImpl diagram = new DiagramImpl();
		return diagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Result createResultFromString(EDataType eDataType, String initialValue) {
		Result result = Result.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertResultToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Team createTeamFromString(EDataType eDataType, String initialValue) {
		Team result = Team.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTeamToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Champion createChampionFromString(EDataType eDataType, String initialValue) {
		Champion result = Champion.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertChampionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Lane createLaneFromString(EDataType eDataType, String initialValue) {
		Lane result = Lane.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLaneToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Stat createStatFromString(EDataType eDataType, String initialValue) {
		Stat result = Stat.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStatToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Computation createComputationFromString(EDataType eDataType, String initialValue) {
		Computation result = Computation.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertComputationToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueType createValueTypeFromString(EDataType eDataType, String initialValue) {
		ValueType result = ValueType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertValueTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagramType createDiagramTypeFromString(EDataType eDataType, String initialValue) {
		DiagramType result = DiagramType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDiagramTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeScale createTimeScaleFromString(EDataType eDataType, String initialValue) {
		TimeScale result = TimeScale.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTimeScaleToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoePackage getLoePackage() {
		return (LoePackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LoePackage getPackage() {
		return LoePackage.eINSTANCE;
	}

} //LoeFactoryImpl
