/**
 */
package ice.master.loe.model.loe.impl;

import ice.master.loe.model.loe.Diagram;
import ice.master.loe.model.loe.Filter;
import ice.master.loe.model.loe.LoePackage;
import ice.master.loe.model.loe.Request;
import ice.master.loe.model.loe.Summoner;
import ice.master.loe.model.loe.TimeScale;
import ice.master.loe.model.loe.Timelapse;
import ice.master.loe.model.loe.ValueType;
import ice.master.loe.model.loe.YValue;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getSummoners <em>Summoners</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getFilters <em>Filters</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getYvalues <em>Yvalues</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getDiagram <em>Diagram</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getTimelapse <em>Timelapse</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getTimeScale <em>Time Scale</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.impl.RequestImpl#getXUnit <em>XUnit</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequestImpl extends MinimalEObjectImpl.Container implements Request {
	/**
	 * The cached value of the '{@link #getSummoners() <em>Summoners</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSummoners()
	 * @generated
	 * @ordered
	 */
	protected EList<Summoner> summoners;

	/**
	 * The cached value of the '{@link #getFilters() <em>Filters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilters()
	 * @generated
	 * @ordered
	 */
	protected EList<Filter> filters;

	/**
	 * The cached value of the '{@link #getYvalues() <em>Yvalues</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getYvalues()
	 * @generated
	 * @ordered
	 */
	protected EList<YValue> yvalues;

	/**
	 * The cached value of the '{@link #getDiagram() <em>Diagram</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDiagram()
	 * @generated
	 * @ordered
	 */
	protected Diagram diagram;

	/**
	 * The cached value of the '{@link #getTimelapse() <em>Timelapse</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimelapse()
	 * @generated
	 * @ordered
	 */
	protected Timelapse timelapse;

	/**
	 * The default value of the '{@link #getTimeScale() <em>Time Scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeScale()
	 * @generated
	 * @ordered
	 */
	protected static final TimeScale TIME_SCALE_EDEFAULT = TimeScale.ALL_THE_TIME;

	/**
	 * The cached value of the '{@link #getTimeScale() <em>Time Scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeScale()
	 * @generated
	 * @ordered
	 */
	protected TimeScale timeScale = TIME_SCALE_EDEFAULT;

	/**
	 * The default value of the '{@link #getXUnit() <em>XUnit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXUnit()
	 * @generated
	 * @ordered
	 */
	protected static final ValueType XUNIT_EDEFAULT = ValueType.NONE;

	/**
	 * The cached value of the '{@link #getXUnit() <em>XUnit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXUnit()
	 * @generated
	 * @ordered
	 */
	protected ValueType xUnit = XUNIT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LoePackage.Literals.REQUEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Summoner> getSummoners() {
		if (summoners == null) {
			summoners = new EObjectContainmentEList<Summoner>(Summoner.class, this, LoePackage.REQUEST__SUMMONERS);
		}
		return summoners;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Filter> getFilters() {
		if (filters == null) {
			filters = new EObjectContainmentEList<Filter>(Filter.class, this, LoePackage.REQUEST__FILTERS);
		}
		return filters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<YValue> getYvalues() {
		if (yvalues == null) {
			yvalues = new EObjectContainmentEList<YValue>(YValue.class, this, LoePackage.REQUEST__YVALUES);
		}
		return yvalues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Diagram getDiagram() {
		return diagram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDiagram(Diagram newDiagram, NotificationChain msgs) {
		Diagram oldDiagram = diagram;
		diagram = newDiagram;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, LoePackage.REQUEST__DIAGRAM,
					oldDiagram, newDiagram);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDiagram(Diagram newDiagram) {
		if (newDiagram != diagram) {
			NotificationChain msgs = null;
			if (diagram != null)
				msgs = ((InternalEObject) diagram).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - LoePackage.REQUEST__DIAGRAM, null, msgs);
			if (newDiagram != null)
				msgs = ((InternalEObject) newDiagram).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - LoePackage.REQUEST__DIAGRAM, null, msgs);
			msgs = basicSetDiagram(newDiagram, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LoePackage.REQUEST__DIAGRAM, newDiagram, newDiagram));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timelapse getTimelapse() {
		return timelapse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimelapse(Timelapse newTimelapse, NotificationChain msgs) {
		Timelapse oldTimelapse = timelapse;
		timelapse = newTimelapse;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					LoePackage.REQUEST__TIMELAPSE, oldTimelapse, newTimelapse);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimelapse(Timelapse newTimelapse) {
		if (newTimelapse != timelapse) {
			NotificationChain msgs = null;
			if (timelapse != null)
				msgs = ((InternalEObject) timelapse).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - LoePackage.REQUEST__TIMELAPSE, null, msgs);
			if (newTimelapse != null)
				msgs = ((InternalEObject) newTimelapse).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - LoePackage.REQUEST__TIMELAPSE, null, msgs);
			msgs = basicSetTimelapse(newTimelapse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LoePackage.REQUEST__TIMELAPSE, newTimelapse,
					newTimelapse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeScale getTimeScale() {
		return timeScale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeScale(TimeScale newTimeScale) {
		TimeScale oldTimeScale = timeScale;
		timeScale = newTimeScale == null ? TIME_SCALE_EDEFAULT : newTimeScale;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LoePackage.REQUEST__TIME_SCALE, oldTimeScale,
					timeScale));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueType getXUnit() {
		return xUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXUnit(ValueType newXUnit) {
		ValueType oldXUnit = xUnit;
		xUnit = newXUnit == null ? XUNIT_EDEFAULT : newXUnit;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LoePackage.REQUEST__XUNIT, oldXUnit, xUnit));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case LoePackage.REQUEST__SUMMONERS:
			return ((InternalEList<?>) getSummoners()).basicRemove(otherEnd, msgs);
		case LoePackage.REQUEST__FILTERS:
			return ((InternalEList<?>) getFilters()).basicRemove(otherEnd, msgs);
		case LoePackage.REQUEST__YVALUES:
			return ((InternalEList<?>) getYvalues()).basicRemove(otherEnd, msgs);
		case LoePackage.REQUEST__DIAGRAM:
			return basicSetDiagram(null, msgs);
		case LoePackage.REQUEST__TIMELAPSE:
			return basicSetTimelapse(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case LoePackage.REQUEST__SUMMONERS:
			return getSummoners();
		case LoePackage.REQUEST__FILTERS:
			return getFilters();
		case LoePackage.REQUEST__YVALUES:
			return getYvalues();
		case LoePackage.REQUEST__DIAGRAM:
			return getDiagram();
		case LoePackage.REQUEST__TIMELAPSE:
			return getTimelapse();
		case LoePackage.REQUEST__TIME_SCALE:
			return getTimeScale();
		case LoePackage.REQUEST__XUNIT:
			return getXUnit();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case LoePackage.REQUEST__SUMMONERS:
			getSummoners().clear();
			getSummoners().addAll((Collection<? extends Summoner>) newValue);
			return;
		case LoePackage.REQUEST__FILTERS:
			getFilters().clear();
			getFilters().addAll((Collection<? extends Filter>) newValue);
			return;
		case LoePackage.REQUEST__YVALUES:
			getYvalues().clear();
			getYvalues().addAll((Collection<? extends YValue>) newValue);
			return;
		case LoePackage.REQUEST__DIAGRAM:
			setDiagram((Diagram) newValue);
			return;
		case LoePackage.REQUEST__TIMELAPSE:
			setTimelapse((Timelapse) newValue);
			return;
		case LoePackage.REQUEST__TIME_SCALE:
			setTimeScale((TimeScale) newValue);
			return;
		case LoePackage.REQUEST__XUNIT:
			setXUnit((ValueType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case LoePackage.REQUEST__SUMMONERS:
			getSummoners().clear();
			return;
		case LoePackage.REQUEST__FILTERS:
			getFilters().clear();
			return;
		case LoePackage.REQUEST__YVALUES:
			getYvalues().clear();
			return;
		case LoePackage.REQUEST__DIAGRAM:
			setDiagram((Diagram) null);
			return;
		case LoePackage.REQUEST__TIMELAPSE:
			setTimelapse((Timelapse) null);
			return;
		case LoePackage.REQUEST__TIME_SCALE:
			setTimeScale(TIME_SCALE_EDEFAULT);
			return;
		case LoePackage.REQUEST__XUNIT:
			setXUnit(XUNIT_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case LoePackage.REQUEST__SUMMONERS:
			return summoners != null && !summoners.isEmpty();
		case LoePackage.REQUEST__FILTERS:
			return filters != null && !filters.isEmpty();
		case LoePackage.REQUEST__YVALUES:
			return yvalues != null && !yvalues.isEmpty();
		case LoePackage.REQUEST__DIAGRAM:
			return diagram != null;
		case LoePackage.REQUEST__TIMELAPSE:
			return timelapse != null;
		case LoePackage.REQUEST__TIME_SCALE:
			return timeScale != TIME_SCALE_EDEFAULT;
		case LoePackage.REQUEST__XUNIT:
			return xUnit != XUNIT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeScale: ");
		result.append(timeScale);
		result.append(", xUnit: ");
		result.append(xUnit);
		result.append(')');
		return result.toString();
	}

} //RequestImpl
