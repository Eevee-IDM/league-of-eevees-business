/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lane Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.LaneValue#getLane <em>Lane</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getLaneValue()
 * @model
 * @generated
 */
public interface LaneValue extends Value {
	/**
	 * Returns the value of the '<em><b>Lane</b></em>' attribute.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.Lane}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lane</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lane</em>' attribute.
	 * @see ice.master.loe.model.loe.Lane
	 * @see #setLane(Lane)
	 * @see ice.master.loe.model.loe.LoePackage#getLaneValue_Lane()
	 * @model
	 * @generated
	 */
	Lane getLane();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.LaneValue#getLane <em>Lane</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lane</em>' attribute.
	 * @see ice.master.loe.model.loe.Lane
	 * @see #getLane()
	 * @generated
	 */
	void setLane(Lane value);

} // LaneValue
