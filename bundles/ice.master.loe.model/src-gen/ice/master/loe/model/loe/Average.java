/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Average</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ice.master.loe.model.loe.LoePackage#getAverage()
 * @model
 * @generated
 */
public interface Average extends ComputedYValue {
} // Average
