/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ice.master.loe.model.loe.LoePackage#getType()
 * @model
 * @generated
 */
public interface Type extends Computable {

} // Type
