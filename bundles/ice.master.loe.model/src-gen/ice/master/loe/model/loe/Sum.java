/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sum</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ice.master.loe.model.loe.LoePackage#getSum()
 * @model
 * @generated
 */
public interface Sum extends ComputedYValue {
} // Sum
