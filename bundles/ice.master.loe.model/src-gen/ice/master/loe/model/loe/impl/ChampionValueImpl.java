/**
 */
package ice.master.loe.model.loe.impl;

import ice.master.loe.model.loe.Champion;
import ice.master.loe.model.loe.ChampionValue;
import ice.master.loe.model.loe.LoePackage;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Champion Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.impl.ChampionValueImpl#getChampion <em>Champion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ChampionValueImpl extends ComputableImpl implements ChampionValue {
	/**
	 * The default value of the '{@link #getChampion() <em>Champion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChampion()
	 * @generated
	 * @ordered
	 */
	protected static final Champion CHAMPION_EDEFAULT = Champion.ANNIE;

	/**
	 * The cached value of the '{@link #getChampion() <em>Champion</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChampion()
	 * @generated
	 * @ordered
	 */
	protected Champion champion = CHAMPION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ChampionValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LoePackage.Literals.CHAMPION_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Champion getChampion() {
		return champion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChampion(Champion newChampion) {
		Champion oldChampion = champion;
		champion = newChampion == null ? CHAMPION_EDEFAULT : newChampion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LoePackage.CHAMPION_VALUE__CHAMPION, oldChampion,
					champion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case LoePackage.CHAMPION_VALUE__CHAMPION:
			return getChampion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case LoePackage.CHAMPION_VALUE__CHAMPION:
			setChampion((Champion) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case LoePackage.CHAMPION_VALUE__CHAMPION:
			setChampion(CHAMPION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case LoePackage.CHAMPION_VALUE__CHAMPION:
			return champion != CHAMPION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (champion: ");
		result.append(champion);
		result.append(')');
		return result.toString();
	}

} //ChampionValueImpl
