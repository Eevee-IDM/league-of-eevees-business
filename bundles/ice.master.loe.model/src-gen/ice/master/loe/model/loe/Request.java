/**
 */
package ice.master.loe.model.loe;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Request</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.Request#getSummoners <em>Summoners</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.Request#getFilters <em>Filters</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.Request#getYvalues <em>Yvalues</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.Request#getDiagram <em>Diagram</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.Request#getTimelapse <em>Timelapse</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.Request#getTimeScale <em>Time Scale</em>}</li>
 *   <li>{@link ice.master.loe.model.loe.Request#getXUnit <em>XUnit</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getRequest()
 * @model
 * @generated
 */
public interface Request extends EObject {
	/**
	 * Returns the value of the '<em><b>Summoners</b></em>' containment reference list.
	 * The list contents are of type {@link ice.master.loe.model.loe.Summoner}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Summoners</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Summoners</em>' containment reference list.
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_Summoners()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Summoner> getSummoners();

	/**
	 * Returns the value of the '<em><b>Filters</b></em>' containment reference list.
	 * The list contents are of type {@link ice.master.loe.model.loe.Filter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filters</em>' containment reference list.
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_Filters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Filter> getFilters();

	/**
	 * Returns the value of the '<em><b>Yvalues</b></em>' containment reference list.
	 * The list contents are of type {@link ice.master.loe.model.loe.YValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Yvalues</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Yvalues</em>' containment reference list.
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_Yvalues()
	 * @model containment="true"
	 * @generated
	 */
	EList<YValue> getYvalues();

	/**
	 * Returns the value of the '<em><b>Diagram</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Diagram</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Diagram</em>' containment reference.
	 * @see #setDiagram(Diagram)
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_Diagram()
	 * @model containment="true"
	 * @generated
	 */
	Diagram getDiagram();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.Request#getDiagram <em>Diagram</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Diagram</em>' containment reference.
	 * @see #getDiagram()
	 * @generated
	 */
	void setDiagram(Diagram value);

	/**
	 * Returns the value of the '<em><b>Timelapse</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timelapse</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timelapse</em>' containment reference.
	 * @see #setTimelapse(Timelapse)
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_Timelapse()
	 * @model containment="true"
	 * @generated
	 */
	Timelapse getTimelapse();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.Request#getTimelapse <em>Timelapse</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timelapse</em>' containment reference.
	 * @see #getTimelapse()
	 * @generated
	 */
	void setTimelapse(Timelapse value);

	/**
	 * Returns the value of the '<em><b>Time Scale</b></em>' attribute.
	 * The default value is <code>"ALL_THE_TIME"</code>.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.TimeScale}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Scale</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Scale</em>' attribute.
	 * @see ice.master.loe.model.loe.TimeScale
	 * @see #setTimeScale(TimeScale)
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_TimeScale()
	 * @model default="ALL_THE_TIME"
	 * @generated
	 */
	TimeScale getTimeScale();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.Request#getTimeScale <em>Time Scale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Scale</em>' attribute.
	 * @see ice.master.loe.model.loe.TimeScale
	 * @see #getTimeScale()
	 * @generated
	 */
	void setTimeScale(TimeScale value);

	/**
	 * Returns the value of the '<em><b>XUnit</b></em>' attribute.
	 * The default value is <code>"NONE"</code>.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XUnit</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XUnit</em>' attribute.
	 * @see ice.master.loe.model.loe.ValueType
	 * @see #setXUnit(ValueType)
	 * @see ice.master.loe.model.loe.LoePackage#getRequest_XUnit()
	 * @model default="NONE"
	 * @generated
	 */
	ValueType getXUnit();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.Request#getXUnit <em>XUnit</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>XUnit</em>' attribute.
	 * @see ice.master.loe.model.loe.ValueType
	 * @see #getXUnit()
	 * @generated
	 */
	void setXUnit(ValueType value);

} // Request
