/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Stat Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link ice.master.loe.model.loe.StatValue#getStat <em>Stat</em>}</li>
 * </ul>
 *
 * @see ice.master.loe.model.loe.LoePackage#getStatValue()
 * @model
 * @generated
 */
public interface StatValue extends Value {
	/**
	 * Returns the value of the '<em><b>Stat</b></em>' attribute.
	 * The literals are from the enumeration {@link ice.master.loe.model.loe.Stat}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stat</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stat</em>' attribute.
	 * @see ice.master.loe.model.loe.Stat
	 * @see #setStat(Stat)
	 * @see ice.master.loe.model.loe.LoePackage#getStatValue_Stat()
	 * @model
	 * @generated
	 */
	Stat getStat();

	/**
	 * Sets the value of the '{@link ice.master.loe.model.loe.StatValue#getStat <em>Stat</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stat</em>' attribute.
	 * @see ice.master.loe.model.loe.Stat
	 * @see #getStat()
	 * @generated
	 */
	void setStat(Stat value);

} // StatValue
