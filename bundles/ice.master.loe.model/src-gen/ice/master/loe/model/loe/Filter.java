/**
 */
package ice.master.loe.model.loe;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ice.master.loe.model.loe.LoePackage#getFilter()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface Filter extends EObject {
} // Filter
