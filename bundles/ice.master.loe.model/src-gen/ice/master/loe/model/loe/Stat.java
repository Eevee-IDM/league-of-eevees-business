/**
 */
package ice.master.loe.model.loe;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Stat</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ice.master.loe.model.loe.LoePackage#getStat()
 * @model
 * @generated
 */
public enum Stat implements Enumerator {
	/**
	 * The '<em><b>KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	KILLS(0, "KILLS", "KILLS"),
	/**
	 * The '<em><b>DEATHS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DEATHS_VALUE
	 * @generated
	 * @ordered
	 */
	DEATHS(1, "DEATHS", "DEATHS"),
	/**
	 * The '<em><b>ASSISTS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ASSISTS_VALUE
	 * @generated
	 * @ordered
	 */
	ASSISTS(2, "ASSISTS", "ASSISTS"),
	/**
	 * The '<em><b>DOUBLE KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DOUBLE_KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	DOUBLE_KILLS(3, "DOUBLE_KILLS", "DOUBLE_KILLS"),
	/**
	 * The '<em><b>TRIPLE KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRIPLE_KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	TRIPLE_KILLS(4, "TRIPLE_KILLS", "TRIPLE_KILLS"),
	/**
	 * The '<em><b>QUADRA KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #QUADRA_KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	QUADRA_KILLS(5, "QUADRA_KILLS", "QUADRA_KILLS"),
	/**
	 * The '<em><b>PENTA KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #PENTA_KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	PENTA_KILLS(6, "PENTA_KILLS", "PENTA_KILLS"),
	/**
	 * The '<em><b>TOTAL DAMAGE DEALT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TOTAL_DAMAGE_DEALT_VALUE
	 * @generated
	 * @ordered
	 */
	TOTAL_DAMAGE_DEALT(7, "TOTAL_DAMAGE_DEALT", "TOTAL_DAMAGE_DEALT"),
	/**
	 * The '<em><b>MAGIC DAMAGE DEALT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MAGIC_DAMAGE_DEALT_VALUE
	 * @generated
	 * @ordered
	 */
	MAGIC_DAMAGE_DEALT(8, "MAGIC_DAMAGE_DEALT", "MAGIC_DAMAGE_DEALT"),
	/**
	 * The '<em><b>PHYSICAL DAMAGE DEALT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #PHYSICAL_DAMAGE_DEALT_VALUE
	 * @generated
	 * @ordered
	 */
	PHYSICAL_DAMAGE_DEALT(9, "PHYSICAL_DAMAGE_DEALT", "PHYSICAL_DAMAGE_DEALT"),
	/**
	 * The '<em><b>TRUE DAMAGE DEALT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRUE_DAMAGE_DEALT_VALUE
	 * @generated
	 * @ordered
	 */
	TRUE_DAMAGE_DEALT(10, "TRUE_DAMAGE_DEALT", "TRUE_DAMAGE_DEALT"),
	/**
	 * The '<em><b>TOTAL DAMAGE DEALT TO CHAMPIONS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TOTAL_DAMAGE_DEALT_TO_CHAMPIONS_VALUE
	 * @generated
	 * @ordered
	 */
	TOTAL_DAMAGE_DEALT_TO_CHAMPIONS(11, "TOTAL_DAMAGE_DEALT_TO_CHAMPIONS", "TOTAL_DAMAGE_DEALT_TO_CHAMPIONS"),
	/**
	 * The '<em><b>MAGIC DAMAGE DEALT TO CHAMPIONS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MAGIC_DAMAGE_DEALT_TO_CHAMPIONS_VALUE
	 * @generated
	 * @ordered
	 */
	MAGIC_DAMAGE_DEALT_TO_CHAMPIONS(12, "MAGIC_DAMAGE_DEALT_TO_CHAMPIONS", "MAGIC_DAMAGE_DEALT_TO_CHAMPIONS"),
	/**
	 * The '<em><b>PHYSICAL DAMAGE DEALT TO CHAMPIONS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS_VALUE
	 * @generated
	 * @ordered
	 */
	PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS(13, "PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS", "PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS"),
	/**
	 * The '<em><b>TRUE DAMAGE DEALT TO CHAMPIONS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRUE_DAMAGE_DEALT_TO_CHAMPIONS_VALUE
	 * @generated
	 * @ordered
	 */
	TRUE_DAMAGE_DEALT_TO_CHAMPIONS(14, "TRUE_DAMAGE_DEALT_TO_CHAMPIONS", "TRUE_DAMAGE_DEALT_TO_CHAMPIONS"),
	/**
	 * The '<em><b>TOTAL HEAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TOTAL_HEAL_VALUE
	 * @generated
	 * @ordered
	 */
	TOTAL_HEAL(15, "TOTAL_HEAL", "TOTAL_HEAL"),
	/**
	 * The '<em><b>DAMAGE SELF MITIGATED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DAMAGE_SELF_MITIGATED_VALUE
	 * @generated
	 * @ordered
	 */
	DAMAGE_SELF_MITIGATED(16, "DAMAGE_SELF_MITIGATED", "DAMAGE_SELF_MITIGATED"),
	/**
	 * The '<em><b>DAMAGE DEALT TO OBJECTIVES</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DAMAGE_DEALT_TO_OBJECTIVES_VALUE
	 * @generated
	 * @ordered
	 */
	DAMAGE_DEALT_TO_OBJECTIVES(17, "DAMAGE_DEALT_TO_OBJECTIVES", "DAMAGE_DEALT_TO_OBJECTIVES"),
	/**
	 * The '<em><b>DAMAGE DEALT TO TURRETS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DAMAGE_DEALT_TO_TURRETS_VALUE
	 * @generated
	 * @ordered
	 */
	DAMAGE_DEALT_TO_TURRETS(18, "DAMAGE_DEALT_TO_TURRETS", "DAMAGE_DEALT_TO_TURRETS"),
	/**
	 * The '<em><b>TIME CCING OTHERS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TIME_CCING_OTHERS_VALUE
	 * @generated
	 * @ordered
	 */
	TIME_CCING_OTHERS(19, "TIME_C_CING_OTHERS", "TIME_C_CING_OTHERS"),
	/**
	 * The '<em><b>TOTAL DAMAGE TAKEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TOTAL_DAMAGE_TAKEN_VALUE
	 * @generated
	 * @ordered
	 */
	TOTAL_DAMAGE_TAKEN(20, "TOTAL_DAMAGE_TAKEN", "TOTAL_DAMAGE_TAKEN"),
	/**
	 * The '<em><b>MAGICAL DAMAGE TAKEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MAGICAL_DAMAGE_TAKEN_VALUE
	 * @generated
	 * @ordered
	 */
	MAGICAL_DAMAGE_TAKEN(21, "MAGICAL_DAMAGE_TAKEN", "MAGICAL_DAMAGE_TAKEN"),
	/**
	 * The '<em><b>PHYSICAL DAMAGE TAKEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #PHYSICAL_DAMAGE_TAKEN_VALUE
	 * @generated
	 * @ordered
	 */
	PHYSICAL_DAMAGE_TAKEN(22, "PHYSICAL_DAMAGE_TAKEN", "PHYSICAL_DAMAGE_TAKEN"),
	/**
	 * The '<em><b>TRUE DAMAGE TAKEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRUE_DAMAGE_TAKEN_VALUE
	 * @generated
	 * @ordered
	 */
	TRUE_DAMAGE_TAKEN(23, "TRUE_DAMAGE_TAKEN", "TRUE_DAMAGE_TAKEN"),
	/**
	 * The '<em><b>GOLD EARNED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GOLD_EARNED_VALUE
	 * @generated
	 * @ordered
	 */
	GOLD_EARNED(24, "GOLD_EARNED", "GOLD_EARNED"),
	/**
	 * The '<em><b>GOLD SPENT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GOLD_SPENT_VALUE
	 * @generated
	 * @ordered
	 */
	GOLD_SPENT(25, "GOLD_SPENT", "GOLD_SPENT"),
	/**
	 * The '<em><b>TURRET KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TURRET_KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	TURRET_KILLS(26, "TURRET_KILLS", "TURRET_KILLS"),
	/**
	 * The '<em><b>INHIBITOR KILLS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #INHIBITOR_KILLS_VALUE
	 * @generated
	 * @ordered
	 */
	INHIBITOR_KILLS(27, "INHIBITOR_KILLS", "INHIBITOR_KILLS"),
	/**
	 * The '<em><b>TOTAL MINIONS KILLED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TOTAL_MINIONS_KILLED_VALUE
	 * @generated
	 * @ordered
	 */
	TOTAL_MINIONS_KILLED(28, "TOTAL_MINIONS_KILLED", "TOTAL_MINIONS_KILLED"),
	/**
	 * The '<em><b>NEUTRAL MINIONS KILLED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NEUTRAL_MINIONS_KILLED_VALUE
	 * @generated
	 * @ordered
	 */
	NEUTRAL_MINIONS_KILLED(29, "NEUTRAL_MINIONS_KILLED", "NEUTRAL_MINIONS_KILLED"),
	/**
	 * The '<em><b>NEUTRAL MINIONS KILLED TEAM JUNGLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE_VALUE
	 * @generated
	 * @ordered
	 */
	NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE(30, "NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE", "NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE"),
	/**
	 * The '<em><b>NEUTRAL MINIONS KILLED ENEMY JUNGLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE_VALUE
	 * @generated
	 * @ordered
	 */
	NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE(31, "NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE",
			"NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE"),
	/**
	 * The '<em><b>TOTAL TIME CROWD CONTROL DEALT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TOTAL_TIME_CROWD_CONTROL_DEALT_VALUE
	 * @generated
	 * @ordered
	 */
	TOTAL_TIME_CROWD_CONTROL_DEALT(32, "TOTAL_TIME_CROWD_CONTROL_DEALT", "TOTAL_TIME_CROWD_CONTROL_DEALT"),
	/**
	 * The '<em><b>VISION WARDS BOUGHT IN GAME</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VISION_WARDS_BOUGHT_IN_GAME_VALUE
	 * @generated
	 * @ordered
	 */
	VISION_WARDS_BOUGHT_IN_GAME(33, "VISION_WARDS_BOUGHT_IN_GAME", "VISION_WARDS_BOUGHT_IN_GAME"),
	/**
	 * The '<em><b>WARDS PLACED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #WARDS_PLACED_VALUE
	 * @generated
	 * @ordered
	 */
	WARDS_PLACED(34, "WARDS_PLACED", "WARDS_PLACED"),
	/**
	 * The '<em><b>WARDS KILLED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #WARDS_KILLED_VALUE
	 * @generated
	 * @ordered
	 */
	WARDS_KILLED(35, "WARDS_KILLED", "WARDS_KILLED"),
	/**
	 * The '<em><b>FIRST BLOOD KILL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIRST_BLOOD_KILL_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_BLOOD_KILL(36, "FIRST_BLOOD_KILL", "FIRST_BLOOD_KILL"),
	/**
	 * The '<em><b>FIRST BLOOD ASSIST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIRST_BLOOD_ASSIST_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_BLOOD_ASSIST(37, "FIRST_BLOOD_ASSIST", "FIRST_BLOOD_ASSIST"),
	/**
	 * The '<em><b>FIRST TOWER KILL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIRST_TOWER_KILL_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_TOWER_KILL(38, "FIRST_TOWER_KILL", "FIRST_TOWER_KILL"),
	/**
	 * The '<em><b>FIRST TOWER ASSIST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIRST_TOWER_ASSIST_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_TOWER_ASSIST(39, "FIRST_TOWER_ASSIST", "FIRST_TOWER_ASSIST"),
	/**
	 * The '<em><b>FIRST INHIBITOR KILL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIRST_INHIBITOR_KILL_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_INHIBITOR_KILL(39, "FIRST_INHIBITOR_KILL", "FIRST_INHIBITOR_KILL"),
	/**
	 * The '<em><b>FIRST INHIBITOR ASSIST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIRST_INHIBITOR_ASSIST_VALUE
	 * @generated
	 * @ordered
	 */
	FIRST_INHIBITOR_ASSIST(40, "FIRST_INHIBITOR_ASSIST", "FIRST_INHIBITOR_ASSIST");

	/**
	 * The '<em><b>KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KILLS_VALUE = 0;

	/**
	 * The '<em><b>DEATHS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DEATHS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DEATHS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DEATHS_VALUE = 1;

	/**
	 * The '<em><b>ASSISTS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ASSISTS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASSISTS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ASSISTS_VALUE = 2;

	/**
	 * The '<em><b>DOUBLE KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DOUBLE KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DOUBLE_KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DOUBLE_KILLS_VALUE = 3;

	/**
	 * The '<em><b>TRIPLE KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRIPLE KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRIPLE_KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRIPLE_KILLS_VALUE = 4;

	/**
	 * The '<em><b>QUADRA KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>QUADRA KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #QUADRA_KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int QUADRA_KILLS_VALUE = 5;

	/**
	 * The '<em><b>PENTA KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PENTA KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PENTA_KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PENTA_KILLS_VALUE = 6;

	/**
	 * The '<em><b>TOTAL DAMAGE DEALT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TOTAL DAMAGE DEALT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOTAL_DAMAGE_DEALT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TOTAL_DAMAGE_DEALT_VALUE = 7;

	/**
	 * The '<em><b>MAGIC DAMAGE DEALT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MAGIC DAMAGE DEALT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MAGIC_DAMAGE_DEALT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAGIC_DAMAGE_DEALT_VALUE = 8;

	/**
	 * The '<em><b>PHYSICAL DAMAGE DEALT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PHYSICAL DAMAGE DEALT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_DAMAGE_DEALT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PHYSICAL_DAMAGE_DEALT_VALUE = 9;

	/**
	 * The '<em><b>TRUE DAMAGE DEALT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRUE DAMAGE DEALT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRUE_DAMAGE_DEALT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRUE_DAMAGE_DEALT_VALUE = 10;

	/**
	 * The '<em><b>TOTAL DAMAGE DEALT TO CHAMPIONS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TOTAL DAMAGE DEALT TO CHAMPIONS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOTAL_DAMAGE_DEALT_TO_CHAMPIONS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TOTAL_DAMAGE_DEALT_TO_CHAMPIONS_VALUE = 11;

	/**
	 * The '<em><b>MAGIC DAMAGE DEALT TO CHAMPIONS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MAGIC DAMAGE DEALT TO CHAMPIONS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MAGIC_DAMAGE_DEALT_TO_CHAMPIONS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAGIC_DAMAGE_DEALT_TO_CHAMPIONS_VALUE = 12;

	/**
	 * The '<em><b>PHYSICAL DAMAGE DEALT TO CHAMPIONS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PHYSICAL DAMAGE DEALT TO CHAMPIONS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS_VALUE = 13;

	/**
	 * The '<em><b>TRUE DAMAGE DEALT TO CHAMPIONS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRUE DAMAGE DEALT TO CHAMPIONS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRUE_DAMAGE_DEALT_TO_CHAMPIONS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRUE_DAMAGE_DEALT_TO_CHAMPIONS_VALUE = 14;

	/**
	 * The '<em><b>TOTAL HEAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TOTAL HEAL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOTAL_HEAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TOTAL_HEAL_VALUE = 15;

	/**
	 * The '<em><b>DAMAGE SELF MITIGATED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DAMAGE SELF MITIGATED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DAMAGE_SELF_MITIGATED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DAMAGE_SELF_MITIGATED_VALUE = 16;

	/**
	 * The '<em><b>DAMAGE DEALT TO OBJECTIVES</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DAMAGE DEALT TO OBJECTIVES</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DAMAGE_DEALT_TO_OBJECTIVES
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DAMAGE_DEALT_TO_OBJECTIVES_VALUE = 17;

	/**
	 * The '<em><b>DAMAGE DEALT TO TURRETS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DAMAGE DEALT TO TURRETS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DAMAGE_DEALT_TO_TURRETS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DAMAGE_DEALT_TO_TURRETS_VALUE = 18;

	/**
	 * The '<em><b>TIME CCING OTHERS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TIME CCING OTHERS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIME_CCING_OTHERS
	 * @model name="TIME_C_CING_OTHERS"
	 * @generated
	 * @ordered
	 */
	public static final int TIME_CCING_OTHERS_VALUE = 19;

	/**
	 * The '<em><b>TOTAL DAMAGE TAKEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TOTAL DAMAGE TAKEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOTAL_DAMAGE_TAKEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TOTAL_DAMAGE_TAKEN_VALUE = 20;

	/**
	 * The '<em><b>MAGICAL DAMAGE TAKEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MAGICAL DAMAGE TAKEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MAGICAL_DAMAGE_TAKEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAGICAL_DAMAGE_TAKEN_VALUE = 21;

	/**
	 * The '<em><b>PHYSICAL DAMAGE TAKEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PHYSICAL DAMAGE TAKEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PHYSICAL_DAMAGE_TAKEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PHYSICAL_DAMAGE_TAKEN_VALUE = 22;

	/**
	 * The '<em><b>TRUE DAMAGE TAKEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRUE DAMAGE TAKEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRUE_DAMAGE_TAKEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRUE_DAMAGE_TAKEN_VALUE = 23;

	/**
	 * The '<em><b>GOLD EARNED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GOLD EARNED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GOLD_EARNED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GOLD_EARNED_VALUE = 24;

	/**
	 * The '<em><b>GOLD SPENT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GOLD SPENT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GOLD_SPENT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GOLD_SPENT_VALUE = 25;

	/**
	 * The '<em><b>TURRET KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TURRET KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TURRET_KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TURRET_KILLS_VALUE = 26;

	/**
	 * The '<em><b>INHIBITOR KILLS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>INHIBITOR KILLS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INHIBITOR_KILLS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int INHIBITOR_KILLS_VALUE = 27;

	/**
	 * The '<em><b>TOTAL MINIONS KILLED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TOTAL MINIONS KILLED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOTAL_MINIONS_KILLED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TOTAL_MINIONS_KILLED_VALUE = 28;

	/**
	 * The '<em><b>NEUTRAL MINIONS KILLED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NEUTRAL MINIONS KILLED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEUTRAL_MINIONS_KILLED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NEUTRAL_MINIONS_KILLED_VALUE = 29;

	/**
	 * The '<em><b>NEUTRAL MINIONS KILLED TEAM JUNGLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NEUTRAL MINIONS KILLED TEAM JUNGLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE_VALUE = 30;

	/**
	 * The '<em><b>NEUTRAL MINIONS KILLED ENEMY JUNGLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NEUTRAL MINIONS KILLED ENEMY JUNGLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE_VALUE = 31;

	/**
	 * The '<em><b>TOTAL TIME CROWD CONTROL DEALT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TOTAL TIME CROWD CONTROL DEALT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TOTAL_TIME_CROWD_CONTROL_DEALT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TOTAL_TIME_CROWD_CONTROL_DEALT_VALUE = 32;

	/**
	 * The '<em><b>VISION WARDS BOUGHT IN GAME</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VISION WARDS BOUGHT IN GAME</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VISION_WARDS_BOUGHT_IN_GAME
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VISION_WARDS_BOUGHT_IN_GAME_VALUE = 33;

	/**
	 * The '<em><b>WARDS PLACED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>WARDS PLACED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WARDS_PLACED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int WARDS_PLACED_VALUE = 34;

	/**
	 * The '<em><b>WARDS KILLED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>WARDS KILLED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WARDS_KILLED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int WARDS_KILLED_VALUE = 35;

	/**
	 * The '<em><b>FIRST BLOOD KILL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST BLOOD KILL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_BLOOD_KILL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_BLOOD_KILL_VALUE = 36;

	/**
	 * The '<em><b>FIRST BLOOD ASSIST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST BLOOD ASSIST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_BLOOD_ASSIST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_BLOOD_ASSIST_VALUE = 37;

	/**
	 * The '<em><b>FIRST TOWER KILL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST TOWER KILL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_TOWER_KILL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_TOWER_KILL_VALUE = 38;

	/**
	 * The '<em><b>FIRST TOWER ASSIST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST TOWER ASSIST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_TOWER_ASSIST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_TOWER_ASSIST_VALUE = 39;

	/**
	 * The '<em><b>FIRST INHIBITOR KILL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST INHIBITOR KILL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_INHIBITOR_KILL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_INHIBITOR_KILL_VALUE = 39;

	/**
	 * The '<em><b>FIRST INHIBITOR ASSIST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIRST INHIBITOR ASSIST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIRST_INHIBITOR_ASSIST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIRST_INHIBITOR_ASSIST_VALUE = 40;

	/**
	 * An array of all the '<em><b>Stat</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Stat[] VALUES_ARRAY = new Stat[] { KILLS, DEATHS, ASSISTS, DOUBLE_KILLS, TRIPLE_KILLS,
			QUADRA_KILLS, PENTA_KILLS, TOTAL_DAMAGE_DEALT, MAGIC_DAMAGE_DEALT, PHYSICAL_DAMAGE_DEALT, TRUE_DAMAGE_DEALT,
			TOTAL_DAMAGE_DEALT_TO_CHAMPIONS, MAGIC_DAMAGE_DEALT_TO_CHAMPIONS, PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS,
			TRUE_DAMAGE_DEALT_TO_CHAMPIONS, TOTAL_HEAL, DAMAGE_SELF_MITIGATED, DAMAGE_DEALT_TO_OBJECTIVES,
			DAMAGE_DEALT_TO_TURRETS, TIME_CCING_OTHERS, TOTAL_DAMAGE_TAKEN, MAGICAL_DAMAGE_TAKEN, PHYSICAL_DAMAGE_TAKEN,
			TRUE_DAMAGE_TAKEN, GOLD_EARNED, GOLD_SPENT, TURRET_KILLS, INHIBITOR_KILLS, TOTAL_MINIONS_KILLED,
			NEUTRAL_MINIONS_KILLED, NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE, NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE,
			TOTAL_TIME_CROWD_CONTROL_DEALT, VISION_WARDS_BOUGHT_IN_GAME, WARDS_PLACED, WARDS_KILLED, FIRST_BLOOD_KILL,
			FIRST_BLOOD_ASSIST, FIRST_TOWER_KILL, FIRST_TOWER_ASSIST, FIRST_INHIBITOR_KILL, FIRST_INHIBITOR_ASSIST, };

	/**
	 * A public read-only list of all the '<em><b>Stat</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Stat> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Stat</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Stat get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Stat result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Stat</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Stat getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Stat result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Stat</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Stat get(int value) {
		switch (value) {
		case KILLS_VALUE:
			return KILLS;
		case DEATHS_VALUE:
			return DEATHS;
		case ASSISTS_VALUE:
			return ASSISTS;
		case DOUBLE_KILLS_VALUE:
			return DOUBLE_KILLS;
		case TRIPLE_KILLS_VALUE:
			return TRIPLE_KILLS;
		case QUADRA_KILLS_VALUE:
			return QUADRA_KILLS;
		case PENTA_KILLS_VALUE:
			return PENTA_KILLS;
		case TOTAL_DAMAGE_DEALT_VALUE:
			return TOTAL_DAMAGE_DEALT;
		case MAGIC_DAMAGE_DEALT_VALUE:
			return MAGIC_DAMAGE_DEALT;
		case PHYSICAL_DAMAGE_DEALT_VALUE:
			return PHYSICAL_DAMAGE_DEALT;
		case TRUE_DAMAGE_DEALT_VALUE:
			return TRUE_DAMAGE_DEALT;
		case TOTAL_DAMAGE_DEALT_TO_CHAMPIONS_VALUE:
			return TOTAL_DAMAGE_DEALT_TO_CHAMPIONS;
		case MAGIC_DAMAGE_DEALT_TO_CHAMPIONS_VALUE:
			return MAGIC_DAMAGE_DEALT_TO_CHAMPIONS;
		case PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS_VALUE:
			return PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS;
		case TRUE_DAMAGE_DEALT_TO_CHAMPIONS_VALUE:
			return TRUE_DAMAGE_DEALT_TO_CHAMPIONS;
		case TOTAL_HEAL_VALUE:
			return TOTAL_HEAL;
		case DAMAGE_SELF_MITIGATED_VALUE:
			return DAMAGE_SELF_MITIGATED;
		case DAMAGE_DEALT_TO_OBJECTIVES_VALUE:
			return DAMAGE_DEALT_TO_OBJECTIVES;
		case DAMAGE_DEALT_TO_TURRETS_VALUE:
			return DAMAGE_DEALT_TO_TURRETS;
		case TIME_CCING_OTHERS_VALUE:
			return TIME_CCING_OTHERS;
		case TOTAL_DAMAGE_TAKEN_VALUE:
			return TOTAL_DAMAGE_TAKEN;
		case MAGICAL_DAMAGE_TAKEN_VALUE:
			return MAGICAL_DAMAGE_TAKEN;
		case PHYSICAL_DAMAGE_TAKEN_VALUE:
			return PHYSICAL_DAMAGE_TAKEN;
		case TRUE_DAMAGE_TAKEN_VALUE:
			return TRUE_DAMAGE_TAKEN;
		case GOLD_EARNED_VALUE:
			return GOLD_EARNED;
		case GOLD_SPENT_VALUE:
			return GOLD_SPENT;
		case TURRET_KILLS_VALUE:
			return TURRET_KILLS;
		case INHIBITOR_KILLS_VALUE:
			return INHIBITOR_KILLS;
		case TOTAL_MINIONS_KILLED_VALUE:
			return TOTAL_MINIONS_KILLED;
		case NEUTRAL_MINIONS_KILLED_VALUE:
			return NEUTRAL_MINIONS_KILLED;
		case NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE_VALUE:
			return NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE;
		case NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE_VALUE:
			return NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE;
		case TOTAL_TIME_CROWD_CONTROL_DEALT_VALUE:
			return TOTAL_TIME_CROWD_CONTROL_DEALT;
		case VISION_WARDS_BOUGHT_IN_GAME_VALUE:
			return VISION_WARDS_BOUGHT_IN_GAME;
		case WARDS_PLACED_VALUE:
			return WARDS_PLACED;
		case WARDS_KILLED_VALUE:
			return WARDS_KILLED;
		case FIRST_BLOOD_KILL_VALUE:
			return FIRST_BLOOD_KILL;
		case FIRST_BLOOD_ASSIST_VALUE:
			return FIRST_BLOOD_ASSIST;
		case FIRST_TOWER_KILL_VALUE:
			return FIRST_TOWER_KILL;
		case FIRST_TOWER_ASSIST_VALUE:
			return FIRST_TOWER_ASSIST;
		case FIRST_INHIBITOR_ASSIST_VALUE:
			return FIRST_INHIBITOR_ASSIST;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Stat(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

	/**
	 * Returns the id of the stat as understood by Riot Games.
	 * @return the id of the stat as understood by Riot Games
	 */
	public String getRiotId() {
		StringBuilder riotId = new StringBuilder();

		boolean nextCharacterIsUpper = false;

		for (Character letter : getLiteral().toCharArray()) {
			if (letter == '_') {
				nextCharacterIsUpper = true;
				continue;
			}
			if (nextCharacterIsUpper)
				riotId.append(Character.toUpperCase(letter));
			else
				riotId.append(Character.toLowerCase(letter));

			nextCharacterIsUpper = false;
		}

		return riotId.toString();
	}

	/**
	 * Returns a human-readable name of the stat
	 * @return a human-readable name of the stat
	 */
	public String getRiotName() {
		StringBuilder riotName = new StringBuilder();

		for (Character letter : getLiteral().toCharArray()) {
			if (letter == '_') {
				riotName.append(" ");
			} else {
				riotName.append(Character.toLowerCase(letter));
			}
		}

		return riotName.toString();
	}

} //Stat
