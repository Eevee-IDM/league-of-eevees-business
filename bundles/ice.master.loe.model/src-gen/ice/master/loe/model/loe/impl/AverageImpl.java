/**
 */
package ice.master.loe.model.loe.impl;

import org.eclipse.emf.ecore.EClass;

import ice.master.loe.model.loe.Average;
import ice.master.loe.model.loe.Computation;
import ice.master.loe.model.loe.LoePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Average</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AverageImpl extends ComputedYValueImpl implements Average {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AverageImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LoePackage.Literals.AVERAGE;
	}

	@Override
	public Computation getComputation() {
		return Computation.AVERAGE;
	}

} //AverageImpl
