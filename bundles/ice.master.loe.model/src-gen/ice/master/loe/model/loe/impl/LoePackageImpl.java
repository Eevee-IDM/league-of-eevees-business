/**
 */
package ice.master.loe.model.loe.impl;

import ice.master.loe.model.loe.Average;
import ice.master.loe.model.loe.Champion;
import ice.master.loe.model.loe.ChampionValue;
import ice.master.loe.model.loe.Computable;
import ice.master.loe.model.loe.Computation;
import ice.master.loe.model.loe.ComputedYValue;
import ice.master.loe.model.loe.Diagram;
import ice.master.loe.model.loe.DiagramType;
import ice.master.loe.model.loe.Filter;
import ice.master.loe.model.loe.FilterByValue;
import ice.master.loe.model.loe.Lane;
import ice.master.loe.model.loe.LaneValue;
import ice.master.loe.model.loe.LoeFactory;
import ice.master.loe.model.loe.LoePackage;
import ice.master.loe.model.loe.Ratio;
import ice.master.loe.model.loe.Request;
import ice.master.loe.model.loe.Result;
import ice.master.loe.model.loe.ResultValue;
import ice.master.loe.model.loe.Stat;
import ice.master.loe.model.loe.StatValue;
import ice.master.loe.model.loe.Sum;
import ice.master.loe.model.loe.Summoner;
import ice.master.loe.model.loe.Team;
import ice.master.loe.model.loe.TeamValue;
import ice.master.loe.model.loe.TimeScale;
import ice.master.loe.model.loe.Timelapse;
import ice.master.loe.model.loe.Type;
import ice.master.loe.model.loe.Value;
import ice.master.loe.model.loe.ValueType;
import ice.master.loe.model.loe.YValue;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LoePackageImpl extends EPackageImpl implements LoePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass summonerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requestEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timelapseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass championValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass teamValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resultValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass laneValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass yValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computedYValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sumEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass averageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ratioEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass computableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass filterByValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagramEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum resultEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum teamEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum championEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum laneEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum statEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum computationEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum valueTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum diagramTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum timeScaleEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see ice.master.loe.model.loe.LoePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private LoePackageImpl() {
		super(eNS_URI, LoeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link LoePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static LoePackage init() {
		if (isInited)
			return (LoePackage) EPackage.Registry.INSTANCE.getEPackage(LoePackage.eNS_URI);

		// Obtain or create and register package
		LoePackageImpl theLoePackage = (LoePackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof LoePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new LoePackageImpl());

		isInited = true;

		// Create package meta-data objects
		theLoePackage.createPackageContents();

		// Initialize created meta-data
		theLoePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theLoePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(LoePackage.eNS_URI, theLoePackage);
		return theLoePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSummoner() {
		return summonerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSummoner_Name() {
		return (EAttribute) summonerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequest() {
		return requestEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequest_Summoners() {
		return (EReference) requestEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequest_Filters() {
		return (EReference) requestEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequest_Yvalues() {
		return (EReference) requestEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequest_Diagram() {
		return (EReference) requestEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequest_Timelapse() {
		return (EReference) requestEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequest_TimeScale() {
		return (EAttribute) requestEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequest_XUnit() {
		return (EAttribute) requestEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFilter() {
		return filterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimelapse() {
		return timelapseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimelapse_From() {
		return (EAttribute) timelapseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimelapse_To() {
		return (EAttribute) timelapseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValue() {
		return valueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChampionValue() {
		return championValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getChampionValue_Champion() {
		return (EAttribute) championValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTeamValue() {
		return teamValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTeamValue_Team() {
		return (EAttribute) teamValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResultValue() {
		return resultValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResultValue_Result() {
		return (EAttribute) resultValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLaneValue() {
		return laneValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLaneValue_Lane() {
		return (EAttribute) laneValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatValue() {
		return statValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStatValue_Stat() {
		return (EAttribute) statValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getYValue() {
		return yValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputedYValue() {
		return computedYValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputedYValue_Computation() {
		return (EAttribute) computedYValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComputedYValue_Value() {
		return (EReference) computedYValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSum() {
		return sumEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAverage() {
		return averageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRatio() {
		return ratioEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getType() {
		return typeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComputable() {
		return computableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComputable_Type() {
		return (EAttribute) computableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFilterByValue() {
		return filterByValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFilterByValue_Value() {
		return (EReference) filterByValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiagram() {
		return diagramEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagram_Name() {
		return (EAttribute) diagramEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDiagram_Type() {
		return (EAttribute) diagramEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getResult() {
		return resultEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTeam() {
		return teamEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getChampion() {
		return championEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLane() {
		return laneEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStat() {
		return statEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getComputation() {
		return computationEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getValueType() {
		return valueTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDiagramType() {
		return diagramTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTimeScale() {
		return timeScaleEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoeFactory getLoeFactory() {
		return (LoeFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		summonerEClass = createEClass(SUMMONER);
		createEAttribute(summonerEClass, SUMMONER__NAME);

		requestEClass = createEClass(REQUEST);
		createEReference(requestEClass, REQUEST__SUMMONERS);
		createEReference(requestEClass, REQUEST__FILTERS);
		createEReference(requestEClass, REQUEST__YVALUES);
		createEReference(requestEClass, REQUEST__DIAGRAM);
		createEReference(requestEClass, REQUEST__TIMELAPSE);
		createEAttribute(requestEClass, REQUEST__TIME_SCALE);
		createEAttribute(requestEClass, REQUEST__XUNIT);

		filterEClass = createEClass(FILTER);

		timelapseEClass = createEClass(TIMELAPSE);
		createEAttribute(timelapseEClass, TIMELAPSE__FROM);
		createEAttribute(timelapseEClass, TIMELAPSE__TO);

		valueEClass = createEClass(VALUE);

		championValueEClass = createEClass(CHAMPION_VALUE);
		createEAttribute(championValueEClass, CHAMPION_VALUE__CHAMPION);

		teamValueEClass = createEClass(TEAM_VALUE);
		createEAttribute(teamValueEClass, TEAM_VALUE__TEAM);

		resultValueEClass = createEClass(RESULT_VALUE);
		createEAttribute(resultValueEClass, RESULT_VALUE__RESULT);

		laneValueEClass = createEClass(LANE_VALUE);
		createEAttribute(laneValueEClass, LANE_VALUE__LANE);

		statValueEClass = createEClass(STAT_VALUE);
		createEAttribute(statValueEClass, STAT_VALUE__STAT);

		yValueEClass = createEClass(YVALUE);

		computedYValueEClass = createEClass(COMPUTED_YVALUE);
		createEAttribute(computedYValueEClass, COMPUTED_YVALUE__COMPUTATION);
		createEReference(computedYValueEClass, COMPUTED_YVALUE__VALUE);

		sumEClass = createEClass(SUM);

		averageEClass = createEClass(AVERAGE);

		ratioEClass = createEClass(RATIO);

		typeEClass = createEClass(TYPE);

		computableEClass = createEClass(COMPUTABLE);
		createEAttribute(computableEClass, COMPUTABLE__TYPE);

		filterByValueEClass = createEClass(FILTER_BY_VALUE);
		createEReference(filterByValueEClass, FILTER_BY_VALUE__VALUE);

		diagramEClass = createEClass(DIAGRAM);
		createEAttribute(diagramEClass, DIAGRAM__NAME);
		createEAttribute(diagramEClass, DIAGRAM__TYPE);

		// Create enums
		resultEEnum = createEEnum(RESULT);
		teamEEnum = createEEnum(TEAM);
		championEEnum = createEEnum(CHAMPION);
		laneEEnum = createEEnum(LANE);
		statEEnum = createEEnum(STAT);
		computationEEnum = createEEnum(COMPUTATION);
		valueTypeEEnum = createEEnum(VALUE_TYPE);
		diagramTypeEEnum = createEEnum(DIAGRAM_TYPE);
		timeScaleEEnum = createEEnum(TIME_SCALE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		valueEClass.getESuperTypes().add(this.getComputable());
		championValueEClass.getESuperTypes().add(this.getValue());
		teamValueEClass.getESuperTypes().add(this.getValue());
		resultValueEClass.getESuperTypes().add(this.getValue());
		laneValueEClass.getESuperTypes().add(this.getValue());
		statValueEClass.getESuperTypes().add(this.getValue());
		computedYValueEClass.getESuperTypes().add(this.getYValue());
		sumEClass.getESuperTypes().add(this.getComputedYValue());
		averageEClass.getESuperTypes().add(this.getComputedYValue());
		ratioEClass.getESuperTypes().add(this.getComputedYValue());
		typeEClass.getESuperTypes().add(this.getComputable());
		filterByValueEClass.getESuperTypes().add(this.getFilter());

		// Initialize classes, features, and operations; add parameters
		initEClass(summonerEClass, Summoner.class, "Summoner", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSummoner_Name(), ecorePackage.getEString(), "name", null, 0, 1, Summoner.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(requestEClass, Request.class, "Request", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRequest_Summoners(), this.getSummoner(), null, "summoners", null, 1, -1, Request.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequest_Filters(), this.getFilter(), null, "filters", null, 0, -1, Request.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequest_Yvalues(), this.getYValue(), null, "yvalues", null, 0, -1, Request.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequest_Diagram(), this.getDiagram(), null, "diagram", null, 0, 1, Request.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequest_Timelapse(), this.getTimelapse(), null, "timelapse", null, 0, 1, Request.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequest_TimeScale(), this.getTimeScale(), "timeScale", "ALL_THE_TIME", 0, 1, Request.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRequest_XUnit(), this.getValueType(), "xUnit", "NONE", 0, 1, Request.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(filterEClass, Filter.class, "Filter", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timelapseEClass, Timelapse.class, "Timelapse", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimelapse_From(), ecorePackage.getEDate(), "from", null, 0, 1, Timelapse.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimelapse_To(), ecorePackage.getEDate(), "to", null, 0, 1, Timelapse.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueEClass, Value.class, "Value", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(championValueEClass, ChampionValue.class, "ChampionValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getChampionValue_Champion(), this.getChampion(), "champion", null, 0, 1, ChampionValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(teamValueEClass, TeamValue.class, "TeamValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTeamValue_Team(), this.getTeam(), "team", null, 0, 1, TeamValue.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resultValueEClass, ResultValue.class, "ResultValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResultValue_Result(), this.getResult(), "result", null, 0, 1, ResultValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(laneValueEClass, LaneValue.class, "LaneValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLaneValue_Lane(), this.getLane(), "lane", null, 0, 1, LaneValue.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(statValueEClass, StatValue.class, "StatValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStatValue_Stat(), this.getStat(), "stat", null, 0, 1, StatValue.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(yValueEClass, YValue.class, "YValue", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(computedYValueEClass, ComputedYValue.class, "ComputedYValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComputedYValue_Computation(), this.getComputation(), "computation", "SUM", 0, 1,
				ComputedYValue.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getComputedYValue_Value(), this.getComputable(), null, "value", null, 0, 1, ComputedYValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sumEClass, Sum.class, "Sum", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(averageEClass, Average.class, "Average", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(ratioEClass, Ratio.class, "Ratio", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(typeEClass, Type.class, "Type", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(computableEClass, Computable.class, "Computable", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getComputable_Type(), this.getValueType(), "type", null, 0, 1, Computable.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(filterByValueEClass, FilterByValue.class, "FilterByValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFilterByValue_Value(), this.getValue(), null, "value", null, 0, 1, FilterByValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(diagramEClass, Diagram.class, "Diagram", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDiagram_Name(), ecorePackage.getEString(), "name", null, 0, 1, Diagram.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDiagram_Type(), this.getDiagramType(), "type", null, 0, 1, Diagram.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(resultEEnum, Result.class, "Result");
		addEEnumLiteral(resultEEnum, Result.WIN);
		addEEnumLiteral(resultEEnum, Result.LOSE);

		initEEnum(teamEEnum, Team.class, "Team");
		addEEnumLiteral(teamEEnum, Team.BLUE);
		addEEnumLiteral(teamEEnum, Team.RED);

		initEEnum(championEEnum, Champion.class, "Champion");
		addEEnumLiteral(championEEnum, Champion.ANNIE);
		addEEnumLiteral(championEEnum, Champion.OLAF);
		addEEnumLiteral(championEEnum, Champion.GALIO);
		addEEnumLiteral(championEEnum, Champion.TWISTEDFATE);
		addEEnumLiteral(championEEnum, Champion.XINZHAO);
		addEEnumLiteral(championEEnum, Champion.URGOT);
		addEEnumLiteral(championEEnum, Champion.LEBLANC);
		addEEnumLiteral(championEEnum, Champion.VLADIMIR);
		addEEnumLiteral(championEEnum, Champion.FIDDLESTICKS);
		addEEnumLiteral(championEEnum, Champion.KAYLE);
		addEEnumLiteral(championEEnum, Champion.MASTERYI);
		addEEnumLiteral(championEEnum, Champion.ALISTAR);
		addEEnumLiteral(championEEnum, Champion.RYZE);
		addEEnumLiteral(championEEnum, Champion.SION);
		addEEnumLiteral(championEEnum, Champion.SIVIR);
		addEEnumLiteral(championEEnum, Champion.SORAKA);
		addEEnumLiteral(championEEnum, Champion.TEEMO);
		addEEnumLiteral(championEEnum, Champion.TRISTANA);
		addEEnumLiteral(championEEnum, Champion.WARWICK);
		addEEnumLiteral(championEEnum, Champion.NUNU);
		addEEnumLiteral(championEEnum, Champion.MISSFORTUNE);
		addEEnumLiteral(championEEnum, Champion.ASHE);
		addEEnumLiteral(championEEnum, Champion.TRYNDAMERE);
		addEEnumLiteral(championEEnum, Champion.JAX);
		addEEnumLiteral(championEEnum, Champion.MORGANA);
		addEEnumLiteral(championEEnum, Champion.ZILEAN);
		addEEnumLiteral(championEEnum, Champion.SINGED);
		addEEnumLiteral(championEEnum, Champion.EVELYNN);
		addEEnumLiteral(championEEnum, Champion.TWITCH);
		addEEnumLiteral(championEEnum, Champion.KARTHUS);
		addEEnumLiteral(championEEnum, Champion.CHOGATH);
		addEEnumLiteral(championEEnum, Champion.AMUMU);
		addEEnumLiteral(championEEnum, Champion.RAMMUS);
		addEEnumLiteral(championEEnum, Champion.ANIVIA);
		addEEnumLiteral(championEEnum, Champion.SHACO);
		addEEnumLiteral(championEEnum, Champion.DRMUNDO);
		addEEnumLiteral(championEEnum, Champion.SONA);
		addEEnumLiteral(championEEnum, Champion.KASSADIN);
		addEEnumLiteral(championEEnum, Champion.IRELIA);
		addEEnumLiteral(championEEnum, Champion.JANNA);
		addEEnumLiteral(championEEnum, Champion.GANGPLANK);
		addEEnumLiteral(championEEnum, Champion.CORKI);
		addEEnumLiteral(championEEnum, Champion.KARMA);
		addEEnumLiteral(championEEnum, Champion.TARIC);
		addEEnumLiteral(championEEnum, Champion.VEIGAR);
		addEEnumLiteral(championEEnum, Champion.TRUNDLE);
		addEEnumLiteral(championEEnum, Champion.SWAIN);
		addEEnumLiteral(championEEnum, Champion.CAITLYN);
		addEEnumLiteral(championEEnum, Champion.BLITZCRANK);
		addEEnumLiteral(championEEnum, Champion.MALPHITE);
		addEEnumLiteral(championEEnum, Champion.KATARINA);
		addEEnumLiteral(championEEnum, Champion.NOCTURNE);
		addEEnumLiteral(championEEnum, Champion.MAOKAI);
		addEEnumLiteral(championEEnum, Champion.RENEKTON);
		addEEnumLiteral(championEEnum, Champion.JARVANIV);
		addEEnumLiteral(championEEnum, Champion.ELISE);
		addEEnumLiteral(championEEnum, Champion.ORIANNA);
		addEEnumLiteral(championEEnum, Champion.MONKEYKING);
		addEEnumLiteral(championEEnum, Champion.BRAND);
		addEEnumLiteral(championEEnum, Champion.LEESIN);
		addEEnumLiteral(championEEnum, Champion.VAYNE);
		addEEnumLiteral(championEEnum, Champion.RUMBLE);
		addEEnumLiteral(championEEnum, Champion.CASSIOPEIA);
		addEEnumLiteral(championEEnum, Champion.SKARNER);
		addEEnumLiteral(championEEnum, Champion.HEIMERDINGER);
		addEEnumLiteral(championEEnum, Champion.NASUS);
		addEEnumLiteral(championEEnum, Champion.NIDALEE);
		addEEnumLiteral(championEEnum, Champion.UDYR);
		addEEnumLiteral(championEEnum, Champion.POPPY);
		addEEnumLiteral(championEEnum, Champion.GRAGAS);
		addEEnumLiteral(championEEnum, Champion.PANTHEON);
		addEEnumLiteral(championEEnum, Champion.EZREAL);
		addEEnumLiteral(championEEnum, Champion.MORDEKAISER);
		addEEnumLiteral(championEEnum, Champion.YORICK);
		addEEnumLiteral(championEEnum, Champion.AKALI);
		addEEnumLiteral(championEEnum, Champion.KENNEN);
		addEEnumLiteral(championEEnum, Champion.GAREN);
		addEEnumLiteral(championEEnum, Champion.LEONA);
		addEEnumLiteral(championEEnum, Champion.MALZAHAR);
		addEEnumLiteral(championEEnum, Champion.TALON);
		addEEnumLiteral(championEEnum, Champion.RIVEN);
		addEEnumLiteral(championEEnum, Champion.KOGMAW);
		addEEnumLiteral(championEEnum, Champion.SHEN);
		addEEnumLiteral(championEEnum, Champion.LUX);
		addEEnumLiteral(championEEnum, Champion.XERATH);
		addEEnumLiteral(championEEnum, Champion.SHYVANA);
		addEEnumLiteral(championEEnum, Champion.AHRI);
		addEEnumLiteral(championEEnum, Champion.GRAVES);
		addEEnumLiteral(championEEnum, Champion.FIZZ);
		addEEnumLiteral(championEEnum, Champion.VOLIBEAR);
		addEEnumLiteral(championEEnum, Champion.RENGAR);
		addEEnumLiteral(championEEnum, Champion.VARUS);
		addEEnumLiteral(championEEnum, Champion.NAUTILUS);
		addEEnumLiteral(championEEnum, Champion.VIKTOR);
		addEEnumLiteral(championEEnum, Champion.SEJUANI);
		addEEnumLiteral(championEEnum, Champion.FIORA);
		addEEnumLiteral(championEEnum, Champion.ZIGGS);
		addEEnumLiteral(championEEnum, Champion.LULU);
		addEEnumLiteral(championEEnum, Champion.DRAVEN);
		addEEnumLiteral(championEEnum, Champion.HECARIM);
		addEEnumLiteral(championEEnum, Champion.KHAZIX);
		addEEnumLiteral(championEEnum, Champion.DARIUS);
		addEEnumLiteral(championEEnum, Champion.JAYCE);
		addEEnumLiteral(championEEnum, Champion.LISSANDRA);
		addEEnumLiteral(championEEnum, Champion.DIANA);
		addEEnumLiteral(championEEnum, Champion.QUINN);
		addEEnumLiteral(championEEnum, Champion.SYNDRA);
		addEEnumLiteral(championEEnum, Champion.AURELIONSOL);
		addEEnumLiteral(championEEnum, Champion.KAYN);
		addEEnumLiteral(championEEnum, Champion.ZOE);
		addEEnumLiteral(championEEnum, Champion.ZYRA);
		addEEnumLiteral(championEEnum, Champion.KAISA);
		addEEnumLiteral(championEEnum, Champion.GNAR);
		addEEnumLiteral(championEEnum, Champion.ZAC);
		addEEnumLiteral(championEEnum, Champion.YASUO);
		addEEnumLiteral(championEEnum, Champion.VELKOZ);
		addEEnumLiteral(championEEnum, Champion.TALIYAH);
		addEEnumLiteral(championEEnum, Champion.CAMILLE);
		addEEnumLiteral(championEEnum, Champion.BRAUM);
		addEEnumLiteral(championEEnum, Champion.JHIN);
		addEEnumLiteral(championEEnum, Champion.KINDRED);
		addEEnumLiteral(championEEnum, Champion.JINX);
		addEEnumLiteral(championEEnum, Champion.TAHMKENCH);
		addEEnumLiteral(championEEnum, Champion.LUCIAN);
		addEEnumLiteral(championEEnum, Champion.ZED);
		addEEnumLiteral(championEEnum, Champion.KLED);
		addEEnumLiteral(championEEnum, Champion.EKKO);
		addEEnumLiteral(championEEnum, Champion.VI);
		addEEnumLiteral(championEEnum, Champion.AATROX);
		addEEnumLiteral(championEEnum, Champion.NAMI);
		addEEnumLiteral(championEEnum, Champion.AZIR);
		addEEnumLiteral(championEEnum, Champion.THRESH);
		addEEnumLiteral(championEEnum, Champion.ILLAOI);
		addEEnumLiteral(championEEnum, Champion.REKSAI);
		addEEnumLiteral(championEEnum, Champion.IVERN);
		addEEnumLiteral(championEEnum, Champion.KALISTA);
		addEEnumLiteral(championEEnum, Champion.BARD);
		addEEnumLiteral(championEEnum, Champion.RAKAN);
		addEEnumLiteral(championEEnum, Champion.XAYAH);
		addEEnumLiteral(championEEnum, Champion.ORNN);

		initEEnum(laneEEnum, Lane.class, "Lane");
		addEEnumLiteral(laneEEnum, Lane.BOT);
		addEEnumLiteral(laneEEnum, Lane.TOP);
		addEEnumLiteral(laneEEnum, Lane.MID);
		addEEnumLiteral(laneEEnum, Lane.JUNGLE);

		initEEnum(statEEnum, Stat.class, "Stat");
		addEEnumLiteral(statEEnum, Stat.KILLS);
		addEEnumLiteral(statEEnum, Stat.DEATHS);
		addEEnumLiteral(statEEnum, Stat.ASSISTS);
		addEEnumLiteral(statEEnum, Stat.DOUBLE_KILLS);
		addEEnumLiteral(statEEnum, Stat.TRIPLE_KILLS);
		addEEnumLiteral(statEEnum, Stat.QUADRA_KILLS);
		addEEnumLiteral(statEEnum, Stat.PENTA_KILLS);
		addEEnumLiteral(statEEnum, Stat.TOTAL_DAMAGE_DEALT);
		addEEnumLiteral(statEEnum, Stat.MAGIC_DAMAGE_DEALT);
		addEEnumLiteral(statEEnum, Stat.PHYSICAL_DAMAGE_DEALT);
		addEEnumLiteral(statEEnum, Stat.TRUE_DAMAGE_DEALT);
		addEEnumLiteral(statEEnum, Stat.TOTAL_DAMAGE_DEALT_TO_CHAMPIONS);
		addEEnumLiteral(statEEnum, Stat.MAGIC_DAMAGE_DEALT_TO_CHAMPIONS);
		addEEnumLiteral(statEEnum, Stat.PHYSICAL_DAMAGE_DEALT_TO_CHAMPIONS);
		addEEnumLiteral(statEEnum, Stat.TRUE_DAMAGE_DEALT_TO_CHAMPIONS);
		addEEnumLiteral(statEEnum, Stat.TOTAL_HEAL);
		addEEnumLiteral(statEEnum, Stat.DAMAGE_SELF_MITIGATED);
		addEEnumLiteral(statEEnum, Stat.DAMAGE_DEALT_TO_OBJECTIVES);
		addEEnumLiteral(statEEnum, Stat.DAMAGE_DEALT_TO_TURRETS);
		addEEnumLiteral(statEEnum, Stat.TIME_CCING_OTHERS);
		addEEnumLiteral(statEEnum, Stat.TOTAL_DAMAGE_TAKEN);
		addEEnumLiteral(statEEnum, Stat.MAGICAL_DAMAGE_TAKEN);
		addEEnumLiteral(statEEnum, Stat.PHYSICAL_DAMAGE_TAKEN);
		addEEnumLiteral(statEEnum, Stat.TRUE_DAMAGE_TAKEN);
		addEEnumLiteral(statEEnum, Stat.GOLD_EARNED);
		addEEnumLiteral(statEEnum, Stat.GOLD_SPENT);
		addEEnumLiteral(statEEnum, Stat.TURRET_KILLS);
		addEEnumLiteral(statEEnum, Stat.INHIBITOR_KILLS);
		addEEnumLiteral(statEEnum, Stat.TOTAL_MINIONS_KILLED);
		addEEnumLiteral(statEEnum, Stat.NEUTRAL_MINIONS_KILLED);
		addEEnumLiteral(statEEnum, Stat.NEUTRAL_MINIONS_KILLED_TEAM_JUNGLE);
		addEEnumLiteral(statEEnum, Stat.NEUTRAL_MINIONS_KILLED_ENEMY_JUNGLE);
		addEEnumLiteral(statEEnum, Stat.TOTAL_TIME_CROWD_CONTROL_DEALT);
		addEEnumLiteral(statEEnum, Stat.VISION_WARDS_BOUGHT_IN_GAME);
		addEEnumLiteral(statEEnum, Stat.WARDS_PLACED);
		addEEnumLiteral(statEEnum, Stat.WARDS_KILLED);
		addEEnumLiteral(statEEnum, Stat.FIRST_BLOOD_KILL);
		addEEnumLiteral(statEEnum, Stat.FIRST_BLOOD_ASSIST);
		addEEnumLiteral(statEEnum, Stat.FIRST_TOWER_KILL);
		addEEnumLiteral(statEEnum, Stat.FIRST_TOWER_ASSIST);
		addEEnumLiteral(statEEnum, Stat.FIRST_INHIBITOR_KILL);
		addEEnumLiteral(statEEnum, Stat.FIRST_INHIBITOR_ASSIST);

		initEEnum(computationEEnum, Computation.class, "Computation");
		addEEnumLiteral(computationEEnum, Computation.AVERAGE);
		addEEnumLiteral(computationEEnum, Computation.SUM);
		addEEnumLiteral(computationEEnum, Computation.PERCENTAGE);

		initEEnum(valueTypeEEnum, ValueType.class, "ValueType");
		addEEnumLiteral(valueTypeEEnum, ValueType.LANE);
		addEEnumLiteral(valueTypeEEnum, ValueType.GAME_RESULT);
		addEEnumLiteral(valueTypeEEnum, ValueType.CHAMPION);
		addEEnumLiteral(valueTypeEEnum, ValueType.TEAM);
		addEEnumLiteral(valueTypeEEnum, ValueType.NONE);

		initEEnum(diagramTypeEEnum, DiagramType.class, "DiagramType");
		addEEnumLiteral(diagramTypeEEnum, DiagramType.PIE_CHART);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.PIE_CHART_3D);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.LINE_CHART);
		addEEnumLiteral(diagramTypeEEnum, DiagramType.BAR_CHART);

		initEEnum(timeScaleEEnum, TimeScale.class, "TimeScale");
		addEEnumLiteral(timeScaleEEnum, TimeScale.DAY);
		addEEnumLiteral(timeScaleEEnum, TimeScale.MONTH);
		addEEnumLiteral(timeScaleEEnum, TimeScale.YEAR);
		addEEnumLiteral(timeScaleEEnum, TimeScale.ALL_THE_TIME);

		// Create resource
		createResource(eNS_URI);
	}

} //LoePackageImpl
