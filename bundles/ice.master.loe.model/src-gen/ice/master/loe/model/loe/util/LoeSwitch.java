/**
 */
package ice.master.loe.model.loe.util;

import ice.master.loe.model.loe.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see ice.master.loe.model.loe.LoePackage
 * @generated
 */
public class LoeSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LoePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LoeSwitch() {
		if (modelPackage == null) {
			modelPackage = LoePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case LoePackage.SUMMONER: {
			Summoner summoner = (Summoner) theEObject;
			T result = caseSummoner(summoner);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.REQUEST: {
			Request request = (Request) theEObject;
			T result = caseRequest(request);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.FILTER: {
			Filter filter = (Filter) theEObject;
			T result = caseFilter(filter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.TIMELAPSE: {
			Timelapse timelapse = (Timelapse) theEObject;
			T result = caseTimelapse(timelapse);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.VALUE: {
			Value value = (Value) theEObject;
			T result = caseValue(value);
			if (result == null)
				result = caseComputable(value);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.CHAMPION_VALUE: {
			ChampionValue championValue = (ChampionValue) theEObject;
			T result = caseChampionValue(championValue);
			if (result == null)
				result = caseValue(championValue);
			if (result == null)
				result = caseComputable(championValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.TEAM_VALUE: {
			TeamValue teamValue = (TeamValue) theEObject;
			T result = caseTeamValue(teamValue);
			if (result == null)
				result = caseValue(teamValue);
			if (result == null)
				result = caseComputable(teamValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.RESULT_VALUE: {
			ResultValue resultValue = (ResultValue) theEObject;
			T result = caseResultValue(resultValue);
			if (result == null)
				result = caseValue(resultValue);
			if (result == null)
				result = caseComputable(resultValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.LANE_VALUE: {
			LaneValue laneValue = (LaneValue) theEObject;
			T result = caseLaneValue(laneValue);
			if (result == null)
				result = caseValue(laneValue);
			if (result == null)
				result = caseComputable(laneValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.STAT_VALUE: {
			StatValue statValue = (StatValue) theEObject;
			T result = caseStatValue(statValue);
			if (result == null)
				result = caseValue(statValue);
			if (result == null)
				result = caseComputable(statValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.YVALUE: {
			YValue yValue = (YValue) theEObject;
			T result = caseYValue(yValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.COMPUTED_YVALUE: {
			ComputedYValue computedYValue = (ComputedYValue) theEObject;
			T result = caseComputedYValue(computedYValue);
			if (result == null)
				result = caseYValue(computedYValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.SUM: {
			Sum sum = (Sum) theEObject;
			T result = caseSum(sum);
			if (result == null)
				result = caseComputedYValue(sum);
			if (result == null)
				result = caseYValue(sum);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.AVERAGE: {
			Average average = (Average) theEObject;
			T result = caseAverage(average);
			if (result == null)
				result = caseComputedYValue(average);
			if (result == null)
				result = caseYValue(average);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.RATIO: {
			Ratio ratio = (Ratio) theEObject;
			T result = caseRatio(ratio);
			if (result == null)
				result = caseComputedYValue(ratio);
			if (result == null)
				result = caseYValue(ratio);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.TYPE: {
			Type type = (Type) theEObject;
			T result = caseType(type);
			if (result == null)
				result = caseComputable(type);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.COMPUTABLE: {
			Computable computable = (Computable) theEObject;
			T result = caseComputable(computable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.FILTER_BY_VALUE: {
			FilterByValue filterByValue = (FilterByValue) theEObject;
			T result = caseFilterByValue(filterByValue);
			if (result == null)
				result = caseFilter(filterByValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case LoePackage.DIAGRAM: {
			Diagram diagram = (Diagram) theEObject;
			T result = caseDiagram(diagram);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Summoner</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Summoner</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSummoner(Summoner object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Request</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Request</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequest(Request object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFilter(Filter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Timelapse</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Timelapse</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTimelapse(Timelapse object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseValue(Value object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Champion Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Champion Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChampionValue(ChampionValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Team Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Team Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTeamValue(TeamValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Result Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Result Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResultValue(ResultValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Lane Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Lane Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLaneValue(LaneValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Stat Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Stat Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatValue(StatValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>YValue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>YValue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseYValue(YValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computed YValue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computed YValue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputedYValue(ComputedYValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sum</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sum</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSum(Sum object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Average</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Average</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAverage(Average object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ratio</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ratio</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRatio(Ratio object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseType(Type object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Computable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Computable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComputable(Computable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Filter By Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Filter By Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFilterByValue(FilterByValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Diagram</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Diagram</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiagram(Diagram object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //LoeSwitch
