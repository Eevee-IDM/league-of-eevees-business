/**
 */
package ice.master.loe.model.loe;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see ice.master.loe.model.loe.LoeFactory
 * @model kind="package"
 * @generated
 */
public interface LoePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "loe";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.master.ice/loe";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "loe";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LoePackage eINSTANCE = ice.master.loe.model.loe.impl.LoePackageImpl.init();

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.SummonerImpl <em>Summoner</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.SummonerImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getSummoner()
	 * @generated
	 */
	int SUMMONER = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMONER__NAME = 0;

	/**
	 * The number of structural features of the '<em>Summoner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMONER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Summoner</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUMMONER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.RequestImpl <em>Request</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.RequestImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getRequest()
	 * @generated
	 */
	int REQUEST = 1;

	/**
	 * The feature id for the '<em><b>Summoners</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__SUMMONERS = 0;

	/**
	 * The feature id for the '<em><b>Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__FILTERS = 1;

	/**
	 * The feature id for the '<em><b>Yvalues</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__YVALUES = 2;

	/**
	 * The feature id for the '<em><b>Diagram</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__DIAGRAM = 3;

	/**
	 * The feature id for the '<em><b>Timelapse</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__TIMELAPSE = 4;

	/**
	 * The feature id for the '<em><b>Time Scale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__TIME_SCALE = 5;

	/**
	 * The feature id for the '<em><b>XUnit</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST__XUNIT = 6;

	/**
	 * The number of structural features of the '<em>Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Request</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Filter <em>Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Filter
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getFilter()
	 * @generated
	 */
	int FILTER = 2;

	/**
	 * The number of structural features of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.TimelapseImpl <em>Timelapse</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.TimelapseImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTimelapse()
	 * @generated
	 */
	int TIMELAPSE = 3;

	/**
	 * The feature id for the '<em><b>From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMELAPSE__FROM = 0;

	/**
	 * The feature id for the '<em><b>To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMELAPSE__TO = 1;

	/**
	 * The number of structural features of the '<em>Timelapse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMELAPSE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Timelapse</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMELAPSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.ComputableImpl <em>Computable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.ComputableImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getComputable()
	 * @generated
	 */
	int COMPUTABLE = 16;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTABLE__TYPE = 0;

	/**
	 * The number of structural features of the '<em>Computable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Computable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Value <em>Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Value
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getValue()
	 * @generated
	 */
	int VALUE = 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE__TYPE = COMPUTABLE__TYPE;

	/**
	 * The number of structural features of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_FEATURE_COUNT = COMPUTABLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VALUE_OPERATION_COUNT = COMPUTABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.ChampionValueImpl <em>Champion Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.ChampionValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getChampionValue()
	 * @generated
	 */
	int CHAMPION_VALUE = 5;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAMPION_VALUE__TYPE = VALUE__TYPE;

	/**
	 * The feature id for the '<em><b>Champion</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAMPION_VALUE__CHAMPION = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Champion Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAMPION_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Champion Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAMPION_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.TeamValueImpl <em>Team Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.TeamValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTeamValue()
	 * @generated
	 */
	int TEAM_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_VALUE__TYPE = VALUE__TYPE;

	/**
	 * The feature id for the '<em><b>Team</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_VALUE__TEAM = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Team Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Team Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEAM_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.ResultValueImpl <em>Result Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.ResultValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getResultValue()
	 * @generated
	 */
	int RESULT_VALUE = 7;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VALUE__TYPE = VALUE__TYPE;

	/**
	 * The feature id for the '<em><b>Result</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VALUE__RESULT = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Result Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Result Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESULT_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.LaneValueImpl <em>Lane Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.LaneValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getLaneValue()
	 * @generated
	 */
	int LANE_VALUE = 8;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_VALUE__TYPE = VALUE__TYPE;

	/**
	 * The feature id for the '<em><b>Lane</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_VALUE__LANE = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Lane Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Lane Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LANE_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.StatValueImpl <em>Stat Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.StatValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getStatValue()
	 * @generated
	 */
	int STAT_VALUE = 9;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAT_VALUE__TYPE = VALUE__TYPE;

	/**
	 * The feature id for the '<em><b>Stat</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAT_VALUE__STAT = VALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Stat Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAT_VALUE_FEATURE_COUNT = VALUE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Stat Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STAT_VALUE_OPERATION_COUNT = VALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.YValue <em>YValue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.YValue
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getYValue()
	 * @generated
	 */
	int YVALUE = 10;

	/**
	 * The number of structural features of the '<em>YValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVALUE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>YValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int YVALUE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.ComputedYValueImpl <em>Computed YValue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.ComputedYValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getComputedYValue()
	 * @generated
	 */
	int COMPUTED_YVALUE = 11;

	/**
	 * The feature id for the '<em><b>Computation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_YVALUE__COMPUTATION = YVALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_YVALUE__VALUE = YVALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Computed YValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_YVALUE_FEATURE_COUNT = YVALUE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Computed YValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPUTED_YVALUE_OPERATION_COUNT = YVALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.SumImpl <em>Sum</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.SumImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getSum()
	 * @generated
	 */
	int SUM = 12;

	/**
	 * The feature id for the '<em><b>Computation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUM__COMPUTATION = COMPUTED_YVALUE__COMPUTATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUM__VALUE = COMPUTED_YVALUE__VALUE;

	/**
	 * The number of structural features of the '<em>Sum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUM_FEATURE_COUNT = COMPUTED_YVALUE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Sum</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SUM_OPERATION_COUNT = COMPUTED_YVALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.AverageImpl <em>Average</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.AverageImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getAverage()
	 * @generated
	 */
	int AVERAGE = 13;

	/**
	 * The feature id for the '<em><b>Computation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVERAGE__COMPUTATION = COMPUTED_YVALUE__COMPUTATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVERAGE__VALUE = COMPUTED_YVALUE__VALUE;

	/**
	 * The number of structural features of the '<em>Average</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVERAGE_FEATURE_COUNT = COMPUTED_YVALUE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Average</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVERAGE_OPERATION_COUNT = COMPUTED_YVALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.RatioImpl <em>Ratio</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.RatioImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getRatio()
	 * @generated
	 */
	int RATIO = 14;

	/**
	 * The feature id for the '<em><b>Computation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATIO__COMPUTATION = COMPUTED_YVALUE__COMPUTATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATIO__VALUE = COMPUTED_YVALUE__VALUE;

	/**
	 * The number of structural features of the '<em>Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATIO_FEATURE_COUNT = COMPUTED_YVALUE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Ratio</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RATIO_OPERATION_COUNT = COMPUTED_YVALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.TypeImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getType()
	 * @generated
	 */
	int TYPE = 15;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__TYPE = COMPUTABLE__TYPE;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = COMPUTABLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_OPERATION_COUNT = COMPUTABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.FilterByValueImpl <em>Filter By Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.FilterByValueImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getFilterByValue()
	 * @generated
	 */
	int FILTER_BY_VALUE = 17;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_BY_VALUE__VALUE = FILTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Filter By Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_BY_VALUE_FEATURE_COUNT = FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Filter By Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILTER_BY_VALUE_OPERATION_COUNT = FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.impl.DiagramImpl <em>Diagram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.impl.DiagramImpl
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getDiagram()
	 * @generated
	 */
	int DIAGRAM = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM__TYPE = 1;

	/**
	 * The number of structural features of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Diagram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Result <em>Result</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Result
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getResult()
	 * @generated
	 */
	int RESULT = 19;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Team <em>Team</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Team
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTeam()
	 * @generated
	 */
	int TEAM = 20;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Champion <em>Champion</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Champion
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getChampion()
	 * @generated
	 */
	int CHAMPION = 21;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Lane <em>Lane</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Lane
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getLane()
	 * @generated
	 */
	int LANE = 22;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Stat <em>Stat</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Stat
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getStat()
	 * @generated
	 */
	int STAT = 23;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.Computation <em>Computation</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.Computation
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getComputation()
	 * @generated
	 */
	int COMPUTATION = 24;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.ValueType <em>Value Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.ValueType
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getValueType()
	 * @generated
	 */
	int VALUE_TYPE = 25;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.DiagramType <em>Diagram Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.DiagramType
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getDiagramType()
	 * @generated
	 */
	int DIAGRAM_TYPE = 26;

	/**
	 * The meta object id for the '{@link ice.master.loe.model.loe.TimeScale <em>Time Scale</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see ice.master.loe.model.loe.TimeScale
	 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTimeScale()
	 * @generated
	 */
	int TIME_SCALE = 27;

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Summoner <em>Summoner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Summoner</em>'.
	 * @see ice.master.loe.model.loe.Summoner
	 * @generated
	 */
	EClass getSummoner();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Summoner#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ice.master.loe.model.loe.Summoner#getName()
	 * @see #getSummoner()
	 * @generated
	 */
	EAttribute getSummoner_Name();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Request <em>Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Request</em>'.
	 * @see ice.master.loe.model.loe.Request
	 * @generated
	 */
	EClass getRequest();

	/**
	 * Returns the meta object for the containment reference list '{@link ice.master.loe.model.loe.Request#getSummoners <em>Summoners</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Summoners</em>'.
	 * @see ice.master.loe.model.loe.Request#getSummoners()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Summoners();

	/**
	 * Returns the meta object for the containment reference list '{@link ice.master.loe.model.loe.Request#getFilters <em>Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Filters</em>'.
	 * @see ice.master.loe.model.loe.Request#getFilters()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Filters();

	/**
	 * Returns the meta object for the containment reference list '{@link ice.master.loe.model.loe.Request#getYvalues <em>Yvalues</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Yvalues</em>'.
	 * @see ice.master.loe.model.loe.Request#getYvalues()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Yvalues();

	/**
	 * Returns the meta object for the containment reference '{@link ice.master.loe.model.loe.Request#getDiagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Diagram</em>'.
	 * @see ice.master.loe.model.loe.Request#getDiagram()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Diagram();

	/**
	 * Returns the meta object for the containment reference '{@link ice.master.loe.model.loe.Request#getTimelapse <em>Timelapse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timelapse</em>'.
	 * @see ice.master.loe.model.loe.Request#getTimelapse()
	 * @see #getRequest()
	 * @generated
	 */
	EReference getRequest_Timelapse();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Request#getTimeScale <em>Time Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Scale</em>'.
	 * @see ice.master.loe.model.loe.Request#getTimeScale()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_TimeScale();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Request#getXUnit <em>XUnit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>XUnit</em>'.
	 * @see ice.master.loe.model.loe.Request#getXUnit()
	 * @see #getRequest()
	 * @generated
	 */
	EAttribute getRequest_XUnit();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Filter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter</em>'.
	 * @see ice.master.loe.model.loe.Filter
	 * @generated
	 */
	EClass getFilter();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Timelapse <em>Timelapse</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timelapse</em>'.
	 * @see ice.master.loe.model.loe.Timelapse
	 * @generated
	 */
	EClass getTimelapse();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Timelapse#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From</em>'.
	 * @see ice.master.loe.model.loe.Timelapse#getFrom()
	 * @see #getTimelapse()
	 * @generated
	 */
	EAttribute getTimelapse_From();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Timelapse#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>To</em>'.
	 * @see ice.master.loe.model.loe.Timelapse#getTo()
	 * @see #getTimelapse()
	 * @generated
	 */
	EAttribute getTimelapse_To();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Value <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Value</em>'.
	 * @see ice.master.loe.model.loe.Value
	 * @generated
	 */
	EClass getValue();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.ChampionValue <em>Champion Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Champion Value</em>'.
	 * @see ice.master.loe.model.loe.ChampionValue
	 * @generated
	 */
	EClass getChampionValue();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.ChampionValue#getChampion <em>Champion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Champion</em>'.
	 * @see ice.master.loe.model.loe.ChampionValue#getChampion()
	 * @see #getChampionValue()
	 * @generated
	 */
	EAttribute getChampionValue_Champion();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.TeamValue <em>Team Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Team Value</em>'.
	 * @see ice.master.loe.model.loe.TeamValue
	 * @generated
	 */
	EClass getTeamValue();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.TeamValue#getTeam <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Team</em>'.
	 * @see ice.master.loe.model.loe.TeamValue#getTeam()
	 * @see #getTeamValue()
	 * @generated
	 */
	EAttribute getTeamValue_Team();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.ResultValue <em>Result Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Result Value</em>'.
	 * @see ice.master.loe.model.loe.ResultValue
	 * @generated
	 */
	EClass getResultValue();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.ResultValue#getResult <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Result</em>'.
	 * @see ice.master.loe.model.loe.ResultValue#getResult()
	 * @see #getResultValue()
	 * @generated
	 */
	EAttribute getResultValue_Result();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.LaneValue <em>Lane Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lane Value</em>'.
	 * @see ice.master.loe.model.loe.LaneValue
	 * @generated
	 */
	EClass getLaneValue();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.LaneValue#getLane <em>Lane</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lane</em>'.
	 * @see ice.master.loe.model.loe.LaneValue#getLane()
	 * @see #getLaneValue()
	 * @generated
	 */
	EAttribute getLaneValue_Lane();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.StatValue <em>Stat Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Stat Value</em>'.
	 * @see ice.master.loe.model.loe.StatValue
	 * @generated
	 */
	EClass getStatValue();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.StatValue#getStat <em>Stat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stat</em>'.
	 * @see ice.master.loe.model.loe.StatValue#getStat()
	 * @see #getStatValue()
	 * @generated
	 */
	EAttribute getStatValue_Stat();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.YValue <em>YValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>YValue</em>'.
	 * @see ice.master.loe.model.loe.YValue
	 * @generated
	 */
	EClass getYValue();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.ComputedYValue <em>Computed YValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computed YValue</em>'.
	 * @see ice.master.loe.model.loe.ComputedYValue
	 * @generated
	 */
	EClass getComputedYValue();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.ComputedYValue#getComputation <em>Computation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Computation</em>'.
	 * @see ice.master.loe.model.loe.ComputedYValue#getComputation()
	 * @see #getComputedYValue()
	 * @generated
	 */
	EAttribute getComputedYValue_Computation();

	/**
	 * Returns the meta object for the containment reference '{@link ice.master.loe.model.loe.ComputedYValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see ice.master.loe.model.loe.ComputedYValue#getValue()
	 * @see #getComputedYValue()
	 * @generated
	 */
	EReference getComputedYValue_Value();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Sum <em>Sum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sum</em>'.
	 * @see ice.master.loe.model.loe.Sum
	 * @generated
	 */
	EClass getSum();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Average <em>Average</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Average</em>'.
	 * @see ice.master.loe.model.loe.Average
	 * @generated
	 */
	EClass getAverage();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Ratio <em>Ratio</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ratio</em>'.
	 * @see ice.master.loe.model.loe.Ratio
	 * @generated
	 */
	EClass getRatio();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see ice.master.loe.model.loe.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Computable <em>Computable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Computable</em>'.
	 * @see ice.master.loe.model.loe.Computable
	 * @generated
	 */
	EClass getComputable();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Computable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ice.master.loe.model.loe.Computable#getType()
	 * @see #getComputable()
	 * @generated
	 */
	EAttribute getComputable_Type();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.FilterByValue <em>Filter By Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Filter By Value</em>'.
	 * @see ice.master.loe.model.loe.FilterByValue
	 * @generated
	 */
	EClass getFilterByValue();

	/**
	 * Returns the meta object for the containment reference '{@link ice.master.loe.model.loe.FilterByValue#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see ice.master.loe.model.loe.FilterByValue#getValue()
	 * @see #getFilterByValue()
	 * @generated
	 */
	EReference getFilterByValue_Value();

	/**
	 * Returns the meta object for class '{@link ice.master.loe.model.loe.Diagram <em>Diagram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagram</em>'.
	 * @see ice.master.loe.model.loe.Diagram
	 * @generated
	 */
	EClass getDiagram();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Diagram#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see ice.master.loe.model.loe.Diagram#getName()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Name();

	/**
	 * Returns the meta object for the attribute '{@link ice.master.loe.model.loe.Diagram#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see ice.master.loe.model.loe.Diagram#getType()
	 * @see #getDiagram()
	 * @generated
	 */
	EAttribute getDiagram_Type();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.Result <em>Result</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Result</em>'.
	 * @see ice.master.loe.model.loe.Result
	 * @generated
	 */
	EEnum getResult();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.Team <em>Team</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Team</em>'.
	 * @see ice.master.loe.model.loe.Team
	 * @generated
	 */
	EEnum getTeam();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.Champion <em>Champion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Champion</em>'.
	 * @see ice.master.loe.model.loe.Champion
	 * @generated
	 */
	EEnum getChampion();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.Lane <em>Lane</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Lane</em>'.
	 * @see ice.master.loe.model.loe.Lane
	 * @generated
	 */
	EEnum getLane();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.Stat <em>Stat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Stat</em>'.
	 * @see ice.master.loe.model.loe.Stat
	 * @generated
	 */
	EEnum getStat();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.Computation <em>Computation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Computation</em>'.
	 * @see ice.master.loe.model.loe.Computation
	 * @generated
	 */
	EEnum getComputation();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.ValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Value Type</em>'.
	 * @see ice.master.loe.model.loe.ValueType
	 * @generated
	 */
	EEnum getValueType();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.DiagramType <em>Diagram Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Diagram Type</em>'.
	 * @see ice.master.loe.model.loe.DiagramType
	 * @generated
	 */
	EEnum getDiagramType();

	/**
	 * Returns the meta object for enum '{@link ice.master.loe.model.loe.TimeScale <em>Time Scale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Time Scale</em>'.
	 * @see ice.master.loe.model.loe.TimeScale
	 * @generated
	 */
	EEnum getTimeScale();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LoeFactory getLoeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.SummonerImpl <em>Summoner</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.SummonerImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getSummoner()
		 * @generated
		 */
		EClass SUMMONER = eINSTANCE.getSummoner();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SUMMONER__NAME = eINSTANCE.getSummoner_Name();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.RequestImpl <em>Request</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.RequestImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getRequest()
		 * @generated
		 */
		EClass REQUEST = eINSTANCE.getRequest();

		/**
		 * The meta object literal for the '<em><b>Summoners</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__SUMMONERS = eINSTANCE.getRequest_Summoners();

		/**
		 * The meta object literal for the '<em><b>Filters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__FILTERS = eINSTANCE.getRequest_Filters();

		/**
		 * The meta object literal for the '<em><b>Yvalues</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__YVALUES = eINSTANCE.getRequest_Yvalues();

		/**
		 * The meta object literal for the '<em><b>Diagram</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__DIAGRAM = eINSTANCE.getRequest_Diagram();

		/**
		 * The meta object literal for the '<em><b>Timelapse</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUEST__TIMELAPSE = eINSTANCE.getRequest_Timelapse();

		/**
		 * The meta object literal for the '<em><b>Time Scale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__TIME_SCALE = eINSTANCE.getRequest_TimeScale();

		/**
		 * The meta object literal for the '<em><b>XUnit</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUEST__XUNIT = eINSTANCE.getRequest_XUnit();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Filter <em>Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Filter
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getFilter()
		 * @generated
		 */
		EClass FILTER = eINSTANCE.getFilter();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.TimelapseImpl <em>Timelapse</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.TimelapseImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTimelapse()
		 * @generated
		 */
		EClass TIMELAPSE = eINSTANCE.getTimelapse();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMELAPSE__FROM = eINSTANCE.getTimelapse_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMELAPSE__TO = eINSTANCE.getTimelapse_To();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Value <em>Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Value
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getValue()
		 * @generated
		 */
		EClass VALUE = eINSTANCE.getValue();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.ChampionValueImpl <em>Champion Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.ChampionValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getChampionValue()
		 * @generated
		 */
		EClass CHAMPION_VALUE = eINSTANCE.getChampionValue();

		/**
		 * The meta object literal for the '<em><b>Champion</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHAMPION_VALUE__CHAMPION = eINSTANCE.getChampionValue_Champion();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.TeamValueImpl <em>Team Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.TeamValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTeamValue()
		 * @generated
		 */
		EClass TEAM_VALUE = eINSTANCE.getTeamValue();

		/**
		 * The meta object literal for the '<em><b>Team</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEAM_VALUE__TEAM = eINSTANCE.getTeamValue_Team();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.ResultValueImpl <em>Result Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.ResultValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getResultValue()
		 * @generated
		 */
		EClass RESULT_VALUE = eINSTANCE.getResultValue();

		/**
		 * The meta object literal for the '<em><b>Result</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESULT_VALUE__RESULT = eINSTANCE.getResultValue_Result();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.LaneValueImpl <em>Lane Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.LaneValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getLaneValue()
		 * @generated
		 */
		EClass LANE_VALUE = eINSTANCE.getLaneValue();

		/**
		 * The meta object literal for the '<em><b>Lane</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LANE_VALUE__LANE = eINSTANCE.getLaneValue_Lane();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.StatValueImpl <em>Stat Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.StatValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getStatValue()
		 * @generated
		 */
		EClass STAT_VALUE = eINSTANCE.getStatValue();

		/**
		 * The meta object literal for the '<em><b>Stat</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STAT_VALUE__STAT = eINSTANCE.getStatValue_Stat();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.YValue <em>YValue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.YValue
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getYValue()
		 * @generated
		 */
		EClass YVALUE = eINSTANCE.getYValue();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.ComputedYValueImpl <em>Computed YValue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.ComputedYValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getComputedYValue()
		 * @generated
		 */
		EClass COMPUTED_YVALUE = eINSTANCE.getComputedYValue();

		/**
		 * The meta object literal for the '<em><b>Computation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTED_YVALUE__COMPUTATION = eINSTANCE.getComputedYValue_Computation();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPUTED_YVALUE__VALUE = eINSTANCE.getComputedYValue_Value();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.SumImpl <em>Sum</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.SumImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getSum()
		 * @generated
		 */
		EClass SUM = eINSTANCE.getSum();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.AverageImpl <em>Average</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.AverageImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getAverage()
		 * @generated
		 */
		EClass AVERAGE = eINSTANCE.getAverage();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.RatioImpl <em>Ratio</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.RatioImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getRatio()
		 * @generated
		 */
		EClass RATIO = eINSTANCE.getRatio();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.TypeImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.ComputableImpl <em>Computable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.ComputableImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getComputable()
		 * @generated
		 */
		EClass COMPUTABLE = eINSTANCE.getComputable();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPUTABLE__TYPE = eINSTANCE.getComputable_Type();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.FilterByValueImpl <em>Filter By Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.FilterByValueImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getFilterByValue()
		 * @generated
		 */
		EClass FILTER_BY_VALUE = eINSTANCE.getFilterByValue();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILTER_BY_VALUE__VALUE = eINSTANCE.getFilterByValue_Value();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.impl.DiagramImpl <em>Diagram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.impl.DiagramImpl
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getDiagram()
		 * @generated
		 */
		EClass DIAGRAM = eINSTANCE.getDiagram();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__NAME = eINSTANCE.getDiagram_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DIAGRAM__TYPE = eINSTANCE.getDiagram_Type();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Result <em>Result</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Result
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getResult()
		 * @generated
		 */
		EEnum RESULT = eINSTANCE.getResult();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Team <em>Team</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Team
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTeam()
		 * @generated
		 */
		EEnum TEAM = eINSTANCE.getTeam();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Champion <em>Champion</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Champion
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getChampion()
		 * @generated
		 */
		EEnum CHAMPION = eINSTANCE.getChampion();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Lane <em>Lane</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Lane
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getLane()
		 * @generated
		 */
		EEnum LANE = eINSTANCE.getLane();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Stat <em>Stat</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Stat
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getStat()
		 * @generated
		 */
		EEnum STAT = eINSTANCE.getStat();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.Computation <em>Computation</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.Computation
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getComputation()
		 * @generated
		 */
		EEnum COMPUTATION = eINSTANCE.getComputation();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.ValueType <em>Value Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.ValueType
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getValueType()
		 * @generated
		 */
		EEnum VALUE_TYPE = eINSTANCE.getValueType();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.DiagramType <em>Diagram Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.DiagramType
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getDiagramType()
		 * @generated
		 */
		EEnum DIAGRAM_TYPE = eINSTANCE.getDiagramType();

		/**
		 * The meta object literal for the '{@link ice.master.loe.model.loe.TimeScale <em>Time Scale</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see ice.master.loe.model.loe.TimeScale
		 * @see ice.master.loe.model.loe.impl.LoePackageImpl#getTimeScale()
		 * @generated
		 */
		EEnum TIME_SCALE = eINSTANCE.getTimeScale();

	}

} //LoePackage
