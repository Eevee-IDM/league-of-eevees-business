/**
 */
package ice.master.loe.model.loe;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Champion</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see ice.master.loe.model.loe.LoePackage#getChampion()
 * @model
 * @generated
 */
public enum Champion implements Enumerator {
	/**
	 * The '<em><b>ANNIE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ANNIE_VALUE
	 * @generated
	 * @ordered
	 */
	ANNIE(1, "ANNIE", "ANNIE"),
	/**
	 * The '<em><b>OLAF</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #OLAF_VALUE
	 * @generated
	 * @ordered
	 */
	OLAF(2, "OLAF", "OLAF"),
	/**
	 * The '<em><b>GALIO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GALIO_VALUE
	 * @generated
	 * @ordered
	 */
	GALIO(3, "GALIO", "GALIO"),
	/**
	 * The '<em><b>TWISTEDFATE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TWISTEDFATE_VALUE
	 * @generated
	 * @ordered
	 */
	TWISTEDFATE(4, "TWISTEDFATE", "TWISTEDFATE"),
	/**
	 * The '<em><b>XINZHAO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #XINZHAO_VALUE
	 * @generated
	 * @ordered
	 */
	XINZHAO(5, "XINZHAO", "XINZHAO"),
	/**
	 * The '<em><b>URGOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #URGOT_VALUE
	 * @generated
	 * @ordered
	 */
	URGOT(6, "URGOT", "URGOT"),
	/**
	 * The '<em><b>LEBLANC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LEBLANC_VALUE
	 * @generated
	 * @ordered
	 */
	LEBLANC(7, "LEBLANC", "LEBLANC"),
	/**
	 * The '<em><b>VLADIMIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VLADIMIR_VALUE
	 * @generated
	 * @ordered
	 */
	VLADIMIR(8, "VLADIMIR", "VLADIMIR"),
	/**
	 * The '<em><b>FIDDLESTICKS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIDDLESTICKS_VALUE
	 * @generated
	 * @ordered
	 */
	FIDDLESTICKS(9, "FIDDLESTICKS", "FIDDLESTICKS"),
	/**
	 * The '<em><b>KAYLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KAYLE_VALUE
	 * @generated
	 * @ordered
	 */
	KAYLE(10, "KAYLE", "KAYLE"),
	/**
	 * The '<em><b>MASTERYI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MASTERYI_VALUE
	 * @generated
	 * @ordered
	 */
	MASTERYI(11, "MASTERYI", "MASTERYI"),
	/**
	 * The '<em><b>ALISTAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ALISTAR_VALUE
	 * @generated
	 * @ordered
	 */
	ALISTAR(12, "ALISTAR", "ALISTAR"),
	/**
	 * The '<em><b>RYZE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RYZE_VALUE
	 * @generated
	 * @ordered
	 */
	RYZE(13, "RYZE", "RYZE"),
	/**
	 * The '<em><b>SION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SION_VALUE
	 * @generated
	 * @ordered
	 */
	SION(14, "SION", "SION"),
	/**
	 * The '<em><b>SIVIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SIVIR_VALUE
	 * @generated
	 * @ordered
	 */
	SIVIR(15, "SIVIR", "SIVIR"),
	/**
	 * The '<em><b>SORAKA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SORAKA_VALUE
	 * @generated
	 * @ordered
	 */
	SORAKA(16, "SORAKA", "SORAKA"),
	/**
	 * The '<em><b>TEEMO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TEEMO_VALUE
	 * @generated
	 * @ordered
	 */
	TEEMO(17, "TEEMO", "TEEMO"),
	/**
	 * The '<em><b>TRISTANA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRISTANA_VALUE
	 * @generated
	 * @ordered
	 */
	TRISTANA(18, "TRISTANA", "TRISTANA"),
	/**
	 * The '<em><b>WARWICK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #WARWICK_VALUE
	 * @generated
	 * @ordered
	 */
	WARWICK(19, "WARWICK", "WARWICK"),
	/**
	 * The '<em><b>NUNU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NUNU_VALUE
	 * @generated
	 * @ordered
	 */
	NUNU(20, "NUNU", "NUNU"),
	/**
	 * The '<em><b>MISSFORTUNE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MISSFORTUNE_VALUE
	 * @generated
	 * @ordered
	 */
	MISSFORTUNE(21, "MISSFORTUNE", "MISSFORTUNE"),
	/**
	 * The '<em><b>ASHE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ASHE_VALUE
	 * @generated
	 * @ordered
	 */
	ASHE(22, "ASHE", "ASHE"),
	/**
	 * The '<em><b>TRYNDAMERE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRYNDAMERE_VALUE
	 * @generated
	 * @ordered
	 */
	TRYNDAMERE(23, "TRYNDAMERE", "TRYNDAMERE"),
	/**
	 * The '<em><b>JAX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #JAX_VALUE
	 * @generated
	 * @ordered
	 */
	JAX(24, "JAX", "JAX"),
	/**
	 * The '<em><b>MORGANA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MORGANA_VALUE
	 * @generated
	 * @ordered
	 */
	MORGANA(25, "MORGANA", "MORGANA"),
	/**
	 * The '<em><b>ZILEAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ZILEAN_VALUE
	 * @generated
	 * @ordered
	 */
	ZILEAN(26, "ZILEAN", "ZILEAN"),
	/**
	 * The '<em><b>SINGED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SINGED_VALUE
	 * @generated
	 * @ordered
	 */
	SINGED(27, "SINGED", "SINGED"),
	/**
	 * The '<em><b>EVELYNN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #EVELYNN_VALUE
	 * @generated
	 * @ordered
	 */
	EVELYNN(28, "EVELYNN", "EVELYNN"),
	/**
	 * The '<em><b>TWITCH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TWITCH_VALUE
	 * @generated
	 * @ordered
	 */
	TWITCH(29, "TWITCH", "TWITCH"),
	/**
	 * The '<em><b>KARTHUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KARTHUS_VALUE
	 * @generated
	 * @ordered
	 */
	KARTHUS(30, "KARTHUS", "KARTHUS"),
	/**
	 * The '<em><b>CHOGATH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CHOGATH_VALUE
	 * @generated
	 * @ordered
	 */
	CHOGATH(31, "CHOGATH", "CHOGATH"),
	/**
	 * The '<em><b>AMUMU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #AMUMU_VALUE
	 * @generated
	 * @ordered
	 */
	AMUMU(32, "AMUMU", "AMUMU"),
	/**
	 * The '<em><b>RAMMUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RAMMUS_VALUE
	 * @generated
	 * @ordered
	 */
	RAMMUS(33, "RAMMUS", "RAMMUS"),
	/**
	 * The '<em><b>ANIVIA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ANIVIA_VALUE
	 * @generated
	 * @ordered
	 */
	ANIVIA(34, "ANIVIA", "ANIVIA"),
	/**
	 * The '<em><b>SHACO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SHACO_VALUE
	 * @generated
	 * @ordered
	 */
	SHACO(35, "SHACO", "SHACO"),
	/**
	 * The '<em><b>DRMUNDO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DRMUNDO_VALUE
	 * @generated
	 * @ordered
	 */
	DRMUNDO(36, "DRMUNDO", "DRMUNDO"),
	/**
	 * The '<em><b>SONA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SONA_VALUE
	 * @generated
	 * @ordered
	 */
	SONA(37, "SONA", "SONA"),
	/**
	 * The '<em><b>KASSADIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KASSADIN_VALUE
	 * @generated
	 * @ordered
	 */
	KASSADIN(38, "KASSADIN", "KASSADIN"),
	/**
	 * The '<em><b>IRELIA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #IRELIA_VALUE
	 * @generated
	 * @ordered
	 */
	IRELIA(39, "IRELIA", "IRELIA"),
	/**
	 * The '<em><b>JANNA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #JANNA_VALUE
	 * @generated
	 * @ordered
	 */
	JANNA(40, "JANNA", "JANNA"),
	/**
	 * The '<em><b>GANGPLANK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GANGPLANK_VALUE
	 * @generated
	 * @ordered
	 */
	GANGPLANK(41, "GANGPLANK", "GANGPLANK"),
	/**
	 * The '<em><b>CORKI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CORKI_VALUE
	 * @generated
	 * @ordered
	 */
	CORKI(42, "CORKI", "CORKI"),
	/**
	 * The '<em><b>KARMA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KARMA_VALUE
	 * @generated
	 * @ordered
	 */
	KARMA(43, "KARMA", "KARMA"),
	/**
	 * The '<em><b>TARIC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TARIC_VALUE
	 * @generated
	 * @ordered
	 */
	TARIC(44, "TARIC", "TARIC"),
	/**
	 * The '<em><b>VEIGAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VEIGAR_VALUE
	 * @generated
	 * @ordered
	 */
	VEIGAR(45, "VEIGAR", "VEIGAR"),
	/**
	 * The '<em><b>TRUNDLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TRUNDLE_VALUE
	 * @generated
	 * @ordered
	 */
	TRUNDLE(48, "TRUNDLE", "TRUNDLE"),
	/**
	 * The '<em><b>SWAIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SWAIN_VALUE
	 * @generated
	 * @ordered
	 */
	SWAIN(50, "SWAIN", "SWAIN"),
	/**
	 * The '<em><b>CAITLYN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CAITLYN_VALUE
	 * @generated
	 * @ordered
	 */
	CAITLYN(51, "CAITLYN", "CAITLYN"),

	/**
	 * The '<em><b>BLITZCRANK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BLITZCRANK_VALUE
	 * @generated
	 * @ordered
	 */
	BLITZCRANK(53, "BLITZCRANK", "BLITZCRANK"),
	/**
	 * The '<em><b>MALPHITE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MALPHITE_VALUE
	 * @generated
	 * @ordered
	 */
	MALPHITE(54, "MALPHITE", "MALPHITE"),
	/**
	 * The '<em><b>KATARINA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KATARINA_VALUE
	 * @generated
	 * @ordered
	 */
	KATARINA(55, "KATARINA", "KATARINA"),
	/**
	 * The '<em><b>NOCTURNE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NOCTURNE_VALUE
	 * @generated
	 * @ordered
	 */
	NOCTURNE(56, "NOCTURNE", "NOCTURNE"),
	/**
	 * The '<em><b>MAOKAI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MAOKAI_VALUE
	 * @generated
	 * @ordered
	 */
	MAOKAI(57, "MAOKAI", "MAOKAI"),
	/**
	 * The '<em><b>RENEKTON</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RENEKTON_VALUE
	 * @generated
	 * @ordered
	 */
	RENEKTON(58, "RENEKTON", "RENEKTON"),
	/**
	 * The '<em><b>JARVANIV</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #JARVANIV_VALUE
	 * @generated
	 * @ordered
	 */
	JARVANIV(59, "JARVANIV", "JARVANIV"),
	/**
	 * The '<em><b>ELISE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ELISE_VALUE
	 * @generated
	 * @ordered
	 */
	ELISE(60, "ELISE", "ELISE"),
	/**
	 * The '<em><b>ORIANNA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ORIANNA_VALUE
	 * @generated
	 * @ordered
	 */
	ORIANNA(61, "ORIANNA", "ORIANNA"),
	/**
	 * The '<em><b>MONKEYKING</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MONKEYKING_VALUE
	 * @generated
	 * @ordered
	 */
	MONKEYKING(62, "MONKEYKING", "MONKEYKING"),
	/**
	 * The '<em><b>BRAND</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #BRAND_VALUE
	 * @generated
	 * @ordered
	 */
	BRAND(63, "BRAND", "BRAND"),
	/**
	 * The '<em><b>LEESIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LEESIN_VALUE
	 * @generated
	 * @ordered
	 */
	LEESIN(64, "LEESIN", "LEESIN"),
	/**
	 * The '<em><b>VAYNE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VAYNE_VALUE
	 * @generated
	 * @ordered
	 */
	VAYNE(67, "VAYNE", "VAYNE"),
	/**
	 * The '<em><b>RUMBLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RUMBLE_VALUE
	 * @generated
	 * @ordered
	 */
	RUMBLE(68, "RUMBLE", "RUMBLE"),
	/**
	 * The '<em><b>CASSIOPEIA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CASSIOPEIA_VALUE
	 * @generated
	 * @ordered
	 */
	CASSIOPEIA(69, "CASSIOPEIA", "CASSIOPEIA"),
	/**
	 * The '<em><b>SKARNER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SKARNER_VALUE
	 * @generated
	 * @ordered
	 */
	SKARNER(72, "SKARNER", "SKARNER"),
	/**
	 * The '<em><b>HEIMERDINGER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #HEIMERDINGER_VALUE
	 * @generated
	 * @ordered
	 */
	HEIMERDINGER(74, "HEIMERDINGER", "HEIMERDINGER"),
	/**
	 * The '<em><b>NASUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NASUS_VALUE
	 * @generated
	 * @ordered
	 */
	NASUS(75, "NASUS", "NASUS"),
	/**
	 * The '<em><b>NIDALEE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NIDALEE_VALUE
	 * @generated
	 * @ordered
	 */
	NIDALEE(76, "NIDALEE", "NIDALEE"),
	/**
	 * The '<em><b>UDYR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #UDYR_VALUE
	 * @generated
	 * @ordered
	 */
	UDYR(77, "UDYR", "UDYR"),
	/**
	 * The '<em><b>POPPY</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #POPPY_VALUE
	 * @generated
	 * @ordered
	 */
	POPPY(78, "POPPY", "POPPY"),
	/**
	 * The '<em><b>GRAGAS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GRAGAS_VALUE
	 * @generated
	 * @ordered
	 */
	GRAGAS(79, "GRAGAS", "GRAGAS"),
	/**
	 * The '<em><b>PANTHEON</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #PANTHEON_VALUE
	 * @generated
	 * @ordered
	 */
	PANTHEON(80, "PANTHEON", "PANTHEON"),
	/**
	 * The '<em><b>EZREAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #EZREAL_VALUE
	 * @generated
	 * @ordered
	 */
	EZREAL(81, "EZREAL", "EZREAL"),
	/**
	 * The '<em><b>MORDEKAISER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MORDEKAISER_VALUE
	 * @generated
	 * @ordered
	 */
	MORDEKAISER(82, "MORDEKAISER", "MORDEKAISER"),
	/**
	 * The '<em><b>YORICK</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #YORICK_VALUE
	 * @generated
	 * @ordered
	 */
	YORICK(83, "YORICK", "YORICK"),
	/**
	 * The '<em><b>AKALI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #AKALI_VALUE
	 * @generated
	 * @ordered
	 */
	AKALI(84, "AKALI", "AKALI"),
	/**
	 * The '<em><b>KENNEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KENNEN_VALUE
	 * @generated
	 * @ordered
	 */
	KENNEN(85, "KENNEN", "KENNEN"),
	/**
	 * The '<em><b>GAREN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GAREN_VALUE
	 * @generated
	 * @ordered
	 */
	GAREN(86, "GAREN", "GAREN"),
	/**
	 * The '<em><b>LEONA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LEONA_VALUE
	 * @generated
	 * @ordered
	 */
	LEONA(89, "LEONA", "LEONA"),
	/**
	 * The '<em><b>MALZAHAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #MALZAHAR_VALUE
	 * @generated
	 * @ordered
	 */
	MALZAHAR(90, "MALZAHAR", "MALZAHAR"),
	/**
	 * The '<em><b>TALON</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TALON_VALUE
	 * @generated
	 * @ordered
	 */
	TALON(91, "TALON", "TALON"),
	/**
	 * The '<em><b>RIVEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RIVEN_VALUE
	 * @generated
	 * @ordered
	 */
	RIVEN(92, "RIVEN", "RIVEN"),
	/**
	 * The '<em><b>KOGMAW</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KOGMAW_VALUE
	 * @generated
	 * @ordered
	 */
	KOGMAW(96, "KOGMAW", "KOGMAW"),
	/**
	 * The '<em><b>SHEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SHEN_VALUE
	 * @generated
	 * @ordered
	 */
	SHEN(98, "SHEN", "SHEN"),
	/**
	 * The '<em><b>LUX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LUX_VALUE
	 * @generated
	 * @ordered
	 */
	LUX(99, "LUX", "LUX"),
	/**
	 * The '<em><b>XERATH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #XERATH_VALUE
	 * @generated
	 * @ordered
	 */
	XERATH(101, "XERATH", "XERATH"),
	/**
	 * The '<em><b>SHYVANA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SHYVANA_VALUE
	 * @generated
	 * @ordered
	 */
	SHYVANA(102, "SHYVANA", "SHYVANA"),
	/**
	 * The '<em><b>AHRI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #AHRI_VALUE
	 * @generated
	 * @ordered
	 */
	AHRI(103, "AHRI", "AHRI"),
	/**
	 * The '<em><b>GRAVES</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GRAVES_VALUE
	 * @generated
	 * @ordered
	 */
	GRAVES(104, "GRAVES", "GRAVES"),
	/**
	 * The '<em><b>FIZZ</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIZZ_VALUE
	 * @generated
	 * @ordered
	 */
	FIZZ(105, "FIZZ", "FIZZ"),
	/**
	 * The '<em><b>VOLIBEAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VOLIBEAR_VALUE
	 * @generated
	 * @ordered
	 */
	VOLIBEAR(106, "VOLIBEAR", "VOLIBEAR"),
	/**
	 * The '<em><b>RENGAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RENGAR_VALUE
	 * @generated
	 * @ordered
	 */
	RENGAR(107, "RENGAR", "RENGAR"),
	/**
	 * The '<em><b>VARUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VARUS_VALUE
	 * @generated
	 * @ordered
	 */
	VARUS(110, "VARUS", "VARUS"),
	/**
	 * The '<em><b>NAUTILUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NAUTILUS_VALUE
	 * @generated
	 * @ordered
	 */
	NAUTILUS(111, "NAUTILUS", "NAUTILUS"),
	/**
	 * The '<em><b>VIKTOR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VIKTOR_VALUE
	 * @generated
	 * @ordered
	 */
	VIKTOR(112, "VIKTOR", "VIKTOR"),
	/**
	 * The '<em><b>SEJUANI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SEJUANI_VALUE
	 * @generated
	 * @ordered
	 */
	SEJUANI(113, "SEJUANI", "SEJUANI"),
	/**
	 * The '<em><b>FIORA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #FIORA_VALUE
	 * @generated
	 * @ordered
	 */
	FIORA(114, "FIORA", "FIORA"),
	/**
	 * The '<em><b>ZIGGS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ZIGGS_VALUE
	 * @generated
	 * @ordered
	 */
	ZIGGS(115, "ZIGGS", "ZIGGS"),
	/**
	 * The '<em><b>LULU</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LULU_VALUE
	 * @generated
	 * @ordered
	 */
	LULU(117, "LULU", "LULU"),
	/**
	 * The '<em><b>DRAVEN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DRAVEN_VALUE
	 * @generated
	 * @ordered
	 */
	DRAVEN(119, "DRAVEN", "DRAVEN"),
	/**
	 * The '<em><b>HECARIM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #HECARIM_VALUE
	 * @generated
	 * @ordered
	 */
	HECARIM(120, "HECARIM", "HECARIM"),
	/**
	 * The '<em><b>KHAZIX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KHAZIX_VALUE
	 * @generated
	 * @ordered
	 */
	KHAZIX(121, "KHAZIX", "KHAZIX"),
	/**
	 * The '<em><b>DARIUS</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DARIUS_VALUE
	 * @generated
	 * @ordered
	 */
	DARIUS(122, "DARIUS", "DARIUS"),
	/**
	 * The '<em><b>JAYCE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #JAYCE_VALUE
	 * @generated
	 * @ordered
	 */
	JAYCE(126, "JAYCE", "JAYCE"),
	/**
	 * The '<em><b>LISSANDRA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LISSANDRA_VALUE
	 * @generated
	 * @ordered
	 */
	LISSANDRA(127, "LISSANDRA", "LISSANDRA"),
	/**
	 * The '<em><b>DIANA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #DIANA_VALUE
	 * @generated
	 * @ordered
	 */
	DIANA(131, "DIANA", "DIANA"),
	/**
	 * The '<em><b>QUINN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #QUINN_VALUE
	 * @generated
	 * @ordered
	 */
	QUINN(133, "QUINN", "QUINN"),
	/**
	 * The '<em><b>SYNDRA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SYNDRA_VALUE
	 * @generated
	 * @ordered
	 */
	SYNDRA(134, "SYNDRA", "SYNDRA"),
	/**
	 * The '<em><b>AURELIONSOL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #AURELIONSOL_VALUE
	 * @generated
	 * @ordered
	 */
	AURELIONSOL(136, "AURELIONSOL", "AURELIONSOL"),
	/**
	 * The '<em><b>KAYN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KAYN_VALUE
	 * @generated
	 * @ordered
	 */
	KAYN(141, "KAYN", "KAYN"),
	/**
	 * The '<em><b>ZOE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ZOE_VALUE
	 * @generated
	 * @ordered
	 */
	ZOE(142, "ZOE", "ZOE"),
	/**
	 * The '<em><b>ZYRA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ZYRA_VALUE
	 * @generated
	 * @ordered
	 */
	ZYRA(143, "ZYRA", "ZYRA"),
	/**
	 * The '<em><b>KAISA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KAISA_VALUE
	 * @generated
	 * @ordered
	 */
	KAISA(145, "KAISA", "KAISA"),
	/**
	 * The '<em><b>GNAR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GNAR_VALUE
	 * @generated
	 * @ordered
	 */
	GNAR(150, "GNAR", "GNAR"),
	/**
	 * The '<em><b>ZAC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ZAC_VALUE
	 * @generated
	 * @ordered
	 */
	ZAC(154, "ZAC", "ZAC"),
	/**
	 * The '<em><b>YASUO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #YASUO_VALUE
	 * @generated
	 * @ordered
	 */
	YASUO(157, "YASUO", "YASUO"),
	/**
	 * The '<em><b>VELKOZ</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VELKOZ_VALUE
	 * @generated
	 * @ordered
	 */
	VELKOZ(161, "VELKOZ", "VELKOZ"),
	/**
	 * The '<em><b>TALIYAH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TALIYAH_VALUE
	 * @generated
	 * @ordered
	 */
	TALIYAH(163, "TALIYAH", "TALIYAH"),
	/**
	 * The '<em><b>CAMILLE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CAMILLE_VALUE
	 * @generated
	 * @ordered
	 */
	CAMILLE(164, "CAMILLE", "CAMILLE"),
	/**
	 * The '<em><b>BRAUM</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #BRAUM_VALUE
	 * @generated
	 * @ordered
	 */
	BRAUM(201, "BRAUM", "BRAUM"),
	/**
	 * The '<em><b>JHIN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #JHIN_VALUE
	 * @generated
	 * @ordered
	 */
	JHIN(202, "JHIN", "JHIN"),
	/**
	 * The '<em><b>KINDRED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KINDRED_VALUE
	 * @generated
	 * @ordered
	 */
	KINDRED(203, "KINDRED", "KINDRED"),
	/**
	 * The '<em><b>JINX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #JINX_VALUE
	 * @generated
	 * @ordered
	 */
	JINX(222, "JINX", "JINX"),
	/**
	 * The '<em><b>TAHMKENCH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #TAHMKENCH_VALUE
	 * @generated
	 * @ordered
	 */
	TAHMKENCH(223, "TAHMKENCH", "TAHMKENCH"),
	/**
	 * The '<em><b>LUCIAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #LUCIAN_VALUE
	 * @generated
	 * @ordered
	 */
	LUCIAN(236, "LUCIAN", "LUCIAN"),
	/**
	 * The '<em><b>ZED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ZED_VALUE
	 * @generated
	 * @ordered
	 */
	ZED(238, "ZED", "ZED"),
	/**
	 * The '<em><b>KLED</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KLED_VALUE
	 * @generated
	 * @ordered
	 */
	KLED(240, "KLED", "KLED"),
	/**
	 * The '<em><b>EKKO</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #EKKO_VALUE
	 * @generated
	 * @ordered
	 */
	EKKO(245, "EKKO", "EKKO"),
	/**
	 * The '<em><b>VI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #VI_VALUE
	 * @generated
	 * @ordered
	 */
	VI(254, "VI", "VI"),
	/**
	 * The '<em><b>AATROX</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #AATROX_VALUE
	 * @generated
	 * @ordered
	 */
	AATROX(266, "AATROX", "AATROX"),
	/**
	 * The '<em><b>NAMI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #NAMI_VALUE
	 * @generated
	 * @ordered
	 */
	NAMI(267, "NAMI", "NAMI"),
	/**
	 * The '<em><b>AZIR</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #AZIR_VALUE
	 * @generated
	 * @ordered
	 */
	AZIR(268, "AZIR", "AZIR"),
	/**
	 * The '<em><b>THRESH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #THRESH_VALUE
	 * @generated
	 * @ordered
	 */
	THRESH(412, "THRESH", "THRESH"),
	/**
	 * The '<em><b>ILLAOI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ILLAOI_VALUE
	 * @generated
	 * @ordered
	 */
	ILLAOI(420, "ILLAOI", "ILLAOI"),
	/**
	 * The '<em><b>REKSAI</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #REKSAI_VALUE
	 * @generated
	 * @ordered
	 */
	REKSAI(421, "REKSAI", "REKSAI"),
	/**
	 * The '<em><b>IVERN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #IVERN_VALUE
	 * @generated
	 * @ordered
	 */
	IVERN(427, "IVERN", "IVERN"),
	/**
	 * The '<em><b>KALISTA</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #KALISTA_VALUE
	 * @generated
	 * @ordered
	 */
	KALISTA(429, "KALISTA", "KALISTA"),
	/**
	 * The '<em><b>BARD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #BARD_VALUE
	 * @generated
	 * @ordered
	 */
	BARD(432, "BARD", "BARD"),
	/**
	 * The '<em><b>RAKAN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #RAKAN_VALUE
	 * @generated
	 * @ordered
	 */
	RAKAN(497, "RAKAN", "RAKAN"),
	/**
	 * The '<em><b>XAYAH</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #XAYAH_VALUE
	 * @generated
	 * @ordered
	 */
	XAYAH(498, "XAYAH", "XAYAH"),
	/**
	 * The '<em><b>ORNN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ORNN_VALUE
	 * @generated
	 * @ordered
	 */
	ORNN(516, "ORNN", "ORNN");

	/**
	 * The '<em><b>ANNIE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ANNIE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ANNIE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ANNIE_VALUE = 1;

	/**
	 * The '<em><b>OLAF</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OLAF</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OLAF
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OLAF_VALUE = 2;

	/**
	 * The '<em><b>GALIO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GALIO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GALIO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GALIO_VALUE = 3;

	/**
	 * The '<em><b>TWISTEDFATE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TWISTEDFATE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TWISTEDFATE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TWISTEDFATE_VALUE = 4;

	/**
	 * The '<em><b>XINZHAO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>XINZHAO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XINZHAO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int XINZHAO_VALUE = 5;

	/**
	 * The '<em><b>URGOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>URGOT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #URGOT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int URGOT_VALUE = 6;

	/**
	 * The '<em><b>LEBLANC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LEBLANC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LEBLANC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LEBLANC_VALUE = 7;

	/**
	 * The '<em><b>VLADIMIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VLADIMIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VLADIMIR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VLADIMIR_VALUE = 8;

	/**
	 * The '<em><b>FIDDLESTICKS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIDDLESTICKS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIDDLESTICKS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIDDLESTICKS_VALUE = 9;

	/**
	 * The '<em><b>KAYLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KAYLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KAYLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KAYLE_VALUE = 10;

	/**
	 * The '<em><b>MASTERYI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MASTERYI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MASTERYI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MASTERYI_VALUE = 11;

	/**
	 * The '<em><b>ALISTAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ALISTAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ALISTAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ALISTAR_VALUE = 12;

	/**
	 * The '<em><b>RYZE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RYZE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RYZE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RYZE_VALUE = 13;

	/**
	 * The '<em><b>SION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SION_VALUE = 14;

	/**
	 * The '<em><b>SIVIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SIVIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SIVIR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SIVIR_VALUE = 15;

	/**
	 * The '<em><b>SORAKA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SORAKA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SORAKA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SORAKA_VALUE = 16;

	/**
	 * The '<em><b>TEEMO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TEEMO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TEEMO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TEEMO_VALUE = 17;

	/**
	 * The '<em><b>TRISTANA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRISTANA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRISTANA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRISTANA_VALUE = 18;

	/**
	 * The '<em><b>WARWICK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>WARWICK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #WARWICK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int WARWICK_VALUE = 19;

	/**
	 * The '<em><b>NUNU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NUNU</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NUNU
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NUNU_VALUE = 20;

	/**
	 * The '<em><b>MISSFORTUNE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MISSFORTUNE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MISSFORTUNE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MISSFORTUNE_VALUE = 21;

	/**
	 * The '<em><b>ASHE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ASHE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASHE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ASHE_VALUE = 22;

	/**
	 * The '<em><b>TRYNDAMERE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRYNDAMERE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRYNDAMERE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRYNDAMERE_VALUE = 23;

	/**
	 * The '<em><b>JAX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JAX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JAX
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JAX_VALUE = 24;

	/**
	 * The '<em><b>MORGANA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MORGANA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MORGANA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MORGANA_VALUE = 25;

	/**
	 * The '<em><b>ZILEAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ZILEAN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZILEAN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ZILEAN_VALUE = 26;

	/**
	 * The '<em><b>SINGED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SINGED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SINGED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SINGED_VALUE = 27;

	/**
	 * The '<em><b>EVELYNN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EVELYNN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EVELYNN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EVELYNN_VALUE = 28;

	/**
	 * The '<em><b>TWITCH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TWITCH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TWITCH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TWITCH_VALUE = 29;

	/**
	 * The '<em><b>KARTHUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KARTHUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KARTHUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KARTHUS_VALUE = 30;

	/**
	 * The '<em><b>CHOGATH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CHOGATH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CHOGATH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CHOGATH_VALUE = 31;

	/**
	 * The '<em><b>AMUMU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AMUMU</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AMUMU
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AMUMU_VALUE = 32;

	/**
	 * The '<em><b>RAMMUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RAMMUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RAMMUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RAMMUS_VALUE = 33;

	/**
	 * The '<em><b>ANIVIA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ANIVIA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ANIVIA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ANIVIA_VALUE = 34;

	/**
	 * The '<em><b>SHACO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SHACO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHACO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SHACO_VALUE = 35;

	/**
	 * The '<em><b>DRMUNDO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DRMUNDO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DRMUNDO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DRMUNDO_VALUE = 36;

	/**
	 * The '<em><b>SONA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SONA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SONA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SONA_VALUE = 37;

	/**
	 * The '<em><b>KASSADIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KASSADIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KASSADIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KASSADIN_VALUE = 38;

	/**
	 * The '<em><b>IRELIA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IRELIA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IRELIA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IRELIA_VALUE = 39;

	/**
	 * The '<em><b>JANNA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JANNA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JANNA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JANNA_VALUE = 40;

	/**
	 * The '<em><b>GANGPLANK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GANGPLANK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GANGPLANK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GANGPLANK_VALUE = 41;

	/**
	 * The '<em><b>CORKI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CORKI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CORKI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CORKI_VALUE = 42;

	/**
	 * The '<em><b>KARMA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KARMA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KARMA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KARMA_VALUE = 43;

	/**
	 * The '<em><b>TARIC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TARIC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TARIC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TARIC_VALUE = 44;

	/**
	 * The '<em><b>VEIGAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VEIGAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VEIGAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VEIGAR_VALUE = 45;

	/**
	 * The '<em><b>TRUNDLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TRUNDLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRUNDLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TRUNDLE_VALUE = 48;

	/**
	 * The '<em><b>SWAIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SWAIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SWAIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SWAIN_VALUE = 50;

	/**
	 * The '<em><b>CAITLYN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CAITLYN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAITLYN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAITLYN_VALUE = 51;

	/**
	 * The '<em><b>BLITZCRANK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BLITZCRANK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BLITZCRANK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BLITZCRANK_VALUE = 53;

	/**
	 * The '<em><b>MALPHITE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MALPHITE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MALPHITE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MALPHITE_VALUE = 54;

	/**
	 * The '<em><b>KATARINA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KATARINA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KATARINA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KATARINA_VALUE = 55;

	/**
	 * The '<em><b>NOCTURNE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NOCTURNE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOCTURNE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NOCTURNE_VALUE = 56;

	/**
	 * The '<em><b>MAOKAI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MAOKAI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MAOKAI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MAOKAI_VALUE = 57;

	/**
	 * The '<em><b>RENEKTON</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RENEKTON</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RENEKTON
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RENEKTON_VALUE = 58;

	/**
	 * The '<em><b>JARVANIV</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JARVANIV</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JARVANIV
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JARVANIV_VALUE = 59;

	/**
	 * The '<em><b>ELISE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ELISE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ELISE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ELISE_VALUE = 60;

	/**
	 * The '<em><b>ORIANNA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ORIANNA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ORIANNA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ORIANNA_VALUE = 61;

	/**
	 * The '<em><b>MONKEYKING</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MONKEYKING</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MONKEYKING
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MONKEYKING_VALUE = 62;

	/**
	 * The '<em><b>BRAND</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BRAND</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BRAND
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BRAND_VALUE = 63;

	/**
	 * The '<em><b>LEESIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LEESIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LEESIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LEESIN_VALUE = 64;

	/**
	 * The '<em><b>VAYNE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VAYNE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VAYNE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VAYNE_VALUE = 67;

	/**
	 * The '<em><b>RUMBLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RUMBLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RUMBLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RUMBLE_VALUE = 68;

	/**
	 * The '<em><b>CASSIOPEIA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CASSIOPEIA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CASSIOPEIA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CASSIOPEIA_VALUE = 69;

	/**
	 * The '<em><b>SKARNER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SKARNER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SKARNER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SKARNER_VALUE = 72;

	/**
	 * The '<em><b>HEIMERDINGER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>HEIMERDINGER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HEIMERDINGER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int HEIMERDINGER_VALUE = 74;

	/**
	 * The '<em><b>NASUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NASUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NASUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NASUS_VALUE = 75;

	/**
	 * The '<em><b>NIDALEE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NIDALEE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NIDALEE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NIDALEE_VALUE = 76;

	/**
	 * The '<em><b>UDYR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>UDYR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UDYR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int UDYR_VALUE = 77;

	/**
	 * The '<em><b>POPPY</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>POPPY</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #POPPY
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int POPPY_VALUE = 78;

	/**
	 * The '<em><b>GRAGAS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GRAGAS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GRAGAS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GRAGAS_VALUE = 79;

	/**
	 * The '<em><b>PANTHEON</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PANTHEON</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PANTHEON
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PANTHEON_VALUE = 80;

	/**
	 * The '<em><b>EZREAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EZREAL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EZREAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EZREAL_VALUE = 81;

	/**
	 * The '<em><b>MORDEKAISER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MORDEKAISER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MORDEKAISER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MORDEKAISER_VALUE = 82;

	/**
	 * The '<em><b>YORICK</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>YORICK</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #YORICK
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int YORICK_VALUE = 83;

	/**
	 * The '<em><b>AKALI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AKALI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AKALI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AKALI_VALUE = 84;

	/**
	 * The '<em><b>KENNEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KENNEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KENNEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KENNEN_VALUE = 85;

	/**
	 * The '<em><b>GAREN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GAREN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GAREN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GAREN_VALUE = 86;

	/**
	 * The '<em><b>LEONA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LEONA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LEONA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LEONA_VALUE = 89;

	/**
	 * The '<em><b>MALZAHAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>MALZAHAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MALZAHAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int MALZAHAR_VALUE = 90;

	/**
	 * The '<em><b>TALON</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TALON</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TALON
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TALON_VALUE = 91;

	/**
	 * The '<em><b>RIVEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RIVEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RIVEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RIVEN_VALUE = 92;

	/**
	 * The '<em><b>KOGMAW</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KOGMAW</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KOGMAW
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KOGMAW_VALUE = 96;

	/**
	 * The '<em><b>SHEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SHEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SHEN_VALUE = 98;

	/**
	 * The '<em><b>LUX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LUX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LUX
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LUX_VALUE = 99;

	/**
	 * The '<em><b>XERATH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>XERATH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XERATH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int XERATH_VALUE = 101;

	/**
	 * The '<em><b>SHYVANA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SHYVANA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHYVANA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SHYVANA_VALUE = 102;

	/**
	 * The '<em><b>AHRI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AHRI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AHRI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AHRI_VALUE = 103;

	/**
	 * The '<em><b>GRAVES</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GRAVES</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GRAVES
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GRAVES_VALUE = 104;

	/**
	 * The '<em><b>FIZZ</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIZZ</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIZZ
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIZZ_VALUE = 105;

	/**
	 * The '<em><b>VOLIBEAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VOLIBEAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VOLIBEAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VOLIBEAR_VALUE = 106;

	/**
	 * The '<em><b>RENGAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RENGAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RENGAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RENGAR_VALUE = 107;

	/**
	 * The '<em><b>VARUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VARUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VARUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VARUS_VALUE = 110;

	/**
	 * The '<em><b>NAUTILUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NAUTILUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAUTILUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NAUTILUS_VALUE = 111;

	/**
	 * The '<em><b>VIKTOR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VIKTOR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VIKTOR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VIKTOR_VALUE = 112;

	/**
	 * The '<em><b>SEJUANI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SEJUANI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SEJUANI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SEJUANI_VALUE = 113;

	/**
	 * The '<em><b>FIORA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FIORA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIORA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FIORA_VALUE = 114;

	/**
	 * The '<em><b>ZIGGS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ZIGGS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZIGGS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ZIGGS_VALUE = 115;

	/**
	 * The '<em><b>LULU</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LULU</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LULU
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LULU_VALUE = 117;

	/**
	 * The '<em><b>DRAVEN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DRAVEN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DRAVEN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DRAVEN_VALUE = 119;

	/**
	 * The '<em><b>HECARIM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>HECARIM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HECARIM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int HECARIM_VALUE = 120;

	/**
	 * The '<em><b>KHAZIX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KHAZIX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KHAZIX
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KHAZIX_VALUE = 121;

	/**
	 * The '<em><b>DARIUS</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DARIUS</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DARIUS
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DARIUS_VALUE = 122;

	/**
	 * The '<em><b>JAYCE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JAYCE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JAYCE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JAYCE_VALUE = 126;

	/**
	 * The '<em><b>LISSANDRA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LISSANDRA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LISSANDRA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LISSANDRA_VALUE = 127;

	/**
	 * The '<em><b>DIANA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>DIANA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIANA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int DIANA_VALUE = 131;

	/**
	 * The '<em><b>QUINN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>QUINN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #QUINN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int QUINN_VALUE = 133;

	/**
	 * The '<em><b>SYNDRA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SYNDRA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYNDRA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SYNDRA_VALUE = 134;

	/**
	 * The '<em><b>AURELIONSOL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AURELIONSOL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AURELIONSOL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AURELIONSOL_VALUE = 136;

	/**
	 * The '<em><b>KAYN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KAYN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KAYN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KAYN_VALUE = 141;

	/**
	 * The '<em><b>ZOE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ZOE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZOE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ZOE_VALUE = 142;

	/**
	 * The '<em><b>ZYRA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ZYRA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZYRA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ZYRA_VALUE = 143;

	/**
	 * The '<em><b>KAISA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KAISA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KAISA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KAISA_VALUE = 145;

	/**
	 * The '<em><b>GNAR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GNAR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GNAR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GNAR_VALUE = 150;

	/**
	 * The '<em><b>ZAC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ZAC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZAC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ZAC_VALUE = 154;

	/**
	 * The '<em><b>YASUO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>YASUO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #YASUO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int YASUO_VALUE = 157;

	/**
	 * The '<em><b>VELKOZ</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VELKOZ</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VELKOZ
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VELKOZ_VALUE = 161;

	/**
	 * The '<em><b>TALIYAH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TALIYAH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TALIYAH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TALIYAH_VALUE = 163;

	/**
	 * The '<em><b>CAMILLE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>CAMILLE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CAMILLE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int CAMILLE_VALUE = 164;

	/**
	 * The '<em><b>BRAUM</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BRAUM</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BRAUM
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BRAUM_VALUE = 201;

	/**
	 * The '<em><b>JHIN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JHIN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JHIN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JHIN_VALUE = 202;

	/**
	 * The '<em><b>KINDRED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KINDRED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KINDRED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KINDRED_VALUE = 203;

	/**
	 * The '<em><b>JINX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>JINX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #JINX
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int JINX_VALUE = 222;

	/**
	 * The '<em><b>TAHMKENCH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>TAHMKENCH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TAHMKENCH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int TAHMKENCH_VALUE = 223;

	/**
	 * The '<em><b>LUCIAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>LUCIAN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LUCIAN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int LUCIAN_VALUE = 236;

	/**
	 * The '<em><b>ZED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ZED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ZED_VALUE = 238;

	/**
	 * The '<em><b>KLED</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KLED</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KLED
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KLED_VALUE = 240;

	/**
	 * The '<em><b>EKKO</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EKKO</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EKKO
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EKKO_VALUE = 245;

	/**
	 * The '<em><b>VI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>VI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int VI_VALUE = 254;

	/**
	 * The '<em><b>AATROX</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AATROX</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AATROX
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AATROX_VALUE = 266;

	/**
	 * The '<em><b>NAMI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>NAMI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NAMI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int NAMI_VALUE = 267;

	/**
	 * The '<em><b>AZIR</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>AZIR</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AZIR
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int AZIR_VALUE = 268;

	/**
	 * The '<em><b>THRESH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>THRESH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #THRESH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int THRESH_VALUE = 412;

	/**
	 * The '<em><b>ILLAOI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ILLAOI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ILLAOI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ILLAOI_VALUE = 420;

	/**
	 * The '<em><b>REKSAI</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>REKSAI</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REKSAI
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int REKSAI_VALUE = 421;

	/**
	 * The '<em><b>IVERN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>IVERN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IVERN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int IVERN_VALUE = 427;

	/**
	 * The '<em><b>KALISTA</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>KALISTA</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #KALISTA
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int KALISTA_VALUE = 429;

	/**
	 * The '<em><b>BARD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>BARD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BARD
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int BARD_VALUE = 432;

	/**
	 * The '<em><b>RAKAN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>RAKAN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RAKAN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int RAKAN_VALUE = 497;

	/**
	 * The '<em><b>XAYAH</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>XAYAH</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XAYAH
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int XAYAH_VALUE = 498;

	/**
	 * The '<em><b>ORNN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ORNN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ORNN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ORNN_VALUE = 516;

	/**
	 * An array of all the '<em><b>Champion</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Champion[] VALUES_ARRAY = new Champion[] { ANNIE, OLAF, GALIO, TWISTEDFATE, XINZHAO, URGOT,
			LEBLANC, VLADIMIR, FIDDLESTICKS, KAYLE, MASTERYI, ALISTAR, RYZE, SION, SIVIR, SORAKA, TEEMO, TRISTANA,
			WARWICK, NUNU, MISSFORTUNE, ASHE, TRYNDAMERE, JAX, MORGANA, ZILEAN, SINGED, EVELYNN, TWITCH, KARTHUS,
			CHOGATH, AMUMU, RAMMUS, ANIVIA, SHACO, DRMUNDO, SONA, KASSADIN, IRELIA, JANNA, GANGPLANK, CORKI, KARMA,
			TARIC, VEIGAR, TRUNDLE, SWAIN, CAITLYN, BLITZCRANK, MALPHITE, KATARINA, NOCTURNE, MAOKAI, RENEKTON,
			JARVANIV, ELISE, ORIANNA, MONKEYKING, BRAND, LEESIN, VAYNE, RUMBLE, CASSIOPEIA, SKARNER, HEIMERDINGER,
			NASUS, NIDALEE, UDYR, POPPY, GRAGAS, PANTHEON, EZREAL, MORDEKAISER, YORICK, AKALI, KENNEN, GAREN, LEONA,
			MALZAHAR, TALON, RIVEN, KOGMAW, SHEN, LUX, XERATH, SHYVANA, AHRI, GRAVES, FIZZ, VOLIBEAR, RENGAR, VARUS,
			NAUTILUS, VIKTOR, SEJUANI, FIORA, ZIGGS, LULU, DRAVEN, HECARIM, KHAZIX, DARIUS, JAYCE, LISSANDRA, DIANA,
			QUINN, SYNDRA, AURELIONSOL, KAYN, ZOE, ZYRA, KAISA, GNAR, ZAC, YASUO, VELKOZ, TALIYAH, CAMILLE, BRAUM, JHIN,
			KINDRED, JINX, TAHMKENCH, LUCIAN, ZED, KLED, EKKO, VI, AATROX, NAMI, AZIR, THRESH, ILLAOI, REKSAI, IVERN,
			KALISTA, BARD, RAKAN, XAYAH, ORNN, };

	/**
	 * A public read-only list of all the '<em><b>Champion</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Champion> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Champion</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Champion get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Champion result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Champion</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Champion getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Champion result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Champion</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Champion get(int value) {
		switch (value) {
		case ANNIE_VALUE:
			return ANNIE;
		case OLAF_VALUE:
			return OLAF;
		case GALIO_VALUE:
			return GALIO;
		case TWISTEDFATE_VALUE:
			return TWISTEDFATE;
		case XINZHAO_VALUE:
			return XINZHAO;
		case URGOT_VALUE:
			return URGOT;
		case LEBLANC_VALUE:
			return LEBLANC;
		case VLADIMIR_VALUE:
			return VLADIMIR;
		case FIDDLESTICKS_VALUE:
			return FIDDLESTICKS;
		case KAYLE_VALUE:
			return KAYLE;
		case MASTERYI_VALUE:
			return MASTERYI;
		case ALISTAR_VALUE:
			return ALISTAR;
		case RYZE_VALUE:
			return RYZE;
		case SION_VALUE:
			return SION;
		case SIVIR_VALUE:
			return SIVIR;
		case SORAKA_VALUE:
			return SORAKA;
		case TEEMO_VALUE:
			return TEEMO;
		case TRISTANA_VALUE:
			return TRISTANA;
		case WARWICK_VALUE:
			return WARWICK;
		case NUNU_VALUE:
			return NUNU;
		case MISSFORTUNE_VALUE:
			return MISSFORTUNE;
		case ASHE_VALUE:
			return ASHE;
		case TRYNDAMERE_VALUE:
			return TRYNDAMERE;
		case JAX_VALUE:
			return JAX;
		case MORGANA_VALUE:
			return MORGANA;
		case ZILEAN_VALUE:
			return ZILEAN;
		case SINGED_VALUE:
			return SINGED;
		case EVELYNN_VALUE:
			return EVELYNN;
		case TWITCH_VALUE:
			return TWITCH;
		case KARTHUS_VALUE:
			return KARTHUS;
		case CHOGATH_VALUE:
			return CHOGATH;
		case AMUMU_VALUE:
			return AMUMU;
		case RAMMUS_VALUE:
			return RAMMUS;
		case ANIVIA_VALUE:
			return ANIVIA;
		case SHACO_VALUE:
			return SHACO;
		case DRMUNDO_VALUE:
			return DRMUNDO;
		case SONA_VALUE:
			return SONA;
		case KASSADIN_VALUE:
			return KASSADIN;
		case IRELIA_VALUE:
			return IRELIA;
		case JANNA_VALUE:
			return JANNA;
		case GANGPLANK_VALUE:
			return GANGPLANK;
		case CORKI_VALUE:
			return CORKI;
		case KARMA_VALUE:
			return KARMA;
		case TARIC_VALUE:
			return TARIC;
		case VEIGAR_VALUE:
			return VEIGAR;
		case TRUNDLE_VALUE:
			return TRUNDLE;
		case SWAIN_VALUE:
			return SWAIN;
		case CAITLYN_VALUE:
			return CAITLYN;
		case BLITZCRANK_VALUE:
			return BLITZCRANK;
		case MALPHITE_VALUE:
			return MALPHITE;
		case KATARINA_VALUE:
			return KATARINA;
		case NOCTURNE_VALUE:
			return NOCTURNE;
		case MAOKAI_VALUE:
			return MAOKAI;
		case RENEKTON_VALUE:
			return RENEKTON;
		case JARVANIV_VALUE:
			return JARVANIV;
		case ELISE_VALUE:
			return ELISE;
		case ORIANNA_VALUE:
			return ORIANNA;
		case MONKEYKING_VALUE:
			return MONKEYKING;
		case BRAND_VALUE:
			return BRAND;
		case LEESIN_VALUE:
			return LEESIN;
		case VAYNE_VALUE:
			return VAYNE;
		case RUMBLE_VALUE:
			return RUMBLE;
		case CASSIOPEIA_VALUE:
			return CASSIOPEIA;
		case SKARNER_VALUE:
			return SKARNER;
		case HEIMERDINGER_VALUE:
			return HEIMERDINGER;
		case NASUS_VALUE:
			return NASUS;
		case NIDALEE_VALUE:
			return NIDALEE;
		case UDYR_VALUE:
			return UDYR;
		case POPPY_VALUE:
			return POPPY;
		case GRAGAS_VALUE:
			return GRAGAS;
		case PANTHEON_VALUE:
			return PANTHEON;
		case EZREAL_VALUE:
			return EZREAL;
		case MORDEKAISER_VALUE:
			return MORDEKAISER;
		case YORICK_VALUE:
			return YORICK;
		case AKALI_VALUE:
			return AKALI;
		case KENNEN_VALUE:
			return KENNEN;
		case GAREN_VALUE:
			return GAREN;
		case LEONA_VALUE:
			return LEONA;
		case MALZAHAR_VALUE:
			return MALZAHAR;
		case TALON_VALUE:
			return TALON;
		case RIVEN_VALUE:
			return RIVEN;
		case KOGMAW_VALUE:
			return KOGMAW;
		case SHEN_VALUE:
			return SHEN;
		case LUX_VALUE:
			return LUX;
		case XERATH_VALUE:
			return XERATH;
		case SHYVANA_VALUE:
			return SHYVANA;
		case AHRI_VALUE:
			return AHRI;
		case GRAVES_VALUE:
			return GRAVES;
		case FIZZ_VALUE:
			return FIZZ;
		case VOLIBEAR_VALUE:
			return VOLIBEAR;
		case RENGAR_VALUE:
			return RENGAR;
		case VARUS_VALUE:
			return VARUS;
		case NAUTILUS_VALUE:
			return NAUTILUS;
		case VIKTOR_VALUE:
			return VIKTOR;
		case SEJUANI_VALUE:
			return SEJUANI;
		case FIORA_VALUE:
			return FIORA;
		case ZIGGS_VALUE:
			return ZIGGS;
		case LULU_VALUE:
			return LULU;
		case DRAVEN_VALUE:
			return DRAVEN;
		case HECARIM_VALUE:
			return HECARIM;
		case KHAZIX_VALUE:
			return KHAZIX;
		case DARIUS_VALUE:
			return DARIUS;
		case JAYCE_VALUE:
			return JAYCE;
		case LISSANDRA_VALUE:
			return LISSANDRA;
		case DIANA_VALUE:
			return DIANA;
		case QUINN_VALUE:
			return QUINN;
		case SYNDRA_VALUE:
			return SYNDRA;
		case AURELIONSOL_VALUE:
			return AURELIONSOL;
		case KAYN_VALUE:
			return KAYN;
		case ZOE_VALUE:
			return ZOE;
		case ZYRA_VALUE:
			return ZYRA;
		case KAISA_VALUE:
			return KAISA;
		case GNAR_VALUE:
			return GNAR;
		case ZAC_VALUE:
			return ZAC;
		case YASUO_VALUE:
			return YASUO;
		case VELKOZ_VALUE:
			return VELKOZ;
		case TALIYAH_VALUE:
			return TALIYAH;
		case CAMILLE_VALUE:
			return CAMILLE;
		case BRAUM_VALUE:
			return BRAUM;
		case JHIN_VALUE:
			return JHIN;
		case KINDRED_VALUE:
			return KINDRED;
		case JINX_VALUE:
			return JINX;
		case TAHMKENCH_VALUE:
			return TAHMKENCH;
		case LUCIAN_VALUE:
			return LUCIAN;
		case ZED_VALUE:
			return ZED;
		case KLED_VALUE:
			return KLED;
		case EKKO_VALUE:
			return EKKO;
		case VI_VALUE:
			return VI;
		case AATROX_VALUE:
			return AATROX;
		case NAMI_VALUE:
			return NAMI;
		case AZIR_VALUE:
			return AZIR;
		case THRESH_VALUE:
			return THRESH;
		case ILLAOI_VALUE:
			return ILLAOI;
		case REKSAI_VALUE:
			return REKSAI;
		case IVERN_VALUE:
			return IVERN;
		case KALISTA_VALUE:
			return KALISTA;
		case BARD_VALUE:
			return BARD;
		case RAKAN_VALUE:
			return RAKAN;
		case XAYAH_VALUE:
			return XAYAH;
		case ORNN_VALUE:
			return ORNN;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Champion(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

	/**
	 * Returns the ID of the champion as expected by LoL API.
	 * @return the ID of the champion as expected by LoL API
	 */
	public String getRiotId() {
		return "" + getValue();
	}

	/**
	 * Returns the name of the champion as displayed by Riot.
	 * @return the name of the champion as displayed by Riot
	 */
	public String getRiotName() {
		if (this == CHOGATH)
			return "Cho'Gath";
		if (this == KHAZIX)
			return "Kha'Zix";
		if (this == KAISA)
			return "Kai'Sa";
		if (this == KOGMAW)
			return "Kog'Maw";
		if (this == REKSAI)
			return "Rek'Sai";
		if (this == VELKOZ)
			return "Vel'Koz";
		if (this == AURELIONSOL)
			return "Aurelion Sol";
		if (this == DRMUNDO)
			return "Dr. Mundo";
		if (this == JARVANIV)
			return "Jarvan IV";
		if (this == LEESIN)
			return "Lee Sin";
		if (this == MASTERYI)
			return "Master Yi";
		if (this == MISSFORTUNE)
			return "Miss Fortune";
		if (this == TAHMKENCH)
			return "Tahm Kench";
		if (this == TWISTEDFATE)
			return "Twisted Fate";
		if (this == XINZHAO)
			return "Xin Zhao";
		if (this == MONKEYKING)
			return "Wukong";

		return getLiteral().substring(0, 1).toUpperCase() + getLiteral().substring(1).toLowerCase();
	}

} //Champion
