/**
 */
package ice.master.loe.model.loe;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Ratio</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see ice.master.loe.model.loe.LoePackage#getRatio()
 * @model
 * @generated
 */
public interface Ratio extends ComputedYValue {
} // Ratio
