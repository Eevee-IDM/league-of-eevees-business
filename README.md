# League of Eevees — Business

## League of Eevees

This repository is part of the [League of Eevees project](https://gitlab.com/Eevee-IDM).

League of Eevees is a school project realized by a team of 6 people during the first year of our Master's Degree. 

The aim of the project is to provide a web interface allowing a League of Legends (LoL) player to query data about its games and to visualize them into diagrams. To this end, the project has been divided into three repositories:

- [a back-end](https://gitlab.com/Eevee-IDM/league-of-eevees-back), which is a REST server interpreting user requests, querying the required information, and sending the result to the web interface
- [a front-end](https://gitlab.com/Eevee-IDM/league-of-eevees-back), which is a web interface written in Angular 5 allowing the user to type its request and displaying the result as diagrams
- [a business part](https://gitlab.com/Eevee-IDM/league-of-eevees-business), which defines both a meta-model of the a user request and the Domain Specific Language (DSL) that can be used to type a request

The project has been developped in an agile method thanks to [Taiga](https://tree.taiga.io/project/kazejiyu-idm/) and used continuous integration tools such as Jenkins and SonarQube.

All the tools can be accessed from our [Start Page](http://ip63.ip-178-33-252.eu).

## Business Repository

### Architecture

The repository is organized as follows:

```
├───.mvn
├───bundles
│   ├───ice.master.loe.gloe.parent
│   │   ├───ice.master.loe.gloe
│   │   ├───ice.master.loe.gloe.ide
│   │   ├───ice.master.loe.gloe.target
│   │   ├───ice.master.loe.gloe.tests
│   │   ├───ice.master.loe.gloe.ui
│   │   └───ice.master.loe.gloe.web
│   ├───ice.master.loe.model
│   ├───ice.master.loe.model.edit
│   └───ice.master.loe.model.editor
├───releng
│   └───ice.master.loe.target
└───tests
```

The `bundle` directory contains the Eclipse plug-ins defining the meta-model and the DSL.  
The `releng` directory contains the target platform defining the runtime environment.

The EMF meta-model of a user request can be found in the `ice.master.loe.model` project.
The xText grammar defining the DSL can be found in the `ice.master.loe.gloe` project.

### Use the repository

#### From a terminal

##### 1. Clone this repository:

> `git clone https://gitlab.com/Eevee-IDM/league-of-eevees-business`

##### 2. Build the project:

> `mvn verify`

> **Note**: The whole build, including the generation of the Xtext parser, is automatized with Maven Tycho.

##### 3. Deploy the binaries:

> `mvn deploy`

> **Note**: you must have the rights to push in the Nexus repository for this command to work.

#### From Eclipse IDE

1. Import all the projects in the workspace
2. Use the target platform located under `releng/ice.master.loe.target` as the active target platform to set up the correct environment.